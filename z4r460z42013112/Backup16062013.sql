/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.0.51a : Database - directorioalmacen
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


/*Table structure for table `tblalmacenes` */

DROP TABLE IF EXISTS `tblalmacenes`;

CREATE TABLE `tblalmacenes` (
  `idalmacenes` int(255) NOT NULL auto_increment,
  `idcategoriaalmacen` int(255) default NULL,
  `idusuarioalmacen` int(255) default NULL,
  `nomalmacen` varchar(255) default NULL,
  `fechaCreacion` date default NULL,
  `descralmacen` varchar(255) default NULL,
  `idusuario` int(255) default NULL,
  PRIMARY KEY  (`idalmacenes`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tblalmacenes` */

insert  into `tblalmacenes`(`idalmacenes`,`idcategoriaalmacen`,`idusuarioalmacen`,`nomalmacen`,`fechaCreacion`,`descralmacen`,`idusuario`) values (1,1,9,'Granero Cuarto Frio','2013-06-03','',1),(3,5,9,'Carniceria la mejor.','2013-06-03','',1),(4,1,9,'Almacen pague menosx','2013-06-03','Almacen que vende sus productos un poco mas economicos.',1);

/*Table structure for table `tblcategorias` */

DROP TABLE IF EXISTS `tblcategorias`;

CREATE TABLE `tblcategorias` (
  `categ_cods` int(255) NOT NULL auto_increment,
  `categ_nom` varchar(255) default NULL,
  `idusuario` int(255) default NULL,
  PRIMARY KEY  (`categ_cods`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `tblcategorias` */

insert  into `tblcategorias`(`categ_cods`,`categ_nom`,`idusuario`) values (1,'Prueba 1',1),(3,'Blue Jeans',1),(4,'Camisetas',9),(5,'Zapatos',1),(6,'Medias',9);

/*Table structure for table `tblcategoriasalma` */

DROP TABLE IF EXISTS `tblcategoriasalma`;

CREATE TABLE `tblcategoriasalma` (
  `idcategoriaalmacen` int(255) NOT NULL auto_increment,
  `nomcategoriaalmcen` varchar(255) default NULL,
  `idusuario` int(20) default NULL,
  `imagen_asoc` varchar(100) default NULL,
  PRIMARY KEY  (`idcategoriaalmacen`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `tblcategoriasalma` */

insert  into `tblcategoriasalma`(`idcategoriaalmacen`,`nomcategoriaalmcen`,`idusuario`,`imagen_asoc`) values (1,'Graneros',1,'pic_tienda.jpg'),(3,'Farmacias',1,'pic_drogueria.jpg'),(4,'Ferreterías',1,'pic_ferreteria.jpg'),(5,'Carnicerías',1,'pic_carnicos.jpg'),(7,'Papelerias',1,'pic_papeleria.jpg'),(6,'Panaderias',1,'pic_panaderia.jpg'),(8,'Odontologías',1,'pic_odontologia.jpg'),(9,'Almacenes',1,'pic_almacenes.jpg'),(10,'Repuestos',1,'pic_repuesto.png'),(11,'Vehículos',1,'pic_vehiculos.jpg');

/*Table structure for table `tblclientes` */

DROP TABLE IF EXISTS `tblclientes`;

CREATE TABLE `tblclientes` (
  `idcliente` int(255) NOT NULL auto_increment,
  `identicliente` varchar(255) default NULL,
  `nombcliente` varchar(255) default NULL,
  `apellicliente` varchar(255) default NULL,
  `nomusuariocliente` varchar(255) default NULL,
  `passcliente` varchar(255) default NULL,
  `fechanacicliente` date default NULL,
  `numcelucliente` varchar(255) default NULL,
  `numtelecliente` varchar(255) default NULL,
  `direccliente` varchar(255) default NULL,
  `email1cliente` varchar(255) default NULL,
  `email2cliente` varchar(255) default NULL,
  `maccliente` varchar(255) default NULL,
  `ipcliente` varchar(255) default NULL,
  `baneado` int(1) default NULL,
  `confirmado` int(1) default NULL,
  PRIMARY KEY  (`idcliente`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `tblclientes` */

insert  into `tblclientes`(`idcliente`,`identicliente`,`nombcliente`,`apellicliente`,`nomusuariocliente`,`passcliente`,`fechanacicliente`,`numcelucliente`,`numtelecliente`,`direccliente`,`email1cliente`,`email2cliente`,`maccliente`,`ipcliente`,`baneado`,`confirmado`) values (22,'1036613885','MARIO ALEJANDRO','BENITEZ OROZCO','1036613885','$P$BGgdhiTKWvBsywgfE5pIy8Dh8r6uF41','1987-09-25','3117349528','3815151','cRA 45 No. 34-45 Apart. 301','maalben@gmail.com','maalben@hotmail.com','0E-EE-E6-A9-45-E7','127.0.0.1',0,1);

/*Table structure for table `tblimagenes` */

DROP TABLE IF EXISTS `tblimagenes`;

CREATE TABLE `tblimagenes` (
  `ImagenId` int(11) NOT NULL auto_increment,
  `ImagenArchivo` varchar(200) character set latin1 default NULL,
  `ImagenEstado` int(11) default NULL,
  PRIMARY KEY  (`ImagenId`)
) ENGINE=MyISAM AUTO_INCREMENT=280 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Data for the table `tblimagenes` */

insert  into `tblimagenes`(`ImagenId`,`ImagenArchivo`,`ImagenEstado`) values (253,'1359947229-2.Analizar.jpg',4),(254,'1359947229-34884339.jpg',4),(255,'1359947592-1CraterLeap.jpg',4),(256,'1359948549-img179.jpg',3),(257,'1359948550-45455666.jpg',3),(258,'1360037660-2.Analizar.jpg',5),(259,'1360037660-botero-adam-eve.jpg',5),(260,'1360037661-analisis-economico-derecho.jpg',5),(261,'1360037661-acuerdos.jpg',5),(262,'1360037661-76756624.jpg',5),(263,'1360037662-1CraterLeap.jpg',5),(264,'1360037661-Ciclos de Vida - Modelo en Espiral.jpg',5),(265,'1360037663-analizar1.jpg',5),(266,'1360037661-292216_363972200327398_276089272449025_982986_812892743_n.jpg',5),(267,'1360037665-caracterdesiervo.jpg',5),(268,'1360037662-b1f5cf7ac2c36f8f1e1b8173189e2a7d_XL.jpg',5),(269,'1360037661-34884339.jpg',5),(270,'1360037665-7373464.jpg',5),(271,'1360037667-45455666 - copia.jpg',5),(277,'1364585833-animales-en-peligro-de-extincion1.jpg',3),(273,'1360466097-76756624.jpg',12),(274,'1360466097-7373464.jpg',12),(275,'1360466099-34884339.jpg',12),(276,'1360466099-45455666.jpg',12),(279,'1364585838-animales-domesticos.jpg',3);

/*Table structure for table `tblimagenproductos` */

DROP TABLE IF EXISTS `tblimagenproductos`;

CREATE TABLE `tblimagenproductos` (
  `id_imgs` int(255) NOT NULL auto_increment,
  `id_prod` int(255) default NULL,
  `nom_img` varchar(255) default NULL,
  `nom_img_peq` varchar(255) default NULL,
  `desc_img` varchar(255) default NULL,
  PRIMARY KEY  (`id_imgs`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `tblimagenproductos` */

/*Table structure for table `tblmodulos` */

DROP TABLE IF EXISTS `tblmodulos`;

CREATE TABLE `tblmodulos` (
  `Id` int(2) NOT NULL,
  `Padre_id` int(2) NOT NULL,
  `Nombre` varchar(55) collate latin1_spanish_ci NOT NULL,
  `Fin` varchar(50) collate latin1_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tblmodulos` */

insert  into `tblmodulos`(`Id`,`Padre_id`,`Nombre`,`Fin`) values (1,0,'Configuración','0'),(20,0,'Clientes','0'),(21,0,'Productos','0'),(4,0,'Almacenes','0'),(14,4,'Listado Almacenes','listado_almacenes.php?optCat=4'),(6,0,'Pedidos','0'),(7,0,'Reportes','0'),(8,1,'Sistema','sistema.php'),(9,1,'Permisos','permisos.php'),(10,20,'Buscar clientes','buscar_clientes.php'),(11,20,'Listado clientes','listado_clientes.php'),(12,21,'Importar Productos','importar_productos.php'),(13,21,'Listado Productos','listado_productos.php'),(15,6,'Ver pedidos ','ver_pedidos.php'),(16,6,'Exportar pedidos ','exportar.php'),(17,7,'Reporte por Vendedor','reporte_vendedor.php'),(18,7,'Reporte por Cliente','reporte_cliente.php'),(5,0,'Vendedores','0'),(19,5,'Ver Vendedores','vendedores_listado.php'),(2,0,'Mercancia','0'),(3,0,'Clientes','0'),(22,2,'Productos','listado_productos.php?optCat=1'),(23,3,'Lista de Clientes','clientes.php'),(24,4,'Ver ruteros por vendedor','ruteros_vende.php'),(25,6,'Cierre','cierre.php'),(27,26,'Ver devoluciones','devoluciones.php'),(26,0,'Devolucion','0'),(28,0,'Mensajeria','0'),(29,28,'Mensajería interna','mensajeria_interna.php'),(30,1,'Cambios de clave','modificar_clave.php'),(31,2,'Categorías de Productos','listado_categorias.php?optCat=2'),(32,2,'Proveedores','listado_proveedores.php?optCat=3'),(33,4,'Categorias Almacenes','listado_categorias_alm.php?optCat=5');

/*Table structure for table `tblpermisos` */

DROP TABLE IF EXISTS `tblpermisos`;

CREATE TABLE `tblpermisos` (
  `Idpermiso` int(2) NOT NULL auto_increment,
  `Modulo` int(2) NOT NULL,
  `Usuario` int(2) NOT NULL,
  `Padre` int(11) NOT NULL,
  PRIMARY KEY  (`Idpermiso`)
) ENGINE=MyISAM AUTO_INCREMENT=413 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tblpermisos` */

insert  into `tblpermisos`(`Idpermiso`,`Modulo`,`Usuario`,`Padre`) values (382,27,1,26),(368,13,1,21),(388,25,1,6),(371,16,1,6),(377,22,1,2),(374,19,1,5),(372,17,1,7),(369,14,1,4),(364,9,1,1),(412,33,1,4),(378,23,1,3),(410,32,8,2),(370,15,1,6),(366,11,1,20),(411,32,9,2),(406,31,9,2),(405,22,9,2),(373,18,1,7),(404,32,1,2),(391,29,1,28),(408,22,8,2),(396,10,1,20),(403,31,1,2),(399,12,1,21),(409,31,8,2);

/*Table structure for table `tblproductos` */

DROP TABLE IF EXISTS `tblproductos`;

CREATE TABLE `tblproductos` (
  `prod_cods` int(255) NOT NULL,
  `provee_cods` int(255) default NULL,
  `categ_cods` int(255) default NULL,
  `prod_nom` varchar(255) default NULL,
  `prod_cant` int(50) default NULL,
  `prod_precio` int(255) default NULL,
  `prod_fech_compr` date default NULL,
  `prod_descr` text,
  `idusuario` int(255) default NULL,
  PRIMARY KEY  (`prod_cods`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `tblproductos` */

insert  into `tblproductos`(`prod_cods`,`provee_cods`,`categ_cods`,`prod_nom`,`prod_cant`,`prod_precio`,`prod_fech_compr`,`prod_descr`,`idusuario`) values (5,1,4,'Manpower',30,15000,'2012-01-13','Otras cosas mas.',9),(4,1,6,'Gamin',75,45000,'2012-02-09','Pantalones con medidas 28 a 40.',9),(3,4,6,'Patprimo',40,3000,'2013-02-05','Medias de color blanco, negro, rojo y azules.',1),(6,3,6,'Rotas',67,3500,'2013-02-08','Jasj as jsjao js a.',1),(7,4,3,'Chevignon',23,7800,'2013-02-04','sdaas as as as',1),(8,4,1,'controles',23,12000,'2013-02-02','as ada das asd',1),(9,3,4,'Sueter',323,8900,'2013-02-06','as ada dasd asd',1),(10,2,5,'New Balance',23,46000,'2013-02-01','ss sds d sds ',1),(11,4,4,'Diamond',34,78000,'2013-01-29','kmkcccccccc.',1),(12,2,3,'Fransua',45,60000,'2013-02-02','rggfg dfg dgd fg dg.',1),(13,3,5,'Zodiacs',30,50000,'2013-02-03','dfsdfsdf sdf sdf.',1);

/*Table structure for table `tblproveedor` */

DROP TABLE IF EXISTS `tblproveedor`;

CREATE TABLE `tblproveedor` (
  `provee_cods` int(255) NOT NULL auto_increment,
  `provee_nom` varchar(255) default NULL,
  `provee_dir` varchar(255) default NULL,
  `ciudad` varchar(255) default NULL,
  `idusuario` int(255) default NULL,
  PRIMARY KEY  (`provee_cods`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `tblproveedor` */

insert  into `tblproveedor`(`provee_cods`,`provee_nom`,`provee_dir`,`ciudad`,`idusuario`) values (1,'Juana de Arco de Gomez','Calle las Manguitas','Medellin',9),(2,'Roberto Gomez Bolaños','Av. Las Brisas','Bucaramanga',1),(3,'Anacleto Diaz','Por las calles de las flores','Medellin',1),(4,'Cataclismo Perez Londono','Juanajuato','Pereira',1),(5,'Diego Leon Camacho','Las balsitas','Mexico',1),(9,'prueba de proveedo','prueba  de direccion provee','cali',9);

/*Table structure for table `tbltipousuario` */

DROP TABLE IF EXISTS `tbltipousuario`;

CREATE TABLE `tbltipousuario` (
  `IdTipo` int(1) NOT NULL,
  `Tipo` varchar(55) collate latin1_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tbltipousuario` */

insert  into `tbltipousuario`(`IdTipo`,`Tipo`) values (1,'Root'),(2,'Administrador'),(3,'Vendedor'),(4,'Facturacion');

/*Table structure for table `tblusuarios` */

DROP TABLE IF EXISTS `tblusuarios`;

CREATE TABLE `tblusuarios` (
  `Id` int(2) NOT NULL auto_increment,
  `Login` varchar(55) collate latin1_spanish_ci NOT NULL,
  `Clave` varchar(55) collate latin1_spanish_ci NOT NULL,
  `Nombre` varchar(99) collate latin1_spanish_ci NOT NULL,
  `UltimoAcceso` datetime NOT NULL,
  `Tipo` int(2) NOT NULL,
  `Estado` int(1) NOT NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tblusuarios` */

insert  into `tblusuarios`(`Id`,`Login`,`Clave`,`Nombre`,`UltimoAcceso`,`Tipo`,`Estado`) values (1,'admin','e10adc3949ba59abbe56e057f20f883e','Root','2013-06-03 22:00:00',1,1),(8,'vendedor2','0407e8c8285ab85509ac2884025dcf42','Diego Armando Maradona','2013-02-06 22:39:41',3,1),(9,'vendedor1','e10adc3949ba59abbe56e057f20f883e','Carlos El Pibe Valderrama','2013-03-29 14:30:18',3,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
