$(document).ready(function() {
    $("#file_upload").fileUpload({
        'uploader': 'uploadify/uploader.swf',
         'cancelImg': 'uploadify/cancel.png',
                'script': 'libs/subirarchivo.php',

        'folder': 'uploads',
        'buttonText': 'Seleccionar imagenes...',
        'checkScript': 'uploadify/check.php',
        'fileDesc': 'archivos imagen',
        'auto':false,
      	'fileExt': '*.jpg',
		'sizeLimit': '4294967296',
        'multi': true,
        'displayData': 'percentage',
        onComplete: function (){
     //alert("Archivos subidos de manera exitosa.");
	 verlistadoimagenes();
            $("#txtdes").val('');
			$("#txtdes2").val('');
		},
		onAllComplete: function() {
		alert("Carga de imagenes completada.");
        window.location.reload();
                }

       });	   
   $('#txtdes2').bind('change', function(){
$('#file_upload').fileUploadSettings('scriptData','&des='+$(this).val());


    });

})

function startUpload(id, conditional, conditional2)
{	if(conditional2.value.length == "") {
		alert("Selecciona el codigo en el cuadro desplegable para poder subir la/las imagen(es) seleccionada(s).");
	} else{
		if(conditional.value != conditional2.value) {
		alert("Ingrese el valor indicado para poder subir la(s) imagenes seleccionada(s).");
	} else{
		$('#'+id).fileUploadStart();
	}}}