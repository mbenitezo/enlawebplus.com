$(document).ready(function() {
    $("#file_upload").fileUpload({
        'uploader': 'uploadify/uploader.swf',
         'cancelImg': 'uploadify/cancel.png',
                'script': 'libs/subirarchivo.php',

        'folder': 'uploads',
        'buttonText': 'CLIC AQUI',
        'checkScript': 'uploadify/check.php',
        'fileDesc': 'archivos imagen',
        'auto':false,
      	'fileExt': '*.jpg',
		'sizeLimit': '4294967296',
        'multi': true,
        'displayData': 'percentage',
        onComplete: function (){
     //alert("Archivos subidos de manera exitosa.");
	 verlistadoimagenes();
			$("#txtdes2").val('');
		},
		onAllComplete: function() {
		alert("Carga de imagenes completada.");
        window.location.reload();
                }

       });	   
   $('#txtdes2').bind('change', function(){
$('#file_upload').fileUploadSettings('scriptData','&des='+$(this).val());


    });

})

function startUpload(id, conditional2)
{	if(conditional2.value.length == "") {
		alert("Selecciona el almacen en el cuadro desplegable para poder subir la/las imagen(es) seleccionada(s).");
	} else{
		
		$('#'+id).fileUploadStart();
	}}