/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.0.51a : Database - directorioalmacen
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `ratings` */

DROP TABLE IF EXISTS `ratings`;

CREATE TABLE `ratings` (
  `id` varchar(11) NOT NULL,
  `total_votes` int(11) NOT NULL,
  `total_value` int(11) NOT NULL,
  `used_ips` longtext,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `ratings` */

insert  into `ratings`(`id`,`total_votes`,`total_value`,`used_ips`) values ('16',1,7,'a:1:{i:0;s:9:\"127.0.0.1\";}'),('5',1,6,'a:1:{i:0;s:9:\"127.0.0.1\";}'),('9',0,0,''),('6',1,2,'a:1:{i:0;s:9:\"127.0.0.1\";}'),('12',1,3,'a:1:{i:0;s:9:\"127.0.0.1\";}'),('19',0,0,''),('13',0,0,'');

/*Table structure for table `tblalmacenes` */

DROP TABLE IF EXISTS `tblalmacenes`;

CREATE TABLE `tblalmacenes` (
  `idalmacenes` int(255) NOT NULL auto_increment,
  `idcategoriaalmacen` int(255) default NULL,
  `idusuarioalmacen` int(255) default NULL,
  `nomalmacen` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `fechaCreacion` date default NULL,
  `descralmacen` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `idusuario` int(255) default NULL,
  PRIMARY KEY  (`idalmacenes`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `tblalmacenes` */

insert  into `tblalmacenes`(`idalmacenes`,`idcategoriaalmacen`,`idusuarioalmacen`,`nomalmacen`,`fechaCreacion`,`descralmacen`,`idusuario`) values (1,1,9,'Granero Cuarto Frio','2013-06-03','',1),(6,1,8,'El graniadito','2013-07-21','Hola...',1),(3,5,9,'Carniceria la mejor.','2013-06-03','',1),(4,1,8,'Almacen pague menosx','2013-06-03','Almacen que vende sus productos un poco mas economicos.',1),(5,1,9,'Almacen Chiroloco','2013-07-21','b b  n n nnnnn',1),(7,1,10,'Las baratijas','2013-07-21','Hola otra vez.',1);

/*Table structure for table `tblasociadoralmacenxusuario` */

DROP TABLE IF EXISTS `tblasociadoralmacenxusuario`;

CREATE TABLE `tblasociadoralmacenxusuario` (
  `id_asocia_alm_usu` int(255) NOT NULL auto_increment,
  `id_almacen` int(255) default NULL,
  `id_usuario` int(255) default NULL,
  PRIMARY KEY  (`id_asocia_alm_usu`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `tblasociadoralmacenxusuario` */

/*Table structure for table `tblcategorias` */

DROP TABLE IF EXISTS `tblcategorias`;

CREATE TABLE `tblcategorias` (
  `categ_cods` int(255) NOT NULL auto_increment,
  `categ_nom` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `idusuario` int(255) default NULL,
  PRIMARY KEY  (`categ_cods`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `tblcategorias` */

insert  into `tblcategorias`(`categ_cods`,`categ_nom`,`idusuario`) values (1,'Prueba 1',1),(3,'Blue Jeans',1),(4,'Camisetas',9),(5,'Zapatos',8),(6,'Medias',9),(7,'Mesasx',9);

/*Table structure for table `tblcategoriasalma` */

DROP TABLE IF EXISTS `tblcategoriasalma`;

CREATE TABLE `tblcategoriasalma` (
  `idcategoriaalmacen` int(255) NOT NULL auto_increment,
  `nomcategoriaalmcen` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `idusuario` int(20) default NULL,
  `imagen_asoc` varchar(100) character set utf8 collate utf8_spanish_ci default NULL,
  PRIMARY KEY  (`idcategoriaalmacen`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `tblcategoriasalma` */

insert  into `tblcategoriasalma`(`idcategoriaalmacen`,`nomcategoriaalmcen`,`idusuario`,`imagen_asoc`) values (1,'Graneros',1,'pic_tienda.jpg'),(3,'Farmacias',1,'pic_drogueria.jpg'),(4,'Ferreterías',1,'pic_ferreteria.jpg'),(5,'Carnicerías',1,'pic_carnicos.jpg'),(7,'Papelerias',1,'pic_papeleria.jpg'),(6,'Panaderias',1,'pic_panaderia.jpg'),(8,'Odontologías',1,'pic_odontologia.jpg'),(9,'Almacenes',1,'pic_almacenes.jpg'),(10,'Repuestos',1,'pic_repuesto.png'),(11,'Vehículos',1,'pic_vehiculos.jpg');

/*Table structure for table `tblclaverecuperacion` */

DROP TABLE IF EXISTS `tblclaverecuperacion`;

CREATE TABLE `tblclaverecuperacion` (
  `idclave` int(2) NOT NULL auto_increment,
  `clave` varchar(20) default NULL,
  PRIMARY KEY  (`idclave`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tblclaverecuperacion` */

insert  into `tblclaverecuperacion`(`idclave`,`clave`) values (1,'123');

/*Table structure for table `tblclientes` */

DROP TABLE IF EXISTS `tblclientes`;

CREATE TABLE `tblclientes` (
  `idcliente` int(255) NOT NULL auto_increment,
  `identicliente` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `nombcliente` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `apellicliente` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `nomusuariocliente` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `passcliente` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `fechanacicliente` date default NULL,
  `numcelucliente` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `numtelecliente` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `direccliente` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `email1cliente` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `email2cliente` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `maccliente` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `ipcliente` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `baneado` int(1) default NULL,
  `confirmado` int(1) default NULL,
  PRIMARY KEY  (`idcliente`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

/*Data for the table `tblclientes` */

insert  into `tblclientes`(`idcliente`,`identicliente`,`nombcliente`,`apellicliente`,`nomusuariocliente`,`passcliente`,`fechanacicliente`,`numcelucliente`,`numtelecliente`,`direccliente`,`email1cliente`,`email2cliente`,`maccliente`,`ipcliente`,`baneado`,`confirmado`) values (22,'1036613885','MARIO ALEJANDRO','BENITEZ OROZCO','1036613885','$P$B5jGBW9jA/2ZZz4GLqsoaQFpfSEOXL/','1987-09-25','3117349528','3815151','cRA 45 No. 34-45 Apart. 301','maalben@gmail.com','maalben@hotmail.com','0E-EE-E6-A9-45-E7','127.0.0.1',0,1),(30,'1111111111','Juana de arco','Perez Perez','1111111111','$P$B60OaPzGbfwFeytpjR8iWw4RtcRDcu/','2013-06-08','3113214543','0000000','cRA 45 No. 34-45 Apart. 301','maalben@yahoo.es','','0E-EE-E6-A9-45-E7','127.0.0.1',0,1),(37,'222222222','Horacio de los Alpes','Gutierres Gomez','222222222','$P$B5syPX8egSYyurqB/YuPClmedaWQUN.','1987-01-10','3258907856','','Calle Las Flores # 54 - 57 Barrio los Almendros','mabsystem2010@gmail.com','','0E-EE-E6-A9-45-E7','127.0.0.1',0,1);

/*Table structure for table `tblcomentarios` */

DROP TABLE IF EXISTS `tblcomentarios`;

CREATE TABLE `tblcomentarios` (
  `idcomentarios` int(255) NOT NULL auto_increment,
  `idproducto` int(20) default NULL,
  `idusuarioalmacen` int(20) default NULL,
  `comentarios` text character set utf8 collate utf8_spanish_ci,
  `idclientes` int(20) default NULL,
  PRIMARY KEY  (`idcomentarios`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tblcomentarios` */

insert  into `tblcomentarios`(`idcomentarios`,`idproducto`,`idusuarioalmacen`,`comentarios`,`idclientes`) values (5,5,1,'<font color=\"#66FFFF\" face=\"verdana\" size=\"6\"><b>jajajajjaaja</b></font><br>',1036613885),(4,5,1,'Me parece que es un muy buen producto.<br><br><br>Hay que tener en cuenta que no algunas tallas, pero en lo personal es muy buen producto.<br><br><br><font color=\"#FF0000\" face=\"comic sans ms\" size=\"4\"><b>Gracias.</b></font><br>',1036613885);

/*Table structure for table `tbldetallepedido` */

DROP TABLE IF EXISTS `tbldetallepedido`;

CREATE TABLE `tbldetallepedido` (
  `id__deta_pedido` int(255) NOT NULL auto_increment,
  `id_pedido` int(255) default NULL,
  `id_prods` int(255) default NULL,
  `id_almacen` int(30) default NULL,
  `id_cate_almacen` int(30) default NULL,
  `cant_prods` int(10) default NULL,
  `precio_prods` int(20) default NULL,
  `categ_nom` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `prod_nom` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `prod_iva` int(20) default NULL,
  PRIMARY KEY  (`id__deta_pedido`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Data for the table `tbldetallepedido` */

insert  into `tbldetallepedido`(`id__deta_pedido`,`id_pedido`,`id_prods`,`id_almacen`,`id_cate_almacen`,`cant_prods`,`precio_prods`,`categ_nom`,`prod_nom`,`prod_iva`) values (40,5,3,1,1,5,3000,'Medias','Patprimo',0),(39,5,5,1,1,5,15000,'Camisetas','Manpower',8),(38,5,13,1,1,10,58000,'Camisetas','Zodiacs',16),(37,4,4,6,1,10,47250,'Medias','Gamin',5),(36,4,11,6,1,10,78000,'Camisetas','Diamond',0),(35,3,11,1,1,5,78000,'Camisetas','Diamond',0),(34,3,12,1,1,3,60000,'Camisetas','Fransua',0),(33,3,4,1,1,1,47250,'Medias','Gamin',5),(32,2,12,1,1,1,60000,'Camisetas','Fransua',0),(31,2,11,1,1,1,78000,'Camisetas','Diamond',0),(30,1,11,3,5,6,78000,'Camisetas','Diamond',0),(29,1,10,3,5,7,46000,'Camisetas','New Balance',0),(28,1,13,3,5,6,58000,'Camisetas','Zodiacs',16),(41,5,4,1,1,5,47250,'Medias','Gamin',5),(42,6,13,1,1,2,58000,'Camisetas','Zodiacs',16),(43,6,12,1,1,4,60000,'Camisetas','Fransua',0),(44,6,4,1,1,6,47250,'Medias','Gamin',5),(45,6,11,1,1,8,78000,'Camisetas','Diamond',0),(46,7,13,1,1,4,58000,'Camisetas','Zodiacs',16),(47,7,4,1,1,2,47250,'Medias','Gamin',5),(48,8,11,1,1,2,78000,'Camisetas','Diamond',0),(49,8,12,1,1,1,60000,'Camisetas','Fransua',0),(50,8,4,1,1,2,47250,'Medias','Gamin',5),(51,9,12,4,1,2,60000,'Camisetas','Fransua',0),(52,9,13,4,1,1,58000,'Camisetas','Zodiacs',16),(53,10,19,6,1,10,65000,'Zapatos','Nike',30);

/*Table structure for table `tblimagenalmacen` */

DROP TABLE IF EXISTS `tblimagenalmacen`;

CREATE TABLE `tblimagenalmacen` (
  `idimagen` int(255) NOT NULL auto_increment,
  `identialmacen` int(255) default NULL,
  `nomimagen` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  PRIMARY KEY  (`idimagen`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `tblimagenalmacen` */

insert  into `tblimagenalmacen`(`idimagen`,`identialmacen`,`nomimagen`) values (5,5,'1374443104-34884339.jpg'),(3,4,'1374440090-45455666.jpg'),(6,3,'1374442004-7373464.jpg'),(11,1,'1374445919-analisis-economico-derecho.jpg'),(12,6,'1374448492-76756624.jpg'),(10,7,'1374444896-analizar1.jpg');

/*Table structure for table `tblimagencliente` */

DROP TABLE IF EXISTS `tblimagencliente`;

CREATE TABLE `tblimagencliente` (
  `idimagen` int(70) NOT NULL auto_increment,
  `identicliente` int(15) default NULL,
  `nomimagen` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  PRIMARY KEY  (`idimagen`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `tblimagencliente` */

insert  into `tblimagencliente`(`idimagen`,`identicliente`,`nomimagen`) values (22,1036613885,'1377410026-Mario2.jpg');

/*Table structure for table `tblimagendescripcion` */

DROP TABLE IF EXISTS `tblimagendescripcion`;

CREATE TABLE `tblimagendescripcion` (
  `idimgdescripcion` int(30) NOT NULL auto_increment,
  `nomimagen` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `idusuario` int(10) default NULL,
  PRIMARY KEY  (`idimgdescripcion`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tblimagendescripcion` */

insert  into `tblimagendescripcion`(`idimgdescripcion`,`nomimagen`,`idusuario`) values (2,'1378653406-291Gafas.jpg',1),(3,'1378653509-IMG_0052.JPG',1),(4,'1378653879-IMG_0061.JPG',1),(5,'1378656492-brenda2.jpg',1);

/*Table structure for table `tblimagenes` */

DROP TABLE IF EXISTS `tblimagenes`;

CREATE TABLE `tblimagenes` (
  `ImagenId` int(11) NOT NULL auto_increment,
  `ImagenArchivo` varchar(200) character set utf8 collate utf8_spanish_ci default NULL,
  `ImagenEstado` int(11) default NULL,
  PRIMARY KEY  (`ImagenId`)
) ENGINE=MyISAM AUTO_INCREMENT=293 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Data for the table `tblimagenes` */

insert  into `tblimagenes`(`ImagenId`,`ImagenArchivo`,`ImagenEstado`) values (253,'1359947229-2.Analizar.jpg',4),(254,'1359947229-34884339.jpg',4),(255,'1359947592-1CraterLeap.jpg',4),(256,'1359948549-img179.jpg',3),(258,'1360037660-2.Analizar.jpg',5),(259,'1360037660-botero-adam-eve.jpg',5),(260,'1360037661-analisis-economico-derecho.jpg',5),(261,'1360037661-acuerdos.jpg',5),(262,'1360037661-76756624.jpg',5),(263,'1360037662-1CraterLeap.jpg',5),(264,'1360037661-Ciclos de Vida - Modelo en Espiral.jpg',5),(265,'1360037663-analizar1.jpg',5),(266,'1360037661-292216_363972200327398_276089272449025_982986_812892743_n.jpg',5),(267,'1360037665-caracterdesiervo.jpg',5),(268,'1360037662-b1f5cf7ac2c36f8f1e1b8173189e2a7d_XL.jpg',5),(269,'1360037661-34884339.jpg',5),(270,'1360037665-7373464.jpg',5),(271,'1360037667-45455666 - copia.jpg',5),(277,'1364585833-animales-en-peligro-de-extincion1.jpg',3),(289,'1375571777-gobierno corporativo.jpg',12),(290,'1375844806-34884339.jpg',3),(280,'1374370880-292216_363972200327398_276089272449025_982986_812892743_n.jpg',4),(279,'1364585838-animales-domesticos.jpg',3),(281,'1374370880-45455666.jpg',4),(282,'1374450625-Foto0018.jpg',13),(283,'1374450625-Foto0020.jpg',13),(285,'1374456899-1CraterLeap.jpg',16),(286,'1374456979-mario2.jpg',16),(287,'1374458207-45455666.jpg',6),(288,'1374462276-analizar1.jpg',9);

/*Table structure for table `tblimagenportada` */

DROP TABLE IF EXISTS `tblimagenportada`;

CREATE TABLE `tblimagenportada` (
  `idportada` int(10) NOT NULL auto_increment,
  `idimagen` int(255) default NULL,
  `idproducto` int(255) default NULL,
  PRIMARY KEY  (`idportada`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `tblimagenportada` */

insert  into `tblimagenportada`(`idportada`,`idimagen`,`idproducto`) values (1,259,5),(3,280,4),(4,282,13),(5,286,16),(6,287,6),(7,288,9),(8,277,3),(9,289,12);

/*Table structure for table `tblmodulos` */

DROP TABLE IF EXISTS `tblmodulos`;

CREATE TABLE `tblmodulos` (
  `Id` int(2) NOT NULL,
  `Padre_id` int(2) NOT NULL,
  `Nombre` varchar(55) collate utf8_unicode_ci NOT NULL,
  `Fin` varchar(50) collate utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `tblmodulos` */

insert  into `tblmodulos`(`Id`,`Padre_id`,`Nombre`,`Fin`) values (1,0,'Configuración','0'),(20,0,'Clientes','0'),(21,0,'Productos','0'),(4,0,'Almacenes','0'),(14,4,'Listado Almacenes','listado_almacenes.php?optCat=4'),(6,0,'Pedidos','0'),(7,0,'Reportes','0'),(8,1,'Sistema','sistema.php'),(9,1,'Permisos','permisos.php?optCat=6'),(10,20,'Buscar clientes','buscar_clientes.php'),(11,20,'Listado clientes',''),(12,21,'Importar Productos','importar_productos.php'),(13,21,'Listado Productos','listado_productos.php'),(15,6,'Pedidos generales','ver_pedidos_todos.php?optCat=7&vista=todos'),(16,6,'Pedidos por Clientes','ver_pedidos_clientes.php?optCat=7&vista=clientes'),(17,7,'Reporte por Vendedor','reporte_vendedor.php'),(18,7,'Reporte por Cliente','reporte_cliente.php'),(5,0,'Vendedores','0'),(19,5,'Ver Vendedores','vendedores_listado.php'),(2,0,'Mercancia','0'),(3,0,'Clientes','0'),(22,2,'Productos','listado_productos.php?optCat=1'),(23,3,'Lista de Clientes','listado_clientes.php?optCat=8'),(24,4,'Ver ruteros por vendedor','ruteros_vende.php'),(25,6,'Pedidos por fecha','ver_pedidos_fechas.php?optCat=7&vista=fechas'),(27,26,'Ver devoluciones','devoluciones.php'),(26,0,'Devolucion','0'),(28,0,'Mensajeria','0'),(29,28,'Mensajería interna','mensajeria_interna.php'),(30,1,'Clave de recuperación','clave_recuperacion.php'),(31,2,'Categorías de Productos','listado_categorias.php?optCat=2'),(32,2,'Proveedores','listado_proveedores.php?optCat=3'),(33,4,'Categorias Almacenes','listado_categorias_alm.php?optCat=5'),(34,6,'Pedidos por rango de totales','ver_pedidos_totales.php?optCat=7&vista=totales');

/*Table structure for table `tblpedidos` */

DROP TABLE IF EXISTS `tblpedidos`;

CREATE TABLE `tblpedidos` (
  `id_pedido` int(255) NOT NULL,
  `total_pedido` int(20) default NULL,
  `fecha_pedido` date default NULL,
  `confirmado` int(1) default NULL,
  `despachado` int(1) default NULL,
  `cedulaCliente` int(15) default NULL,
  `idusuario` int(255) default NULL,
  `idalmacen` int(255) default NULL,
  `nomalmacen` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  PRIMARY KEY  (`id_pedido`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `tblpedidos` */

insert  into `tblpedidos`(`id_pedido`,`total_pedido`,`fecha_pedido`,`confirmado`,`despachado`,`cedulaCliente`,`idusuario`,`idalmacen`,`nomalmacen`) values (4,1230000,'2013-07-07',0,0,222222222,8,6,'El graniadito'),(3,615000,'2013-07-07',0,0,1111111111,9,1,'Granero Cuarto Frio'),(2,138000,'2013-07-06',0,0,1111111111,9,1,'Granero Cuarto Frio'),(1,1090000,'2013-06-21',0,0,1036613885,9,3,'Carniceria la mejor.'),(5,815000,'2013-07-07',0,0,222222222,9,1,'Granero Cuarto Frio'),(6,1250000,'2013-07-07',0,0,1036613885,9,1,'Granero Cuarto Frio'),(7,326500,'2013-07-07',0,0,222222222,8,1,'Almacen pague menosx'),(8,310500,'2013-07-07',1,0,1036613885,8,1,'Almacen pague menosx'),(9,178000,'2013-07-11',0,0,222222222,8,4,'Almacen pague menosx'),(10,650000,'2013-08-12',0,0,1111111111,8,6,'El graniadito');

/*Table structure for table `tblpermisos` */

DROP TABLE IF EXISTS `tblpermisos`;

CREATE TABLE `tblpermisos` (
  `Idpermiso` int(2) NOT NULL auto_increment,
  `Modulo` int(2) NOT NULL,
  `Usuario` int(2) NOT NULL,
  `Padre` int(11) NOT NULL,
  PRIMARY KEY  (`Idpermiso`)
) ENGINE=MyISAM AUTO_INCREMENT=435 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tblpermisos` */

insert  into `tblpermisos`(`Idpermiso`,`Modulo`,`Usuario`,`Padre`) values (423,29,1,28),(424,30,1,1),(371,16,1,6),(432,23,8,3),(430,22,1,2),(434,34,1,6),(372,17,1,7),(369,14,1,4),(364,9,1,1),(419,8,1,1),(378,23,1,3),(416,32,8,2),(370,15,1,6),(366,11,1,20),(411,32,9,2),(406,31,9,2),(405,22,9,2),(373,18,1,7),(420,24,1,4),(422,27,1,26),(408,22,8,2),(396,10,1,20),(421,25,1,6),(418,15,8,6),(399,12,1,21),(409,31,8,2),(417,15,9,6),(425,31,1,2),(426,32,1,2),(427,33,1,4),(428,14,9,4),(429,14,8,4);

/*Table structure for table `tblproductos` */

DROP TABLE IF EXISTS `tblproductos`;

CREATE TABLE `tblproductos` (
  `prod_cods` int(255) NOT NULL,
  `provee_cods` int(255) default NULL,
  `categ_cods` int(255) default NULL,
  `prod_nom` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `prod_cant` int(50) default NULL,
  `prod_precio` float(20,0) default NULL,
  `prod_fech_compr` date default NULL,
  `prod_descr` text,
  `idusuario` int(255) default NULL,
  `prod_iva` float default NULL,
  `idusuarioalmacen` int(255) default NULL,
  `oculto` int(1) default NULL,
  `borrado` int(1) default NULL,
  PRIMARY KEY  (`prod_cods`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `tblproductos` */

insert  into `tblproductos`(`prod_cods`,`provee_cods`,`categ_cods`,`prod_nom`,`prod_cant`,`prod_precio`,`prod_fech_compr`,`prod_descr`,`idusuario`,`prod_iva`,`idusuarioalmacen`,`oculto`,`borrado`) values (5,1,4,'Manpower',30,15000,'2012-01-13','<div style=\"text-align: center;\"><b><font color=\"#000099\" size=\"6\" face=\"comic sans ms\">Camisetas de ultima moda en colombia</font></b></div><div style=\"text-align: center;\"><b><font color=\"#000099\" size=\"6\" face=\"comic sans ms\"><br></font></b></div><div style=\"text-align: center;\"><img src=\"http://lh3.ggpht.com/-cqUsnu4CrEw/SxS02gmYorI/AAAAAAAAAME/-YghjHmTZa4/camisetas-negra-tirantes-pandora-colores-oscuros-mimousha.gif\" alt=\"\" align=\"none\"></div><div style=\"text-align: center;\"><br></div><div style=\"text-align: justify;\"><font color=\"#ff0000\" size=\"4\"><b>Aprovecha nuestras grandes promociones de nuestras camisetas.</b></font></div><div style=\"text-align: justify;\"><font color=\"#ff0000\"><b><br></b></font></div><div style=\"text-align: center;\"><br></div><div style=\"text-align: center;\"><br></div><div style=\"text-align: justify;\"><font color=\"#ff0000\" size=\"6\"><b>Muchas gracias en confiar en nosotros.</b></font></div><div style=\"text-align: justify;\"><font color=\"#ff0000\" size=\"6\"><b><br></b></font></div><div style=\"text-align: center;\"><img src=\"http://sofanaranja.com/wp-content/sn_camisetas_003_malas_comp.png\" alt=\"\" align=\"none\"></div><div style=\"text-align: center;\"><font size=\"6\" color=\"#000099\" face=\"impact\"><b><u><br></u></b></font></div><div style=\"text-align: center;\"><font size=\"6\" color=\"#000099\" face=\"impact\"><b><u>Otro estilo de camisetas</u></b></font></div><div style=\"text-align: center;\"><font size=\"6\" color=\"#000099\" face=\"impact\"><b><u><br></u></b></font></div><div style=\"text-align: center;\"><img src=\"http://www.camisetas.info/images/Roly/productos/0425-60.jpg\" alt=\"\" align=\"none\"></div><div style=\"text-align: center;\"><font color=\"#ff0000\" size=\"6\"><b><br></b></font></div>',9,8,1,0,0),(4,1,6,'Gamin',75,45000,'2012-02-09','Pantalones con medidas 28 a 40.',9,5,1,0,0),(3,1,6,'Patprimo',40,3000,'2013-02-05','Medias de color blanco, negro, rojo y azules.',9,0,1,0,0),(6,3,6,'Rotas',67,3500,'2013-02-08','Jasj as jsjao js a.',9,0,1,0,0),(7,3,6,'Chevignon',23,7800,'2013-02-04','sdaas as as as',9,0,3,0,0),(8,10,4,'controles',23,12000,'2013-02-02','as ada das asd',9,0,3,0,0),(9,3,4,'Sueter',323,8900,'2013-02-06','as ada dasd asd',9,0,1,0,0),(10,9,4,'New Balance',23,46000,'2013-02-01','ss sds d sds ',9,0,3,1,0),(11,9,4,'Diamond',34,78000,'2013-01-29','kmkcccccccc.',9,0,3,0,0),(12,2,4,'Fransua',45,60000,'2013-02-02','rggfg dfg dgd fg dg.',9,0,1,0,0),(13,3,4,'Zodiacs',30,50000,'2013-02-03','dfsdfsdf sdf sdf.',9,16,3,0,0),(14,5,5,'Prueba de ingresox',123456,100,'2013-07-18','OPOLOLOLO',8,0,4,1,0),(15,5,5,'Prueba de ingreso',12345,12000,'2013-07-17','<font face=\"comic sans ms\"><b>MMAAMAAMAMA</b></font>',8,5,4,1,0),(16,3,4,'TENDIDOS',9999,12500,'2013-07-18','Productos para temporada de frío.',9,0,1,0,0),(17,5,5,'ALMOHADAS',8888,13000,'2013-07-18','Para tener comodidad al dormir.',8,6,4,0,0),(19,5,5,'Nike',150,50000,'2013-08-04','<div><br></div><b><font size=\"6\" face=\"comic sans ms\" color=\"#000099\">Hola</font></b>',8,30,6,0,0),(18,10,7,'Comedor ingles',15,70000,'2013-07-20','Mesa con un estilo muy decorativo.',9,12,3,0,0);

/*Table structure for table `tblproveedor` */

DROP TABLE IF EXISTS `tblproveedor`;

CREATE TABLE `tblproveedor` (
  `provee_cods` int(255) NOT NULL auto_increment,
  `provee_nom` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `provee_dir` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `ciudad` varchar(255) character set utf8 collate utf8_spanish_ci default NULL,
  `idusuario` int(255) default NULL,
  PRIMARY KEY  (`provee_cods`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `tblproveedor` */

insert  into `tblproveedor`(`provee_cods`,`provee_nom`,`provee_dir`,`ciudad`,`idusuario`) values (1,'Juana de Arco de Gomez','Calle las Manguitas','Medellin',9),(2,'Roberto Gomez Bolaños','Av. Las Brisas','Bucaramanga',9),(3,'Anacleto Diaz','Por las calles de las flores','Medellin',9),(4,'Cataclismo Perez Londono','Juanajuato','Pereira',1),(5,'Diego Leon Camacho','Las balsitas','Mexico',8),(9,'prueba de proveedo','prueba  de direccion provee','cali',9),(10,'Leonidas','Barrio las luciernagass jeje','Jardinesx',9);

/*Table structure for table `tbltipousuario` */

DROP TABLE IF EXISTS `tbltipousuario`;

CREATE TABLE `tbltipousuario` (
  `IdTipo` int(1) NOT NULL,
  `Tipo` varchar(55) character set utf8 collate utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tbltipousuario` */

insert  into `tbltipousuario`(`IdTipo`,`Tipo`) values (1,'Root'),(2,'Administrador'),(3,'Vendedor'),(4,'Facturacion');

/*Table structure for table `tblusuarios` */

DROP TABLE IF EXISTS `tblusuarios`;

CREATE TABLE `tblusuarios` (
  `Id` int(2) NOT NULL auto_increment,
  `Login` varchar(55) character set utf8 collate utf8_spanish_ci NOT NULL,
  `Clave` varchar(55) character set utf8 collate utf8_spanish_ci NOT NULL,
  `Nombre` varchar(99) character set utf8 collate utf8_spanish_ci NOT NULL,
  `UltimoAcceso` datetime NOT NULL,
  `Tipo` int(2) NOT NULL,
  `Estado` int(1) NOT NULL,
  `correoVendedor` varchar(100) character set utf8 collate utf8_spanish_ci default NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=2147483648 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tblusuarios` */

insert  into `tblusuarios`(`Id`,`Login`,`Clave`,`Nombre`,`UltimoAcceso`,`Tipo`,`Estado`,`correoVendedor`) values (1,'admin','e10adc3949ba59abbe56e057f20f883e','Root','2013-09-07 21:52:31',1,1,'maalben@gmail.com'),(8,'vendedor2','e10adc3949ba59abbe56e057f20f883e','Diego Armando Maradona','2013-09-01 02:26:43',3,1,'maalben@hotmail.com'),(9,'vendedor1','e10adc3949ba59abbe56e057f20f883e','Carlos El Pibe Valderrama','2013-08-24 16:47:55',3,1,'maalben@yahoo.es'),(2,'Administrador1','e10adc3949ba59abbe56e057f20f883e','Administrador General','2013-07-13 10:55:28',1,1,'maalben@gmail.com'),(3,'Administrador2','e10adc3949ba59abbe56e057f20f883e','Administrador','2013-07-12 21:02:35',1,1,'maalben@gmail.com'),(10,'juana','e10adc3949ba59abbe56e057f20f883e','Juana','2013-07-16 22:15:33',3,2,'juana@gmail.com');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
