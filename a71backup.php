<?php
  // A71-BackUp 1.8 (08.04.2008) By Alessandro Marinuzzi [Alecos]
  // > A71BackUp is under GPL - updates to http://www.alecos.it <
  //=============================================================
  //
  //=============================================================
  $password = "123";  // Change the default password
  //=============================================================
  $excludir = "";  // Full path of dir to exclude from the root
  //=============================================================
  $tar_file = $_SERVER['HTTP_HOST'].'_'.date("d-m-Y").'.tar';
  //=============================================================
  $dbhost1 = "localhost";  // Database Host
  $dbuser1 = "root";  // Databse User
  $dbpsw1 = "";  // Database Password
  $dbname1 = "directorioalmacen";  // Database Name
  $dbfile1 = "mydbname_1.sql";  // Database File
  //=============================================================
/*  $dbhost2 = "localhost";  // Database Host
  $dbuser2 = "mydbuser";  // Databse User
  $dbpsw2 = "mydbpassword";  // Database Password
  $dbname2 = "mydbname_2";  // Database Name
  $dbfile2 = "mydbname_2.sql";  // Database File
  //=============================================================
  $dbhost3 = "localhost";  // Database Host
  $dbuser3 = "mydbuser";  // Databse User
  $dbpsw3 = "mydbpassword";  // Database Password
  $dbname3 = "mydbname_3";  // Database Name
  $dbfile3 = "mydbname_3.sql";  // Database File
  //=============================================================
  $dbhost4 = "localhost";  // Database Host
  $dbuser4 = "mydbuser";  // Databse User
  $dbpsw4 = "mydbpassword";  // Database Password
  $dbname4 = "mydbname_4";  // Database Name
  $dbfile4 = "mydbname_4.sql";  // Database File
  //=============================================================
  $dbhost5 = "localhost";  // Database Host
  $dbuser5 = "mydbuser";  // Databse User
  $dbpsw5 = "mydbpassword";  // Database Password
  $dbname5 = "mydbname_5";  // Database Name
  $dbfile5 = "mydbname_5.sql";  // Database File       */
  //=============================================================
  //
  ###############################################################
  ############## Do not change the following code! ##############
  ###############################################################
  ini_set('memory_limit', '-1');
  ini_set('register_globals', 0);
  ini_set('max_execution_time', 0);
  //=============================================================
  if ($_POST['backup'] == "on") {
    $web_root = $_SERVER['DOCUMENT_ROOT'];
    chdir("$web_root");
  }
  //=============================================================
  if (!isset($_POST['password'])) {
    echo '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<HTML>

<HEAD>
  <META HTTP-EQUIV="content-type" CONTENT="text/html;CHARSET=iso-8859-1">
  <META NAME="author" CONTENT="Alecos [Alessandro Marinuzzi]">
  <META NAME="generator" CONTENT="Alecos [Alessandro Marinuzzi]">
  <META NAME="robots" CONTENT="noindex, nofollow">
  <TITLE>[A71-BackUp: Restricted Area]</TITLE>
  <STYLE TYPE="text/css">
  H3, H4, H5, H6 {
    font-family: Arial, Verdana, Helvetica, sans-serif;
    font-size: 16px;
    color: #FFFFFF;
    background-color: inherit;
    font-style: normal;
    line-height: normal;
    font-weight: bold;
    font-variant: normal;
  }
  H2 {
    font-family: Arial, Verdana, Helvetica, sans-serif;
    font-size: 20px;
    color: #FFFFFF;
    background-color: inherit;
    font-style: normal;
    line-height: normal;
    font-weight: bold;
    font-variant: normal;
  }
  H1 {
    font-family: Arial, Verdana, Helvetica, sans-serif;
    font-size: 24px;
    color: #FFFFFF;
    background-color: inherit;
    font-style: normal;
    line-height: normal;
    font-weight: bold;
    font-variant: normal;
  }
  A:LINK {
    color: #008080;
    background-color: inherit;
    text-decoration: none;
  }
  A:VISITED {
    color: #008080;
    background-color: inherit;
    text-decoration: none;
  }
  A:HOVER {
    color: #0000CD;
    background-color: inherit;
    text-decoration: none;
  }
  BODY {
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    margin-bottom: 0px;
    background: #FFFFFF;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 12px;
    color: #FFFFFF;
    font-style: normal;
    line-height: normal;
    font-weight: normal;
    font-variant: normal;
  }
  TD {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 12px;
    color: #FFFFFF;
    background-color: inherit;
    font-style: normal;
    line-height: normal;
    font-weight: normal;
    font-variant: normal;
  }
  </STYLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF" LINK="#008080" ALINK="#C0D9D9" VLINK="#808080" TEXT="#FFFFFF" MARGINWIDTH="0" MARGINHEIGHT="0" TOPMARGIN="0" LEFTMARGIN="0">

<TABLE WIDTH="97%" ALIGN="CENTER" CELLPADDING="15" CELLSPACING="0" BORDER="0">
  <TR>
    <TD>&nbsp;</TD>
  </TR>
  <TR>
    <TD>  
      <TABLE BGCOLOR="#000080" ALIGN="CENTER" CELLPADDING="5" CELLSPACING="0" BORDER="1">
        <TR>
          <TD>
            <TABLE ALIGN="CENTER" CELLPADDING="3" CELLSPACING="0" BORDER="0">
              <TR>
                <TD COLSPAN="2" ALIGN="CENTER"><B>A71-BackUp 1.8 By Alecos</B></TD>
              </TR>
              <TR>
                <TD COLSPAN="2"><HR SIZE="2" WIDTH="100%" ALIGN="CENTER"></TD>
              </TR>
              <TR>
                <TD COLSPAN="2"><FORM NAME="post" METHOD="POST" ACTION="' . $_SERVER["PHP_SELF"] . '"></TD>
              </TR>
              <TR>
                <TD ALIGN="RIGHT"><B>Password:&nbsp;&nbsp;</B></TD><TD ALIGN="LEFT"><INPUT TYPE="password" NAME="password" SIZE="15" VALUE="' . $_POST["password"] . '"></TD>
              </TR>
              <TR>
                <TD ALIGN="RIGHT"><INPUT TYPE="radio" CHECKED NAME="backup" VALUE="on"><B>Full BackUp</B></TD><TD ALIGN="LEFT"><INPUT TYPE="radio" NAME="backup" VALUE="off"><B>Partial BackUp</B></TD>
              </TR>
              <TR>
                <TD COLSPAN="2" ALIGN="CENTER"><INPUT TYPE="checkbox" CHECKED NAME="backupdb" VALUE="on"><B>MySQL Backup</B></TD>
              </TR>
              <TR>
                <TD COLSPAN="2">
                  <TABLE BGCOLOR="#FFFFFF" WIDTH="100%" ALIGN="CENTER" BORDER="1">
                    <TR>
                      <TD ALIGN="CENTER">&nbsp;<FONT COLOR="#FF0000">Enter Your Password!</FONT>&nbsp;</TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
              <TR>
                <TD COLSPAN="2" ALIGN="CENTER"><INPUT TYPE="submit" NAME="submit" VALUE=" BackUp "></FORM></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

</BODY>

</HTML>';
  } else {
    if ($_POST['password'] == $password) {
      /**
      * Dump MySQL database
      *
      * Here is an inline example:
      * <code>
      * $connection = @mysql_connect($dbhost,$dbuser,$dbpsw);
      * $dumper = new MySQLDump($dbname,'filename.sql',false,false);
      * $dumper->doDump();
      * </code>
      *
      * Special thanks to:
      * - Andrea Ingaglio <andrea@coders4fun.com> helping in development of all class code
      * - Dylan Pugh for precious advices halfing the size of the output file and for helping in debug
      *
      * @name    MySQLDump
      * @author  Daniele Vigan� - CreativeFactory.it <daniele.vigano@creativefactory.it>
      * @version 2.20 - 02/11/2007
      * @license http://opensource.org/licenses/gpl-license.php GNU Public License
      */
      class MySQLDump {
        /**
        * @access private
        */
        var $database = null;
        /**
        * @access private
        */
        var $compress = false;
        /**
        * @access private
        */
        var $hexValue = false;
        /**
        * The output filename
        * @access private
        */
        var $filename = null;
        /**
        * The pointer of the output file
        * @access private
        */
        var $file = null;
        /**
        * @access private
        */
        var $isWritten = false;
        /**
        * Class constructor
        * @param string $db The database name
        * @param string $filepath The file where the dump will be written
        * @param boolean $compress It defines if the output file is compress (gzip) or not
        * @param boolean $hexValue It defines if the outup values are base-16 or not
        */
        function MYSQLDump($db = null, $filepath = 'dump.sql', $compress = false, $hexValue = false) {
          $this->compress = $compress;
          if ( !$this->setOutputFile($filepath) )
            return false;
          return $this->setDatabase($db);
        }
        /**
        * Sets the database to work on
        * @param string $db The database name
        */
        function setDatabase($db) {
          $this->database = $db;
          if ( !@mysql_select_db($this->database) )
            return false;
          return true;
        }
        /**
        * Returns the database where the class is working on
        * @return string
        */
        function getDatabase() {
          return $this->database;
        }
        /**
        * Sets the output file type (It can be made only if the file hasn't been already written)
        * @param boolean $compress If it's true, the output file will be compressed
        */
        function setCompress($compress) {
          if ( $this->isWritten )
            return false;
          $this->compress = $compress;
          $this->openFile($this->filename);
          return true;
        }
        /**
        * Returns if the output file is or not compressed
        * @return boolean
        */
        function getCompress() {
          return $this->compress;
        }
        /**
        * Sets the output file
        * @param string $filepath The file where the dump will be written
        */
        function setOutputFile($filepath) {
          if ( $this->isWritten )
            return false;
          $this->filename = $filepath;
          $this->file = $this->openFile($this->filename);
          return $this->file;
        }
        /**
        * Returns the output filename
        * @return string
        */
        function getOutputFile() {
          return $this->filename;
        }
        /**
        * Writes to file the $table's structure
        * @param string $table The table name
        */
        function getTableStructure($table) {
          if ( !$this->setDatabase($this->database) )
            return false;
          // Structure Header
          $structure = "-- \n";
          $structure .= "-- Table structure for table `{$table}` \n";
          $structure .= "-- \n\n";
          // Dump Structure
          $structure .= 'DROP TABLE IF EXISTS `'.$table.'`;'."\n";
          $structure .= "CREATE TABLE `".$table."` (\n";
          $records = @mysql_query('SHOW FIELDS FROM `'.$table.'`');
          if ( @mysql_num_rows($records) == 0 )
            return false;
          while ( $record = mysql_fetch_assoc($records) ) {
            $structure .= '`'.$record['Field'].'` '.$record['Type'];
            if ( !empty($record['Default']) )
              $structure .= ' DEFAULT \''.$record['Default'].'\'';
            if ( @strcmp($record['Null'],'YES') != 0 )
              $structure .= ' NOT NULL';
            if ( !empty($record['Extra']) )
              $structure .= ' '.$record['Extra'];
            $structure .= ",\n";
          }
          $structure = @ereg_replace(",\n$", null, $structure);
          // Save all Column Indexes
          $structure .= $this->getSqlKeysTable($table);
          $structure .= "\n)";
          //Save table engine
          $records = @mysql_query("SHOW TABLE STATUS LIKE '".$table."'");
          echo $query;
          if ( $record = @mysql_fetch_assoc($records) ) {
            if ( !empty($record['Engine']) )
              $structure .= ' ENGINE='.$record['Engine'];
            if ( !empty($record['Auto_increment']) )
              $structure .= ' AUTO_INCREMENT='.$record['Auto_increment'];
          }
          $structure .= ";\n\n-- --------------------------------------------------------\n\n";
          $this->saveToFile($this->file,$structure);
        }
        /**
        * Writes to file the $table's data
        * @param string $table The table name
        * @param boolean $hexValue It defines if the output is base 16 or not
        */
        function getTableData($table,$hexValue = true) {
          if ( !$this->setDatabase($this->database) )
            return false;
          // Header
          $data = "-- \n";
          $data .= "-- Dumping data for table `$table` \n";
          $data .= "-- \n\n";
          $records = mysql_query('SHOW FIELDS FROM `'.$table.'`');
          $num_fields = @mysql_num_rows($records);
          if ( $num_fields == 0 )
            return false;
          // Field names
          $selectStatement = "SELECT ";
          $insertStatement = "INSERT INTO `$table` (";
          $hexField = array();
          for ($x = 0; $x < $num_fields; $x++) {
            $record = @mysql_fetch_assoc($records);
            if ( ($hexValue) && ($this->isTextValue($record['Type'])) ) {
              $selectStatement .= 'HEX(`'.$record['Field'].'`)';
              $hexField [$x] = true;
            }
            else
              $selectStatement .= '`'.$record['Field'].'`';
            $insertStatement .= '`'.$record['Field'].'`';
            $insertStatement .= ", ";
            $selectStatement .= ", ";
          }
          $insertStatement = @substr($insertStatement,0,-2).') VALUES';
          $selectStatement = @substr($selectStatement,0,-2).' FROM `'.$table.'`';
          $records = @mysql_query($selectStatement);
          $num_rows = @mysql_num_rows($records);
          $num_fields = @mysql_num_fields($records);
          // Dump data
          if ( $num_rows > 0 ) {
            $data .= $insertStatement;
            for ($i = 0; $i < $num_rows; $i++) {
              $record = @mysql_fetch_assoc($records);
              $data .= ' (';
              for ($j = 0; $j < $num_fields; $j++) {
                $field_name = @mysql_field_name($records, $j);
                if ( $hexField[$j] && (@strlen($record[$field_name]) > 0) )
                  $data .= "0x".$record[$field_name];
                else
                  $data .= "'".@str_replace('\"','"',@mysql_escape_string($record[$field_name]))."'";
                $data .= ',';
              }
              $data = @substr($data,0,-1).")";
              $data .= ( $i < ($num_rows-1) ) ? ',' : ';';
              $data .= "\n";
              //if data in greather than 1MB save
              if (strlen($data) > 1048576) {
                $this->saveToFile($this->file,$data);
                $data = '';
              }
            }
            $data .= "\n-- --------------------------------------------------------\n\n";
            $this->saveToFile($this->file,$data);
          }
        }
        /**
        * Writes to file all the selected database tables structure
        * @return boolean
        */
        function getDatabaseStructure() {
          $records = @mysql_query('SHOW TABLES');
          if ( @mysql_num_rows($records) == 0 )
            return false;
          while ( $record = @mysql_fetch_row($records) ) {
            $structure .= $this->getTableStructure($record[0]);
          }
          return true;
        }
        /**
        * Writes to file all the selected database tables data
        * @param boolean $hexValue It defines if the output is base-16 or not
        */
        function getDatabaseData($hexValue = true) {
          $records = @mysql_query('SHOW TABLES');
          if ( @mysql_num_rows($records) == 0 )
            return false;
          while ( $record = @mysql_fetch_row($records) ) {
            $this->getTableData($record[0],$hexValue);
          }
        }
        /**
        * Writes to file the selected database dump
        */
        function doDump() {
          $this->saveToFile($this->file,"SET FOREIGN_KEY_CHECKS = 0;\n\n");
          $this->getDatabaseStructure();
          $this->getDatabaseData($this->hexValue);
          $this->saveToFile($this->file,"SET FOREIGN_KEY_CHECKS = 1;\n\n");
          $this->closeFile($this->file);
          return true;
        }
        /**
        * @deprecated Look at the doDump() method
        */
        function writeDump($filename) {
          if ( !$this->setOutputFile($filename) )
            return false;
          $this->doDump();
          $this->closeFile($this->file);
          return true;
        }
        /**
        * @access private
        */
        function getSqlKeysTable ($table) {
          $primary = "";
          unset($unique);
          unset($index);
          unset($fulltext);
          $results = mysql_query("SHOW KEYS FROM `{$table}`");
          if ( @mysql_num_rows($results) == 0 )
            return false;
          while($row = mysql_fetch_object($results)) {
            if (($row->Key_name == 'PRIMARY') AND ($row->Index_type == 'BTREE')) {
              if ( $primary == "" )
                $primary = "  PRIMARY KEY  (`{$row->Column_name}`";
              else
                $primary .= ", `{$row->Column_name}`";
            }
            if (($row->Key_name != 'PRIMARY') AND ($row->Non_unique == '0') AND ($row->Index_type == 'BTREE')) {
              if ( (!is_array($unique)) OR ($unique[$row->Key_name]=="") )
                $unique[$row->Key_name] = "  UNIQUE KEY `{$row->Key_name}` (`{$row->Column_name}`";
              else
                $unique[$row->Key_name] .= ", `{$row->Column_name}`";
            }
            if (($row->Key_name != 'PRIMARY') AND ($row->Non_unique == '1') AND ($row->Index_type == 'BTREE')) {
              if ( (!is_array($index)) OR ($index[$row->Key_name]=="") )
                $index[$row->Key_name] = "  KEY `{$row->Key_name}` (`{$row->Column_name}`";
              else
                $index[$row->Key_name] .= ", `{$row->Column_name}`";
            }
            if (($row->Key_name != 'PRIMARY') AND ($row->Non_unique == '1') AND ($row->Index_type == 'FULLTEXT')) {
              if ( (!is_array($fulltext)) OR ($fulltext[$row->Key_name]=="") )
                $fulltext[$row->Key_name] = "  FULLTEXT `{$row->Key_name}` (`{$row->Column_name}`";
              else
                $fulltext[$row->Key_name] .= ", `{$row->Column_name}`";
            }
          }
          $sqlKeyStatement = '';
          // generate primary, unique, key and fulltext
          if ( $primary != "" ) {
            $sqlKeyStatement .= ",\n";
            $primary .= ")";
            $sqlKeyStatement .= $primary;
          }
          if (is_array($unique)) {
            foreach ($unique as $keyName => $keyDef) {
              $sqlKeyStatement .= ",\n";
              $keyDef .= ")";
              $sqlKeyStatement .= $keyDef;
      
            }
          }
          if (is_array($index)) {
            foreach ($index as $keyName => $keyDef) {
              $sqlKeyStatement .= ",\n";
              $keyDef .= ")";
              $sqlKeyStatement .= $keyDef;
            }
          }
          if (is_array($fulltext)) {
            foreach ($fulltext as $keyName => $keyDef) {
              $sqlKeyStatement .= ",\n";
              $keyDef .= ")";
              $sqlKeyStatement .= $keyDef;
            }
          }
          return $sqlKeyStatement;
        }
        /**
        * @access private
        */
        function isTextValue($field_type) {
          switch ($field_type) {
            case "tinytext":
            case "text":
            case "mediumtext":
            case "longtext":
            case "binary":
            case "varbinary":
            case "tinyblob":
            case "blob":
            case "mediumblob":
            case "longblob":
              return True;
              break;
            default:
              return False;
          }
        }
        /**
        * @access private
        */
        function openFile($filename) {
          $file = false;
          if ( $this->compress )
            $file = @gzopen($filename, "w9");
          else
            $file = @fopen($filename, "w");
          return $file;
        }
        /**
        * @access private
        */
        function saveToFile($file, $data) {
          if ( $this->compress )
            @gzwrite($file, $data);
          else
            @fwrite($file, $data);
          $this->isWritten = true;
        }
        /**
        * @access private
        */
        function closeFile($file) {
          if ( $this->compress )
            @gzclose($file);
          else
            @fclose($file);
        }
      }
      if ($_POST['backupdb'] == "on") {
        $connection1 = @mysql_connect($dbhost1,$dbuser1,$dbpsw1);
        if ($connection1) {
          $dumper = new MySQLDump($dbname1,$dbfile1,false,false);
          $dumper->doDump();
        }
        $connection2 = @mysql_connect($dbhost2,$dbuser2,$dbpsw2);
        if ($connection2) {
          $dumper = new MySQLDump($dbname2,$dbfile2,false,false);
          $dumper->doDump();
        }
        $connection3 = @mysql_connect($dbhost3,$dbuser3,$dbpsw3);
        if ($connection3) {
          $dumper = new MySQLDump($dbname3,$dbfile3,false,false);
          $dumper->doDump();
        }
        $connection4 = @mysql_connect($dbhost4,$dbuser4,$dbpsw4);
        if ($connection4) {
          $dumper = new MySQLDump($dbname4,$dbfile4,false,false);
          $dumper->doDump();
        }
        $connection5 = @mysql_connect($dbhost5,$dbuser5,$dbpsw5);
        if ($connection5) {
          $dumper = new MySQLDump($dbname5,$dbfile5,false,false);
          $dumper->doDump();
        }
      }  
      Class Tar_Archive {
        var $tar_file;
        var $fp;
        function Tar_Archive($tar_file) {
          $this->tar_file = $tar_file;
          $this->fp = fopen($this->tar_file, "wb");
          $tree = $this->build_tree();
          $this->process_tree($tree);
          fputs($this->fp, pack("a512", ""));
          fclose($this->fp);
          ignore_user_abort(true);
          header("Pragma: public");
          header("Expires: 0");
          header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
          header("Cache-Control: private",false);
          header("Content-Type: application/octet-stream");
          header("Content-Disposition: attachment; filename=".basename($tar_file).";");
          header("Content-Transfer-Encoding: binary");
          header("Content-Length: ".filesize($tar_file));
          readfile($tar_file);
          unlink($tar_file);
          global $dbfile1;
          global $dbfile2;
          global $dbfile3;
          global $dbfile4;
          global $dbfile5;
          @unlink($dbfile1);
          @unlink($dbfile2);
          @unlink($dbfile3);
          @unlink($dbfile4);
          @unlink($dbfile5);
        }
        function build_tree($dir = '.') {
          $output = array();
          $handle = opendir($dir);
          while(false !== ($readdir = readdir($handle))) {
            if ($readdir != '.' && $readdir != '..') {
              $path = $dir.'/'.$readdir;
              if (!is_link($path)) {
                if (is_file($path)) {
                  $output[] = substr($path, 2, strlen($path));
                } elseif (is_dir($path)) {
                  global $excludir;
                  if (!empty($excludir)) {
                    $pos = strpos($path, $excludir);
                    if (($pos !== false) && (is_dir($excludir))) {
                      $output[] = "";
                    } else {
                      $output[] = substr($path, 2, strlen($path)).'/';
                      $output = array_merge($output, $this->build_tree($path));
                    }
                  } else {
                    $output[] = substr($path, 2, strlen($path)).'/';
                    $output = array_merge($output, $this->build_tree($path));
                  }  
                }
              }
            }
          }
          closedir($handle);
          return $output;
        }
        function process_tree($tree) {
          foreach($tree as $pathfile) {
            if (substr($pathfile, -1, 1) == '/') {
              fputs($this->fp, $this->build_header($pathfile));
            } elseif ($pathfile != $this->tar_file) {
              $filesize = filesize($pathfile);
              $block_len = 512*ceil($filesize/512)-$filesize;
              fputs($this->fp, $this->build_header($pathfile));
              fputs($this->fp, file_get_contents($pathfile));
              fputs($this->fp, pack("a".$block_len, ""));
            }
          }
          return true;
        }
        function build_header($pathfile) {
          if (strlen($pathfile) > 999999) die('Error');
            $info = stat($pathfile);
            if (is_dir($pathfile)) $info[7] = 0;
              $header = pack("a100a8a8a8a12A12a8a1a100a255", $pathfile, sprintf("%6s ", decoct($info[2])), sprintf("%6s ", decoct($info[4])), sprintf("%6s ", decoct($info[5])), sprintf("%11s ",decoct($info[7])), sprintf("%11s", decoct($info[9])), sprintf("%8s", " "), (is_dir($pathfile) ? "5" : "0"), "", "");
              clearstatcache();
              $checksum = 0;
              for ($i = 0; $i < 512; $i++) {
                $checksum += ord(substr($header,$i,1));
              }
              $checksum_data = pack("a8", sprintf("%6s ", decoct($checksum)));
              for ($i = 0, $j = 148; $i < 7; $i++, $j++) {
                $header[$j] = $checksum_data[$i];
              }  
              return $header;
        }
      }
      $tar = & new Tar_Archive("$tar_file");
    } else {
      echo '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<HTML>

<HEAD>
  <META HTTP-EQUIV="content-type" CONTENT="text/html;CHARSET=iso-8859-1">
  <META NAME="author" CONTENT="Alecos [Alessandro Marinuzzi]">
  <META NAME="generator" CONTENT="Alecos [Alessandro Marinuzzi]">
  <META NAME="robots" CONTENT="noindex, nofollow">
  <TITLE>[A71-BackUp: Restricted Area]</TITLE>
  <STYLE TYPE="text/css">
  H3, H4, H5, H6 {
    font-family: Arial, Verdana, Helvetica, sans-serif;
    font-size: 16px;
    color: #FFFFFF;
    background-color: inherit;
    font-style: normal;
    line-height: normal;
    font-weight: bold;
    font-variant: normal;
  }
  H2 {
    font-family: Arial, Verdana, Helvetica, sans-serif;
    font-size: 20px;
    color: #FFFFFF;
    background-color: inherit;
    font-style: normal;
    line-height: normal;
    font-weight: bold;
    font-variant: normal;
  }
  H1 {
    font-family: Arial, Verdana, Helvetica, sans-serif;
    font-size: 24px;
    color: #FFFFFF;
    background-color: inherit;
    font-style: normal;
    line-height: normal;
    font-weight: bold;
    font-variant: normal;
  }
  A:LINK {
    color: #008080;
    background-color: inherit;
    text-decoration: none;
  }
  A:VISITED {
    color: #008080;
    background-color: inherit;
    text-decoration: none;
  }
  A:HOVER {
    color: #0000CD;
    background-color: inherit;
    text-decoration: none;
  }
  BODY {
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    margin-bottom: 0px;
    background: #FFFFFF;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 12px;
    color: #FFFFFF;
    font-style: normal;
    line-height: normal;
    font-weight: normal;
    font-variant: normal;
  }
  TD {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 12px;
    color: #FFFFFF;
    background-color: inherit;
    font-style: normal;
    line-height: normal;
    font-weight: normal;
    font-variant: normal;
  }
  </STYLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF" LINK="#008080" ALINK="#C0D9D9" VLINK="#808080" TEXT="#FFFFFF" MARGINWIDTH="0" MARGINHEIGHT="0" TOPMARGIN="0" LEFTMARGIN="0">

<TABLE WIDTH="97%" ALIGN="CENTER" CELLPADDING="15" CELLSPACING="0" BORDER="0">
  <TR>
    <TD>&nbsp;</TD>
  </TR>
  <TR>
    <TD>  
      <TABLE BGCOLOR="#000080" ALIGN="CENTER" CELLPADDING="5" CELLSPACING="0" BORDER="1">
        <TR>
          <TD>
            <TABLE ALIGN="CENTER" CELLPADDING="3" CELLSPACING="0" BORDER="0">
              <TR>
                <TD COLSPAN="2" ALIGN="CENTER"><B>A71-BackUp 1.8 By Alecos</B></TD>
              </TR>
              <TR>
                <TD COLSPAN="2"><HR SIZE="2" WIDTH="100%" ALIGN="CENTER"></TD>
              </TR>
              <TR>
                <TD COLSPAN="2"><FORM NAME="post" METHOD="POST" ACTION="' . $_SERVER["PHP_SELF"] . '"></TD>
              </TR>
              <TR>
                <TD ALIGN="RIGHT"><B>Password:&nbsp;&nbsp;</B></TD><TD ALIGN="LEFT"><INPUT TYPE="password" NAME="password" SIZE="15" VALUE="' . $_POST["password"] . '"></TD>
              </TR>
              <TR>
                <TD ALIGN="RIGHT"><INPUT TYPE="radio" CHECKED NAME="backup" VALUE="on"><B>Full BackUp</B></TD><TD ALIGN="LEFT"><INPUT TYPE="radio" NAME="backup" VALUE="off"><B>Partial BackUp</B></TD>
              </TR>
              <TR>
                <TD COLSPAN="2" ALIGN="CENTER"><INPUT TYPE="checkbox" CHECKED NAME="backupdb" VALUE="on"><B>MySQL Backup</B></TD>
              </TR>
              <TR>
                <TD COLSPAN="2">
                  <TABLE BGCOLOR="#FFFFFF" WIDTH="100%" ALIGN="CENTER" BORDER="1">
                    <TR>
                      <TD ALIGN="CENTER">&nbsp;<FONT COLOR="#FF0000">Entered Wrong Password!</FONT>&nbsp;</TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
              <TR>
                <TD COLSPAN="2" ALIGN="CENTER"><INPUT TYPE="submit" NAME="submit" VALUE=" BackUp "></FORM></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

</BODY>

</HTML>';
    }
  }
?>