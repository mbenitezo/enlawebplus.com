function creaAjax(){
var objetoAjax=false;
try {
/*Para navegadores distintos a internet explorer*/
objetoAjax = new ActiveXObject("Msxml2.XMLHTTP");
   } catch (e) {
    try {
     /*Para explorer*/
     objetoAjax = new ActiveXObject("Microsoft.XMLHTTP");
     } 
     catch (E) {
     objetoAjax = false;
   }
  }

  if (!objetoAjax && typeof XMLHttpRequest!='undefined') {
   objetoAjax = new XMLHttpRequest();
  }
  return objetoAjax;
}

 function FAjax (url,capa,valores,metodo)
{
   var ajax=creaAjax();
   var capaContenedora = document.getElementById(capa);

/*Creamos y ejecutamos la instancia si el metodo elegido es POST*/
 if(metodo.toUpperCase()=='POST'){
//        document.write="Enviando";
    ajax.open ('POST', url, true);
    ajax.onreadystatechange = function() {
			 if (ajax.readyState==1) {
					 //capaContenedora.innerHTML="Cargando.......";
					 document.getElementById('capaContenedora').innerHTML = '<center><img src="load1.gif" /></center>';
			 }
			 else if (ajax.readyState==4){
				if(ajax.status==200)
				{
					 document.getElementById(capa).innerHTML=ajax.responseText; 
				}
				else if(ajax.status==404)
					 {
	
						 capaContenedora.innerHTML = "La direccion existe";
					 }
				 else
					 {
						 capaContenedora.innerHTML = "Error: ".ajax.status;
					 }
			}
    }
    ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    ajax.send(valores);
    return;
}

}
