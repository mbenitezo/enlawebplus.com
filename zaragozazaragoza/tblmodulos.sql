/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.0.51a : Database - directorioalmacen
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `tblmodulos` */

DROP TABLE IF EXISTS `tblmodulos`;

CREATE TABLE `tblmodulos` (
  `Id` int(2) NOT NULL,
  `Padre_id` int(2) NOT NULL,
  `Nombre` varchar(55) character set latin1 collate latin1_general_ci NOT NULL,
  `Fin` varchar(50) character set latin1 collate latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `tblmodulos` */

insert  into `tblmodulos`(`Id`,`Padre_id`,`Nombre`,`Fin`) values (1,0,'Configuración','0'),(20,0,'Clientes','0'),(21,0,'Productos','0'),(4,0,'Almacenes','0'),(14,4,'Listado Almacenes','listado_almacenes.php?optCat=4'),(6,0,'Pedidos','0'),(7,0,'Reportes','0'),(8,1,'Sistema','sistema.php'),(9,1,'Permisos','permisos.php?optCat=6'),(10,20,'Buscar clientes','buscar_clientes.php'),(11,20,'Listado clientes',''),(12,21,'Importar Productos','importar_productos.php'),(13,21,'Listado Productos','listado_productos.php'),(15,6,'Pedidos generales','ver_pedidos_todos.php?optCat=7&vista=todos'),(16,6,'Pedidos por Clientes','ver_pedidos_clientes.php?optCat=7&vista=clientes'),(17,7,'Reporte por Vendedor','reporte_vendedor.php'),(18,7,'Reporte por Cliente','reporte_cliente.php'),(5,0,'Vendedores','0'),(19,5,'Ver Vendedores','vendedores_listado.php'),(2,0,'Mercancía','0'),(3,0,'Clientes','0'),(22,2,'Productos','listado_productos.php?optCat=1'),(23,3,'Lista de Clientes','listado_clientes.php?optCat=8'),(24,4,'Ver ruteros por vendedor','ruteros_vende.php'),(25,6,'Pedidos por fecha','ver_pedidos_fechas.php?optCat=7&vista=fechas'),(27,26,'Ver devoluciones','devoluciones.php'),(26,0,'Devolucion','0'),(28,0,'Mensajería','0'),(29,28,'Mensajería interna','mensajeria_interna.php'),(30,1,'Clave de recuperación','clave_recuperacion.php'),(31,2,'Categorías de Productos','listado_categorias.php?optCat=2'),(32,2,'Proveedores','listado_proveedores.php?optCat=3'),(33,4,'Categorias Almacenes','listado_categorias_alm.php?optCat=5'),(34,6,'Pedidos por rango de totales','ver_pedidos_totales.php?optCat=7&vista=totales');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
