<?php session_start();
$cncliente = $_SESSION["cccliente"];
include("../connection/conn.php");
include("../nucleum/funciones.php");
?>
<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="es"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Pedido confirmado satisfactoriamente.</title>

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/foundation.css">

  <script src="js/vendor/custom.modernizr.js"></script>
</head>
<body>

<?php include("menu.php"); ?> 
 
  <!-- Main Page Content and Sidebar -->
 
  <div class="row">
 
    <!-- Contact Details -->
    <div class="large-9 columns">
      <h3>Pedido realizado !!</h3>
      <p>
      
<font face="Arial, Helvetica, sans-serif"><p align="center">El pedido se ha procesado satisfactoriamente, en breve llegar&aacute; un mensaje a su correo indicando un resumen de su solicitud. <br><br> Tenga en cuenta que ser&aacute; contactado por el vendedor de los productos solicitados para confirmar su pedido.<br><br> Haga clic <a href="index.php"> aqu&iacute; </a> para ir al inicio.   </p></font>  
	
      
      </p>
 
      
    </div>
 
    <!-- End Contact Details -->
 
<?php include("sidebar.php"); ?> 

  </div>
 
  <!-- End Main Content and Sidebar -->
 
<?php  include("footer.php"); ?> 
 
 
  <!-- Map Modal -->
 
  <div class="reveal-modal" id="mapModal">
    <h4>Where We Are</h4>
    <p><img src="http://placehold.it/800x600" /></p>
 
    <!-- Any anchor with this class will close the modal. This also inherits certain styles, which can be overriden. -->
    <a href="#" class="close-reveal-modal">&times;</a>
  </div>

  <script>
  document.write('<script src=js/vendor/' +
  ('__proto__' in {} ? 'zepto' : 'jquery') +
  '.js><\/script>')
  </script>
  <script src="js/foundation.min.js"></script>
  <script>
    $(document).foundation();
  </script>
</body>
</html>
