<?php session_start();
$cncliente = $_SESSION["cccliente"];
include("../connection/conn.php");
include("../nucleum/funciones.php");
    $pattern = "1234567890abcdefghijklmnopqrstuvwxyz";
    for($i=0;$i<8;$i++) {
      $key .= $pattern{rand(0,35)};
    }
$mac = new GetMacAddr(PHP_OS);
//echo $mac->mac_addr;
//echo "<br>". getRealIP();
?>
<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="es"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Welcome to Foundation</title>

  <script src="../system/application/calendario/src/js/jscal2.js"></script>
  <script src="../system/application/calendario/src/js/lang/es.js"></script>

<link rel="stylesheet" type="text/css" href="../system/application/calendario/src/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="../system/application/calendario/src/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="../system/application/calendario/src/css/win2k/win2k.css" />

<link href="../system/application/css/consolidated_common.css" rel="stylesheet" type="text/css">

<link href="../system/application/css/OtrosEstilos.css" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/foundation.css">

  <script src="js/vendor/custom.modernizr.js"></script>
<script src="../system/application/javascript/livevalidation_standalone.js" type="text/javascript"></script>
</head>
<body>

<?php include("menu.php"); ?> 
 
  <!-- Main Page Content and Sidebar -->
 
  <div class="row">
 
    <!-- Contact Details -->
    <div class="large-9 columns">
 <form id="form1" name="form1" method="post" action="guardar_nuevo_cliente.php">
      <h3>Subscribete en nuestro sitio !!</h3>
      <p>Antes de registrarse en nuestro sitio, es importante que leas y aceptes la <a href="politica_privacidad.php">pol&iacute;tica de privacidad  </a>de la informaci&oacute;n en nuestro sitio.<br><br>	
<em>Se le solicitará que inicie sesión cuando haya creado y confirmado su cuenta.</em></p>
 
      <div class="section-container auto" data-section> 
        <section class="section">
          <h5 class="title"><a href="#panel1">Nuevo Usuario</a></h5>
          <div class="content" data-slug="panel1">
            
              <input type="hidden" name="macaddress" value="<?= $mac->mac_addr; ?>">
			  <input type="hidden" name="addressip" value="<?= getRealIP(); ?>">
              
              
              <div class="row collapse">
<label style="background-color:#000"><div align="center"><font color="#FFFFFF">#  C&eacute;dula o NIT:</font></div></label>
                <div class="large-12 columns">
                <input name="ccnit" type="text" id="ccnit" placeholder="Ingresa aqu&iacute; tu c&eacute;dula">
<script type="text/javascript">var f1 = new LiveValidation('ccnit');f1.add(Validate.Presence);</script>
                </div>
              </div>
 
              <br>
 
              <div class="row collapse">
                  <label style="background-color:#000"><div align="center"><font color="#FFFFFF"> Nombres: </font></div></label>
                <div class="large-12 columns">
                  <input name="nmbcliente" type="text" id="nmbcliente" placeholder="Ingresa aqu&iacute; tu nombre">
<script type="text/javascript">var f1 = new LiveValidation('nmbcliente');f1.add(Validate.Presence);</script>                </div>
              </div>

              <br>
               
              <div class="row collapse">
<label style="background-color:#000"><div align="center"><font color="#FFFFFF"> Apellidos: </font></div></label>
                <div class="large-12 columns">
                  <input name="apldocliente" type="text" id="apldocliente" placeholder="Ingresa aqu&iacute; tus apellidos">
<script type="text/javascript">var f1 = new LiveValidation('apldocliente');f1.add(Validate.Presence);</script>                </div>
              </div>

              <br>              
              

              <div class="row collapse">
<label style="background-color:#000"><div align="center"><font color="#FFFFFF"> Tu cumpleanos (aaaa-mm-dd): </font><img src="../system/application/calendario/img.gif" name="f_btn1" width="24" height="14" border="0" id="f_btn1" style="cursor: pointer; border: 1px solid blue;" title="Seleccionar fecha" onMouseOver="this.style.background='blue';" onMouseOut="this.style.background=''" /></div></label>
                <div class="large-12 columns">

                  <input name="date" type="text" id="f_date1" placeholder="Cu&aacute;ndo cumples a&ntilde;os ?">

<script type="text/javascript">var f1 = new LiveValidation('f_date1');f1.add(Validate.Presence);</script>        
          </div>
              </div>

              <br>


              <div class="row collapse">
<label style="background-color:#000"><div align="center"><font color="#FFFFFF"> Celular:</font> </div></label>
                <div class="large-12 columns">
                  <input type="text" name="nmcelular" id="nmcelular" placeholder="Ingresa aqu&iacute; tu n&uacute;mero de celular">
<script type="text/javascript">var f1 = new LiveValidation('nmcelular');f1.add(Validate.Presence);</script>                </div>
              </div>

              <br>


              <div class="row collapse">
<label style="background-color:#000"><div align="center"><font color="#FFFFFF"> Tel&eacute;fono fijo (Opcional):</font> </div></label>
                <div class="large-12 columns">
                  <input type="text" name="nmtelefono" id="nmtelefono" placeholder="Tel&eacute;fono fijo (Opcional)">
<script type="text/javascript">var f1 = new LiveValidation('nmcelular');f1.add(Validate.Presence);</script>                </div>
              </div>

              <br>

             <div class="row collapse">
<label style="background-color:#000"><div align="center"><font color="#FFFFFF"> Direccion de residencia (Especifique): </font></div></label>
                <div class="large-12 columns">
                  <input name="dircliente" type="text" id="dircliente" placeholder="D&oacute;nde vives ?">
<script type="text/javascript">var f1 = new LiveValidation('dircliente');f1.add(Validate.Presence);</script>                </div>
            </div>

              <br>


             <div class="row collapse">
<label style="background-color:#000"><div align="center"><font color="#FFFFFF">E-mail 1: </font></div></label>
                <div class="large-12 columns">
                  <input type="text" name="email1cliente" id="email1cliente" placeholder="Correo electr&oacute;nico principal">
<script type="text/javascript">

		            var f20 = new LiveValidation('email1cliente');

		            f20.add(Validate.Email );
					f20.add(Validate.Presence);

		          </script>                </div>
            </div>

              <br>

             <div class="row collapse">
<label style="background-color:#000"><div align="center"><font color="#FFFFFF"> E-mail 2:</font> </div></label>
                <div class="large-12 columns">
                  <input type="text" name="email2cliente" id="email2cliente" placeholder="E-mail alternativo (Opcional)">
<script type="text/javascript">
     	            var f20 = new LiveValidation('email2cliente');
		            f20.add(Validate.Email );
		          </script>                  
</div>
            </div>

              <br>

             <div class="row collapse">
<label style="background-color:#000"><div align="center"><font color="#FFFFFF"> C&oacute;digo de verificaci&oacute;n:</font><br> <font size="+5"><?= $key;?></font></div>
<div class="large-12 columns">
<input  name="tmptxt" id="tmptxt" type="text" placeholder="Ingresa aqu&iacute; el c&oacute;digo de verficaci&oacute;n">
      <script type="text/javascript">

		            var f14 = new LiveValidation('tmptxt');

					f14.add(Validate.Presence);

		            f14.add(Validate.Inclusion, { within: [ '<?= $key;?>' ] } );

		          </script>                </div><br><br>
        </label>    </div>

              <br>
      <p>Antes de registrate en nuestro sitio, es importante que leas y aceptes la <a href="politica_privacidad.php">pol&iacute;tica de privacidad  </a>de la informaci&oacute;n en nuestro sitio.<br><br> <strong>* Confirmo que he leído, comprendido y aceptado en totalidad la política de privacidad</strong>	
        <input type="checkbox" name="terminos" id="terminos"  class="storage">
<script type="text/javascript">var f18 = new LiveValidation('terminos');f18.add( Validate.Acceptance );</script>
        <br><br>	
<em>Se le solicitará que inicie sesión cuando haya creado y confirmado su cuenta.</em></p>
           </div>
        </section>
              </div>   
              <button type="submit" class="radius button">Registrarse</button>&nbsp;&nbsp;<button type="button" class="radius button" onClick="javascript:location.href='index.php';"  >Volver al inicio</button>
            
          </form>
    </div>
 
    <!-- End Contact Details -->
 
<?php include("sidebar.php"); ?> 

  </div>
 
  <!-- End Main Content and Sidebar -->
 
<?php  include("footer.php"); ?> 
 
 
  <!-- Map Modal -->
 
  <div class="reveal-modal" id="mapModal">
    <h4>Where We Are</h4>
    <p><img src="http://placehold.it/800x600" /></p>
 
    <!-- Any anchor with this class will close the modal. This also inherits certain styles, which can be overriden. -->
    <a href="#" class="close-reveal-modal">&times;</a>
  </div>

  <script>
  document.write('<script src=js/vendor/' +
  ('__proto__' in {} ? 'zepto' : 'jquery') +
  '.js><\/script>')
  </script>
  <script src="js/foundation.min.js"></script>
  <script>
    $(document).foundation();
  </script>
<script type="text/javascript">//<![CDATA[

      Calendar.setup({

        inputField : "f_date1",

        trigger    : "f_btn1",

        onSelect   : function() { this.hide() },

        showTime   : 12,

        dateFormat : "%Y-%m-%d"

      });

    //]]></script>
    
</body>
</html>
