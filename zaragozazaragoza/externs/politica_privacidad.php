<?php session_start();
$cncliente = $_SESSION["cccliente"];
include("../connection/conn.php");
include("../nucleum/funciones.php");
?>
<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="es"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Welcome to Foundation</title>
<link href="../system/application/css/consolidated_common.css" rel="stylesheet" type="text/css">

<link href="../system/application/css/OtrosEstilos.css" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/foundation.css">

  <script src="js/vendor/custom.modernizr.js"></script>
</head>
<body>

<?php include("menu.php"); ?> 
 
  <!-- Main Page Content and Sidebar -->
 
  <div class="row">
 






<p align="center"><strong>AVISO DE PRIVACIDAD PARA ESTE SITIO WEB COMERCIAL DE  DIRECTORIOALMACENES EN VIGENCIA A PARTIR DEL 15 DE NOVIEMBRE DE 2013</strong> </p>
<p align="center">  En  general, usted puede visitar este Sitio Web sin suministrarnos ninguna  información personal. Si opta por suministrarnos información personal, la  utilizaremos de acuerdo con nuestros principios de protecci&oacute;n de datos y el  aviso a las personas.</p>
<p align="center"> <br />
  <strong>PRINCIPIOS  GLOBALES DE PROTECCI&Oacute;N DE DATOS DE DIRECTORIOALMACENES</strong> </p>
<p align="center">  Actualmente reconocemos la importancia de mantener la privacidad  de los datos personales y confidenciales de sus clientes y asociados. El objetvo de nuestro sitio en gran medida nos obliga  a recopilar y manejar ese tipo de informaci&oacute;n, y tenemos la responsabilidad de  protegerla mientras est&eacute; en nuestro poder.  <br />
  En nuestro sitio se ha creado un conjunto de principios globales  de protecci&oacute;n de datos, que la gu&iacute;a en sus esfuerzos relacionados con la  preservaci&oacute;n de la privacidad, manejo y protecci&oacute;n de datos personales y  sensibles. <br />
  Respetamos  su privacidad de la siguiente manera: </p>
<ul type="disc">
  <li>Publicando avisos de privacidad que le explican c&oacute;mo y por qu&eacute; manejamos informaci&oacute;n personal. </li>
  <li>Respetando sus opciones acerca de c&oacute;mo recopilamos, utilizamos y compartimos su informaci&oacute;n, donde sea apropiado. </li>
  <li>Recopilando, utilizando y reteniendo s&oacute;lo los datos personales que sean pertinentes y &uacute;tiles para nuestra interacci&oacute;n de negocios. </li>
  <li>Aplicando esfuerzos razonables para mantener la informaci&oacute;n personal exacta y actualizada. </li>
  <li>Utilizando m&eacute;todos de seguridad para proteger la informaci&oacute;n personal. </li>
  <li>Limitando el acceso a la informaci&oacute;n personal y su divulgaci&oacute;n. </li>
  <li>Reteniendo só&oacute;o la informaci&oacute;n personal que sea necesaria para cumplir nuestras obligaciones       legales y de negocios. </li>
  <li>Ofreci&eacute;ndole la posibilidad de visualizar y actualizar su informaci&oacute;n personal, donde sea apropiado. </li>
  <li>Brind&aacute;ndole la oportunidad de hacer preguntas y presentar quejas relacionadas con la privacidad. </li>
</ul>
<p>Para  obtener m&aacute;s informaci&oacute;n acerca de nuestros procedimientos de privacidad para las personas, vea el siguiente aviso: </p>
<p><strong>AVISO DE  PROTECCI&Oacute;N DE DATOS PARA LAS PERSONAS EN ESTE SITIO WEB DIRECTORIOALMACENES </strong><strong> </strong></p>






<p>Nosotros comunicamos su pol&iacute;tica de protecci&oacute;n de datos; la cual incluye la  definici&oacute;n de la administraci&oacute;n de los datos personales que los usuarios que entregan libre y voluntariamente.&nbsp; <br />
  Hemos hecho un esfuerzo invaluable en implementar est&aacute;ndares de seguridad para la protecci&oacute;n de los  datos de car&aacute;cter personal con el prop&oacute;sito de evitar su alteraci&oacute;n, p&eacute;rdida,  mal uso, deterioro, suministro o acceso no autorizado por parte de terceros que  lo puedan utilizar para finalidades distintas de las que han sido autorizadas  por el usuario. <br />
  Por lo anterior se puede apreciar que nosotros procesamos sus datos personales para:&nbsp;&nbsp;&nbsp; <br />
  &nbsp;&nbsp; </p>
<ul type="disc">
  <li>Establecer&nbsp; una relaci&oacute;n contractual con usted. </li>
  <li>Para que usted disponga de las opciones que no est&aacute;n disponibles cuando no se est&aacute; registrado. </li>
  <li>Para la confirmaci&oacute;n de pedidos de compras. </li>
  <li>Para que los vendedores de almacenes le puedan suministrar informaci&oacute;n de descuentos a su correo personal.</li>
</ul>
<p>  El  Usuario garantiza que los Datos Personales facilitados en nuestro sitio son veraces y se hace responsable de comunicarnos cualquier cambio o  actualizaci&oacute;n de los mismos.<br />
  Recuerde que puede realizar procesos de cambios de informaci&oacute;n personal en la opci&oacute;n Editar Perfil dentro de nuestras opciones disponibles.<br />
DirectorioAlmacene se reserva el derecho a modificar la presente pol&iacute;tica para  ajustarla a los cambios legislativos o a los requerimientos del medio.&nbsp;  Cuando se presenten estos cambios, ser&aacute;n divulgados por nosotros a trav&eacute;s de  este medio con suficiente anticipaci&oacute;n a la fecha de su implementaci&oacute;n. </p>
<p align="center"><strong>AVISO DE LOS PROCEDIMIENTOS DE PRIVACIDAD EN ESTE  SITIO WEB DIRECTORIOALMACENES</strong> <br />
  INFORMACI&Oacute;N  RECOLECTADA AUTOM&Aacute;TICAMENTE<br />
  <br />
  <em>Información no identificable </em>– Como muchos otros sitios de Internet,  este Sitio Web recolecta autom&aacute;ticamente algunos datos no identificables respecto de los usuarios de la pagina Web, tales como la dirección del  protocolo de Internet (IP) de su computadora, la fecha y hora de acceso al Sitio Web, el sistema operativo que usted utiliza. Esta información no identificable se utiliza para fines de administraci&oacute;n del Sitio Web y del  sistema, y para mejorar el Sitio Web. Su informaci&oacute;n no identificable podr&iacute;a  ser divulgada a terceros y archivada de manera permanente para uso futuro. <br />
  <em>Cookies</em> – El Sitio Web utiliza  &quot;cookies&quot;, una tecnolog&iacute;a que instala informaci&oacute;n en la computadora  del usuario del Sitio Web para permitir que el Sitio Web reconozca las futuras  visitas desde esa computadora. Las cookies facilitan el uso del Sitio Web. Por  ejemplo, la informaci&oacute;n proporcionada mediante las cookies se emplea para  reconocerlo como un usuario que ya ha visitado el Sitio Web, para realizar el  seguimiento de su actividad en el Sitio Web y así responder a sus necesidades, para ofrecerle uso de la p&aacute;gina web con informaci&oacute;n y contenido personalizados,  para llenar de manera autom&aacute;tica y eficaz formularios en l&iacute;nea con informaci&oacute;n personal y para facilitar su experiencia con el Sitio Web en otros aspectos. <br />
  Los  anuncios publicitarios que se muestran en el Sitio Web tambi&eacute;n pueden contener cookies u otras tecnolog&iacute;as. Dichos anuncios pueden ser proporcionados por empresas publicitarias de terceros y DirectorioAlmacenes no tiene ning&uacute;n control ni responsabilidad de las cookies ni otras tecnolog&iacute;as usadas en los anuncios  publicitarios ni del uso y la divulgaci&oacute;n de la informaci&oacute;n recopilada mediante  cookies de anuncios publicitarios. <br />
  Usted  puede optar por rechazar las cookies si su navegador lo permite, aunque tal acci&oacute;n podr&iacute;a afectar su uso del Sitio Web y su capacidad de acceso o uso de ciertas caracter&iacute;sticas del Sitio Web. <br /><br />
  <strong><u>INFORMACI&Oacute;N  UTILIZADA</u></strong> <br /><br />
  <em>Informaci&oacute;n de seguimiento</em> -  DirectorioAlmacenes puede utilizar informaci&oacute;n que no es de car&aacute;cter personal para crear informes globales de informaci&oacute;n de seguimiento, relativos a datos demogr&aacute;ficos  de los usuarios del Sitio Web y el uso del Sitio Web, y luego suministrar esos informes a terceros. Ninguno de dichos datos de seguimiento contenidos en los  informes puede ser vinculado a las identidades u otra informaci&oacute;n personal de los  usuarios individuales. <br /><br />
  <strong><u>OTROS  ASUNTOS</u></strong> <br /><br />
  <em>Seguridad</em> – DirectorioAlmacenes se esfuerza por mantener sus servidores, aplicaciones y bases de datos seguros y libres de todo acceso y uso no autorizados, utilizando tecnolog&iacute;a destinadas a proteger la informaci&oacute;n que mantiene. Lamentablemente, nadie puede garantizar un 100% de seguridad. Si le preocupan asuntos de seguridad relacionados con ciertos datos personales, por favor no los transmita por Internet. <br />
  <em>Otros  sitios web</em> – Este  Sitio Web puede contener enlaces a otros sitios web o recursos de Internet. Al  hacer clic en uno de esos enlaces, usted es dirigido a otro sitio Web o recurso  de Internet que puede recolectar informaci&oacute;n acerca de usted, voluntariamente o mediante cookies u otras tecnolog&iacute;as. DirectorioAlmacenes no tiene responsabilidad ni control de los otros sitios web o recursos de Internet mencionados, ni de la recolecci&oacute;n, uso y divulgaci&oacute;n que hacen de sus datos personales. Usted debe  examinar las pol&iacute;ticas de confidencialidad de los otros sitios web y recursos  de Internet mencionados para entender el modo en que recopilan y usan la informaci&oacute;n. </p>
<em>Modificaciones del Aviso de privacidad</em> – De tanto en tanto y por cualquier motivo, DirectorioAlmacenes puede modificar, complementar o enmendar este Aviso en lo relacionado  con su futuro uso del Sitio Web, publicando la fecha del Aviso modificado en el Sitio Web. La presente notificaci&oacute;n fue actualizada por &uacute;ltima vez en julio de  2013.














    </div>
 
    <!-- End Contact Details -->
 

  </div>
 
  <!-- End Main Content and Sidebar -->
 
<?php  include("footer.php"); ?> 
 
 
  <!-- Map Modal -->
 
  <div class="reveal-modal" id="mapModal">
    <h4>Where We Are</h4>
    <p><img src="http://placehold.it/800x600" /></p>
 
    <!-- Any anchor with this class will close the modal. This also inherits certain styles, which can be overriden. -->
    <a href="#" class="close-reveal-modal">&times;</a>
  </div>

  <script>
  document.write('<script src=js/vendor/' +
  ('__proto__' in {} ? 'zepto' : 'jquery') +
  '.js><\/script>')
  </script>
  <script src="js/foundation.min.js"></script>
  <script>
    $(document).foundation();
  </script>
</body>
</html>
