<?php
include("../../nucleum/funciones.php");
    $pattern = "1234567890abcdefghijklmnopqrstuvwxyz";
    for($i=0;$i<8;$i++) {
      $key .= $pattern{rand(0,35)};
    }
$mac = new GetMacAddr(PHP_OS);
//echo $mac->mac_addr;
//echo "<br>". getRealIP();
?>
<html>
<head>
<title>Inscripci&oacute;n de clientes al sistema</title>

<script src="../../system/application/calendario/src/js/jscal2.js"></script>
<script src="../../system/application/calendario/src/js/lang/es.js"></script>
<link rel="stylesheet" type="text/css" href="../../system/application/calendario/src/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="../../system/application/calendario/src/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="../../system/application/calendario/src/css/win2k/win2k.css" />
<link href="../../system/application/css/styles.css" rel="stylesheet" type="text/css">
<link href="../../system/application/css/consolidated_common.css" rel="stylesheet" type="text/css">
<link href="file:///C|/xampp/htdocs/DirectorioAlmacenActualizado3/system/application/css/OtrosEstilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
b {
	font-size: 20px;
}

</style>
  <!--[if IE]>
    <style type="text/css">
	  
	  #main li {
        width:auto;
      }
      
	  /* fix for fieldset background spill bug in all flavours of IE */
	  fieldset {
        position: relative;
        margin: 2em 0 1em 0;
      }
      legend {
        position: absolute;
        top: -0.5em;
        left: 0.2em;
      }
	  
    </style>
  <![endif]-->
  
  <!--[if IE 6]>
    <style type="text/css">
      #doc {
		 width:58em;
	  }
		
		#main .supportBox {
			margin-left:40px;
		}
	</style>
  <![endif]-->
<script src="../../system/application/javascript/livevalidation_standalone.js" type="text/javascript"></script> 

</head>
<body>
<form id="form1" name="form1" method="post" action="guardar_nuevo_cliente.php" >
<input type="hidden" name="macaddress" value="<?= $mac->mac_addr; ?>">
<input type="hidden" name="addressip" value="<?= getRealIP(); ?>">
  <table width="660" border="0" align="center">
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="4" align="center"><strong>Registrate para utilizar los servicios del sitio</strong></td>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
      <td height="44" colspan="2">#  C&eacute;dula o NIT:&nbsp;&nbsp;&nbsp;
        <input name="ccnit" type="text" id="ccnit" size="15" />
        <script type="text/javascript">var f1 = new LiveValidation('ccnit');f1.add(Validate.Presence);</script>
        </td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td width="71" height="44">Nombres:</td>
      <td width="256"><input name="nmbcliente" type="text" id="nmbcliente" size="30" /><script type="text/javascript">var f1 = new LiveValidation('nmbcliente');f1.add(Validate.Presence);</script></td>
      <td colspan="2">Apellidos:
      <input name="apldocliente" type="text" id="apldocliente" size="30" /><script type="text/javascript">var f1 = new LiveValidation('apldocliente');f1.add(Validate.Presence);</script></td>
    </tr>
    <tr>
      <td height="38" colspan="4">Fecha de cumplea&ntilde;os:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input name="date" type="text" class="Estilo33" id="f_date1" size="11" /><script type="text/javascript">var f1 = new LiveValidation('f_date1');f1.add(Validate.Presence);</script>        
      &nbsp;<img src="../../system/application/calendario/img.gif" name="f_btn1" width="24" height="14" border="0" id="f_btn1" style="cursor: pointer; border: 1px solid blue;" title="Seleccionar fecha" onMouseOver="this.style.background='blue';" onMouseOut="this.style.background=''" /> (aaaa-mm-dd)</td>
    </tr>
    <tr>
      <td height="38"># Celular:</td>
      <td><input type="text" name="nmcelular" id="nmcelular" /><script type="text/javascript">var f1 = new LiveValidation('nmcelular');f1.add(Validate.Presence);</script></td>
      <td width="109"># Telefono:</td>
      <td width="206"><input type="text" name="nmtelefono" id="nmtelefono" /><script type="text/javascript">var f1 = new LiveValidation('nmtelefono');f1.add(Validate.Presence);</script></td>
    </tr>
    <tr>
      <td height="37" colspan="4">Direccion de residencia (Especifique):&nbsp;&nbsp;
      <input name="dircliente" type="text" id="dircliente" size="50" /><script type="text/javascript">var f1 = new LiveValidation('dircliente');f1.add(Validate.Presence);</script></td>
    </tr>
    <tr>
      <td height="46">E-mail 1:</td>
      <td><input type="text" name="email1cliente" id="email1cliente" /><script type="text/javascript">
		            var f20 = new LiveValidation('email1cliente');
		            f20.add(Validate.Email );
					f20.add(Validate.Presence);
		          </script></td>
      <td>E-mail 2:</td>
      <td><input type="text" name="email2cliente" id="email2cliente" /><script type="text/javascript">
		            var f20 = new LiveValidation('email2cliente');
		            f20.add(Validate.Email );
					f20.add(Validate.Presence);
		          </script></td>
    </tr>
    <tr>
      <td height="37" colspan="2" align="right">C&oacute;digo de verificaci&oacute;n?:&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" valign="middle" bgcolor="#000000"><b><?= $key;?></b></td>
      <td><input name="tmptxt" id="tmptxt" type="text" size="20">
      <script type="text/javascript">
		            var f14 = new LiveValidation('tmptxt');
					f14.add(Validate.Presence);
		            f14.add(Validate.Inclusion, { within: [ '<?= $key;?>' ] } );
		          </script>
 </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td align="center"><input type="submit" name="button" id="button" value="Registrar" ></td>
    </tr>
  </table>
</form>

<script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "f_date1",
        trigger    : "f_btn1",
        onSelect   : function() { this.hide() },
        showTime   : 12,
        dateFormat : "%Y-%m-%d"
      });
    //]]></script>   
</body>

</html>