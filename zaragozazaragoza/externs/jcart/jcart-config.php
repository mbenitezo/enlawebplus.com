<?php

// JCART v1.1
// http://conceptlogic.com/jcart/

///////////////////////////////////////////////////////////////////////
// REQUIRED SETTINGS

// THE HTML NAME ATTRIBUTES USED IN YOUR ADD-TO-CART FORM
$jcart['item_id']		= 'my-item-id';			// ITEM ID
$jcart['item_name']		= 'my-item-name';		// ITEM NAME
$jcart['item_price']	= 'my-item-price';		// ITEM PRICE
$jcart['item_qty']		= 'my-item-qty';		// ITEM QTY
$jcart['item_add']		= 'my-add-button';		// ADD-TO-CART BUTTON

// PATH TO THE DIRECTORY CONTAINING JCART FILES
$jcart['path'] = 'jcart/';

// THE PATH AND FILENAME WHERE SHOPPING CART CONTENTS SHOULD BE POSTED WHEN A VISITOR CLICKS THE CHECKOUT BUTTON
// USED AS THE ACTION ATTRIBUTE FOR THE SHOPPING CART FORM
$jcart['form_action']	= 'checkout.php';

// YOUR PAYPAL SECURE MERCHANT ACCOUNT ID
$jcart['paypal_id']		= '';


///////////////////////////////////////////////////////////////////////
// OPTIONAL SETTINGS

// OVERRIDE DEFAULT CART TEXT
$jcart['text']['cart_title']				= 'Canasta';		// Shopping Cart
$jcart['text']['single_item']				= 'Elemento agregado';		// Item
$jcart['text']['multiple_items']			= 'Elementos agregados';		// Items
$jcart['text']['currency_symbol']			= '';		// $
$jcart['text']['subtotal']					= 'Total del pedido';		// Subtotal

$jcart['text']['update_button']				= 'Actualiza';		// update
$jcart['text']['checkout_button']			= 'Hacer compra';		// checkout
$jcart['text']['checkout_paypal_button']	= 'Confirmar Pedido';		// Checkout with PayPal
$jcart['text']['remove_link']				= "";		// remove
$jcart['text']['empty_button']				= 'Vacio';		// empty
$jcart['text']['empty_message']				= 'El pedido no se ha realizado !!!';		// Your cart is empty!
$jcart['text']['item_added_message']		= 'Agregado al carrito !!!';		// Item added!

$jcart['text']['price_error']				= 'Invalido formato del precio';		// Invalid price format!
$jcart['text']['quantity_error']			= '<b>La cantidad debe ser un valor numerico !!!</b>';		// Item quantities must be whole numbers!
$jcart['text']['checkout_error']			='<b>Tu pedido no puede ser procesado !!!</b>';		// Your order could not be processed!

// OVERRIDE THE DEFAULT BUTTONS WITH YOUR IMAGES BY SETTING THE PATH FOR EACH IMAGE
$jcart['button']['checkout']				= '';
$jcart['button']['paypal_checkout']			= '';
$jcart['button']['update']					= '';
$jcart['button']['empty']					= '';

?>
