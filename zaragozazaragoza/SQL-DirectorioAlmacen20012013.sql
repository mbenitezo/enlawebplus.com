/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.0.51a : Database - directorioalmacen
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`directorioalmacen` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `directorioalmacen`;

/*Table structure for table `tblcategorias` */

DROP TABLE IF EXISTS `tblcategorias`;

CREATE TABLE `tblcategorias` (
  `categ_cods` int(255) NOT NULL auto_increment,
  `categ_nom` varchar(255) default NULL,
  PRIMARY KEY  (`categ_cods`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `tblcategorias` */

insert  into `tblcategorias`(`categ_cods`,`categ_nom`) values (1,'Prueba 1'),(3,'Blue Jeans'),(4,'Camisetas'),(5,'Zapatos'),(6,'Medias');

/*Table structure for table `tblimagenes` */

DROP TABLE IF EXISTS `tblimagenes`;

CREATE TABLE `tblimagenes` (
  `ImagenId` int(11) NOT NULL auto_increment,
  `ImagenArchivo` varchar(200) character set latin1 default NULL,
  `ImagenEstado` int(11) default NULL,
  PRIMARY KEY  (`ImagenId`)
) ENGINE=MyISAM AUTO_INCREMENT=135 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Data for the table `tblimagenes` */

/*Table structure for table `tblimagenproductos` */

DROP TABLE IF EXISTS `tblimagenproductos`;

CREATE TABLE `tblimagenproductos` (
  `id_imgs` int(255) NOT NULL auto_increment,
  `id_prod` int(255) default NULL,
  `nom_img` varchar(255) default NULL,
  `nom_img_peq` varchar(255) default NULL,
  `desc_img` varchar(255) default NULL,
  PRIMARY KEY  (`id_imgs`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `tblimagenproductos` */

/*Table structure for table `tblmodulos` */

DROP TABLE IF EXISTS `tblmodulos`;

CREATE TABLE `tblmodulos` (
  `Id` int(2) NOT NULL,
  `Padre_id` int(2) NOT NULL,
  `Nombre` varchar(55) collate latin1_spanish_ci NOT NULL,
  `Fin` varchar(33) collate latin1_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tblmodulos` */

insert  into `tblmodulos`(`Id`,`Padre_id`,`Nombre`,`Fin`) values (1,0,'Configuración','0'),(20,0,'Clientes','0'),(21,0,'Productos','0'),(4,0,'Rutas','0'),(14,4,'Ver Ruteros','vendedores_rutas.php'),(6,0,'Pedidos','0'),(7,0,'Reportes','0'),(8,1,'Sistema','sistema.php'),(9,1,'Permisos','permisos.php'),(10,20,'Buscar clientes','buscar_clientes.php'),(11,20,'Listado clientes','listado_clientes.php'),(12,21,'Importar Productos','importar_productos.php'),(13,21,'Listado Productos','listado_productos.php'),(15,6,'Ver pedidos ','ver_pedidos.php'),(16,6,'Exportar pedidos ','exportar.php'),(17,7,'Reporte por Vendedor','reporte_vendedor.php'),(18,7,'Reporte por Cliente','reporte_cliente.php'),(5,0,'Vendedores','0'),(19,5,'Ver Vendedores','vendedores_listado.php'),(2,0,'Mercancia','0'),(3,0,'Exportar','0'),(22,2,'Productos','listado_productos.php'),(23,3,'Exportar Documentos','exportar.php'),(24,4,'Ver ruteros por vendedor','ruteros_vende.php'),(25,6,'Cierre','cierre.php'),(27,26,'Ver devoluciones','devoluciones.php'),(26,0,'Devolucion','0'),(28,0,'Mensajeria','0'),(29,28,'Mensajería interna','mensajeria_interna.php'),(30,1,'Cambios de clave','modificar_clave.php'),(31,2,'Categorías de Productos','listado_categorias.php'),(32,2,'Proveedores','listado_proveedores.php');

/*Table structure for table `tblpermisos` */

DROP TABLE IF EXISTS `tblpermisos`;

CREATE TABLE `tblpermisos` (
  `Idpermiso` int(2) NOT NULL auto_increment,
  `Modulo` int(2) NOT NULL,
  `Usuario` int(2) NOT NULL,
  `Padre` int(11) NOT NULL,
  PRIMARY KEY  (`Idpermiso`)
) ENGINE=MyISAM AUTO_INCREMENT=405 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tblpermisos` */

insert  into `tblpermisos`(`Idpermiso`,`Modulo`,`Usuario`,`Padre`) values (382,27,1,26),(368,13,1,21),(388,25,1,6),(371,16,1,6),(377,22,1,2),(374,19,1,5),(372,17,1,7),(369,14,1,4),(364,9,1,1),(378,23,1,3),(370,15,1,6),(366,11,1,20),(373,18,1,7),(404,32,1,2),(391,29,1,28),(396,10,1,20),(403,31,1,2),(399,12,1,21),(400,24,8,4);

/*Table structure for table `tblproductos` */

DROP TABLE IF EXISTS `tblproductos`;

CREATE TABLE `tblproductos` (
  `prod_cods` int(255) NOT NULL,
  `provee_cods` int(255) default NULL,
  `categ_cods` int(255) default NULL,
  `prod_nom` varchar(255) default NULL,
  `prod_cant` int(50) default NULL,
  `prod_precio` int(255) default NULL,
  `prod_fech_compr` date default NULL,
  `prod_descr` text,
  `idusuario` int(255) default NULL,
  PRIMARY KEY  (`prod_cods`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `tblproductos` */

insert  into `tblproductos`(`prod_cods`,`provee_cods`,`categ_cods`,`prod_nom`,`prod_cant`,`prod_precio`,`prod_fech_compr`,`prod_descr`,`idusuario`) values (1,1,4,'Chevignon',50,35000,'2013-01-11','La mama de los pollitos.',1),(2,3,5,'Sketcher',15,28000,'2013-01-03','Hay tallas 28 a 35.',1);

/*Table structure for table `tblproveedor` */

DROP TABLE IF EXISTS `tblproveedor`;

CREATE TABLE `tblproveedor` (
  `provee_cods` int(255) NOT NULL auto_increment,
  `provee_nom` varchar(255) default NULL,
  `provee_dir` varchar(255) default NULL,
  `ciudad` varchar(255) default NULL,
  PRIMARY KEY  (`provee_cods`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `tblproveedor` */

insert  into `tblproveedor`(`provee_cods`,`provee_nom`,`provee_dir`,`ciudad`) values (1,'Juana de Arco de Gomez','Calle las Manguitas','Medellin'),(2,'Roberto Gomez Bolaños','Av. Las Brisas','Bucaramanga'),(3,'Diego de los Angeles Rengifo','Barrio Belen No. 34-45 Villaniza','Medellin');

/*Table structure for table `tbltipousuario` */

DROP TABLE IF EXISTS `tbltipousuario`;

CREATE TABLE `tbltipousuario` (
  `IdTipo` int(1) NOT NULL,
  `Tipo` varchar(55) collate latin1_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tbltipousuario` */

insert  into `tbltipousuario`(`IdTipo`,`Tipo`) values (1,'Root'),(2,'Administrador'),(3,'Vendedor'),(4,'Facturacion');

/*Table structure for table `tblusuarios` */

DROP TABLE IF EXISTS `tblusuarios`;

CREATE TABLE `tblusuarios` (
  `Id` int(2) NOT NULL auto_increment,
  `Login` varchar(55) collate latin1_spanish_ci NOT NULL,
  `Clave` varchar(55) collate latin1_spanish_ci NOT NULL,
  `Nombre` varchar(99) collate latin1_spanish_ci NOT NULL,
  `UltimoAcceso` datetime NOT NULL,
  `Tipo` int(2) NOT NULL,
  `Estado` int(1) NOT NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tblusuarios` */

insert  into `tblusuarios`(`Id`,`Login`,`Clave`,`Nombre`,`UltimoAcceso`,`Tipo`,`Estado`) values (1,'admin','e10adc3949ba59abbe56e057f20f883e','Root','2013-01-08 19:54:35',1,1),(8,'vendedor2','76bfaf88ae4d178d004bad31146faeed','Vendedor2','2013-01-07 01:40:18',3,1),(9,'vendedor1','76bfaf88ae4d178d004bad31146faeed','Vendedor1','2013-01-05 22:45:38',3,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
