var Projects = {
  links: [
    { name: 'Lightview', href: 'http://www.nickstakenburg.com/projects/lightview', 
	  hook: { tip: 'topRight', target: 'bottomLeft' }
	},
	{ name: 'Prototip', href: 'http://www.nickstakenburg.com/projects/prototip', 
	  hook: { tip: 'topRight', target: 'bottomLeft' }
	},
    { name: 'Starbox', href: 'http://www.nickstakenburg.com/projects/starbox', 
	  hook: { tip: 'topRight', target: 'bottomLeft' }
	}
  ],
  
  getTipContent: function(name) {
    if (!$('ideas')) return;
	
	var wrapper = new Element('div');
	
	var links = wrapper.appendChild(new Element('ul'));
	links.insert(new Element('li', { className: 'top' }).update('<!-- round -->'));
	this.links.each(function(l,i) {
	  var li = links.appendChild(new Element('li'));
	  var target = (l.name == name) ? '_self' : '_self';
	  var a = li.appendChild(new Element('a', { className: 'idea' + l.name, 'target': target, 'href': l.href }).update(l.name));
	  if (i + 1 == this.links.length) a.addClassName('last');
    }.bind(this));
	links.insert(new Element('li', { className: 'bottom' }).update('<!-- round -->'));
	
	return wrapper;
  },
  
  createIdeas: function(name) {
	var content = this.getTipContent(name);

    var hook = this.links.find(function(l) { return l.name.toUpperCase() == name.toUpperCase(); }).hook;
	new Tip('ideas', content, { className: 'ideaTip', effect: 'appear', hook: hook, hideOn: false, hideAfter: 1.5 });
  }	
}