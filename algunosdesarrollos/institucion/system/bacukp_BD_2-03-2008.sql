/*
SQLyog Community Edition- MySQL GUI v6.06
Host - 5.0.45-community-nt : Database - jhonalquiler
*********************************************************************
Server version : 5.0.45-community-nt
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

create database if not exists `jhonalquiler`;

USE `jhonalquiler`;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `alquiladas` */

DROP TABLE IF EXISTS `alquiladas`;

CREATE TABLE `alquiladas` (
  `id` int(255) NOT NULL auto_increment,
  `id_cliente` int(255) default NULL,
  `id_pelicula` int(255) default NULL,
  `precio` int(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `alquiladas` */

insert  into `alquiladas`(`id`,`id_cliente`,`id_pelicula`,`precio`) values (5,2,2,2000),(4,2,1,2000),(6,2,1,2000),(7,1,4,2000),(8,2,3,2000),(9,1,2,2000);

/*Table structure for table `auxiliar` */

DROP TABLE IF EXISTS `auxiliar`;

CREATE TABLE `auxiliar` (
  `id_pelicula` int(255) NOT NULL auto_increment,
  `estado` int(1) default NULL COMMENT '1 estado alquilada, 0 no se encuentra alquilada',
  PRIMARY KEY  (`id_pelicula`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `auxiliar` */

insert  into `auxiliar`(`id_pelicula`,`estado`) values (1,0),(2,0),(3,1),(4,1);

/*Table structure for table `clientes` */

DROP TABLE IF EXISTS `clientes`;

CREATE TABLE `clientes` (
  `id` int(255) NOT NULL auto_increment,
  `nombrec` varchar(255) collate latin1_general_ci default NULL COMMENT 'nombre del cliente',
  `direccionc` varchar(255) collate latin1_general_ci default NULL COMMENT 'direccion del cliente',
  `telefonoc` varchar(255) collate latin1_general_ci default NULL COMMENT 'telefono del cliente',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `clientes` */

insert  into `clientes`(`id`,`nombrec`,`direccionc`,`telefonoc`) values (1,'Super Mario','Cll 32No37-27','12121212'),(2,'Anacleto','Barrio La Manguita con la troncal','12277272'),(3,'Teofilio de los ángeles','Las Flores','9383939');

/*Table structure for table `peliculas` */

DROP TABLE IF EXISTS `peliculas`;

CREATE TABLE `peliculas` (
  `id` int(255) NOT NULL auto_increment,
  `nombre` varchar(255) collate latin1_general_ci default NULL COMMENT 'Nombre de la pelicula',
  `descripcion` text collate latin1_general_ci COMMENT 'descripcion de la pelicula',
  `tipo` varchar(50) collate latin1_general_ci default NULL COMMENT 'Tipo de pelicula',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `peliculas` */

insert  into `peliculas`(`id`,`nombre`,`descripcion`,`tipo`) values (1,'Años Dorados','No apta para niños.','Otro'),(2,'Jumánji','Una peliculas de animales salvajes.','Aventura'),(3,'El único','El prota es Jet Lee','Acción'),(4,'Resident Evil 3','Una de las mejores peliculas.','Acción');

/*Table structure for table `tabs_tabla_1` */

DROP TABLE IF EXISTS `tabs_tabla_1`;

CREATE TABLE `tabs_tabla_1` (
  `id` int(11) NOT NULL auto_increment,
  `datos` text collate latin1_general_ci,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `tabs_tabla_1` */

insert  into `tabs_tabla_1`(`id`,`datos`) values (1,'tab1'),(2,'tab2'),(3,'tab3'),(4,'tab4'),(5,'tab5');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
