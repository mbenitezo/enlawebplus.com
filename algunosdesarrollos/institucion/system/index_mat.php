<?php
include_once("cPeliculas.php");
$objpelicula = new cPelicula;
$consultas=$objpelicula->consultarMat();
$consultaf=$objpelicula->consultarProfe();
?>

<html>
<head>
<title>Ingreso de Materias y Profesores al Sistema</title>
<!--
    <link rel="stylesheet" type="text/css" href="ext-all.css" />
    <script type="text/javascript" src="ext-base.js"></script>
    <script type="text/javascript" src="ext-all.js"></script>
    <script language="javascript" src="hello.js"></script>
    <link rel="stylesheet" type="text/css" href="examples.css" />
    <style type="text/css">
    .x-panel-body p {
        margin:10px;
        font-size:12px;
    }
    </style> -->

<script type="text/javascript" src="ajax.js"></script>
<script type="text/javascript" src="prototype2.js"></script>
<script type="text/javascript" src="effects2.js"></script>
<script type="text/javascript" src="accordion.js"></script>

    <script type="text/javascript">
            
        //
        //  In my case I want to load them onload, this is how you do it!
        // 
        Event.observe(window, 'load', loadAccordions, false);
    
        //
        //    Set up all accordions
        //
        function loadAccordions() {
            var topAccordion = new accordion('horizontal_container', {
                classNames : {
                    toggle : 'horizontal_accordion_toggle',
                    toggleActive : 'horizontal_accordion_toggle_active',
                    content : 'horizontal_accordion_content'
                },
                defaultSize : {
                    width : 550
                },
                direction : 'horizontal'
            });
            
            var bottomAccordion = new accordion('vertical_container');
            
            var nestedVerticalAccordion = new accordion('vertical_nested_container', {
              classNames : {
                    toggle : 'vertical_accordion_toggle',
                    toggleActive : 'vertical_accordion_toggle_active',
                    content : 'vertical_accordion_content'
                }
            });
            
            // Open first one
            bottomAccordion.activate($$('#vertical_container .accordion_toggle')[0]);
            
            // Open second one
            topAccordion.activate($$('#horizontal_container .horizontal_accordion_toggle')[2]);
        }
        
    </script>
    
    <!-- CSS -->
    <style type="text/css" >
         /*
            Horizontal Accordion
        */
        
        .horizontal_accordion_toggle {
            /* REQUIRED */
            float: left;    /* This make sure it stays horizontal */
            /* REQUIRED */

            display: block;
            height: 300px;
            width: 40px;
            background: url(h_accordion_toggle.jpg) no-repeat top left #a9d06a;
            color: #ffffff;
            text-decoration: none;
            outline: none;
            border-right: 1px solid #cde99f;
            cursor: pointer;
            margin: 0 0 0 0;
        }
        
        .horizontal_accordion_toggle_active {
            background: url(h_accordion_toggle_active.jpg) no-repeat top left #e0542f;
            border-right: 1px solid #f68263;
        }
        
        .horizontal_accordion_content {
            /* REQUIRED */
            height: 100px;    /* We need to define a height for the accordion as it stretches the width */
            float: left;    /* This make sure it stays horizontal */
            /* REQUIRED */
            
            overflow: hidden;
            background-color: #ffffff;
            color: #444444;
        }
            
            .horizontal_accordion_content p {
                width: 300px;
                line-height: 150%;
                padding: 5px 10px 15px 10px;
            }
                    
                    
    /* Container styling*/
    #horizontal_container {
      margin: 20px auto 20px auto;
      width: 680px;   
      height: 100px;    
    }
    
    #vertical_nested_container {
      margin: 20px auto 20px auto;
      width: 620px;
    }

    </style>
<link href="css_.css" rel="stylesheet" type="text/css" /> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<p><b>Seleccione la columna de acuerdo a las siguientes especificaciones:</b></p>
<p><b>1->Materias --- 2->Profesores --- 3->Accesos</b></p>
<!-- <script type="text/javascript" src="examples.js"></script><!-- EXAMPLES -->

<div id="horizontal_container" >
<h3 class="horizontal_accordion_toggle"></h3>
<div class="horizontal_accordion_content">

<b>* * * * * *&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MATERIAS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* * * * *</b>
<br><br>

<table border="0" width="400">
<tr><td>
<div align="left"><b>Ingrese el nombre de la Materia</b></div>
<form name="ingMaterias" action="" onSubmit="guardamateria(); return false">  
<table width="200" align="left" border="0">
<tr>                                          
<td>Nombre:</td>
<td><input type="text" name="materia" size="30"></td>
<td align="right"><input type="submit" name="Submit" value="Agregar"></td>
</tr>
</table>
</form>
<br><br>
<b>Materias existentes en el sistema</b>
<table width="200" align="left" border="0">
<tr>
<td><div id="vermaterias"><?php include('listaMaterias.php');?></div></td></tr><tr><td><div id="edicionMateria"></div></td>
</tr>
</table>
</td>
</tr>
</table>                

</div>
<h3 class="horizontal_accordion_toggle"></h3>
<div class="horizontal_accordion_content">

<b>* * * * * *&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PROFESORES &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* * * * *</b>
<br><br>


   
<table border="0" width="400">
<tr><td>
  

    <table border="0" class="spoiler" align="left" cellpadding="6" cellspacing="0" width="90%">
    <tr>
    <td>
<input value="Ingresar Profesor" class="show_hide" onClick="if (this.parentNode.parentNode.parentNode.getElementsByTagName('div')[0].style.display != '') { this.parentNode.parentNode.parentNode.getElementsByTagName('div')[0].style.display = ''; this.value = 'Ocultar Formulario'; } else { this.parentNode.parentNode.parentNode.getElementsByTagName('div')[0].style.display = 'none'; this.value = 'Ingresar Profesor'; }" type="button">
  </td>
  </tr>
  <tr>
  <td align="center">
  <div style=" padding: 1px; display: none; width: 90%; text-align: left; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial;">
<form name="ingProfesores" action="" onSubmit="guardaprofesor(); return false">
<table width="200" align="left" border="0">
<tr>                                          
<td><b>*&nbsp;Nombre:</b></td>
<td><textarea name="profesor" cols="15" rows="1"></textarea></td>
<td align="right"><input type="submit" name="Submit" value="Agregar"></td>
</tr>
<tr>                                          
<td><b>*&nbsp;C&eacute;dula:</b></td>
<td><input type="text" name="cedula" size="15"></td>
<td><b>Direcci&oacute;n:</b></td>
<td><textarea name="direccion" cols="15" rows="1"></textarea></td>
</tr>
<tr>                                          
<td><b>Tel&eacute;fono:</b></td>
<td><input type="text" name="telefono1" size="15"></td>
<td><b>Tel&eacute;fono (Alterno):</b></td>
<td><input type="text" name="telefono2" size="15"></td>
</tr>
<tr>                                          
<td><b>Email:</b></td>
<td><input type="text" name="email1" size="10"></td>
<td><b>Email (Alterno):</b></td>
<td><input type="text" name="email2" size="10"></td>
</tr>
</table>
</form>
   </div>    
  
   </td>
   </tr>
   <tr>
   <td>
<table width="200" align="left" border="0">

<tr>
<td><b>Profesores registrados en el sistema</b><div id="verprofesores"><?php include('listaProfesores.php');?></div></td></tr><tr><td><div id="edicionProfesor"></div></td>
</tr>
</table>
</td>
</tr>
</table>                
   </td>
   </tr>
   </table>
</div>
<h3 class="horizontal_accordion_toggle"></h3>
<div class="horizontal_accordion_content">
<b>* * * * * *&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ACCESO AL SISTEMA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* * * * *</b>
<br><br>

<table border="0" width="400">
<tr><td>
<div align="left"><b>Registre los accesos de los profesores con sus respectivas materias</b></div>
<form name="ingAcceso" action="" onSubmit="guardacceso(); return false">  
<table width="500" align="left" border="0">
<tr>                                          
<td width="60">Materia:</td>
<td width="10"><select name="materia">
<?php while($rowm=mysql_fetch_array($consultas)){ ?>
<option value="<?php echo $rowm["id"]; ?>"><?php echo utf8_encode($rowm["nombre_mat"]); ?></option>
<?php } ?>
</select>
</td>
</tr>
<tr>                                          
<td width="60">Profesor:</td>
<td width="10"><select name="profesor">
<?php while($rowf=mysql_fetch_array($consultaf)){ ?>
<option value="<?php echo $rowf["id"]; ?>"><?php echo utf8_encode($rowf["nombre"]); ?></option>
<?php } ?>
</select>
</td>
</tr>
<tr>                                          
<td width="60">Clave de Acceso:</td>
<td width="10"><input type="text" name="clave" size="10"> &nbsp;&nbsp;&nbsp; <input type="submit" name="submit" value="Guardar"></td>
</tr>

</table>
</form>
<?php 
if(ereg("MSIE", $_SERVER["HTTP_USER_AGENT"])){
?>
<br><br><br><br><br><br><br><br>
<?php
}else{
?>
<br><br>
<?php
}
?>


<b>Listado de Accesos asignados</b>
<table width="200" align="left" border="0">
<tr>
<td><div id="veraccesos"><?php include('listaAcceso.php');?></div></td></tr><tr><td><div id="edicionAcceso"></div></td>
</tr>
</table>
</td>
</tr>
</table>                
</div>

<script type="text/javascript" >
  
    //
    // You can hide the accordions on page load like this, it maintains accessibility
    //
    // Special thanks go out to Will Shaver @ http://primedigit.com/
    //
    var verticalAccordions = $$('.accordion_toggle');
    verticalAccordions.each(function(accordion) {
        $(accordion.next(0)).setStyle({
          height: '0px'
        });
    });

    
</script>

                </body>
</html>