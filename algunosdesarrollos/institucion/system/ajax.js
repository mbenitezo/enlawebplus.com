// Declaro un array en el cual los indices son los ID's de los DIVS que funcionan como pesta�a y los valores son los identificadores de las secciones a cargar
var tabsId=new Array();
tabsId['tab1']='seccion1';
tabsId['tab2']='seccion2';
tabsId['tab3']='seccion3';
tabsId['tab4']='seccion4';
tabsId['tab5']='seccion5';
tabsId['tab6']='seccion6';


// Declaro el ID del DIV que actuar� como contenedor de los datos recibidos
var contenedor='tabContenido';


function objetoAjax(){
	var xmlhttp=false;
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(E){
			xmlhttp = false;
  		}
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}


function cargaContenido()
{
    /* Recorro las pesta�as para dejar en estado "apagado" a todas menos la que se ha clickeado. Teniendo en cuenta que solo puede haber una pesta�a "encendida"
    a la vez resultar�a mas �ptimo hacer un while hasta encontrar a esa pesta�a, cambiarle el estilo y luego salir, pero, creanme, se complicar�a un poco el
    ejemplo y no es mi intenci�n complicarlos */
    for(key in tabsId)
    {
        // Obtengo el elemento
        elemento=document.getElementById(key);
        // Si es la pesta�a activa
        if(elemento.className=='tabOn')
        {
            // Cambio el estado de la pesta�a a inactivo 
            elemento.className='tabOff';
        }
    }
    // Cambio el estado de la pesta�a que se ha clickeado a activo
    this.className='tabOn';
    
    /* De aqui hacia abajo se tratatan la petici�n y recepci�n de datos */
    
    // Obtengo el identificador vinculado con el ID del elemento HTML que referencia a la secci�n a cargar
    seccion=tabsId[this.id];
    
    // Coloco un mensaje mientras se reciben los datos
    tabContenedor.innerHTML='<img src="loading2.gif"> Cargando, por favor espere...';
    
    // Creo el objeto AJAX y envio la petici�n por POST (para evitar cacheos de datos)
    var ajax=objetoAjax();
    ajax.open("POST", "tabs_o_pestanas_con_javascript_no_intrusivo_proceso.php", true);
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax.send('seccion='+seccion);
    
    ajax.onreadystatechange=function()
    {
        if(ajax.readyState==4)
        {
            // Al recibir la respuesta coloco directamente el HTML en la capa contenedora
            tabContenedor.innerHTML=ajax.responseText;
        }
    }
}

function mouseSobre()
{
    // Si el evento no se produjo en la pesta�a seleccionada...
    if(this.className!='tabOn')
    {
        // Cambio el color de fondo de la pesta�a
        this.className='tabHover';
    }
}

function mouseFuera()
{
    // Si el evento no se produjo en la pesta�a seleccionada...
    if(this.className!='tabOn')
    {
        // Cambio el color de fondo de la pesta�a
        this.className='tabOff';
    }
}

onload=function()
{
    for(key in tabsId)
    {
        // Voy obteniendo los ID's de los elementos declarados en el array que representan a las pesta�as
        elemento=document.getElementById(key);
        // Asigno que al hacer click en una pesta�a se llame a la funcion cargaContenido
        elemento.onclick=cargaContenido;
        /* El cambio de estilo es en 2 funciones diferentes debido a la incompatibilidad del string de backgroundColor devuelto por Mozilla e IE.
        Se podr�a pasar de rgb(xxx, xxx, xxx) a formato #xxxxxx pero complicar�a innecesariamente el ejemplo */
        elemento.onmouseover=mouseSobre;
        elemento.onmouseout=mouseFuera;
    }
    // Obtengo la capa contenedora de datos
    tabContenedor=document.getElementById(contenedor);
}



///////////////////////EMPIEZA CODIGO PARA LAS PELICULAS


function enviarDatosPelicula(){
	//donde se mostrar� lo resultados
	divResultado = document.getElementById('resultado');
	divFormulario = document.getElementById('formulario');
	divResultado.innerHTML= 'Actualizando...';
	
	//valores de los cajas de texto
	id=document.frmpelicula.idpelicula.value;
	nom=document.frmpelicula.nombres.value;
	
	//instanciamos el objetoAjax
	ajax=objetoAjax();
	//usando del medoto POST
	//archivo que realizar� la operacion ->actualizacion.php
	ajax.open("POST", "actualizacion.php",true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			//mostrar los nuevos registros en esta capa
			divResultado.innerHTML = ajax.responseText
			//una vez actualizacion ocultamos formulario
			divFormulario.style.display="none";

		}
	}
	//muy importante este encabezado ya que hacemos uso de un formulario
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	//enviando los valores
	ajax.send("idpelicula="+id+"&nombres="+nom)
}

function pedirDatos(idgrados){
	//donde se mostrar� el formulario con los datos
	divFormulario = document.getElementById('formulario');
	
	//instanciamos el objetoAjax
	ajax=objetoAjax();
	//uso del medotod POST
	ajax.open("POST", "consulta_por_id.php");
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			//mostrar resultados en esta capa
			divFormulario.innerHTML = ajax.responseText
			divFormulario.style.display="block";
		}
	}
	//como hacemos uso del metodo POST
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	//enviando el codigo del empleado
	ajax.send("idemp="+idgrados)
}



function eliminarDato(idgrados){
	//donde se mostrar� el resultado de la eliminacion
	divResultado = document.getElementById('resultado');
	
	
	//usaremos un cuadro de confirmacion	
	var eliminar = confirm("De verdad desea eliminar este dato?")
	if ( eliminar ) {
		//instanciamos el objetoAjax
		ajax=objetoAjax();
		//uso del medotod GET
		//indicamos el archivo que realizar� el proceso de eliminaci�n
		//junto con un valor que representa el id del empleado
		ajax.open("GET", "eliminacion.php?idgrados="+idgrados);
		divResultado.innerHTML= '<img src="anim.gif">';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}
		//como hacemos uso del metodo GET
		//colocamos null
		ajax.send(null)
	}
}



function enviarDatosPelicula2(){
  //valores de las cajas de texto
  noms=document.nueva_pelicula.nombress.value;
        if(noms == ""){
            alert("Debe ingresar un nombre");
            document.nueva_pelicula.nombress.focus();
            return false;
        }
  //donde se mostrar� lo resultados
  divResultado = document.getElementById('resultado');
  divResultado.innerHTML= '<img src="anim.gif">';
 //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medoto POST
  //archivo que realizar� la operacion
  //registro.php
  ajax.open("POST", "registro.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divResultado.innerHTML = ajax.responseText
  //llamar a funcion para limpiar los inputs
  LimpiarCamposs();
  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores
  ajax.send("nombres="+noms)
}

function LimpiarCamposs(){
  document.nueva_pelicula.nombress.value="";
  document.nueva_pelicula.nombress.focus();
  }
  
//TERMINA CODIGO PARA LAS PELICULAS




//EMPIEZA CODIGO PARA LOS CLIENTES
//EMPIEZA CODIGO PARA LOS CLIENTES
//EMPIEZA CODIGO PARA LOS CLIENTES
//EMPIEZA CODIGO PARA LOS CLIENTES
//EMPIEZA CODIGO PARA LOS CLIENTES


function enviarDatosClienteC(){
    //donde se mostrar� lo resultados
    divResultado = document.getElementById('resultadoc');
    divFormulario = document.getElementById('formularioc');
    divResultado.innerHTML= 'Actualizando...';

    
    //valores de los cajas de texto
    id=document.frmcliente.idcliente.value;
    nomc=document.frmcliente.nombres.value;
    direc=document.frmcliente.grados.value;
      
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //usando del medoto POST
    //archivo que realizar� la operacion ->actualizacion.php
    ajax.open("POST", "actualizacionc.php",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar los nuevos registros en esta capa
            divResultado.innerHTML = ajax.responseText
            //una vez actualizacion ocultamos formulario
            divFormulario.style.display="none";

        }
    }
    //muy importante este encabezado ya que hacemos uso de un formulario
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando los valores
    ajax.send("idcliente="+id+"&nombres="+nomc+"&direccion="+direc)
}

function pedirDatosC(idcliente){
    //donde se mostrar� el formulario con los datos
    divFormulario = document.getElementById('formularioc');
    
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "consulta_por_idc.php");
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="block";
        }
    }
    //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("idemp="+idcliente)
}


function pedirDatosEst(){
    //donde se mostrar� el formulario con los datos
    divFormulario = document.getElementById('formularioc');
    divFormulario.innerHTML= 'Espere por favor, realizando la petici&oacute;n...<br>'+'<img src="anim.gif">';
    consul = document.estudents.grado.value;
    //alert(consul);
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "prueba2.php",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="block";
        }
    }
    //enviando el codigo del empleado
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send("grado="+consul)
}


function eliminarDatoC(idcliente){
    //donde se mostrar� el resultado de la eliminacion
    divResultado = document.getElementById('resultadoc');
    
    
    //usaremos un cuadro de confirmacion    
    var eliminar = confirm("De verdad desea eliminar este dato?")
    if ( eliminar ) {
        //instanciamos el objetoAjax
        ajax=objetoAjax();
        //uso del medotod GET
        //indicamos el archivo que realizar� el proceso de eliminaci�n
        //junto con un valor que representa el id del empleado
        ajax.open("GET", "eliminacionc.php?idcliente="+idcliente);
        divResultado.innerHTML= '<img src="anim.gif">';
        ajax.onreadystatechange=function() {
            if (ajax.readyState==4) {
                //mostrar resultados en esta capa
                divResultado.innerHTML = ajax.responseText
            }
        }
        //como hacemos uso del metodo GET
        //colocamos null
        ajax.send(null)
    }
}



function enviarDatosCliente2(){
  //valores de las cajas de texto
  noms=document.nuevo_cliente.nombress.value;
  grados=document.nuevo_cliente.grados.value;
  yearx=document.nuevo_cliente.year.value;
  
        if((noms == "") || (grados == "")){
            alert("Debe seleccionar o ingresar una opci�n");
            return false;
        }

  //donde se mostrar� lo resultados
  divResultado = document.getElementById('resultadoc');
  divResultado.innerHTML= '<img src="anim.gif">';
  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medoto POST
  //archivo que realizar� la operacion
  //registro.php
  ajax.open("POST", "registroc.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divResultado.innerHTML = ajax.responseText
  //llamar a funcion para limpiar los inputs
  LimpiarCampossC();
  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores
  ajax.send("nombres="+noms+"&dire="+grados+"&year="+yearx)
}

function LimpiarCampossC(){
  document.nuevo_cliente.nombress.value="";
  document.nuevo_cliente.nombress.focus();
  }

//TERMINA CODIGO PARA LOS CLIENTES  
  
function pedirDatosAl(idpelicula,idcliente,cant,nombre_cli,precio,Submit){
    //donde se mostrar� el formulario con los datos
    divResultadoP = document.getElementById('resultadoP');
    divFormularioP = document.getElementById('formularioP');
    divResultadoP.innerHTML= '<img src="anim.gif">';
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "cambiarestado.php",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divResultadoP.innerHTML = ajax.responseText
            divFormularioP.style.display="block";
        }
    }
    //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("idpelicula="+idpelicula+"&idcliente="+idcliente+"&cant="+cant+"&nombre_cli="+nombre_cli+"&precio="+precio+"&Submit="+Submit)
} 

function buscaclientes(){
    //donde se mostrar� el formulario con los datos
    divResponClientes = document.getElementById('resp_clientes');
    divResponClientes.innerHTML= '<img src="anim.gif">';
    criteriox=document.findclient.criterio.value;
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "consul_sel_clientes.php",true);
        ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divResponClientes.innerHTML = ajax.responseText
           }
    }
   //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send("criterio="+criteriox)      
   
} 

///////////////////////EMPIEZA CODIGO PARA LOS CONCEPTOS


function enviarDatosConcepto(){
    //donde se mostrar� lo resultados
    divResultado = document.getElementById('resultado');
    divFormulario = document.getElementById('formulario');
    divResultado.innerHTML= 'Actualizando...';
    
    //valores de los cajas de texto
    id=document.frmconcepto.idconcepto.value;
    nom=document.frmconcepto.nombres.value;
    grad=document.frmconcepto.grados.value;
     
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //usando del medoto POST
    //archivo que realizar� la operacion ->actualizacion.php
    ajax.open("POST", "actualizacion_con.php",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar los nuevos registros en esta capa
            divResultado.innerHTML = ajax.responseText
            //una vez actualizacion ocultamos formulario
            divFormulario.style.display="none";

        }
    }
    //muy importante este encabezado ya que hacemos uso de un formulario
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando los valores
    ajax.send("idconcepto="+id+"&grado="+grad+"&nombres="+nom)
}

function pedirDatosCon(idconcepto){
    //donde se mostrar� el formulario con los datos
    divFormulario = document.getElementById('formulario');
    
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "consulta_por_id_con.php");
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="block";
        }
    }
    //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("idemp="+idconcepto)
}



function eliminarDatoCon(idconcepto){
    //donde se mostrar� el resultado de la eliminacion
    divResultado = document.getElementById('resultado');
    
    
    //usaremos un cuadro de confirmacion    
    var eliminar = confirm("De verdad desea eliminar este dato?, eliminar� las notas pertenecientes a �ste concepto")
    if ( eliminar ) {
        //instanciamos el objetoAjax
        ajax=objetoAjax();
        //uso del medotod GET
        //indicamos el archivo que realizar� el proceso de eliminaci�n
        //junto con un valor que representa el id del empleado
        ajax.open("GET", "eliminacion_con.php?idconcepto="+idconcepto);
        divResultado.innerHTML= '<img src="anim.gif">';
        ajax.onreadystatechange=function() {
            if (ajax.readyState==4) {
                //mostrar resultados en esta capa
                divResultado.innerHTML = ajax.responseText
            }
        }
        //como hacemos uso del metodo GET
        //colocamos null
        ajax.send(null)
    }
}


function eliminarNota(idnota){
    //donde se mostrar� el resultado de la eliminacion
    divResultado = document.getElementById('resultadon');
     if(idnota != 123456789123456789){
    var eliminar = confirm("De verdad desea eliminar esta Nota?")
    
     }else{
     
    var eliminar = true
     
     }
    
    //usaremos un cuadro de confirmacion    
    if ( eliminar ) {
        //instanciamos el objetoAjax
        ajax=objetoAjax();
        //uso del medotod GET
        //indicamos el archivo que realizar� el proceso de eliminaci�n
        //junto con un valor que representa el id del empleado
        ajax.open("GET", "eliminacion_nota.php?idnotas="+idnota);
        divResultado.innerHTML= '<img src="anim.gif">';
        ajax.onreadystatechange=function() {
            if (ajax.readyState==4) {
                //mostrar resultados en esta capa
                divResultado.innerHTML = ajax.responseText
            }
        }
        //como hacemos uso del metodo GET
        //colocamos null
        ajax.send(null)
    }
}

function juanita(){
    //donde se mostrar� el resultado de la eliminacion
    divResultado = document.getElementById('resultadon');
    divResultado.innerHTML= '<img src="anim.gif">';
        ajax=objetoAjax();
        ajax.open("POST", "solu_actualiza.php",true);
        ajax.onreadystatechange=function() {
            if (ajax.readyState==4) {
                //mostrar resultados en esta capa
                divResultado.innerHTML = ajax.responseText
               }
        }
            ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
            ajax.send(null)
}

function enviarDatosConceptos(){
  //valores de las cajas de texto
  noms=document.nuevo_concepto.nombress.value;
  grad=document.nuevo_concepto.grados.value;
  year=document.nuevo_concepto.year.value;
  periodo=document.nuevo_concepto.periodo.value;
  //alert(periodo);
        if((noms == "")||(grad == "")){
           alert("Debe ingresar o seleccionar un opci�n");     
           return false;
        }
            
  //donde se mostrar� lo resultados
  divResultado = document.getElementById('resultado');
  divResultado.innerHTML= '<img src="anim.gif">';
  
 //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medoto POST
  //archivo que realizar� la operacion
  //registro.php
  ajax.open("POST", "registro_con.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divResultado.innerHTML = ajax.responseText
  //llamar a funcion para limpiar los inputs
  LimpiarCamposs();
  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores
  ajax.send("nombres="+noms+"&grado="+grad+"&year="+year+"&periodo="+periodo)
}

function LimpiarCamposs(){
  document.nuevo_concepto.nombress.value="";
  document.nuevo_concepto.nombress.focus();
  }
  
//TERMINA CODIGO PARA LAS PELICULAS

function enviaNotas(){
    cant=document.notas.cantidad.value;   
    grado=document.notas.codigogrado.value;   
    concep=document.notas.concepto.value;   
            if(concep == ""){
                 alert("Debe seleccionar un concepto");
                 return false;
            }

  //donde se mostrar� lo resultados
  divResultado = document.getElementById('resultadon');
    //     alert(cant+grado+concep);
    var i;
    for(i=1; i<=cant; i++){
    nota = eval("document.notas.estu"+i+".value");
    codi = eval("document.notas.codigo"+i+".value");
    faltas = eval("document.notas.faltas"+i+".value");
      divResultado.innerHTML= 'Espere por favor, guardando datos del estudiante <br>'+'<img src="anim.gif">';

    //       alert(nota+"---"+codi)
  ajax=objetoAjax();
  ajax.open("POST", "prueba3.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divResultado.innerHTML = ajax.responseText
//  divResultado.style.display="none";  
  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores
  ajax.send("codigogrado="+grado+"&estu"+i+"="+nota+"&codigo"+i+"="+codi+"&faltas"+i+"="+faltas+"&concepto="+concep)
  
}
    }
    
    
function guardamateria(){
  //valores de las cajas de texto
  noms=document.ingMaterias.materia.value;
        if(noms == ""){
            alert("Debe ingresar un Nombre");
            return false;
        }

  //donde se mostrar� lo resultados
  divResultado = document.getElementById('vermaterias');
  divResultado.innerHTML= '<img src="anim.gif">';
  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medoto POST
  //archivo que realizar� la operacion
  //registro.php
  ajax.open("POST", "registro_mat.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divResultado.innerHTML = ajax.responseText
  //llamar a funcion para limpiar los inputs
  LimpiaCampMaterias();
  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores
  ajax.send("nombres="+noms)
}

function LimpiaCampMaterias(){
  document.ingMaterias.materia.value="";
  document.ingMaterias.materia.focus();
  }

function EditarMateria(idmateria){
    //donde se mostrar� el formulario con los datos
    divFormulario = document.getElementById('edicionMateria');
    
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "consulta_por_id_mate.php");
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="block";
        }
    }
    //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("idemp="+idmateria)
} 

function validarEdicionMateria(){
    //donde se mostrar� lo resultados
    divResultado = document.getElementById('vermaterias');
    divFormulario = document.getElementById('edicionMateria');
    divResultado.innerHTML= 'Actualizando...';
    
    //valores de los cajas de texto
    id=document.frmmateria.idmateria.value;
    nom=document.frmmateria.nombres.value;
    
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //usando del medoto POST
    //archivo que realizar� la operacion ->actualizacion.php
    ajax.open("POST", "actualizacion_materia.php",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar los nuevos registros en esta capa
            divResultado.innerHTML = ajax.responseText
            //una vez actualizacion ocultamos formulario
            divFormulario.style.display="none";

        }
    }
    //muy importante este encabezado ya que hacemos uso de un formulario
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando los valores
    ajax.send("idmateria="+id+"&nombres="+nom)
}

function eliminarMateria(idmateria){
    //donde se mostrar� el resultado de la eliminacion
    divResultado = document.getElementById('vermaterias');
    
    
    //usaremos un cuadro de confirmacion    
    var eliminar = confirm("De verdad desea eliminar este dato?")
    if ( eliminar ) {
        //instanciamos el objetoAjax
        ajax=objetoAjax();
        //uso del medotod GET
        //indicamos el archivo que realizar� el proceso de eliminaci�n
        //junto con un valor que representa el id del empleado
        ajax.open("GET", "eliminacion_mate.php?idmateria="+idmateria);
        divResultado.innerHTML= '<img src="anim.gif">';
        ajax.onreadystatechange=function() {
            if (ajax.readyState==4) {
                //mostrar resultados en esta capa
                divResultado.innerHTML = ajax.responseText
            }
        }
        //como hacemos uso del metodo GET
        //colocamos null
        ajax.send(null)
    }
}

function guardaprofesor(){
  //valores de las cajas de texto
  noms=document.ingProfesores.profesor.value;
  ced=document.ingProfesores.cedula.value;
  dir=document.ingProfesores.direccion.value;
  tel1=document.ingProfesores.telefono1.value;
  tel2=document.ingProfesores.telefono2.value;
  cor1=document.ingProfesores.email1.value;
  cor2=document.ingProfesores.email2.value;

        if((noms == "") || (ced == "")){
            alert("Debe ingresar un NOMBRE o una CEDULA");
            return false;
        }

  //donde se mostrar� lo resultados
  divResultado = document.getElementById('verprofesores');
  divResultado.innerHTML= '<img src="anim.gif">';
  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medoto POST
  //archivo que realizar� la operacion
  //registro.php
  ajax.open("POST", "registro_profe.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divResultado.innerHTML = ajax.responseText
  //llamar a funcion para limpiar los inputs
  LimpiaProfesores();
  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores
  ajax.send("nombre="+noms+"&cedula="+ced+"&direccion="+dir+"&telefono1="+tel1+"&telefono2="+tel2+"&correo1="+cor1+"&correo2="+cor2)
}

function LimpiaProfesores(){
  document.ingProfesores.profesor.value = "";
  document.ingProfesores.cedula.value = "";
  document.ingProfesores.direccion.value = "";
  document.ingProfesores.telefono1.value = "";
  document.ingProfesores.telefono2.value = "";
  document.ingProfesores.email1.value = "";
  document.ingProfesores.email2.value = "";
  document.ingProfesores.profesor.focus();  
  
  }
  
  function cancela1(){
   divFormulario = document.getElementById('edicionMateria');
/////////////////////////////   divFormulario.innerHTML= '<b>Actualizando</b>&nbsp;&nbsp;&nbsp;<img src="upload_progress.gif">';
//divFormulario.innerHTML= '&nbsp;<img src="upload_progress.gif">';
    ajax=objetoAjax();
        ajax.open("POST", "actualizacion_materia.php");
        ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="none";
        }
    }
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("enblanco=nada")
 
  }   

  function cancela2(){
   divFormulario = document.getElementById('edicionProfesor');
/////////////////////////////   divFormulario.innerHTML= '<b>Actualizando</b>&nbsp;&nbsp;&nbsp;<img src="upload_progress.gif">';
//divFormulario.innerHTML= '&nbsp;<img src="upload_progress.gif">';
    ajax=objetoAjax();
        ajax.open("POST", "actualizacion_profesor.php");
        ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="none";
        }
    }
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("enblanco=nada")
 
  }
  
    function cancela3(){
   divFormulario = document.getElementById('edicionAcceso');
/////////////////////////////   divFormulario.innerHTML= '<b>Actualizando</b>&nbsp;&nbsp;&nbsp;<img src="upload_progress.gif">';
//divFormulario.innerHTML= '&nbsp;<img src="upload_progress.gif">';
    ajax=objetoAjax();
        ajax.open("POST", "actualizacion_profesor.php");
        ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="none";
        }
    }
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("enblanco=nada")
 
  }
  
function EditarProfesor(idprofesor){
    //donde se mostrar� el formulario con los datos
    divFormulario = document.getElementById('edicionProfesor');
    
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "consulta_por_id_profe.php");
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="block";
        }
    }
    //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("idemp="+idprofesor)
} 

function validarEdicionProfesor(){
    //valores de los cajas de texto
    id=document.frmprofesor.idprofesor.value;
    nom=document.frmprofesor.profesor.value;
    ced=document.frmprofesor.cedula.value;
    dir=document.frmprofesor.direccion.value;
    tel1=document.frmprofesor.telefono1.value;
    tel2=document.frmprofesor.telefono2.value;
    cor1=document.frmprofesor.email1.value;
    cor2=document.frmprofesor.email2.value;

        if((nom == "") || (ced == "")){
            alert("Debe ingresar un NOMBRE o una CEDULA");
            return false;
        }


    //donde se mostrar� lo resultados
    divResultado = document.getElementById('verprofesores');
    divFormulario = document.getElementById('edicionProfesor');
    divResultado.innerHTML= 'Actualizando...';
    
    
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //usando del medoto POST
    //archivo que realizar� la operacion ->actualizacion.php
    ajax.open("POST", "actualizacion_profesor.php",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar los nuevos registros en esta capa
            divResultado.innerHTML = ajax.responseText
            //una vez actualizacion ocultamos formulario
            divFormulario.style.display="none";

        }
    }
    //muy importante este encabezado ya que hacemos uso de un formulario
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando los valores
    ajax.send("idprofesor="+id+"&nombre="+nom+"&cedula="+ced+"&direccion="+dir+"&telefono1="+tel1+"&telefono2="+tel2+"&correo1="+cor1+"&correo2="+cor2)
}

function eliminarProfesor(idprofesor){
    //donde se mostrar� el resultado de la eliminacion
    divResultado = document.getElementById('verprofesores');
    
    
    //usaremos un cuadro de confirmacion    
    var eliminar = confirm("De verdad desea eliminar este registro?")
    if ( eliminar ) {
        //instanciamos el objetoAjax
        ajax=objetoAjax();
        //uso del medotod GET
        //indicamos el archivo que realizar� el proceso de eliminaci�n
        //junto con un valor que representa el id del empleado
        ajax.open("GET", "eliminacion_profe.php?idprofesor="+idprofesor);
        divResultado.innerHTML= '<img src="anim.gif">';
        ajax.onreadystatechange=function() {
            if (ajax.readyState==4) {
                //mostrar resultados en esta capa
                divResultado.innerHTML = ajax.responseText
            }
        }
        //como hacemos uso del metodo GET
        //colocamos null
        ajax.send(null)
    }
} 

function guardacceso(){
  //valores de las cajas de texto
  materia=document.ingAcceso.materia.value;
  profesor=document.ingAcceso.profesor.value;
  clave=document.ingAcceso.clave.value;
  
        if(clave == ""){
            alert("Debe ingresar una Clave");
            return false;
        }

  //donde se mostrar� lo resultados
  divResultado = document.getElementById('veraccesos');
  divResultado.innerHTML= '<img src="anim.gif">';
  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medoto POST
  //archivo que realizar� la operacion
  //registro.php
  ajax.open("POST", "registro_acceso.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divResultado.innerHTML = ajax.responseText
  //llamar a funcion para limpiar los inputs
  document.ingAcceso.clave.value = "";
  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores
  ajax.send("codmateria="+materia+"&codprofesor="+profesor+"&clave="+clave)
}

function EditarAcceso(idacceso){
    //donde se mostrar� el formulario con los datos
    divFormulario = document.getElementById('edicionAcceso');
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "consulta_por_id_acceso.php");
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="block";
        }
    }
    //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("idemp="+idacceso)
}

function validarEdicionAcceso(){
    //valores de los cajas de texto
           id=document.modAcceso.idmateria.value;
    idmateria=document.modAcceso.materia.value;
   idprofesor=document.modAcceso.profesor.value;
        clave=document.modAcceso.clave.value;

    //alert(id+idmateria+idprofesor+clave);

        if((clave == "") || (idmateria == "") || (idprofesor == "")){
            alert("Debe seleccionar una opci�n o ingresar una clave");
            return false;
        }
    //donde se mostrar� lo resultados
    divResultado = document.getElementById('veraccesos');
    divFormulario = document.getElementById('edicionAcceso');
    divResultado.innerHTML= 'Actualizando...';
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //usando del medoto POST
    //archivo que realizar� la operacion ->actualizacion.php
    ajax.open("POST", "actualizacion_acceso.php",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar los nuevos registros en esta capa
            divResultado.innerHTML = ajax.responseText
            //una vez actualizacion ocultamos formulario
            divFormulario.style.display="none";
        }
    }
    //muy importante este encabezado ya que hacemos uso de un formulario
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando los valores
    ajax.send("idacceso="+id+"&idmat="+idmateria+"&idprof="+idprofesor+"&clav="+clave)
}

function eliminarAcceso(idacceso){
    //donde se mostrar� el resultado de la eliminacion
    divResultado = document.getElementById('veraccesos');
    
    
    //usaremos un cuadro de confirmacion    
    var eliminar = confirm("De verdad desea eliminar este registro?")
    if ( eliminar ) {
        //instanciamos el objetoAjax
        ajax=objetoAjax();
        //uso del medotod GET
        //indicamos el archivo que realizar� el proceso de eliminaci�n
        //junto con un valor que representa el id del empleado
        ajax.open("GET", "eliminacion_acceso.php?idacc="+idacceso);
        divResultado.innerHTML= '<img src="anim.gif">';
        ajax.onreadystatechange=function() {
            if (ajax.readyState==4) {
                //mostrar resultados en esta capa
                divResultado.innerHTML = ajax.responseText
            }
        }
        //como hacemos uso del metodo GET
        //colocamos null
        ajax.send(null)
    }
}

function cargaAlumnos(){    

    divResultado = document.getElementById('cargapagina');
    divResultado.innerHTML= 'Actualizando...';
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //usando del medoto POST
    //archivo que realizar� la operacion ->actualizacion.php
    ajax.open("POST", "SimpleSuggest/suggest.html",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar los nuevos registros en esta capa
            divResultado.innerHTML = ajax.responseText
            //una vez actualizacion ocultamos formulario
            divFormulario.style.display="none";
        }
    }
    //muy importante este encabezado ya que hacemos uso de un formulario
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando los valores
    ajax.send()
}

function consultaAlumno(){
    //donde se mostrar� el formulario con los datos
    divFormulario = document.getElementById('encontrados');
    divFormulario.innerHTML= 'Buscando...';
    consul = document.frmSearch.txtSearch.value;
    //alert(consul);
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "encontrados.php",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="block";
            document.frmSearch.txtSearch.value = "";
            document.frmSearch.txtSearch.focus();
        }
    }
    //enviando el codigo del empleado
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send("nombre="+consul)
    
}

function configuracion(){
    //donde se mostrar� el formulario con los datos
    divFormulario = document.getElementById('formulario');
    year = document.frmyear.year.value;
    periodo = document.frmyear.periodo.value;

    
//    alert(year+periodo);
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "compro_config.php");
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="block";
        }
    }
    //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("year="+year+"&periodo="+periodo)
} 