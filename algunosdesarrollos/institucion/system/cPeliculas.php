<?php 
//referenciamos la clase DBManager
include_once("conexion.php");

//implementamos la clase pelicula
class cPelicula{
 //constructor	
 function cPelicula(){
 }	
 
 // consulta las peliculas de la BD
 function consultar(){
   //creamos el objeto $con a partir de la clase DBManager
   $con = new DBManager;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectar()==true){
   $anio = "select valor from year";
   $q_anio = @mysql_query($anio);
   $f_anio = mysql_fetch_array($q_anio);
   $dd = $f_anio['valor'];
     $query = "select id,nombre,year from grados where year='$dd' order by id";
	 $result = @mysql_query($query);
	 if (!$result)
	   return false;
	 else
	   return $result;
   }
 }

 function traegrado($codigo){
   //creamos el objeto $con a partir de la clase DBManager
   $con = new DBManager;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectar()==true){
     $query = "select id,nombre from grados where id=$codigo";
     $result = @mysql_query($query);
     $fff = mysql_fetch_array($result);
     if (!$result)
       return false;
     else
       return $fff["nombre"];
   }
 }
 
 //inserta una nueva pelicula en la base de datos
 function crear($noms){
     $espa = utf8_decode($noms);
     $con = new DBManager;
   if($con->conectar()==true){
       $dd = date("Y");
     $query = "INSERT INTO grados (nombre,year) 
	 VALUES ('$espa','$dd')";
     $result = mysql_query($query);
     if (!$result)
	   return false;
     else
       return true;
   }
 }
 // actualizar una nueva pelicula en la base de datos
 function actualizar($cod,$nomm){
     $ccod = utf8_decode($cod);
     $nnomm = utf8_decode($nomm);
     $con = new DBManager;
   if($con->conectar()==true) {
     $query = "UPDATE grados SET nombre='$nnomm' 
	 WHERE id=$cod";
     $result = mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 // consulta pelicula por su codigo
 function consultarid($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "SELECT * FROM grados WHERE id=$cod";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return $result;
    }
  
 }
 
  //elimina una pelicula en la base de datos
 function eliminar($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "DELETE FROM grados WHERE id=$cod";
//     $query = "DELETE FROM auxiliar WHERE id_pelicula=$cod";
     $result = @mysql_query($query);
     if (!$result)
	   return false;
     else
       return true;
   }
 }



//implementamos la clase Clientes

 // consulta los clientes de la BD
 function consultarC(){
   //creamos el objeto $con a partir de la clase DBManager
   $con = new DBManager;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectar()==true){
     $query = "select * from clientes order by nombrec";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return $result;
   }
 }
 //inserta un nuevo cliente en la base de datos
 function crearC($nomc,$dire,$telef){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "INSERT INTO clientes (nombrec, direccionc, telefonoc) 
     VALUES ('$nomc','$dire','$telef')";
     $result = mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 // actualizar un nuev cliente en la base de datos
 function actualizarC($cod,$nomc,$direcc,$telefc){
     //$ccod = utf8_decode($cod);
     $nomcc = utf8_decode($nomc);
     $ddirecc = utf8_decode($direcc);
     $ttelefc = utf8_decode($telefc);  
   $con = new DBManager;
   if($con->conectar()==true) {
     $query = "UPDATE clientes SET nombrec='$nomcc', direccionc='$ddirecc', telefonoc='$ttelefc' 
     WHERE id=$cod";
     $result = mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 // consulta clientes por su codigo
 function consultaridC($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "SELECT * FROM clientes WHERE id=$cod";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return $result;
    }
  
 }
 
  //elimina un cliente en la base de datos
 function eliminarC($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "DELETE FROM clientes WHERE id=$cod";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 
 
 ///////MATERIASSSSSSS
 
 function consultarMat(){
   //creamos el objeto $con a partir de la clase DBManager
   $con = new DBManager;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectar()==true){
     $query = "select * from materias order by nombre_mat";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return $result;
   }
 }
 
  function crearMat($nomc){
   $con = new DBManager; 
   $nnomms = utf8_decode($nomc);
   if($con->conectar()==true){
     $query = "INSERT INTO materias (nombre_mat) 
     VALUES ('$nnomms')";
     $result = mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 
  function consultaridMateria($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "SELECT * FROM materias WHERE id=$cod";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return $result;
    }
  
 }
 
  function actualizarMateria($cod,$nomm){
     $ccod = utf8_decode($cod);
     $nnomm = utf8_decode($nomm);
     $con = new DBManager;
   if($con->conectar()==true) {
     $query = "UPDATE materias SET nombre_mat='$nnomm' 
     WHERE id=$cod";
     $result = mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 
  function eliminarMateria($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "DELETE FROM materias WHERE id=$cod";
//     $query = "DELETE FROM auxiliar WHERE id_pelicula=$cod";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 } 
 
 
 //////////////PROFESORES
 
  function consultarProfe(){
   //creamos el objeto $con a partir de la clase DBManager
   $con = new DBManager;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectar()==true){
     $query = "select * from personal_profesores order by nombre";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return $result;
   }
 }
 
   function crearProfe($nom,$ced,$dire,$tel1,$tel2,$cor1,$cor2){
   $con = new DBManager; 
   $nombre = utf8_decode($nom);
   $cedula = $ced;
   $direccion = utf8_decode($dire);
   $telefono1 = $tel1;
   $telefono2 = $tel2;
   $correo1 = $cor1;
   $correo2 = $cor2;
   
   if($con->conectar()==true){
     $query = "INSERT INTO personal_profesores (nombre,cedula,direccion,telefono1,telefono2,correo1,correo2) 
     VALUES ('$nombre','$cedula','$direccion','$telefono1','$telefono2','$correo1','$correo2')";
     $result = mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 
   function consultaridProfesor($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "SELECT * FROM personal_profesores WHERE id=$cod";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return $result;
    }
  
 }
 
   function actualizarProfesor($codigo,$nombre,$cedula,$direccion,$tele1,$tele2,$correo1,$correo2){
     $nom = utf8_decode($nombre);
     $dir = utf8_decode($direccion);
     $tel1 = utf8_decode($tele1);
     $tel2 = utf8_decode($tele2);
     $cor1 = utf8_decode($correo1);
     $cor2 = utf8_decode($correo2);
     $con = new DBManager;
   if($con->conectar()==true) {
     $query = "UPDATE personal_profesores SET nombre='$nom',cedula='$cedula',direccion='$dir',telefono1='$tel1',telefono2='$tel2',correo1='$cor1',correo2='$cor2' 
     WHERE id=$codigo";
     $result = mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 
   function eliminarProfesor($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "DELETE FROM personal_profesores WHERE id=$cod";
//     $query = "DELETE FROM auxiliar WHERE id_pelicula=$cod";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 
     function consultarAccesos(){
   //creamos el objeto $con a partir de la clase DBManager
   $con = new DBManager;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectar()==true){
     $query = "select * from acceso_profe_mate order by id";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return $result;
   }
 }
 
   function crearAcceso($mat,$prof,$cla){
   $con = new DBManager; 
   $clave = utf8_decode($cla);
   if($con->conectar()==true){
            $averigua = "select id_profesor,id_materia,clave from acceso_profe_mate";
            $q_av = @mysql_query($averigua);
            $fetch = mysql_fetch_array($q_av);
            $profe = $fetch["id_profesor"];
            $mater = $fetch["id_materia"];
            $clave = $fetch["clave"];
                if(($clave == $cla)&&($mater == $mat)){
                echo "El acceso a ingresar ya se encuentra registrado<br>";
                
                }else{
       
     $query = "INSERT INTO acceso_profe_mate (id_profesor,id_materia,clave) 
     VALUES ('$prof','$mat','$clave')";
     $result = mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
  } }
 }
 

 
  function traemateria($codigo1){
   //creamos el objeto $con a partir de la clase DBManager
   $con = new DBManager;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectar()==true){
     $query = "select id,nombre_mat from materias where id=$codigo1";
     $result = @mysql_query($query);
     $mater = mysql_fetch_array($result);
     if (!$result)
       return false;
     else
       return utf8_encode($mater["nombre_mat"]);
   }
 }
   function traeprofesor($codigo2){
   //creamos el objeto $con a partir de la clase DBManager
   $con = new DBManager;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectar()==true){
     $query = "select id,nombre from personal_profesores where id=$codigo2";
     $result = @mysql_query($query);
     $profes = mysql_fetch_array($result);
     if (!$result)
       return false;
     else
       return utf8_encode($profes["nombre"]);
   }
 }
 
    function consultaridAcceso($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "SELECT * FROM acceso_profe_mate WHERE id=$cod";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return $result;
    }
  
 }
 
   function actualizarAcceso($codigo,$materia,$profesor,$cla){
     $clave = utf8_decode($cla);
     $con = new DBManager;
   if($con->conectar()==true) {
     $query = "UPDATE acceso_profe_mate SET id_profesor='$profesor',id_materia='$materia',clave='$clave'
     WHERE id=$codigo";
     $result = mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 
    function eliminarAcceso($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "DELETE FROM acceso_profe_mate WHERE id=$cod";
//     $query = "DELETE FROM auxiliar WHERE id_pelicula=$cod";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 } 
 
 function traeyear(){
 $con = new DBManager;
 $t = "select valor from year";
 $qt = mysql_query($t);
 $f = mysql_fetch_array($qt);
    return $f["valor"];
 }
 
}
?>
