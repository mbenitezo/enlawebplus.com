<html>
<head><title>FUNDAMENTO PSICOL&Oacute;GICO - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>FUNDAMENTO PSICOL&Oacute;GICO</b></p></td>
</tr>
<tr>
<td><p align="justify">
El objeto de la psicolog&iacute;a moderna es estudiar y analizar las estructuras cognoscitivas en cuanto al pensamiento y el comportamiento, por que muchos de los fen&oacute;menos t&iacute;picos del comportamiento en la edad escolar pueden explicarse, en parte, al menos, por los progresos cognitivos en esta edad.
<br>
De nuevo el an&aacute;lisis del pensamiento y el comportamiento en t&eacute;rminos cognitivos no contradice, sino que complementa, las interpretaciones din&aacute;micas y psico-anal&iacute;ticas.
<br><br>
Los conocimientos de la psicolog&iacute;a pueden aplicarse en el campo de la pedagog&iacute;a, al campo de la vida escolar y al de los aprendizajes que ella conlleva.
<br><br>
Desde siempre la aplicaci&oacute;n de la psicolog&iacute;a en el campo de la pedagog&iacute;a se concentra en forma de trabajo preciso y pr&aacute;cticas que evitan el fracaso escolar, entendiendo estas palabras como el resultado de un proceso de dificultades para seguir los aprendizajes y que suele culminar en una situaci&oacute;n de fracaso. Tambi&eacute;n se ocupa de la inadaptaci&oacute;n escolar, buscando las causas que se hallan detr&aacute;s de ambos fen&oacute;menos y la b&uacute;squeda de soluciones al respecto.
<br><br>
Por tales fen&oacute;menos se persigue utilizar cierta corriente psico-pedag&oacute;gica que propenda por mejorar nuestro &aacute;mbito escolar en todos los aspectos del ser (afectivo, cognitivo, el hacer, convivencial y social). Es posible que en diversas ocasiones como educadores nos hemos preguntado: &iquest;por qu&eacute; nuestros estudiantes a pensar de que tienen la misma capacidad intelectual aceptable, no dan el mismo rendimiento en todas las asignaturas? <br>Lo primero que debemos hacer como docentes es ampliar y reformular nuestra idea de lo que cuenta como intelecto humano, as&iacute; podemos dise�ar formas m&aacute;s apropiadas de evaluarlo y educarlo.
<br><br>
Es necesario reconciliar un enfoque pluralista de la cognici&oacute;n en el plan desarrollista unilineal de Piaget. De acuerdo con Feldman, un psic&oacute;logo desarrollista orientado a la educaci&oacute;n, los logros cognitivos pueden ocurrir en una serie de dominios. Algunos de ellos, como el l&oacute;gico-matem&aacute;tico que estudio  Piaget, son universales ya que los individuos en todo el mundo, por la sola virtud de que pertenecen a la misma especie, deben confrontarlos y dominarles. 
<br><br>
Pero hay otros dominios que son sumamente idiosincr&aacute;sicos: jugar ajedrez. La pericia de resolver crucigramas, no es esencial en ninguna divisi&oacute;n de la sociedad, aunque algunos alcanzan grandes logros en estos dominios, dentro de una cultura  particular. La teor&iacute;a de Howard  Gardner de las  INTELIGENCIAS M&Uacute;LTIPLES tiene una gran importancia para los maestros, pues nos permite identificar las caracter&iacute;sticas de cada uno  de esos tipos de inteligencia y como desarrollarlas en el aula.
<br><br>
Es por eso que nuestra Instituci&oacute;n busca a trav&eacute;s de  el empleo de ciertos enfoqu&eacute;s psico - anal&iacute;ticos propender por el desarrollo de las inteligencias como son: La inteligencia ling�&iacute;stica para el desarrollo de las habilidades ling�&iacute;sticas, la inteligencia musical en aras del desarrollo de la composici&oacute;n,  la inteligencia l&oacute;gico-matem&aacute;tica, la inteligencia espacial, la inteligencia cinest&eacute;sicocorporal y el desarrollo de las inteligencias personales para que nuestros estudiantes puedan desenvolverse de manera competente y efectiva dentro del contexto social que ha escogido o debe escoger - vivir.

</p>
</td>
</tr>
</table>
</body>
</html>