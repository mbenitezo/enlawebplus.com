<html>
<head><title>PERFILES - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<style>
LI { list-style: url(li.png)   }
</style>
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>PERF&Iacute;L DE LOS ASPIRANTES A LA PERSONER&Iacute;A ESTUDIANTIL</b></p></td>
</tr>
<tr>
<td><p align="justify">
El personero como l&iacute;der de la Instituci&oacute;n Educativa debe reunir y afianzar ciertas cualidades humanas (ser), acad&eacute;micas (saber) y calidades t&eacute;cnicas (hacer).
<br><br>
Ha de ser l�der con gran calidad humana, comprometido con la solidaridad, la tolerancia, la participaci&oacute;n, y la equidad para el desarrollo de adecuadas relaciones interpersonales.
<br><br>
El personero necesita:
<br><br>
Demostrar buen comportamiento, buen rendimiento acad&eacute;mico, respetuoso con sus compa&ntilde;eros, padres de familia, docentes y dem&aacute;s personas que laboran en la Instituci&oacute;n educativa; con sus actuaciones dejar en alto la Instituci&oacute;n Educativa en todos los lugares dentro y fuera de ella.
<br><br>
<b>SABER Y CONOCER</b>
<br>
<ul>

<li>
Sus roles y competencias.
</li>
<li>
El manual de convivencia y el proyecto educativo institucional.
</li>
<li>
La realidad de su comunidad educativa.
</li>
<li>
La Constituci&oacute;n Pol&iacute;tica de Colombia.
</li>
<li>
La Ley general de Educaci&oacute;n (115/94), y su decreto reglamentario 1860 del 94. La Ley 715 de 2002.
</li>
<li>
Los espacios y mecanismos de participaci&oacute;n democr&aacute;tica.
</li>
<li>
Mecanismos de defensa de los derechos (derechos de petici&oacute;n, acci&oacute;n de tutela, acci&oacute;n de cumplimiento, recuso de reposici&oacute;n y recurso de apelaci&oacute;n).
</li>
<li>
La ley de la juventud.
</li>
<li>
Los derechos humanos y el derecho internacional humanitario.
</li>
<li>
Metodolog&iacute;as para el manejo dialogado de los conflictos.
</li>
<li>
Metodolog&iacute;as para el manejo dialogado de los conflictos.
</li>
</ul>
<br>
<b>HACER</b>
<br>
<ul>

<li>
Promover la defensa de los derechos y el cumplimiento de los deberes de los estudiantes.
</li>
<li>
Desarrollar estrategias de convivencia democr&aacute;tica y la resoluci&oacute;n pac&iacute;fica de los conflictos.
</li>
<li>
Escuchar, orientar y apoyar a los estudiantes en los problemas con sus familias, compa&ntilde;eros de estudio y otros miembros  de la comunidad educativa.
</li>
<li>
Servir de mediadores entre sus compa&ntilde;eros, educadores y directivos de la instituci&oacute;n educativa.
</li>
<li>
Interactuar con otras personer&iacute;as y actores locales.
</li>
<li>
Formular proyectos y gestionarlos para el mejoramiento de la convivencia en la instituci&oacute;n.
</li>
<li>
Participar en el comit&eacute; animador del municipio si lo hay.
</li>
</ul>

<br>
<b>TENER</b>
<br>
<ul>
<li>
Un comit&eacute; de apoyo que lo acompa&ntilde;e, oriente y ayude a cumplir con sus funciones en la Instituci&oacute;n Educativa.(consejo de estudiantes).
</li>
<li>
Un proyecto de desarrollo de la personer&iacute;a como espacio program&aacute;tico.
</li>
<li>
Un espacio f&iacute;sico en al instituci&oacute;n que le permita un interacci&oacute;n constante con sus compa&ntilde;eros.
</li>
<li>
Dotaci&oacute;n que incluya los elementos necesarios para que la personer&iacute;a desempe&ntilde;e sus funciones.
</li>                                                                                                            
</ul>

</p>
</td>
</tr>
</table>
</body>
</html>