<html>
<head><title>VISI&Oacute;N - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>VISI&Oacute;N</b></p></td>
</tr>
<tr>
<td><p align="justify">
En el 2015 la INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER t&eacute;cnica en medio ambiente, ser&aacute; l&iacute;der a nivel Municipal, del Bajo Cauca y de Antioquia en la formaci&oacute;n integral de personas, capaces de gestionar y liderar proyectos ambientales productivos, comprometidas con el manejo adecuado, recuperaci&oacute;n y conservaci&oacute;n de los recursos naturales de su entorno socio ambiental, desarrollando valores y actitudes que promuevan un comportamiento dirigido hacia la transformaci&oacute;n superadora de la realidad tanto en sus aspectos naturales como sociales.
</p>
</td>
</tr>
</table>
</body>
</html>