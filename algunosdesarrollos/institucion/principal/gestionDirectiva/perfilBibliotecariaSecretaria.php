<html>
<head><title>PERFILES - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<style>
LI { list-style: url(li.png)   }
</style> 
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>PERF&Iacute;L DE LA BIBLIOTECARIA Y SECRETARIA</b></p></td>
</tr>
<tr>
<td>
<ul style="margin:0; padding:0;">
<li><p align="justify">
Poseer excelentes relaciones humanas que faciliten el servicio a la comunidad educativa.
</p>
</li>

<li><p align="justify">
Buena presentaci&oacute;n personal.
</p>
</li>

<li><p align="justify">
Alto nivel de eficiencia.
</p>
</li>

<li><p align="justify">
Ser leales, honrados, honestos y serviciales.
</p>
</li>

<li><p align="justify">
Ser discreto (a), tolerante, prudente en la prestaci&oacute;n del servicio.</p>
</li>

<li><p align="justify">
Respetuoso, comprensivo y con capacidad de organizaci&oacute;n, orden y pulcritud en su lugar de trabajo.</p>
</li>
</ul>

</td>
</tr>
</table>
</body>
</html>