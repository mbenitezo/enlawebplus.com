<html>
<head><title>PERFILES - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<style>
LI { list-style: url(li.png)   }
</style> 
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>PERF&Iacute;L DEL SER HUMANO QUE SE QUIERE FORMAR</b></p></td>
</tr>
<tr>

<td>
<ul>

<li><p align="justify">El ser humano que se quiere formar, como el ciudadano del ma&ntilde;ana, comprometido con el desarrollo de su comunidad y de su regi&oacute;n, permitiendo la integraci&oacute;n de los entes, dise&ntilde;ando estrategias adecuadas de consolidaci&oacute;n acad&eacute;mica y de trabajo.</p></li>

<li><p align="justify">
Adoptar un comportamiento responsable sobre el uso adecuado de los recursos naturales.
</p></li>

<li><p align="justify">
Cr&iacute;tico al actuar.
</p></li>

<li><p align="justify">
Que sea impulsador de pol&iacute;ticas que tengan que ver con el mejoramiento de la calidad de vida de las personas especialmente de las menos favorecidas.
<br>Interesado en recobrar informaci&oacute;n, sobre la evoluci&oacute;n de las explicaciones científicas a problemas de los seres humanos, y en general a todas aquellas personas que necesitan la ayuda de otros para su desarrollo cultural.
</p></li>

<li><p align="justify">
Persona activa en el trabajo en equipo.
</p></li>

<li><p align="justify">
Persona conocedora de la estructura y funcionamiento de su Instituci&oacute;n y el cuidado que debe tener  para no contaminarlo, ni deteriorarlo.
</p></li>

<li><p align="justify">
El estudiante al ser el centro del proceso educativo, es el resultado que la comunidad integralmente debe formar, un L&Iacute;DER SOCIO AMBIENTAL.
</p></li>

<li><p align="justify">
Persona con visi&oacute;n emprendedora, humanista, social, ecol&oacute;gica con alto sentido &eacute;tico, investigativo y de creatividad; comprometido con la b&uacute;squeda de la satisfacci&oacute;n de las necesidades de la regi&oacute;n mediante la utilizaci&oacute;n adecuada de la ciencia, la tecnolog&iacute;a y la t&eacute;cnica.
</p></li>

<li><p align="justify">
Un l&iacute;der social que orienta positivamente a su comunidad y contribuya con la participaci&oacute;n comunitaria para el desarrollo sostenible de su regi&oacute;n.
</p></li>

<li><p align="justify">
Una persona que integra habilidades y saberes fomentando la mentalidad empresarial.
</p></li>
</ul> 

</td>
</tr>

</table>
</body>
</html>