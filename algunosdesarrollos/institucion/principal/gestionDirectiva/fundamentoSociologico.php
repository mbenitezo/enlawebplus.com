<html>
<head><title>FUNDAMENTO SOCIOL&Oacute;GICO - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>FUNDAMENTO SOCIOL&Oacute;GICO</b></p></td>
</tr>
<tr>
<td><p align="justify">
La educaci&oacute;n siempre ha tendido a ofrecer elementos orientadores a la sociedad, sobre la base de sus an&aacute;lisis cr&iacute;ticos, prospectivos, y constructivos.
<br><br>
Al encaminar un periodo de trabajo muy significativo en la formaci&oacute;n de j&oacute;venes volcados para la formaci&oacute;n integral de un pueblo, se pregunta y analiza el c&oacute;mo continuar&aacute; enfrentando los desaf&iacute;os que le presenta la sociedad actual de manera que se vinculen activa y efectivamente en los procesos de cambio que comienzan o requieren la sociedad (comunidad) en la cual act&uacute;a.
<br> <br>
Nunca antes el mundo experiment&oacute; cambios tan acelerados y continuos. Cambios que alteran las funciones tradicionales y presentan nuevos problemas a todos los miembros de la sociedad.

En este medio, la educaci&oacute;n recibe el impacto de las inseguridades, preocupaciones, posibilidades que las nuevas situaciones ofrecen. Educar en una &eacute;poca de crisis y transici&oacute;n en donde no se pierda la perspectiva humana pareciera ser una de los nuevos desaf&iacute;os.
<br><br>
Debemos pensar en la reestructuraci&oacute;n de la sociedad por medio de la educaci&oacute;n aceptaci&oacute;n, aceptando la crisis como un componente de la vida actual y extrayendo de ella los aspectos positivos.
<br><br>

Toda reflexi&oacute;n sobre educaci&oacute;n exige una exig&iacute;s sobre el hombre y el de sus condiciones y situaci&oacute;n cultural, de ah&iacute; la importancia de reconocer que no hay una educaci&oacute;n fuera de la sociedad, ni tampoco los hombres viven aislados.
<br>
El hombre se hace consciente de ello y por eso se educa, reflexiona sobre una realidad concreta y busca permanentemente ser m&aacute;s. En ello est&aacute; la base de la educaci&oacute;n, de ah&iacute; que es sujeto de su propia educaci&oacute;n y no objeto.
<br><br>
La educaci&oacute;n tiene un car&aacute;cter permanente, todos estamos educ&aacute;ndonos, distinguiendo ciertos grados de educaci&oacute;n, pero ellos no son absolutos, no existen pues personas educadas, tampoco existen ignorantes absolutos. El saber y la ignorancia son relativos.
<br><br>
La educaci&oacute;n es ante todo fundamento de lo social. La educaci&oacute;n no es s&oacute;lo un instrumento de la cultura, ayuda a definir y moldear al hombre hacedor de cultura. La educaci&oacute;n ha cumplido papeles espec&iacute;ficos en cada momento de nuestra historia. A cada situaci�n y condici&oacute;n de desarrollo corresponde una educaci&oacute;n. 
<br><br>
En la Instituci&oacute;n Educativa Francisco de Paula Santander, luchamos por ofrecer una educaci&oacute;n que ofrezca al estudiante la posibilidad de vivir mejor en un mundo t�cnico e industrializado. Preocupa y produce cierta perplejidad al no saber como inferir en la conducci&oacute;n de acontecimientos que se imponen con tanta fuerza y a veces de manera desmedida, por fortuna pasamos la &eacute;poca en la que se crey&oacute; que la educaci&oacute;n todo lo pod&iacute;a, hoy sabemos que la educaci&oacute;n por s&iacute; sola no consigue realizar lo que de ella se espera. El gran desaf&iacute;o es el de rescatar las tareas de la educaci&oacute;n dentro de una nueva estrategia de cambio social.
</p>
</td>
</tr>
</table>
</body>
</html>