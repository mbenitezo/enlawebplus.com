<html>
<head><title>PROYECCI&Oacute;N Y DEFINICI&Oacute;N DE OPORTUNIDADES. - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>PROYECCI&Oacute;N Y DEFINICI&Oacute;N DE OPORTUNIDADES.</b></p></td>
</tr>
<tr>
<td><p align="justify">
Proyectar en lo pedag&oacute;gico implica ver hacia el futuro respondiendo a una comprensi&oacute;n del presente que contribuya a lo que la &eacute;poca y la sociedad demandan. Proyectar es una forma de optar; elegir opciones frente al conocimiento,  frente a la calidad, frente a la pedagog&iacute;a, frente a la comunidad, frente a las metodolog&iacute;as e, incluso frente a &eacute;tico y pol&iacute;tico. 
<br><br>
La Instituci&oacute;n Educativa Francisco de Paula Santander t&eacute;cnica en Medio Ambiente se proyecta  a su comunidad  mediante el programa que ofrece a sus educandos en recursos naturales, con miras a que estos ayuden a la generaci&oacute;n de empleo y a la reactivaci&oacute;n  y mejoramiento de la actividad productiva. estructurando en sus programas una orientaci&oacute;n empresarial, liderando proyectos para la incorporaci&oacute;n y el desarrollo de las personas en actividades de  producci&oacute;n, conservaci&oacute;n, restauraci&oacute;n o aprovechamiento racional de los recursos que contribuyan al desarrollo social, econ&oacute;mico, cultural, ambiental y tecnol&oacute;gico de su regi&oacute;n.
<br><br>
Como apoyo a este programa se realizan las siguientes actividades:
<br><br>
Reinado Socio ambiental: El cual se realiza con el objetivo de fomentar la toma de conciencia por la reutilizaci&oacute;n y uso apropiado de los residuos s&oacute;lidos. Evento que convoca a la comunidad en general y donde se tiene la oportunidad de apreciar la creatividad art&iacute;stica mediante la elaboraci&oacute;n de hermosas carrozas y trajes confeccionados con elementos  reciclados.
<br><br>
El proyecto pedag&oacute;gico del PRAES es otro medio de proyecci&oacute;n a la comunidad ya que a trav&eacute;s de el se realizan diferentes actividades durante el a&ntilde;o como son: limpieza del cauce de  las quebradas, campa&ntilde;a de recolecci&oacute;n y manejo adecuado de residuos s&oacute;lidos, campa&ntilde;as de arborizaci&oacute;n, campa&ntilde;as de sensibilizaci&oacute;n sobre la conservaci&oacute;n de la fauna y la flora y actividades culturales referentes al medio ambiente.
</p>
</td>
</tr>
</table>
</body>
</html>