<html>
<head><title>FUNDAMENTO ANTROPOL&Oacute;GICO - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>FUNDAMENTO ANTROPOL&Oacute;GICO</b></p></td>
</tr>
<tr>
<td><p align="justify">
La raz&oacute;n de ser del hombre y la humanidad se plantean como &quot;ser en el mundo con los otros&quot; y as&iacute; lograr la madurez o personalidad como el progreso de la humanidad.
<br>
<br>
El hombre moderno parece haber perdido la esperanza de resolver el interrogante de su vida y con ello el problema se agudiza debido a tanta miseria humana que lleva la hombre a comportarse cada vez m&aacute;s inhumanamente de ah&iacute; las angustias y vac&iacute;os que se vuelven m&aacute;s acrecientes por no saberse de donde viene y para donde va.
<br>
<br>
Hablar del hombre como ser cultural y como ser hist&oacute;rico es necesario ya que la cultura es un elemento muy de la especie humana, esta variable cultural es de gran importancia para el hombre que se encuentra en un grupo social.
<br>
<br>
Dentro de la visi&oacute;n integral del ser humano es menester considerar la estructura social, en la cual los individuos se desenvuelven e interrelacionan entre s&iacute;. En nuestra sociedad se dan relaciones de distinto orden entre las personas. Son las llamadas relaciones sociales.
<br>
Las personas tambi&eacute;n tienen distintos roles dentro de un grupo social, que es importante considerar. Existen educadores, estudiantes administradores, m�dicos, comerciantes, agricultores, pescadores, mineros, empleados, entre docenas de oficios y empleos. Pero igual hay roles o papeles que desempe�an las personas en organizaciones menos estructuradas como curanderos, l&iacute;deres comunales, cacharreros. Toda esta variable social y cultural tiene una estrecha relaci&oacute;n entre s&iacute; y en algunas cosas no es f&aacute;cil distinguir una de otra.
<br>
<br>
Esta variable socio-cultural abarca un conjunto de ideas valores, sentimientos y practicas de un pueblo que en un momento dado, condiciona al ser que la sociedad debe re-crear en cada individuo a trav&eacute;s de la educaci&oacute;n. S&oacute;lo as&iacute; podr&aacute; la sociedad afianzar su identidad  y fundamentar su comunidad.
<br>
<br>
De la misma forma, s&oacute;lo as&iacute;, el individuo podr&aacute; experimentar como miembro realmente integrado a su sociedad.
<br>
<br>
Nuestra Instituci&oacute;n Educativa a trav&eacute;s del proceso educativo como medio utilizado por la sociedad para moldear sus nuevas generaciones, necesariamente tendr&aacute; que ajustarse a esta historicidad cultural del hombre y a las exigencias de la cultura a&uacute;n y con toda la nueva concepci&oacute;n de la educaci&oacute;n como proceso integral del educando profundizando la experiencia personal y la de la sociedad, para lograr su madurez y la transformaci&oacute;n de la realidad social que la circunda.
</p>
</td>
</tr>
</table>
</body>
</html>