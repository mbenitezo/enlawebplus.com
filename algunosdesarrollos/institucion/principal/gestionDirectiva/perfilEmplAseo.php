<html>
<head><title>PERFILES - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<style>
LI { list-style: url(li.png)   }
</style> 
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>PERF&Iacute;L DE LOS EMPLEADOS DEL ASEO</b></p></td>
</tr>
<tr>
<td>
<ul style="margin:0; padding:0;">
<li><p align="justify">
Gran sentido de pertenencia en la instituci&oacute;n.
</p>
</li>

<li><p align="justify">
Din&aacute;micos y oportunos.
</p>
</li>

<li><p align="justify">
Con sentido de responsabilidad y compromiso.
</p>
</li>

<li><p align="justify">
Organizados y con capacidad de eficiencia en el manejo de los recursos.
</p>
</li>

<li><p align="justify">
Alto nivel de prudencia y discreci&oacute;n.</p>
</li>

<li><p align="justify">
De excelentes relaciones humanas.</p>
</li>

<li><p align="justify">
Honestos, honrados y leales.</p>
</li>
</ul>

</td>
</tr>
</table>
</body>
</html>