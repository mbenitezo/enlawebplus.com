<html>
<head><title>INTEGRACI&Oacute;N DE EQUIPOS DE TRABAJO. - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>INTEGRACI&Oacute;N DE EQUIPOS DE TRABAJO.</b></p></td>
</tr>
<tr>
<td><p align="justify">
La Instituci&oacute;n Educativa Francisco de Paula Santander T&eacute;cnica en Medio Ambiente, Tiene conformados unos equipos de trabajo organizados seg�n las &aacute;reas del conocimiento, los cuales tienen como fin organizar, estructurar, evaluar y articular la gesti&oacute;n directiva, la gesti&oacute;n administrativa y financiera, la gesti&oacute;n acad&eacute;mica y pedag&oacute;gica y la gesti&oacute;n de comunidad y convivencia en aras de lograr la calidad educativa.
</p>
<br>

<table border="1" width="700" align="center">
<tr>
<td colspan="5"><p align="center"><b>MESAS SE TRABAJO</b></p></td>
</tr>
<tr>
<td><div align="center"><b>Mesa de Matem&aacute;ticas</b></div></td>
<td><div align="center"><b>Mesa de Humanidades</b></div></td>
<td><div align="center"><b>Mesa de Humanidades Lengua Extranjera</b></div></td>
<td><div align="center"><b>Mesa de Educaci&oacute;n F&iacute;ca</b></div></td>
<td><div align="center"><b>Mesa de Ciencias Sociales</b></div></td>
</tr>
<tr>
<td><div align="justify">Edinson Benitez<br>Evelio Renter&iacute;a<br>Mar&iacute;a  E. Zapata<br>Dar&iacute;o Gaviria</div></td>
<td><div align="justify">Carlos Bedoya<br>Jorge Mosquera<br>Digna Herrera<br>Maritza Garc&iacute;a</div></td>
<td><div align="justify">Francisco Perea<br>Arsecio R&iacute;os<br>Sodad Gonz&aacute;lez<br>Martha Londo&ntilde;o</div></td>
<td><div align="justify">Jos&eacute; Caicedo<br>Olga Luc&iacute;a Roa<br>Enrique Franco<br>Day Serna</div></td>
<td><div align="justify">Soraida  Molina<br>Dar&iacute;o Zuleta<br>Dasney Caro<br>Nicol&aacute;s Coutt&iacute;n</div></td>
</tr>
<tr>
<td><div align="center"><b>Mesa de Ciencias Naturales</b></div></td>
<td><div align="center"><b>Mesa de &Eacute;tica y Valores</b></div></td>
<td><div align="center"><b>Mesa de Educaci&oacute;n Art&iacute;stica</b></div></td>
<td><div align="center"><b>Mesa de Religi&oacute;n</b></div></td>
<td><div align="center"><b>Mesa de Tecnolog&iacute;a</b></div></td>
</tr>
<tr>
<td><div align="justify">Nubia Perlaza<br>Humberto Mosquera<br>Marina Palacio<br>Manuel Renter&iacute;a</div></td>
<td><div align="justify">Mat&iacute;as Mosquera<br>Teodolinda Perlaza<br>Elena Restrepo</div></td>
<td><div align="justify">Marlon G&oacute;mez<br>Fanny Restrepo<br>Luisa F. Pino</div></td>
<td><div align="justify">Armando Pinto<br>Rosa Murillo<br>Doris Cardona<br>Mar&iacute;a Palacio</div></td>
<td><div align="justify">Nelida Tamayo<br>Oliva Castrill&oacute;n<br>Berenice Piedrahita</div></td>
</tr>

</table>

</td>
</tr>
</table>
</body>
</html>