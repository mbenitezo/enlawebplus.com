<html>
<head><title>PROCESOS COMUNICATIVOS - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<style>
LI { list-style: url(li.png)   }
</style> 

<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>PROCESOS COMUNICATIVOS</b></p></td>
</tr>
<tr>
<td><p align="justify">
Un principio fundamental para el desarrollo del Proyecto Educativo Institucional lo constituye la construcci&oacute;n de la comunidad educativa.
<br>
En el desarrollo de la nueva educaci&oacute;n, los procesos comunicativos no se desarrollan internamente &uacute;nicamente en las instituciones, forman parte de este proceso los padres, docentes, estudiantes y directivos, la instituci&oacute;n educativa trasciende sus l&iacute;mites espaciales para establecer relaciones con las familias, los l&iacute;deres comunitarios, las organizaciones e instituciones de su entorno. De esta manera se logra conformar una comunidad educativa representativa.
<br><br>
Cada comunidad educativa determina formas de participaci&oacute;n para que se den dichos procesos comunicativos a trav&eacute;s del di&aacute;logo permanente con la Junta Municipal de Educaci&oacute;n (JUME) y el gobierno escolar as&iacute; como con los mecanismos que regulen la toma de decisiones. Todo esto en aras de que las comunidades:
<br>
<ul style="margin:0; padding:0;">

<li><p align="justify">
Participen en los foros educativos y en los debates sobre actividades escolares.
</p></li>
<li><p align="justify">
Apoyen el trabajo educativo
</p></li>
 
<li><p align="justify">
Acuerden estrategias y programas que mejoren la educaci&oacute;n y, por ende, a las mismas comunidades</p></li> 
<li><p align="justify">
Sean gestoras de  cultura.
</p></li>
</ul>

<p align="justify">
La instituci&oacute;n, a trav&eacute;s de estos procesos comunicativos se dirige a la comunidad para reconocer lo que posee, requiere y busca, as&iacute; como para brindar un enriquecimiento  en t&eacute;rminos de posibilidades de mejoramiento de lo existente, de este modo la instituci&oacute;n educativa aprende de la comunidad, se apropia y transforma lo que ella le ofrece. En correspondencia, esta instituci&oacute;n le ofrece sus reflexiones y conocimientos orientados a un desarrollo propio y de la comunidad.
<br><br>

En este proceso comunicativo todos los miembros de la comunidad tienen mucho que decir y hacer para mejorar el ambiente educativo y construir el sentido de la acci&oacute;n institucional y de la misma comunidad. Se hace necesario  que haya un acercamiento entre la instituci&oacute;n y la comunidad, a fin de generar unas relaciones y un conocimiento mutuo que permitan el enriquecimiento de ambas partes.
<br><br>

El proceso comunicativo escuela- comunidad no puede ser solamente formal,  ya que no se aprovechan otras formas de vivir en comunidad, las cuales le aportan al desarrollo comunitario y estimulan el  inter&eacute;s de los miembros.

</p>
</td>
</tr>
</table>
</body>
</html>