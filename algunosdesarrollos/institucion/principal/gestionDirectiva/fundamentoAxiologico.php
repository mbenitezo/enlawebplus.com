<html>
<head><title>FUNDAMENTO AXIOL&Oacute;GICO - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>FUNDAMENTO AXIOL&Oacute;GICO</b></p></td>
</tr>
<tr>
<td><p align="justify">
El estudio de la historia nos muestra c&oacute;mo cada sociedad forma y protege el crecimiento del tipo de hombre y mujer m&aacute;s acorde con las caracter&iacute;sticas de esa sociedad que lo concibe, puesto que ese hombre o mujer ser&aacute;n los encargados de mantenerla y soportarla como es, respetando sus caracter&iacute;sticas y manteniendo sus valores.
<br><br>
Para el mantenimiento eficaz de una sociedad es necesario poseer un sistema de valores claro y consistente, el cual debe ser compartido con la mayor&iacute;a de sus miembros.
<br>
Un valor es la creencia u opini&oacute;n de un individuo o grupo social, es aquello que se le da sentido a la vida, por lo que vale la pena luchar, es una  preferencia justificada por una experiencia previa, para formar un valor, este primero se elige, luego se aprecia y por &uacute;ltimo se practica. Los valores no son absolutos, son relativos y dependen del tiempo, el espacio, las experiencias previas, la forma en que son interpretados y la naturaleza humana.
<br><br>
La familia es el ambiente que influye m&aacute;s significativamente en el desarrollo del individuo, ya que es a partir de la convivencia diaria que el ni�o y la ni�a aprenden a integrar costumbres y valores que se comparten dentro del n&uacute;cleo familiar. Es necesario que la familia asuma valores que orienten el curso de la vida familiar. Pero no podemos desconocer que nuestras familias pasan por momentos de cambios valorativos y entonces no podemos esperar, en la actualidad, que esta educaci&oacute;n sea solamente a nivel familiar, la cual, por supuesto, es fundamental; pero la realidad supera la teor&iacute;a &iquest;c&oacute;mo llevar estos valores a las familias desintegradas, a los hu&eacute;rfanos, a quienes tuvieron por progenitores seres deformes moralmente? El cuestionamiento es: &iquest;c&oacute;mo llegar masivamente a una educaci&oacute;n superior de valores?, entonces requerimos de una abundante generaci&oacute;n de l&iacute;deres en todos los &aacute;mbitos: pol&iacute;tico, social, religioso, cultural, deportivo, educativo; que abarque todo los campos de la actividad humana, y para ello, el compromiso de quienes estamos conscientes de esta soluci&oacute;n es contribuir a forjar personas con valores. 
<br><br>
Si deseamos modificar la realidad actual y aspirar a erradicar los grandes detractores de la humanidad: miseria, ignorancia, guerra, abandono, narcotr&aacute;fico, explotaci&oacute;n irracional de la naturaleza y del hombre, tendremos que provocar un renacimiento moral y luchar por cambiar con valores el fondo de la persona. Nadie puede dar de lo que no tiene. No podemos pedir honestidad, veracidad, generosidad, lealtad, respeto por el otro y por si mismo, amor, si antes no hemos sembrado profundamente estas verdades y valores en el esp&iacute;ritu humano.
<br><br>
Los valores se convierten en virtudes cuando logramos aterrizar el valor en acci&oacute;n. Para que la pr&aacute;ctica sea una realidad, el desaf&iacute;o es traducir la teor&iacute;a en pr&aacute;ctica cotidiana &#147;existen potencialidades en el ser humano, que solamente pueden surgir cuando son iluminadas por los valores&#148;.
</p>
</td>
</tr>
</table>
</body>
</html>