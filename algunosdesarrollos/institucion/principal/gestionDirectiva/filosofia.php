<html>
<head><title>FILOSOF&Iacute;A - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>FILOSOF&Iacute;A EDUCATIVA DEL PLANTEL</b></p></td>
</tr>
<tr>
<td><p align="justify">
La educaci&oacute;n se considera como un proceso contin&uacute;o permanente que busca la realizaci&oacute;n del hombre.
<br>
La acci&oacute;n educativa esta basada en las caracter&iacute;sticas y necesidades sentidas por los grupos sociales de alumnos y alumnas, padres de familias y comunidad en general, que se convierten en situaciones de aprendizajes orientadas hacia el mejoramiento de las condiciones de vida y deben propiciar la participaci&oacute;n activa de cada uno de los miembros del grupo.
<br><br>
Nuestra Instituci&oacute;n busca formar personas que:
<br>
<ul type="square">
<li>
Contribuyan al desarrollo equilibrado del individuo y de la sociedad sobre la base del respeto por la vida y los derechos humanos.
</li>
<br><li>
Estimulen la formaci&oacute;n de actitudes y h&aacute;bitos que favorezcan la conservaci&oacute;n de la salud f&iacute;sica y mental de la persona y el uso racional del tiempo.
</li>
<br><li>
Fomenten en la persona el esp&iacute;ritu de defensa, conservaci&oacute;n, recuperaci&oacute;n y utilizaci&oacute;n de los recursos naturales y de los bienes y servicios de la comunidad.
</li>
<br><li>
Desarrollen su capacidad y mentalidad empresarial.
</li>
<br><li>
Sean capaces de gestionar y liderar proyectos productivos que redunden en beneficio de la comunidad.
</li>
</ul>
</p>
<br>
<p align="justify">
Con este perfil se obtendr&aacute; una persona con elementos conceptuales y actitudinales para que pueda intervenir adecuadamente en la toma de decisiones sobre el manejo de los recursos naturales y del ambiente, guiando a la comunidad hacia la toma de conciencia de su realidad global, del tipo de relaciones que los hombres establecen entre s&iacute; y con la naturaleza y de los problemas derivados de dichas relaciones y sus causas profundas.
<br>
Desarrollando valores y actitudes que promuevan un comportamiento dirigido hacia la transformaci&oacute;n superadora de la realidad tanto en sus aspectos naturales como sociales.
</p>
</td>
</tr>
</table>
</body>
</html>