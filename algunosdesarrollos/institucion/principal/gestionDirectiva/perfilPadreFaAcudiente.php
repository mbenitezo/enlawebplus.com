<html>
<head><title>PERFILES - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<style>
LI { list-style: url(li.png)   }
</style> 
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>PERF&Iacute;L DEL PADRE DE FAMILIA Y/O ACUDIENTE</b></p></td>
</tr>
<tr>
<td>
<ul style="margin:0; padding:0;">
<li><p align="justify">
Nuestros padres deben ser: Comprensivos, responsables, ejercedores de autoridad sobre sus hijos, abiertos al di&aacute;logo, part&iacute;cipes de los actos formativos y acad&eacute;micos, conocedores de la misi&oacute;n, visi&oacute;n y filosof&iacute;a de la Instituci&oacute;n, en s&iacute; comprometidos con la instituci&oacute;n.
</p>
</li>

<li><p align="justify">
Ser constantes con los compromisos adquiridos, hacer que sus hijos se vinculen a los procesos.
</p>
</li>

<li><p align="justify">
Que conozcan y se comprometan con visi&oacute;n, misi&oacute;n y filosof&iacute;a de la instituci&oacute;n.
</p>
</li>

<li><p align="justify">
Ser orientadores y promotores de la formaci&oacute;n en sus hijos a trav&eacute;s del di&aacute;logo, la concertaci&oacute;n y la soluci&oacute;n.
</p>
</li>

<li><p align="justify">
Que respeten con sus actuaciones y actitudes la instituci&oacute;n.</p>
</li>

<li><p align="justify">
Fomentar el respeto por la instituci&oacute;n.</p>
</li>

<li><p align="justify">
Que sea l&iacute;der en las relaciones docentes y estudiantes.</p>
</li>

<li><p align="justify">
Responsables con las funciones de acudiente que tienen.</p>
</li>

</ul>

</td>
</tr>
</table>
</body>
</html>