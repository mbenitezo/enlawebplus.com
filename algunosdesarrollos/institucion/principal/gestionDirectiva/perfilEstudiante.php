<html>
<head><title>PERFILES - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<style>
LI { list-style: url(li.png)   }
</style> 
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>PERF&Iacute;L DEL ESTUDIANTE</b></p></td>
</tr>
<tr>

<td>
<ul style="margin:0; padding:0;">
<li><p align="justify">
El estudiante al ser &#147;el centro del proceso educativo&#148;, es el resultado que la comunidad integralmente debe formar: un L&iacute;der Socio Ambiental.
</p></li>

<li><p align="justify">
Persona con visi&oacute;n emprendedora, humanista, social, ecol&oacute;gica, con alto sentido &eacute;tico, investigativo y de creatividad; comprometido con la b&uacute;squeda de la satisfacci&oacute;n de las necesidades de la regi&oacute;n, mediante la utilizaci&oacute;n adecuada de la ciencia, la tecnolog&iacute;a y la t&eacute;cnica.
</p></li>

<li><p align="justify">
Un l&iacute;der social que orienta positivamente a su comunidad y construye con la participaci&oacute;n comunitaria para el desarrollo sostenible de la regi&oacute;n.
</p></li>


<li><p align="justify">
Una persona que integra habilidades y saberes fomentando la mentalidad empresarial.
</p></li>

<li><p align="justify">
Tiene visi&oacute;n del desarrollo sostenible a partir del conocimiento y aplicaci&oacute;n del R&eacute;gimen Legal del Medio Ambiente.</p></li>

<li><p align="justify">
Conocedor de las estructuras b&aacute;sicas de la composici&oacute;n y funcionamiento del municipio a partir del ordenamiento territorial, de los planes de desarrollo municipal.
</p></li>

<li><p align="justify">
Persona capaz de generar desarrollo de su comunidad, a partir de t&eacute;cnicas agroecol&oacute;gicas, artesanales y ecotur&iacute;sticas.
</p></li>

<li><p align="justify">
Una persona capaz de satisfacer las necesidades b&aacute;sicas mediante el uso adecuado de los recursos existentes, el respeto por la cultura local y la soluci&oacute;n efectiva y cient&iacute;fica a las situaciones que afecten su comunidad.
</p></li>

<li><p align="justify">
Persona capaz de establecer di&aacute;logo fraternal entre el hombre y la naturaleza y el hombre y sus semejantes.
</p></li>

<li><p align="justify">
Persona que desarrolla sus capacidades f&iacute;sicas, afectivas, intelectuales, art&iacute;sticas, morales y religiosas a la luz de la sencillez y transparencia, para lograr un talante integral en el entorno de la propia libertad.
</p></li>

<li><p align="justify">
Persona con capacidad de opci&oacute;n y decisi&oacute;n vocacional, protagonista de su propia historia.
</p></li>

<li><p align="justify">
Persona capaz de apreciar con recta conciencia los valores socio ambientales y a conocer, y amar a Dios, a trav&eacute;s del di&aacute;logo y el entendimiento.
</p></li>

<li><p align="justify">
Una Persona con capacidad de auto � control, que le permite tomar opciones libres y conscientes frente a las influencias sociales, pol&iacute;ticas, religiosas, etc.
</p></li>

<li><p align="justify">
Una persona con capacidad de comprensi&oacute;n, solidaridad, tolerancia y respeto hacia los dem&aacute;s.
</p></li>

<li><p align="justify">
Una persona con capacidad de comprometerse en la promoci&oacute;n de la justicia con criterio social y ambiental.
</p></li>
</ul>
En consecuencia deber&aacute; caracterizarse por:
<ul>
<li>Ser L&iacute;der y Promotor (a).</li>
<li>Educado integralmente como estudiante epist&eacute;mico, axiol&oacute;gico, reflexivo y cr&iacute;tico.</li>
<li>Afectivo y fraterno.</li>
<li>Gestor de cambio.</li>
<li>Recursivo y creativo.</li>
<li>Comprensivo y tolerante.</li>
<li>Sencillo, transparente y leal.</li>
<li>Libre y responsable.</li>
<li>Con profunda autoestima.</li>
<li>Aut&oacute;nomo y &eacute;tico.</li>
<li>Con conciencia nacionalista y c&iacute;vica.</li>
<li>Respetuoso de su entorno.</li>
<li>Alegre y jovial.</li>

</ul>
</td>
</tr>
</table>
</body>
</html>