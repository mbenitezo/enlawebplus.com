<html>
<head><title>PERFILES DEL EDUCADOR - INSTITUCI&Oacute;N EDUCATIVA FRANCISCO DE PAULA SANTANDER</title></head>
<style>
LI { list-style: url(li.png)   }
</style> 
<body>
<table border="0" width="600" align="center">
<tr>
<td><p align="center"><b>PERF&Iacute;L DEL EDUCADOR</b></p></td>
</tr>
<tr>
<td><p align="justify">
Consideramos que el verdadero maestro no es aquel cuya funci&oacute;n consiste exclusivamente en &#147;dictar&#148; clase, sino que va mucho m&aacute;s all&aacute;, es un l&iacute;der de la comunidad, un dador de ejemplo, un orientador integral de la juventud. Su mejor ense&ntilde;anza la constituye su propia existencia, es la vida misma.  <br>El verdadero maestro es ingenioso, creativo, sencillo, decoroso, optimista, entusiasta, es recto en su comportamiento y beligerante en la defensa de la justicia.  Es tolerante, defensor incondicional de los derechos humanos, amante de la libertad y la democracia, con sentido de pertenencia.
<br><br>
Trabajamos por un maestro con identidad profesional, esp&iacute;ritu cient�fico, sentimientos de solidaridad y justicia social; por un maestro que rechaza toda pr&aacute;ctica autoritaria y deformante; que maneje una concepci&oacute;n amplia y abierta con respecto a la vida, y la labor educativa. Un maestro que permanezca inscrito en los procesos de cambio del pa&iacute;s en la pr&aacute;ctica que los desarrolla y en la realidad que lo circunda.
<br><br>
Trabajamos por un maestro ideal e integral, que encuentre su identidad en los espacios de la ciencia y de la cultura, que sea riguroso y comprensivo, que se preocupe por la defensa de su gremio y luche por unas condiciones de vida que lo dignifiquen, que sea adem&aacute;s, y por sobre todo, un ciudadano ejemplar que ejerza y reivindique sus derechos civiles y pol&iacute;ticos.
<br><br>
Nuestro docente debe tener valores aut&eacute;nticos, que haga de su trabajo una verdadera integraci&oacute;n educativa, donde el educando sea part&iacute;cipe activo de un proceso de cambio. Nuestro maestro debe estar comprometido consigo mismo, con el disc&iacute;pulo  y con el saber; empleando un sistema de correcci&oacute;n y est&iacute;mulo liberalizante y humanizante que motive al alumno a la conquista de los valores, saberes y al aprovechamiento sostenible de los recursos naturales.
<br><br>
Somos conscientes que este modelo ideal de maestro est&aacute; lejos, muy lejos del real, del hist&oacute;rico, del que como hemos dicho podemos entender pero no justificar.
<br><br>
Convertirnos en maestros ideales, no es labor de un d&iacute;a, ese es nuestro sue�o, nuestra utop&iacute;a y con ella nos comprometemos. Reconocernos como somos es un buen punto de partida.
</p>
</td>
</tr>
</table>
</body>
</html>