<?
include("panel/conectar.php");
include("funciones.php");
conectar();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Validation Hints</title>
    <link rel="stylesheet" href="globals.css" type="text/css" media="all" />

<style type="text/css">
form {
	width:500px;
	border:1px solid #ccc;
}
fieldset {
	border:0;
	padding:10px;
	margin:10px;
	position:relative;
}
label {
	display:block;
	font:normal 12px/17px verdana;
}
input {width:160px;}


span.hint {
	font:normal 11px/14px verdana;
	background:#eee url(bg-span-hint-gray.gif) no-repeat top left;
	color:#444;
	border:1px solid #888;
	padding:5px 5px 5px 40px;
	width:250px;
	position:absolute;
	margin: -12px 0 0 14px;
	display:none;
}


fieldset.welldone span.hint {
	background:#9fd680 url(bg-span-hint-welldone.jpg) no-repeat top left;
	border-color:#749e5c;
	color:#000;
}
fieldset.kindagood span.hint {
	background:#ffffcc url(bg-span-hint-kindagood.jpg) no-repeat top left;
	border-color:#cc9933;
}


fieldset.welldone {
	background:transparent url(bg-fieldset-welldone.gif) no-repeat 194px 19px;
}
fieldset.kindagood {
	background:transparent url(bg-fieldset-kindagood.gif) no-repeat 194px 19px;
}


body,td,th {
	color: #000;
}
</style>
</head>
<body>
<table width="942" border="0">
<tr>
<td width="1067"><div id="headersss">
  <h1>Edición  de autores</h1></div></td>
</tr>
  <tr>
    <td>
&nbsp;&nbsp;<center>
  <table width="924" border="0">
    <tr>
      <td width="113" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Apellido del Autor</font></strong></td>
            <td width="113" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Nombre del Autor</font></strong></td>
      <td width="129" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Nacido:</font></strong></td>
      <td width="156" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Fallecido:</font></strong></td>
      <td width="344" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Descripción</font></strong></td>
      <td width="43" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Editar</font></strong></td>
    </tr>
<?
$consulta_autores = mysql_query("select AutorId, AutorApellido, AutorNombre, AutorNacimiento, AutorMuerte, AutorDescripcion FROM autores ORDER BY AutorApellido") or die ("Problemas en la consulta de mostrar autores");
$cuenta_autores = mysql_num_rows($consulta_autores);
		if($cuenta_autores == 0){
				echo "No hay autores registrados";
				exit;
			}
while($datos_autores=mysql_fetch_array($consulta_autores)){
		   $i++;  
	  if($i==2){
	  	$color="#666666";
		$color2="#FFFFFF";
	  	$i=0;
	  }else{  
	  	$color="#FFFFFF";
		$color2="#000000";
	  }			
?>
    <tr bgcolor="<?=$color?>">
      <td align="center"><font color="<?=$color2?>"><?=utf8_encode($datos_autores["AutorApellido"])?></font></td>
      <td align="center"><font color="<?=$color2?>"><?=utf8_encode($datos_autores["AutorNombre"])?></font></td>
      <td align="center"><font color="<?=$color2?>"><?=fecha_espanol($datos_autores["AutorNacimiento"]);?></font></td>
      <td align="center"><font color="<?=$color2?>">
	  <?
      if ($datos_autores["AutorMuerte"] == "No fallecido"){
		  echo "No fallecido";
		  }else{
		fecha_espanol($datos_autores["AutorMuerte"]);
			  }
	  ?></font></td>
      <td align="center" style="font-size:9px"><font color="<?=$color2?>"><?=utf8_encode($datos_autores["AutorDescripcion"])?></font></td>
      <td align="center" style="font-size:9px"><a href="Form_editar_autor.php?id=<?=$datos_autores["AutorId"] ?>"><img src="images/edit.png" height="16" width="16" />  </a></td>
    </tr>
<?
}
?>
  </table><input type="button" name="button" value="Buscar" onclick="location.href='SimpleSuggest7/buscar_programa.php';"/>&nbsp;&nbsp;
  <input type="button" name="button" value="Cerrar" onclick="javascript:window.close();"/>
  </center>
    </td>
  </tr>
</table>
</body>
</html>
