<?
include("panel/conectar.php");
include("funciones.php");
conectar();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Validation Hints</title>
    <link rel="stylesheet" href="globals.css" type="text/css" media="all" />

<style type="text/css">
form {
	width:500px;
	border:1px solid #ccc;
}
fieldset {
	border:0;
	padding:10px;
	margin:10px;
	position:relative;
}
label {
	display:block;
	font:normal 12px/17px verdana;
}
input {width:160px;}


span.hint {
	font:normal 11px/14px verdana;
	background:#eee url(bg-span-hint-gray.gif) no-repeat top left;
	color:#444;
	border:1px solid #888;
	padding:5px 5px 5px 40px;
	width:250px;
	position:absolute;
	margin: -12px 0 0 14px;
	display:none;
}


fieldset.welldone span.hint {
	background:#9fd680 url(bg-span-hint-welldone.jpg) no-repeat top left;
	border-color:#749e5c;
	color:#000;
}
fieldset.kindagood span.hint {
	background:#ffffcc url(bg-span-hint-kindagood.jpg) no-repeat top left;
	border-color:#cc9933;
}


fieldset.welldone {
	background:transparent url(bg-fieldset-welldone.gif) no-repeat 194px 19px;
}
fieldset.kindagood {
	background:transparent url(bg-fieldset-kindagood.gif) no-repeat 194px 19px;
}


body,td,th {
	color: #000;
}
</style>
</head>
<body>
<table width="942" border="0">
<tr>
<td width="1067"><div id="headersss">
  <h1>Edición de libros</h1></div></td>
</tr>
  <tr>
    <td>
&nbsp;&nbsp;<center>
  <table width="924" border="0">
    <tr>
      <td width="124" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Nombre del libro</font></strong></td>
            <td width="113" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">ISBN</font></strong></td>
      <td width="130" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Editorial</font></strong></td>
      <td width="160" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Autor</font></strong></td>
      <td width="112" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Género</font></strong></td>
      <td width="98" align="center" bgcolor="#990000"><strong><font color="#FFFFFF"># de Páginas</font></strong></td>
      <td width="110" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Año de edición</font></strong></td>
      <td width="43" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Editar</font></strong></td>
    </tr>
<?
$consulta_libros = mysql_query("select LibroId, LibroNombre, EditorialId, AutorId, LibroGenero, LibroNumeroPaginas, LibroAnoEdicion, LibroISBN FROM libros ORDER BY LibroNombre") or die ("Problemas en la consulta de mostrar libros");
$cuenta_libros = mysql_num_rows($consulta_libros);
		if($cuenta_libros == 0){
				echo "No hay libros registrados";
				exit;
			}
while($datos_libros=mysql_fetch_array($consulta_libros)){
		   $i++;  
	  if($i==2){
	  	$color="#666666";
		$color2="#FFFFFF";
	  	$i=0;
	  }else{  
	  	$color="#FFFFFF";
		$color2="#000000";
	  }			
?>
    <tr bgcolor="<?=$color?>">
      <td align="center"><font color="<?=$color2?>"><?=utf8_encode($datos_libros["LibroNombre"])?></font></td>
      <td align="center"><font color="<?=$color2?>"><?=utf8_encode($datos_libros["LibroISBN"])?></font></td>
      <td align="center"><font color="<?=$color2?>"><?=traeEditorial($datos_libros["EditorialId"])?></font></td>
      <td align="center"><font color="<?=$color2?>">
<?=traeAutor($datos_libros["AutorId"])?></font></td>
      <td align="center"><font color="<?=$color2?>"><?=traeGenero($datos_libros["LibroGenero"])?></font></td>
      <td align="center"><font color="<?=$color2?>"><?=$datos_libros["LibroNumeroPaginas"]?></font></td>
      <td align="center" style="font-size:9px"><font color="<?=$color2?>"><?=$datos_libros["LibroAnoEdicion"]?></font></td>
      <td align="center" style="font-size:9px"><a href="Form_editar_libro.php?id=<?=$datos_libros["LibroId"] ?>"><img src="images/edit.png" height="16" width="16" />  </a></td>
    </tr>
<?
}
?>
  </table><input type="button" name="button" value="Buscar" onclick="location.href='SimpleSuggest13/buscar_programa.php';"/>&nbsp;&nbsp;
  <input type="button" name="button" value="Cerrar" onclick="javascript:window.close();"/>
  </center>
    </td>
  </tr>
</table>
</body>
</html>
