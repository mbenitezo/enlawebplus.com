<?
include("panel/conectar.php");
conectar();

$identificador = $_REQUEST["id"];
if(isset($_REQUEST["submit"])){	

$NombreEditorial = strtoupper(utf8_decode($_REQUEST["NombreEditorial"]));
$DireccionEditorial = utf8_decode($_REQUEST["DireccionEditorial"]);
$PaisEditorial = utf8_decode($_REQUEST["PaisEditorial"]);


$ins_editorial = mysql_query("update editoriales set EditorialNombre='". $NombreEditorial ."', EditorialDireccion='". $DireccionEditorial ."', EditorialPais='". $PaisEditorial ."' WHERE EditorialId=".$identificador) or die ("Problemas en la inserción editorial");
?>
<script>
alert("Información ingresada correctamente");
location.href="Modificar_editorial.php";
</script>
<?
exit;	
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Validation Hints</title>
    <link rel="stylesheet" href="globals.css" type="text/css" media="all" />

<style type="text/css">
form {
	width:500px;
	border:1px solid #ccc;
}
fieldset {
	border:0;
	padding:10px;
	margin:10px;
	position:relative;
}
label {
	display:block;
	font:normal 12px/17px verdana;
}
input {width:160px;}


span.hint {
	font:normal 11px/14px verdana;
	background:#eee url(bg-span-hint-gray.gif) no-repeat top left;
	color:#444;
	border:1px solid #888;
	padding:5px 5px 5px 40px;
	width:250px;
	position:absolute;
	margin: -12px 0 0 14px;
	display:none;
}


fieldset.welldone span.hint {
	background:#9fd680 url(bg-span-hint-welldone.jpg) no-repeat top left;
	border-color:#749e5c;
	color:#000;
}
fieldset.kindagood span.hint {
	background:#ffffcc url(bg-span-hint-kindagood.jpg) no-repeat top left;
	border-color:#cc9933;
}


fieldset.welldone {
	background:transparent url(bg-fieldset-welldone.gif) no-repeat 194px 19px;
}
fieldset.kindagood {
	background:transparent url(bg-fieldset-kindagood.gif) no-repeat 194px 19px;
}


</style>

<script type="text/javascript">

// This function checks if the username field
// is at least 6 characters long.
// If so, it attaches class="welldone" to the 
// containing fieldset.

function checkNombreEditorialForLength(whatYouTyped) {
	var fieldset = whatYouTyped.parentNode;
	var txt = whatYouTyped.value;
	if (txt.length > 3 && txt.length < 8) {
		fieldset.className = "kindagood";
	} else if (txt.length > 7) {
		fieldset.className = "welldone";
	} else {
		fieldset.className = "";
	}
}

function checkDireccionEditorialForLength(whatYouTyped) {
	var fieldset = whatYouTyped.parentNode;
	var txt = whatYouTyped.value;
	if (txt.length > 3 && txt.length < 8) {
		fieldset.className = "kindagood";
	} else if (txt.length > 7) {
		fieldset.className = "welldone";
	} else {
		fieldset.className = "";
	}
}

function checkPaisEditorialForLength(whatYouTyped) {
	var fieldset = whatYouTyped.parentNode;
	var txt = whatYouTyped.value;
	if (txt.length > 3 && txt.length < 8) {
		fieldset.className = "kindagood";
	} else if (txt.length > 7) {
		fieldset.className = "welldone";
	} else {
		fieldset.className = "";
	}
}






// this part is for the form field hints to display
// only on the condition that the text input has focus.
// otherwise, it stays hidden.

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      oldonload();
      func();
    }
  }
}


function prepareInputsForHints() {
  var inputs = document.getElementsByTagName("input");
  for (var i=0; i<inputs.length; i++){
    inputs[i].onfocus = function () {
      this.parentNode.getElementsByTagName("span")[0].style.display = "inline";
    }
    inputs[i].onblur = function () {
      this.parentNode.getElementsByTagName("span")[0].style.display = "none";
    }
  }
}
addLoadEvent(prepareInputsForHints);

</script>

<script language="JavaScript" type="text/javascript">
<!--
function validar_enviar(){

    if (document.basicform.NombreEditorial.value=="")
    {
        alert("Debes escribir el nombre de la editorial.")
        document.basicform.NombreEditorial.focus()
        return false;
    }

if (document.basicform.DireccionEditorial.value=="")
    {
        alert("Escribe la dirección de la editorial.")
        document.basicform.DireccionEditorial.focus()
        return false;
    }
if (document.basicform.PaisEditorial.value=="")
    {
        alert("Coloca el pais de la editorial, si no sabes coloca DESCONOCIDO.")
        document.basicform.PaisEditorial.focus()
        return false;
    }

    }
//-->
    </script>
</head>
<body>
<table width="509" border="0">
<tr>
<td width="503"><div id="headersss">
  <h1>Modificar Editorial</h1></div></td>
</tr>
  <tr>
    <td>
<form action="#" name="basicform" id="basicform" onSubmit="return validar_enviar();" >

<?
$edicion = mysql_query("select EditorialId,EditorialNombre,EditorialDireccion,EditorialPais FROM editoriales WHERE EditorialId=".$identificador) or die ("Error en la consulta");

$info_editorial = mysql_fetch_array($edicion);
?>
<input type="hidden" name="id" value="<?=$info_editorial['EditorialId']?>" />

<fieldset>
	<label for="codigomateria">Nombre de la editorial</label>
	<input
		type="text"
		id="NombreEditorial" name="NombreEditorial"
		onkeyup="checkNombreEditorialForLength(this);" value="<?=utf8_encode($info_editorial['EditorialNombre'])?>" />
	<span class="hint">Ingresa el nombre de la editorial, no debes dejar el campo vacío..</span>
</fieldset>


<fieldset>
	<label for="nombrePrograma">Dirección de la editorial:</label>
<input type="text" id="DireccionEditorial" name="DireccionEditorial"
		onkeyup="checkDireccionEditorialForLength(this);" value="<?=utf8_encode($info_editorial['EditorialDireccion'])?>" />
	<span class="hint">Escriba la dirección de la editorial y también la ciudad.</span>
</fieldset>

<fieldset>
	<label for="descripcion">País de la editorial:</label>
	<input
		type="text"
		id="PaisEditorial" name="PaisEditorial"
		onkeyup="checkPaisEditorialForLength(this);" value="<?=utf8_encode($info_editorial['EditorialPais'])?>" />
	<span class="hint">Escribe el paisd de la editorial, si no sabes coloca DESCONOCIDO.</span>
</fieldset>
<input type="submit" name="submit" value="Guardar" />&nbsp;&nbsp;<input type="button" name="button" value="Cancelar" onClick="javascript:history.go(-1);"/>&nbsp;&nbsp;<input type="button" name="button" value="Cerrar" onClick="javascript:window.close();"/>

</form>    
    </td>
  </tr>
</table>




</body>
</html>
