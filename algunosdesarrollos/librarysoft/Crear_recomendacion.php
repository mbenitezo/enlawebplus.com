<?
if(isset($_REQUEST["submit"])){
	
echo "Esta opción se encuentra desactivada";

exit;	
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Validation Hints</title>
    <link rel="stylesheet" href="globals.css" type="text/css" media="all" />

<style type="text/css">
form {
	width:500px;
	border:1px solid #ccc;
}
fieldset {
	border:0;
	padding:10px;
	margin:10px;
	position:relative;
}
label {
	display:block;
	font:normal 12px/17px verdana;
}
input {width:160px;}


span.hint {
	font:normal 11px/14px verdana;
	background:#eee url(bg-span-hint-gray.gif) no-repeat top left;
	color:#444;
	border:1px solid #888;
	padding:5px 5px 5px 40px;
	width:250px;
	position:absolute;
	margin: -12px 0 0 14px;
	display:none;
}


fieldset.welldone span.hint {
	background:#9fd680 url(bg-span-hint-welldone.jpg) no-repeat top left;
	border-color:#749e5c;
	color:#000;
}
fieldset.kindagood span.hint {
	background:#ffffcc url(bg-span-hint-kindagood.jpg) no-repeat top left;
	border-color:#cc9933;
}


fieldset.welldone {
	background:transparent url(bg-fieldset-welldone.gif) no-repeat 194px 19px;
}
fieldset.kindagood {
	background:transparent url(bg-fieldset-kindagood.gif) no-repeat 194px 19px;
}


</style>
		<style type="text/css" media="screen">
			body {
				font: 11px arial;
			}
			.suggest_link {
				background-color: #FFFFFF;
				padding: 2px 6px 2px 6px;
			}
			.suggest_link_over {
				background-color: #000000;
				padding: 2px 6px 2px 6px;
                text-decoration: none;
                color: white;
                cursor:pointer;
			}
			#search_suggest {
				position: absolute; 
				background-color: #FFFFFF; 
				text-align: left; 
				border: 3px solid #000000;			
			}
			#search_suggest2 {
				position: absolute; 
				background-color: #FFFFFF; 
				text-align: left; 
				border: 3px solid #000000;			
			}		
		</style>
        		<script language="JavaScript" type="text/javascript" src="ajax_search2.js"></script>
<script type="text/javascript">

// This function checks if the username field
// is at least 6 characters long.
// If so, it attaches class="welldone" to the 
// containing fieldset.

function checkUsernameForLength(whatYouTyped) {
	var fieldset = whatYouTyped.parentNode;
	var txt = whatYouTyped.value;
	if (txt.length > 3 && txt.length < 8) {
		fieldset.className = "kindagood";
	} else if (txt.length > 7) {
		fieldset.className = "welldone";
	} else {
		fieldset.className = "";
	}
}


function checkIsbnForLength(whatYouTyped) {
	var fieldset = whatYouTyped.parentNode;
	var txt = whatYouTyped.value;
	if (txt.length > 3 && txt.length < 4) {
		fieldset.className = "kindagood";
	} else if (txt.length > 3) {
		fieldset.className = "welldone";
	} else {
		fieldset.className = "";
	}
}


function checkApellidosForLength(whatYouTyped) {
	var fieldset = whatYouTyped.parentNode;
	var txt = whatYouTyped.value;
	if (txt.length > 3 && txt.length < 8) {
		fieldset.className = "kindagood";
	} else if (txt.length > 7) {
		fieldset.className = "welldone";
	} else {
		fieldset.className = "";
	}
}





function checkCodigoForLength(whatYouTyped) {
	var fieldset = whatYouTyped.parentNode;
	var txt = whatYouTyped.value;
	if (txt.length > 3 && txt.length < 4) {
		fieldset.className = "kindagood";
	} else if (txt.length > 4) {
		fieldset.className = "welldone";
	} else {
		fieldset.className = "";
	}
}


// If the password is at least 4 characters long, the containing 
// fieldset is assigned class="kindagood".
// If it's at least 8 characters long, the containing
// fieldset is assigned class="welldone", to give the user
// the indication that they've selected a harder-to-crack
// password.

function checkPassword(whatYouTyped) {
	var fieldset = whatYouTyped.parentNode;
	var txt = whatYouTyped.value;
	if (txt.length > 3 && txt.length < 4) {
		fieldset.className = "kindagood";
	} else if (txt.length > 4) {
		fieldset.className = "welldone";
	} else {
		fieldset.className = "";


	}


}


// this part is for the form field hints to display
// only on the condition that the text input has focus.
// otherwise, it stays hidden.

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      oldonload();
      func();
    }
  }
}


function prepareInputsForHints() {
  var inputs = document.getElementsByTagName("input");
  for (var i=0; i<inputs.length; i++){
    inputs[i].onfocus = function () {
      this.parentNode.getElementsByTagName("span")[0].style.display = "inline";
    }
    inputs[i].onblur = function () {
      this.parentNode.getElementsByTagName("span")[0].style.display = "none";
    }
  }
}
addLoadEvent(prepareInputsForHints);

</script>
<script language="JavaScript" type="text/javascript">
<!--
function validar_enviar(){

    if (document.basicform.txtSearch.value=="")
    {
        alert("Escribe el nombre del profesor")
        document.basicform.txtSearch.focus()
        return false;
    }

if (document.basicform.txtSearch2.value=="")
    {
        alert("Escribe el nombre del libro")
        document.basicform.txtSearch2.focus()
        return false;
    }
if (document.basicform.reservacion.value=="")
    {
        alert("Seleccione la fecha inicial del préstamo")
        document.basicform.reservacion.focus()
        return false;
    }


    }
//-->
    </script>

	<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></LINK>
	<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
</head>
<body onLoad="javascript:document.basicform.txtSearch.focus();">
<table width="509" border="0">
<tr>
<td width="503"><div id="headersss">
  <h1>Recomendaciones de libros</h1></div></td>
</tr>
  <tr>
    <td>
<form
	action="#"
	name="basicform"
	id="basicform" onSubmit="return validar_enviar();" >



	<label for="username">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nombre del profesor que hace la recomendación:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input
		type="text"
		id="txtSearch" name="txtSearch"	onkeyup="searchSuggest();" size="80" autocomplete="off" style="width:320px;" /><input type="image" src="images/erase.png" width="20"  height="20" title="Borrar" onClick="javascript:document.basicform.txtSearch.value='';document.basicform.txtSearch.focus();return false;" style="width:20px;" >

<br />  
<div id="search_suggest"></div>
<br /><br /><br /><br />


	<label for="username">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Código ISBN del libro que recomienda:</label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<input
		type="text"
		id="txtSearch2" name="txtSearch2"	onkeyup="searchSuggest2();" size="80" autocomplete="off" style="width:320px;" /><input type="image" src="images/erase.png" width="20"  height="20" title="Borrar" onClick="javascript:document.basicform.txtSearch2.value='';document.basicform.txtSearch2.focus();return false;" style="width:20px;" >

<br />
<div id="search_suggest2"></div>
<br /><br /><br /><br />


	<label for="codigo">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Grado a quien recomienda:</label>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
		type="text"
		id="codigo"
		onkeyup="checkCodigoForLength(this);" />
<br /><br />


<br />
<br /><br /><br />

<input type="submit" name="submit" value="Crear recomendación" />&nbsp;&nbsp;<input type="button" name="button" value="Cerrar" onClick="javascript:window.close();" style="width:160px;" />

</form>    
    </td>
  </tr>
</table>




</body>
</html>
