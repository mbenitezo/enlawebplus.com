<?
include("panel/conectar.php");
conectar();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Validation Hints</title>
    <link rel="stylesheet" href="globals.css" type="text/css" media="all" />

<style type="text/css">
form {
	width:500px;
	border:1px solid #ccc;
}
fieldset {
	border:0;
	padding:10px;
	margin:10px;
	position:relative;
}
label {
	display:block;
	font:normal 12px/17px verdana;
}
input {width:160px;}


span.hint {
	font:normal 11px/14px verdana;
	background:#eee url(bg-span-hint-gray.gif) no-repeat top left;
	color:#444;
	border:1px solid #888;
	padding:5px 5px 5px 40px;
	width:250px;
	position:absolute;
	margin: -12px 0 0 14px;
	display:none;
}


fieldset.welldone span.hint {
	background:#9fd680 url(bg-span-hint-welldone.jpg) no-repeat top left;
	border-color:#749e5c;
	color:#000;
}
fieldset.kindagood span.hint {
	background:#ffffcc url(bg-span-hint-kindagood.jpg) no-repeat top left;
	border-color:#cc9933;
}


fieldset.welldone {
	background:transparent url(bg-fieldset-welldone.gif) no-repeat 194px 19px;
}
fieldset.kindagood {
	background:transparent url(bg-fieldset-kindagood.gif) no-repeat 194px 19px;
}


</style>
</head>
<body>
<table width="736" border="0">
<tr>
<td width="735"><div id="headersss">
  <h1>Consultar programa</h1></div></td>
</tr>
  <tr>
    <td>
&nbsp;&nbsp;<center>
  <table width="719" border="0">
    <tr>
      <td width="99" align="center"><strong>Código del programa</strong></td>
      <td width="211" align="center"><strong>Nombre del programa</strong></td>
      <td width="387" align="center"><strong>Descripción del programa</strong></td>
    </tr>
<?
$consulta_programas = mysql_query("select CodPrograma, nomPrograma, descPrograma FROM programas ORDER BY nomPrograma") or die ("Problemas en la consulta");
$cuenta_programas = mysql_num_rows($consulta_programas);
		if($cuenta_programas == 0){
				echo "No hay programas registrados";
				exit;
			}
while($datos_programas=mysql_fetch_array($consulta_programas)){			
?>
    <tr>
      <td align="center"><?=utf8_encode($datos_programas["CodPrograma"])?></td>
      <td align="center"><?=utf8_encode($datos_programas["nomPrograma"])?></td>
      <td align="center"><?=utf8_encode($datos_programas["descPrograma"])?></td>
    </tr>
<?
}
?>
  </table><input type="button" name="button" value="Buscar" onclick="location.href='SimpleSuggest/buscar_programa.php';"/>&nbsp;&nbsp;
  <input type="button" name="button" value="Cerrar" onclick="javascript:window.close();"/>
  </center>
    </td>
  </tr>
</table>
</body>
</html>
