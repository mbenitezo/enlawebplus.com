var edButtons = new Array();
var edOpenTags = new Array();
function edButton(a, b, c, d, e, f) {
    this.id = a;
    this.display = b;
    this.tagStart = c;
    this.tagEnd = d;
    this.access = e;
    this.open = f
}
edButtons.push(new edButton('ed_bold', 'strong', '<strong>', '</strong>', 'b'));
edButtons.push(new edButton('ed_italic', 'em', '<em>', '</em>', 'i'));
edButtons.push(new edButton('ed_link', 'link', '', '</a>', 'a'));
edButtons.push(new edButton('ed_block', 'cite', '<blockquote>', '</blockquote>', 'q'));
var extendedStart = edButtons.length;
edButtons.push(new edButton('ed_html', 'HTML', '[html]', '[/html]\n\n', '1'));
edButtons.push(new edButton('ed_css', 'CSS', '[css]', '[/css]\n\n', '2'));
edButtons.push(new edButton('ed_js', 'JS', '[js]', '[/js]\n\n', '3'));
edButtons.push(new edButton('ed_php', 'PHP', '[php]', '[/php]\n\n', '5'));
function edLink(a, b, c) {
    this.display = a;
    this.URL = b;
    if (!c) {
        c = 0
    }
    this.newWin = c
}
function edShowButton(a, b, i) {
    if (b.access) {
        var c = ' accesskey = "' + b.access + '"'
    } else {
        var c = ''
    }
    switch (b.id) {
    case 'ed_link':
        document.write('<input type="button" id="' + b.id + '_' + a + '" ' + c + ' class="ed_button" onclick="edInsertLink(\'' + a + '\', ' + i + ');" value="' + b.display + '" />');
        break;
    default:
        document.write('<input type="button" id="' + b.id + '_' + a + '" ' + c + ' class="ed_button" onclick="edInsertTag(\'' + a + '\', ' + i + ');" value="' + b.display + '"  />');
        break
    }
}
function edAddTag(a, b) {
    if (edButtons[b].tagEnd != '') {
        edOpenTags[a][edOpenTags[a].length] = b;
        document.getElementById(edButtons[b].id + '_' + a).value = '/' + document.getElementById(edButtons[b].id + '_' + a).value
    }
}
function edRemoveTag(a, b) {
    for (i = 0; i < edOpenTags[a].length; i++) {
        if (edOpenTags[a][i] == b) {
            edOpenTags[a].splice(i, 1);
            document.getElementById(edButtons[b].id + '_' + a).value = document.getElementById(edButtons[b].id + '_' + a).value.replace('/', '')
        }
    }
}
function edCheckOpenTags(a, b) {
    var c = 0;
    for (i = 0; i < edOpenTags[a].length; i++) {
        if (edOpenTags[a][i] == b) {
            c++
        }
    }
    if (c > 0) {
        return true
    } else {
        return false
    }
}
function edCloseAllTags(a) {
    var b = edOpenTags[a].length;
    for (o = 0; o < b; o++) {
        edInsertTag(a, edOpenTags[a][edOpenTags[a].length - 1])
    }
}
function edToolbar(a) {
    document.write('<div id="ed_toolbar_' + a + '"><span>');
    for (i = 0; i < extendedStart; i++) {
        edShowButton(a, edButtons[i], i)
    }
    if (edShowExtraCookie()) {
        document.write('<input type="button" id="ed_close_' + a + '" class="ed_button" onclick="edCloseAllTags(\'' + a + '\');" value="Close Tags" />' + '<input type="button" id="ed_extra_show_' + a + '" class="ed_button" onclick="edShowExtra(\'' + a + '\')" value="&raquo; CODE" style="visibility: hidden;" />' + '</span><br />' + '<span id="ed_extra_buttons_' + a + '">' + '<input type="button" id="ed_extra_hide_' + a + '" class="ed_button" onclick="edHideExtra(\'' + a + '\');" value="&laquo;" />')
    } else {
        document.write('<input type="button" id="ed_close_' + a + '" class="ed_button" onclick="edCloseAllTags(\'' + a + '\');" value="Close Tags" />' + '<input type="button" id="ed_extra_show_' + a + '" class="ed_button" onclick="edShowExtra(\'' + a + '\')" value="&raquo; CODE" />' + '</span><br />' + '<span id="ed_extra_buttons_' + a + '" style="display: none;">' + '<input type="button" id="ed_extra_hide_' + a + '" class="ed_button" onclick="edHideExtra(\'' + a + '\');" value="&laquo;" />')
    }
    for (i = extendedStart; i < edButtons.length; i++) {
        edShowButton(a, edButtons[i], i)
    }
    document.write('</span>');
    document.write('</div>');
    edOpenTags[a] = new Array()
}
function edShowExtra(a) {
    document.getElementById('ed_extra_show_' + a).style.visibility = 'hidden';
    if ($('ed_extra_show_' + a).SexyElement.padre) {
      $('ed_extra_show_' + a).SexyElement.padre.setStyle('visibility', 'hidden');
    }
    document.getElementById('ed_extra_buttons_' + a).style.display = 'block';
    edSetCookie('js_quicktags_extra', 'show', new Date("December 31, 2100"))
}
function edHideExtra(a) {
    document.getElementById('ed_extra_buttons_' + a).style.display = 'none';
    document.getElementById('ed_extra_show_' + a).style.visibility = 'visible';
    if ($('ed_extra_show_' + a).SexyElement.padre) {
      $('ed_extra_show_' + a).SexyElement.padre.setStyle('visibility', 'visible');
    }
    edSetCookie('js_quicktags_extra', 'hide', new Date("December 31, 2100"))
}
function edInsertTag(a, i) {
    myField = document.getElementById(a);
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        if (sel.text.length > 0) {
            sel.text = edButtons[i].tagStart + sel.text + edButtons[i].tagEnd
        } else { if (!edCheckOpenTags(a, i) || edButtons[i].tagEnd == '') {
                sel.text = edButtons[i].tagStart;
                edAddTag(a, i)
            } else {
                sel.text = edButtons[i].tagEnd;
                edRemoveTag(a, i)
            }
        }
        myField.focus()
    } else if (myField.selectionStart || myField.selectionStart == '0') {
        var b = myField.selectionStart;
        var c = myField.selectionEnd;
        var d = c;
        var e = myField.scrollTop;
        if (b != c) {
            myField.value = myField.value.substring(0, b) + edButtons[i].tagStart + myField.value.substring(b, c) + edButtons[i].tagEnd + myField.value.substring(c, myField.value.length);
            d += edButtons[i].tagStart.length + edButtons[i].tagEnd.length
        } else { if (!edCheckOpenTags(a, i) || edButtons[i].tagEnd == '') {
                myField.value = myField.value.substring(0, b) + edButtons[i].tagStart + myField.value.substring(c, myField.value.length);
                edAddTag(a, i);
                d = b + edButtons[i].tagStart.length
            } else {
                myField.value = myField.value.substring(0, b) + edButtons[i].tagEnd + myField.value.substring(c, myField.value.length);
                edRemoveTag(a, i);
                d = b + edButtons[i].tagEnd.length
            }
        }
        myField.focus();
        myField.selectionStart = d;
        myField.selectionEnd = d;
        myField.scrollTop = e
    } else { if (!edCheckOpenTags(a, i) || edButtons[i].tagEnd == '') {
            myField.value += edButtons[i].tagStart;
            edAddTag(a, i)
        } else {
            myField.value += edButtons[i].tagEnd;
            edRemoveTag(a, i)
        }
        myField.focus()
    }
}
function edInsertContent(a, b) {
    myField = document.getElementById(a);
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = b;
        myField.focus()
    } else if (myField.selectionStart || myField.selectionStart == '0') {
        var c = myField.selectionStart;
        var d = myField.selectionEnd;
        var e = myField.scrollTop;
        myField.value = myField.value.substring(0, c) + b + myField.value.substring(d, myField.value.length);
        myField.focus();
        myField.selectionStart = c + b.length;
        myField.selectionEnd = c + b.length;
        myField.scrollTop = e
    } else {
        myField.value += b;
        myField.focus()
    }
}
function edInsertLink(a, i, b) {
    myField = document.getElementById(a);
    if (!b) {
        b = 'http://'
    }
    if (!edCheckOpenTags(a, i)) {
        var c = prompt('Enter the URL', b);
        if (c) {
            edButtons[i].tagStart = '<a href="' + c + '">';
            edInsertTag(a, i)
        }
    } else {
        edInsertTag(a, i)
    }
}
function edSetCookie(a, b, c, d, e) {
    document.cookie = a + "=" + escape(b) + ((c) ? "; expires=" + c.toGMTString() : "") + ((d) ? "; path=" + d : "") + ((e) ? "; domain=" + e : "")
}
function edShowExtraCookie() {
    var a = document.cookie.split(';');
    for (var i = 0; i < a.length; i++) {
        var b = a[i];
        while (b.charAt(0) == ' ') {
            b = b.substring(1, b.length)
        }
        if (b.indexOf('js_quicktags_extra') == 0) {
            if (b.substring(19, b.length) == 'show') {
                return true
            } else {
                return false
            }
        }
    }
    return false
}