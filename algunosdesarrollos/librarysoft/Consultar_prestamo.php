<?
include("panel/conectar.php");
conectar();
include("funciones.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Validation Hints</title>
    <link rel="stylesheet" href="globals.css" type="text/css" media="all" />

<style type="text/css">
form {
	width:500px;
	border:1px solid #ccc;
}
fieldset {
	border:0;
	padding:10px;
	margin:10px;
	position:relative;
}
label {
	display:block;
	font:normal 12px/17px verdana;
}
input {width:160px;}


span.hint {
	font:normal 11px/14px verdana;
	background:#eee url(bg-span-hint-gray.gif) no-repeat top left;
	color:#444;
	border:1px solid #888;
	padding:5px 5px 5px 40px;
	width:250px;
	position:absolute;
	margin: -12px 0 0 14px;
	display:none;
}


fieldset.welldone span.hint {
	background:#9fd680 url(bg-span-hint-welldone.jpg) no-repeat top left;
	border-color:#749e5c;
	color:#000;
}
fieldset.kindagood span.hint {
	background:#ffffcc url(bg-span-hint-kindagood.jpg) no-repeat top left;
	border-color:#cc9933;
}


fieldset.welldone {
	background:transparent url(bg-fieldset-welldone.gif) no-repeat 194px 19px;
}
fieldset.kindagood {
	background:transparent url(bg-fieldset-kindagood.gif) no-repeat 194px 19px;
}


body,td,th {
	color: #000;
}
</style>
</head>
<body>
<table width="935" border="0">
<tr>
<td width="735"><div id="headersss">
  <h1>Consultar préstamo</h1></div></td>
</tr>
  <tr>
    <td>
&nbsp;&nbsp;<div align="left">
  <table width="929" border="0">
    <tr>
      <td width="253" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Libro prestado</font></strong></td>
      <td width="252" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Prestado por:</font></strong></td>
      <td width="213" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Fecha inicial del préstamo</font></strong></td>
      <td width="193" align="center" bgcolor="#990000"><strong><font color="#FFFFFF">Fecha final del préstamo</font></strong></td>
    </tr>
<?
$consulta_prestamos = mysql_query("select PrestamosEstudiantesId, PrestamosLibrosId, PrestamosFechaInicial, PrestamosFechaFinal FROM prestamos ORDER BY PrestamosId DESC") or die ("Problemas en la consulta de consultar prestamos");
$cuenta_prestamos = mysql_num_rows($consulta_prestamos);
		if($cuenta_prestamos == 0){
				echo "No hay reservas registradas";
				exit;
			}
while($datos_prestamos=mysql_fetch_array($consulta_prestamos)){
		   $i++;  
	  if($i==2){
	  	$color="#666666";
		$color2="#FFFFFF";
	  	$i=0;
	  }else{  
	  	$color="#FFFFFF";
		$color2="#000000";
	  }			
?>
    <tr bgcolor="<?=$color?>">
      <td align="center"><font color="<?=$color2?>"><?=traeLibro($datos_prestamos["PrestamosLibrosId"])?></font></td>
      <td align="center"><font color="<?=$color2?>"><?=traeEstudiante($datos_prestamos["PrestamosEstudiantesId"])?></font></td>
      <td align="center"><font color="<?=$color2?>"><?=fecha_espanol2($datos_prestamos["PrestamosFechaInicial"])?></font></td>
      <td align="center"><font color="<?=$color2?>"><?=fecha_espanol2($datos_prestamos["PrestamosFechaFinal"])?></font></td>
    </tr>
<?
}
?>
  </table><div align="center"><input type="button" name="button" value="Buscar" onclick="location.href='SimpleSuggest19/buscar_programa.php?opt=0';"/>&nbsp;&nbsp;
  <input type="button" name="button" value="Cerrar" onclick="javascript:window.close();"/></div>
  </div>
    </td>
  </tr>
</table>
</body>
</html>
