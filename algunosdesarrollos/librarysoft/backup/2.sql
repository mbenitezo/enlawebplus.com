/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.0.51a : Database - lalvirtu_stars
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`lalvirtu_stars` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `lalvirtu_stars`;

/*Table structure for table `estudiantes` */

DROP TABLE IF EXISTS `estudiantes`;

CREATE TABLE `estudiantes` (
  `id` int(255) NOT NULL auto_increment,
  `codEstudiantes` int(255) default NULL,
  `codPrograma` int(255) default NULL,
  `nomEstudiante` varchar(255) default NULL,
  `apeEstudiante` varchar(255) default NULL,
  `emailEstudiante` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Table structure for table `programas` */

DROP TABLE IF EXISTS `programas`;

CREATE TABLE `programas` (
  `id` int(255) NOT NULL auto_increment,
  `CodPrograma` int(255) default NULL,
  `nomPrograma` varchar(255) default NULL,
  `descPrograma` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
