/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.0.51a : Database - lalvirtu_stars
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`lalvirtu_stars` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `lalvirtu_stars`;

/*Table structure for table `autores` */

DROP TABLE IF EXISTS `autores`;

CREATE TABLE `autores` (
  `AutorId` int(255) NOT NULL auto_increment,
  `AutorApellido` varchar(255) default NULL,
  `AutorNombre` varchar(255) default NULL,
  `AutorNacimiento` varchar(255) default NULL,
  `AutorMuerte` varchar(255) default NULL,
  `AutorDescripcion` longtext,
  PRIMARY KEY  (`AutorId`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
