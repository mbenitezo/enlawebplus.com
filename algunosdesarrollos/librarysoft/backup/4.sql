/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.0.51a : Database - lalvirtu_stars
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`lalvirtu_stars` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `lalvirtu_stars`;

/*Table structure for table `editoriales` */

DROP TABLE IF EXISTS `editoriales`;

CREATE TABLE `editoriales` (
  `EditorialId` int(255) NOT NULL auto_increment,
  `EditorialNombre` varchar(255) default NULL,
  `EditorialDireccion` varchar(255) default NULL,
  `EditorialPais` varchar(255) default NULL,
  PRIMARY KEY  (`EditorialId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
