/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.0.51a : Database - lalvirtu_stars
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`lalvirtu_stars` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `lalvirtu_stars`;

/*Table structure for table `programas` */

DROP TABLE IF EXISTS `programas`;

CREATE TABLE `programas` (
  `id` int(255) NOT NULL auto_increment,
  `CodPrograma` int(255) default NULL,
  `nomPrograma` varchar(255) default NULL,
  `descPrograma` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `programas` */

insert  into `programas`(`id`,`CodPrograma`,`nomPrograma`,`descPrograma`) values (9,222,'CÁLCULO BÁSICO','Aquí se habla sobre logaritmos y ecuaciones básicas.'),(8,111,'TEORÍA DE SISTEMAS','Máteria que habla sobre los procesos de la teoría del sistema.'),(11,333,'CÁLCULO DIFERENCIAL','Todo lo relacionado en el manejo de fórmulas matemáticas.');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
