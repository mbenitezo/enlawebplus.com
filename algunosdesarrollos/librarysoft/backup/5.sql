/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.0.51a : Database - lalvirtu_stars
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`lalvirtu_stars` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `lalvirtu_stars`;

/*Table structure for table `prestamos` */

DROP TABLE IF EXISTS `prestamos`;

CREATE TABLE `prestamos` (
  `PrestamosId` int(255) NOT NULL auto_increment,
  `PrestamosEstudiantesId` int(255) default NULL,
  `PrestamosLibrosId` int(255) default NULL,
  `PrestamosFechaInicial` date default NULL,
  `PrestamosFechaFinal` date default NULL,
  PRIMARY KEY  (`PrestamosId`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Table structure for table `reservas` */

DROP TABLE IF EXISTS `reservas`;

CREATE TABLE `reservas` (
  `ReservasId` int(255) NOT NULL auto_increment,
  `ReservasEstudiante` int(255) default NULL,
  `ReservasLibro` int(255) default NULL,
  `ReservasFecha` date default NULL,
  PRIMARY KEY  (`ReservasId`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
