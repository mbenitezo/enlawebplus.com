// Declaro un array en el cual los indices son los ID's de los DIVS que funcionan como pesta�a y los valores son los identificadores de las secciones a cargar
var tabsId=new Array();
tabsId['tab1']='seccion1';
tabsId['tab2']='seccion2';
tabsId['tab3']='seccion3';
tabsId['tab4']='seccion4';
tabsId['tab5']='seccion5';
// Declaro el ID del DIV que actuar� como contenedor de los datos recibidos
var contenedor='tabContenido';


function objetoAjax(){
	var xmlhttp=false;
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(E){
			xmlhttp = false;
  		}
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}


function cargaContenido()
{
    /* Recorro las pesta�as para dejar en estado "apagado" a todas menos la que se ha clickeado. Teniendo en cuenta que solo puede haber una pesta�a "encendida"
    a la vez resultar�a mas �ptimo hacer un while hasta encontrar a esa pesta�a, cambiarle el estilo y luego salir, pero, creanme, se complicar�a un poco el
    ejemplo y no es mi intenci�n complicarlos */
    for(key in tabsId)
    {
        // Obtengo el elemento
        elemento=document.getElementById(key);
        // Si es la pesta�a activa
        if(elemento.className=='tabOn')
        {
            // Cambio el estado de la pesta�a a inactivo 
            elemento.className='tabOff';
        }
    }
    // Cambio el estado de la pesta�a que se ha clickeado a activo
    this.className='tabOn';
    
    /* De aqui hacia abajo se tratatan la petici�n y recepci�n de datos */
    
    // Obtengo el identificador vinculado con el ID del elemento HTML que referencia a la secci�n a cargar
    seccion=tabsId[this.id];
    
    // Coloco un mensaje mientras se reciben los datos
    tabContenedor.innerHTML='<img src="loading2.gif"> Cargando, por favor espere...';
    
    // Creo el objeto AJAX y envio la petici�n por POST (para evitar cacheos de datos)
    var ajax=objetoAjax();
    ajax.open("POST", "tabs_o_pestanas_con_javascript_no_intrusivo_proceso.php", true);
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax.send('seccion='+seccion);
    
    ajax.onreadystatechange=function()
    {
        if(ajax.readyState==4)
        {
            // Al recibir la respuesta coloco directamente el HTML en la capa contenedora
            tabContenedor.innerHTML=ajax.responseText;
        }
    }
}

function mouseSobre()
{
    // Si el evento no se produjo en la pesta�a seleccionada...
    if(this.className!='tabOn')
    {
        // Cambio el color de fondo de la pesta�a
        this.className='tabHover';
    }
}

function mouseFuera()
{
    // Si el evento no se produjo en la pesta�a seleccionada...
    if(this.className!='tabOn')
    {
        // Cambio el color de fondo de la pesta�a
        this.className='tabOff';
    }
}

onload=function()
{
    for(key in tabsId)
    {
        // Voy obteniendo los ID's de los elementos declarados en el array que representan a las pesta�as
        elemento=document.getElementById(key);
        // Asigno que al hacer click en una pesta�a se llame a la funcion cargaContenido
        elemento.onclick=cargaContenido;
        /* El cambio de estilo es en 2 funciones diferentes debido a la incompatibilidad del string de backgroundColor devuelto por Mozilla e IE.
        Se podr�a pasar de rgb(xxx, xxx, xxx) a formato #xxxxxx pero complicar�a innecesariamente el ejemplo */
        elemento.onmouseover=mouseSobre;
        elemento.onmouseout=mouseFuera;
    }
    // Obtengo la capa contenedora de datos
    tabContenedor=document.getElementById(contenedor);
}



///////////////////////EMPIEZA CODIGO PARA LAS PELICULAS


function enviarDatosPelicula(){
	//donde se mostrar� lo resultados
	divResultado = document.getElementById('resultado');
	divFormulario = document.getElementById('formulario');
	divResultado.innerHTML= 'Actualizando...';
	
	//valores de los cajas de texto
	id=document.frmpelicula.idpelicula.value;
	nom=document.frmpelicula.nombres.value;
	tipo=document.frmpelicula.tipo.value;
	descr=document.frmpelicula.descripcion.value;
	
	//instanciamos el objetoAjax
	ajax=objetoAjax();
	//usando del medoto POST
	//archivo que realizar� la operacion ->actualizacion.php
	ajax.open("POST", "actualizacion.php",true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			//mostrar los nuevos registros en esta capa
			divResultado.innerHTML = ajax.responseText
			//una vez actualizacion ocultamos formulario
			divFormulario.style.display="none";

		}
	}
	//muy importante este encabezado ya que hacemos uso de un formulario
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	//enviando los valores
	ajax.send("idpelicula="+id+"&nombres="+nom+"&tipo="+tipo+"&descripcion="+descr)
}

function pedirDatos(idpelicula){
	//donde se mostrar� el formulario con los datos
	divFormulario = document.getElementById('formulario');
	
	//instanciamos el objetoAjax
	ajax=objetoAjax();
	//uso del medotod POST
	ajax.open("POST", "consulta_por_id.php");
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			//mostrar resultados en esta capa
			divFormulario.innerHTML = ajax.responseText
			divFormulario.style.display="block";
		}
	}
	//como hacemos uso del metodo POST
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	//enviando el codigo del empleado
	ajax.send("idemp="+idpelicula)
}



function eliminarDato(idpelicula){
	//donde se mostrar� el resultado de la eliminacion
	divResultado = document.getElementById('resultado');
	
	
	//usaremos un cuadro de confirmacion	
	var eliminar = confirm("De verdad desea eliminar este dato?")
	if ( eliminar ) {
		//instanciamos el objetoAjax
		ajax=objetoAjax();
		//uso del medotod GET
		//indicamos el archivo que realizar� el proceso de eliminaci�n
		//junto con un valor que representa el id del empleado
		ajax.open("GET", "eliminacion.php?idpelicula="+idpelicula);
		divResultado.innerHTML= '<img src="anim.gif">';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}
		//como hacemos uso del metodo GET
		//colocamos null
		ajax.send(null)
	}
}



function enviarDatosPelicula2(){
  //donde se mostrar� lo resultados
  divResultado = document.getElementById('resultado');
  divResultado.innerHTML= '<img src="anim.gif">';
  //valores de las cajas de texto
  noms=document.nueva_pelicula.nombress.value;
  tip=document.nueva_pelicula.tipo.value;
  descr=document.nueva_pelicula.descripcion.value;
  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medoto POST
  //archivo que realizar� la operacion
  //registro.php
  ajax.open("POST", "registro.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divResultado.innerHTML = ajax.responseText
  //llamar a funcion para limpiar los inputs
  LimpiarCamposs();
  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores
  ajax.send("nombres="+noms+"&tipo="+tip+"&descripcion="+descr)
}

function LimpiarCamposs(){
  document.nueva_pelicula.nombress.value="";
  document.nueva_pelicula.tipo.value="";
  document.nueva_pelicula.descripcion.value="";
  document.nueva_pelicula.nombress.focus();
  }
  
//TERMINA CODIGO PARA LAS PELICULAS




//EMPIEZA CODIGO PARA LOS CLIENTES
//EMPIEZA CODIGO PARA LOS CLIENTES
//EMPIEZA CODIGO PARA LOS CLIENTES
//EMPIEZA CODIGO PARA LOS CLIENTES
//EMPIEZA CODIGO PARA LOS CLIENTES


function enviarDatosClienteC(){
    //donde se mostrar� lo resultados
    divResultado = document.getElementById('resultadoc');
    divFormulario = document.getElementById('formularioc');
    divResultado.innerHTML= 'Actualizando...';
    
    //valores de los cajas de texto
    id=document.frmcliente.idcliente.value;
    nomc=document.frmcliente.nombres.value;
    direc=document.frmcliente.direccion.value;
    telef=document.frmcliente.telefono.value;
    
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //usando del medoto POST
    //archivo que realizar� la operacion ->actualizacion.php
    ajax.open("POST", "actualizacionc.php",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar los nuevos registros en esta capa
            divResultado.innerHTML = ajax.responseText
            //una vez actualizacion ocultamos formulario
            divFormulario.style.display="none";

        }
    }
    //muy importante este encabezado ya que hacemos uso de un formulario
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando los valores
    ajax.send("idcliente="+id+"&nombres="+nomc+"&direccion="+direc+"&telefono="+telef)
}

function pedirDatosC(idcliente){
    //donde se mostrar� el formulario con los datos
    divFormulario = document.getElementById('formularioc');
    
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "consulta_por_idc.php");
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="block";
        }
    }
    //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("idemp="+idcliente)
}



function eliminarDatoC(idcliente){
    //donde se mostrar� el resultado de la eliminacion
    divResultado = document.getElementById('resultadoc');
    
    
    //usaremos un cuadro de confirmacion    
    var eliminar = confirm("De verdad desea eliminar este dato?")
    if ( eliminar ) {
        //instanciamos el objetoAjax
        ajax=objetoAjax();
        //uso del medotod GET
        //indicamos el archivo que realizar� el proceso de eliminaci�n
        //junto con un valor que representa el id del empleado
        ajax.open("GET", "eliminacionc.php?idcliente="+idcliente);
        divResultado.innerHTML= '<img src="anim.gif">';
        ajax.onreadystatechange=function() {
            if (ajax.readyState==4) {
                //mostrar resultados en esta capa
                divResultado.innerHTML = ajax.responseText
            }
        }
        //como hacemos uso del metodo GET
        //colocamos null
        ajax.send(null)
    }
}



function enviarDatosCliente2(){
  //donde se mostrar� lo resultados
  divResultado = document.getElementById('resultadoc');
  divResultado.innerHTML= '<img src="anim.gif">';
  //valores de las cajas de texto
  noms=document.nuevo_cliente.nombress.value;
  dire=document.nuevo_cliente.direccion.value;
  tele=document.nuevo_cliente.telefono.value;
  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medoto POST
  //archivo que realizar� la operacion
  //registro.php
  ajax.open("POST", "registroc.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divResultado.innerHTML = ajax.responseText
  //llamar a funcion para limpiar los inputs
  LimpiarCampossC();
  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores
  ajax.send("nombres="+noms+"&dire="+dire+"&telef="+tele)
}

function LimpiarCampossC(){
  document.nuevo_cliente.nombress.value="";
  document.nuevo_cliente.direccion.value="";
  document.nuevo_cliente.telefono.value="";
  document.nuevo_cliente.nombress.focus();
  }

//TERMINA CODIGO PARA LOS CLIENTES  
  
function pedirDatosAl(idpelicula,idcliente,cant,nombre_cli,precio,Submit){
    //donde se mostrar� el formulario con los datos
    divResultadoP = document.getElementById('resultadoP');
    divFormularioP = document.getElementById('formularioP');
    divResultadoP.innerHTML= '<img src="anim.gif">';
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "cambiarestado.php",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divResultadoP.innerHTML = ajax.responseText
            divFormularioP.style.display="block";
        }
    }
    //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("idpelicula="+idpelicula+"&idcliente="+idcliente+"&cant="+cant+"&nombre_cli="+nombre_cli+"&precio="+precio+"&Submit="+Submit)
} 


function buscaclientes(){
    //donde se mostrar� el formulario con los datos
    divResponClientes = document.getElementById('resp_clientes');
    divResponClientes.innerHTML= '<img src="anim.gif">';
    criterio=document.findclient.criterio.value;
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "consul_sel_clientes.php",true);
        ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divResponClientes.innerHTML = ajax.responseText
           }
    }
   //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send("criterio="+criterio)      
   
} 

function buscapeliculas(){
    //donde se mostrar� el formulario con los datos
    divResponPeliculas = document.getElementById('resp_peliculas');
    divResponPeliculas.innerHTML= '<img src="anim.gif">';
    criteriox=document.findmovies.criterio.value;
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "consul_sel_peliculas.php",true);
        ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divResponPeliculas.innerHTML = ajax.responseText
           }
    }
   //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send("criterio="+criteriox)      
   
}

function siguientepaso(){
    //donde se mostrar� el formulario con los datos
    divSegundopaso = document.getElementById('segundopaso');
    divSegundopaso.innerHTML= '<img src="anim.gif">';
     
	theDate=document.steptwo.theDate.value;
	nomsolicitante=document.steptwo.nomsolicitante.value;
    idsolicitante=document.steptwo.idsolicitante.value;
    cantidad=document.steptwo.cantidad.value;
	tipo=document.steptwo.tipo.value;
	if(theDate == ""){
                  alert("Seleccione la fecha de prestamo")
                  document.steptwo.theDate.focus()
                  return false;
        }
		if(nomsolicitante == ""){
                  alert("Seleccione la persona que va a prestar el libro")
                  document.steptwo.nomsolicitante.focus()
                  return false;
        }
		if(cantidad == ""){
                  alert("Debe agregar una cantidad especifica")
                  document.steptwo.cantidad.focus()
                  return false;
        }
		
    
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "PrestamoLibros_Insertar2.php",true);
        ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divSegundopaso.innerHTML = ajax.responseText
           }
    }
   //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send("nombre_cli="+nomsolicitante+"&cant="+cantidad+"&precio="+theDate+"&idcliente="+idsolicitante+"&tipo="+tipo)      
   
}



function ultimopaso(){
	
        var confirmar = confirm("Seguro de registrar prestamo?")
    if ( confirmar ) {


  //donde se mostrar� lo resultados
  divSegundopaso = document.getElementById('resultadoP');
  //divSegundopaso.innerHTML= '<img src="anim.gif">';  
  //valores de las cajas de texto
  //this.form.elements['submit'].disabled=true;
  cantid = document.stepthree.cant.value;
  idcliente = document.stepthree.idcliente.value;
  nombre_cli = document.stepthree.nombre_cli.value;
  precio = document.stepthree.fecha.value;
  
  nlibro = '';
  nucanti = '';
  colibros = '';
  		for(m=1; m <= cantid; m++)
		{
			nombrea = eval("document.stepthree.nombre"+m+".value");
			ilibro = eval("document.stepthree.nombre"+m+".value");
			cantidae = eval("document.stepthree.cantidad"+m+".value");			
			clibro = eval("document.stepthree.cliente"+m+".value");
			if(m==cantid){
				nlibro += nombrea;
				nucanti += cantidae;
				colibros += clibro;
				}else{
				nlibro += nombrea + ',';
				nucanti += cantidae + ',';
				colibros += clibro + ',';
					}
			
			if(eval("document.stepthree.nombre"+m+".value")!="" && eval("document.stepthree.cantidad"+m+".value")==""){
					alert("Debe agregar una cantidad especifica para el libro agregado");
                  return false;
				}

  //instanciamos el objetoAjax
    ajax=objetoAjax();
    }
	ajax.open("GET", "registroAlquiler_paso3.php?fecha="+precio+"&cantt="+cantid+"&idcliente="+idcliente+"&nombrePres="+nombre_cli+"&nombresLibro="+nlibro+"&catXli="+nucanti+"&codiLibros="+colibros);

divSegundopaso.innerHTML= '<img src="anim.gif">';
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divSegundopaso.innerHTML = ajax.responseText
  divSegundopaso.style.display="block";
  //llamar a funcion para limpiar los inputs
 // LimpiarCampossC();
  }
  }
ajax.send(null)
}} 