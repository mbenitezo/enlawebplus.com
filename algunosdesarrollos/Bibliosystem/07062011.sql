/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.0.51a : Database - biblioteca
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`biblioteca` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `biblioteca`;

/*Table structure for table `auxiliar` */

DROP TABLE IF EXISTS `auxiliar`;

CREATE TABLE `auxiliar` (
  `id_pelicula` int(255) NOT NULL auto_increment,
  `estado` varchar(2) NOT NULL,
  `id_cliente` int(255) NOT NULL,
  PRIMARY KEY  (`id_pelicula`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `auxiliar` */

insert  into `auxiliar`(`id_pelicula`,`estado`,`id_cliente`) values (1,'1',5),(2,'1',5),(3,'1',4),(4,'1',5),(5,'1',4);

/*Table structure for table `detalle_prestamo` */

DROP TABLE IF EXISTS `detalle_prestamo`;

CREATE TABLE `detalle_prestamo` (
  `id_prestamo` int(255) unsigned NOT NULL auto_increment COMMENT 'Identificador unico primario',
  `fecha_prestamo` date NOT NULL COMMENT 'Fecha de registro de libro',
  `id_solicitante` int(255) NOT NULL COMMENT 'Identificador del estudiante',
  `id_libros` int(255) NOT NULL COMMENT 'Identificador unico de libro',
  `cantidad` int(255) NOT NULL COMMENT 'Cantidad de libros prestado',
  `tipo_solicitante` varchar(2) NOT NULL COMMENT 'Para saber si es profesor o estudiante quien solicita el prestamo.',
  `entregado` int(2) NOT NULL,
  PRIMARY KEY  (`id_prestamo`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/*Data for the table `detalle_prestamo` */

insert  into `detalle_prestamo`(`id_prestamo`,`fecha_prestamo`,`id_solicitante`,`id_libros`,`cantidad`,`tipo_solicitante`,`entregado`) values (1,'2011-06-03',5,3,7,'E',0),(2,'2011-06-03',5,1,5,'E',0),(3,'2011-06-03',5,3,3,'E',0),(4,'2011-06-03',5,2,4,'E',0),(5,'2011-06-03',1,1,6,'E',0),(11,'2011-06-03',3,3,8,'E',0),(6,'2011-06-03',4,1,5,'E',0),(7,'2011-06-03',4,2,2,'E',0),(8,'2011-06-03',4,3,4,'E',0),(14,'2011-06-04',5,1,20,'E',0),(12,'2011-06-03',3,2,10,'E',0),(9,'2011-06-03',3,1,3,'E',0),(10,'2011-06-03',3,3,5,'E',0),(13,'2011-06-03',3,1,7,'E',0),(19,'2011-06-05',1,4,1,'E',0),(15,'2011-06-05',4,2,2,'E',0),(16,'2011-06-05',4,3,4,'E',1),(17,'2011-06-05',4,4,1,'E',1),(18,'2011-06-05',4,5,7,'E',1),(20,'2011-06-05',1,4,1,'E',1),(21,'2011-06-05',5,1,2,'E',1),(22,'2011-06-05',5,4,3,'E',0),(23,'2011-06-05',5,4,3,'E',0),(24,'2011-06-05',5,4,3,'E',0),(25,'2011-06-05',5,4,1,'E',0),(26,'2011-06-05',5,4,1,'E',0),(27,'2011-06-05',5,4,2,'E',0),(28,'2011-06-05',5,4,2,'E',0),(29,'2011-06-05',5,4,3,'E',0),(30,'2011-06-05',5,4,1,'E',0),(31,'2011-06-05',5,4,1,'E',0),(32,'2011-06-05',5,1,4,'E',1),(33,'2011-06-05',5,1,4,'E',1),(34,'2011-06-05',5,2,1,'E',1),(35,'2011-06-05',5,1,1,'E',1),(36,'2011-06-05',5,1,1,'E',1),(37,'2011-06-11',5,2,2,'E',1);

/*Table structure for table `estudiantes` */

DROP TABLE IF EXISTS `estudiantes`;

CREATE TABLE `estudiantes` (
  `id` int(255) NOT NULL auto_increment COMMENT 'Identificador de estudiante',
  `cod_estudiante` varchar(255) NOT NULL COMMENT 'Codigo estudiantil',
  `tipo_doc` varchar(255) NOT NULL COMMENT 'Seleccion de tipo de documento',
  `num_doc` varchar(255) NOT NULL COMMENT 'Ingreso de numero de documento',
  `nom_completo` varchar(255) NOT NULL COMMENT 'Nombre del estudiante',
  `grado` varchar(255) NOT NULL COMMENT 'Grado que cursa el estudiante',
  `telefono` varchar(255) NOT NULL COMMENT 'Telefono para llamar al estudiante',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `estudiantes` */

insert  into `estudiantes`(`id`,`cod_estudiante`,`tipo_doc`,`num_doc`,`nom_completo`,`grado`,`telefono`) values (1,'0001','CC','1036613885','Mario alejandro benitez orozco','91','4545'),(2,'0002','CC','1045145432','Juanito de los angeles','82','65456456'),(3,'0003','CC','2242639','Pedro casanova guzman','63','777777'),(4,'0004','TI','893759389','Juan perez palacios','91','8888888'),(5,'0005','RC','34232222','Anacleto diaz','102','99979'),(6,'1515155','RC','asdasgsdf','asdfa qwrqwer','124','87878878'),(7,'0211211','TI','1524587412','Benito cardenas','111','5413136'),(9,'5645553','TI','667865564','ZDHD G DSFG SDG SGFG','0','21245458');

/*Table structure for table `grados` */

DROP TABLE IF EXISTS `grados`;

CREATE TABLE `grados` (
  `id_grado` int(255) unsigned NOT NULL auto_increment COMMENT 'Grado',
  `codigo_grado` varchar(255) NOT NULL COMMENT 'numero del grado',
  `nombre_grado` varchar(255) NOT NULL COMMENT 'Nombre del grado',
  PRIMARY KEY  (`id_grado`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

/*Data for the table `grados` */

insert  into `grados`(`id_grado`,`codigo_grado`,`nombre_grado`) values (1,'0','Preescolar'),(2,'11','Primero A'),(3,'12','Primero B'),(4,'13','Primero C'),(5,'14','Primero D'),(6,'21','Segundo A'),(7,'22','Segundo B'),(8,'23','Segundo C'),(9,'24','Segundo D'),(10,'31','Tercero A'),(11,'32','Tercero B'),(12,'33','Tercero C'),(13,'34','Tercero D'),(14,'41','Cuarto A'),(15,'42','Cuarto B'),(16,'43','Cuarto C'),(17,'44','Cuarto D'),(18,'51','Quinto A'),(19,'52','Quinto B'),(20,'53','Quinto C'),(21,'54','Quinto D'),(22,'61','Sexto A'),(23,'62','Sexto B'),(24,'63','Sexto C'),(25,'64','Sexto D'),(26,'71','Séptimo A'),(27,'72','Séptimo B'),(28,'73','Séptimo C'),(29,'74','Séptimo D'),(30,'81','Octavo A'),(31,'82','Octavo B'),(32,'83','Octavo C'),(33,'84','Octavo D'),(34,'91','Noveno A'),(35,'92','Noveno B'),(36,'93','Noveno C'),(37,'94','Noveno D'),(38,'101','Décimo A'),(39,'102','Décimo B'),(40,'103','Décimo C'),(41,'104','Décimo D'),(42,'111','Undécimo A'),(43,'112','Undécimo B'),(44,'113','Undécimo C'),(45,'114','Undécimo D'),(46,'121','Clei 1'),(47,'122','Clei 2'),(48,'123','Clei 3'),(49,'124','Clei 4'),(50,'125','Clei 5'),(51,'126','Clei 6');

/*Table structure for table `libros` */

DROP TABLE IF EXISTS `libros`;

CREATE TABLE `libros` (
  `idLibro` int(255) NOT NULL auto_increment COMMENT 'Identiificador primario de la tabla libros.',
  `nom_libro` varchar(255) NOT NULL COMMENT 'Nombre del libro',
  `nom_autor` varchar(255) NOT NULL COMMENT 'Nombre del autor',
  `nom_editorial` varchar(255) NOT NULL COMMENT 'Editorial del libro',
  `cant_libro` varchar(255) NOT NULL COMMENT 'Cantidad de libros existentes',
  PRIMARY KEY  (`idLibro`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `libros` */

insert  into `libros`(`idLibro`,`nom_libro`,`nom_autor`,`nom_editorial`,`cant_libro`) values (1,'Juana de arco','La novicia rebelde','Caterpila','20'),(2,'La odisea','Homero','Cruzcol','10'),(3,'La maria','Jorge Isaac','Columbus','12'),(4,'Blanca nieves y los 7 enanitos','Pablo neruda','Casablanca','50'),(5,'El principito','Pombo','Laberinto','30');

/*Table structure for table `profesores` */

DROP TABLE IF EXISTS `profesores`;

CREATE TABLE `profesores` (
  `idProfesor` int(255) unsigned NOT NULL auto_increment COMMENT 'Id unico de profesor',
  `nom_profesor` varchar(255) NOT NULL COMMENT 'Nombre completo del maestro',
  `doc_profesor` varchar(255) NOT NULL COMMENT 'Documento de identidad del profesor',
  `cel_profesor` varchar(255) NOT NULL COMMENT 'Celular del profesor',
  `dir_profesor` varchar(255) NOT NULL COMMENT 'Direccion de grupo al cual pertenece el profesor',
  PRIMARY KEY  (`idProfesor`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `profesores` */

insert  into `profesores`(`idProfesor`,`nom_profesor`,`doc_profesor`,`cel_profesor`,`dir_profesor`) values (1,'BENAVIDES MENA ANACLETO DE JESUS','12336588','12336588','3'),(2,'pedrosa guadalupe','28282882','28282882','93');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
