/*
SQLyog Ultimate v8.55 
MySQL - 5.5.8 : Database - stars
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`stars` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `stars`;

/*Table structure for table `noticias` */

DROP TABLE IF EXISTS `noticias`;

CREATE TABLE `noticias` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `titulo_noticia` varchar(255) DEFAULT NULL,
  `detalle_noticia` longtext,
  `cate_noticia` int(2) DEFAULT NULL,
  `estado_noticia` int(2) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `usu_creador` int(2) DEFAULT NULL,
  `nombre_imagen` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `noticias` */

insert  into `noticias`(`id`,`titulo_noticia`,`detalle_noticia`,`cate_noticia`,`estado_noticia`,`fecha_creacion`,`usu_creador`,`nombre_imagen`) values (2,'asdA SDasdf asDF ASdf asdfsad fasd fsadf sadf asdf sadf ','dsa dasdf sadf asf asdf fgdfhg dfjfdhfdgslijaslkc nbbasdfnbasdfmhwesd lsdasd sd asdfasdkkas dks ks k ksa dks kadk sad ajsd sdhsadh sadhf sadhf sd hsad hfsadk fsdhf sakjfsajdf sajdfhjskhdf jshdfsahd fjshad fjkhsdjf hsdk jfhsk dfhksd f sdfsad fsa dfsad fsad fasd f',1,0,'2012-03-17 06:19:09',1,'prueba(20120317061909).jpg'),(3,'asdA SDasdf asDF ASdf asdfsad fasd fsadf sadf asdf sadf ','dsa dasdf sadf asf asdf fgdfhg dfjfdhfdgslijaslkc nbbasdfnbasdfmhwesd lsdasd sd asdfasdkkas dks ks k ksa dks kadk sad ajsd sdhsadh sadhf sadhf sd hsad hfsadk fsdhf sakjfsajdf sajdfhjskhdf jshdfsahd fjshad fjkhsdjf hsdk jfhsk dfhksd f sdfsad fsa dfsad fsad fasd f',1,0,'2012-03-17 06:19:29',1,'prueba(20120317061929).jpg');

/*Table structure for table `ratings` */

DROP TABLE IF EXISTS `ratings`;

CREATE TABLE `ratings` (
  `id` varchar(11) NOT NULL,
  `total_votes` int(11) NOT NULL,
  `total_value` int(11) NOT NULL,
  `used_ips` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ratings` */

insert  into `ratings`(`id`,`total_votes`,`total_value`,`used_ips`) values ('2id',1,4,'a:1:{i:0;s:9:\"127.0.0.1\";}');

/*Table structure for table `rol` */

DROP TABLE IF EXISTS `rol`;

CREATE TABLE `rol` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nombre_rol` varchar(255) DEFAULT NULL,
  `descripcion_rol` varchar(255) DEFAULT NULL,
  `Opc_Roles` int(2) DEFAULT NULL,
  `Opc_Usuarios` int(2) DEFAULT NULL,
  `Opc_Categoria` int(2) DEFAULT NULL,
  `Opc_Noticias` int(2) DEFAULT NULL,
  `Tipo_autorizacion` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `rol` */

insert  into `rol`(`id`,`nombre_rol`,`descripcion_rol`,`Opc_Roles`,`Opc_Usuarios`,`Opc_Categoria`,`Opc_Noticias`,`Tipo_autorizacion`) values (1,'Administrador','Rol con acceso a todos las opciones del sistema',1,1,1,1,1),(2,'Supervisor','Rol con acceso a la sección de noticias.',0,0,0,1,0),(4,'Periodista','Rol con acceso a la opción de Noticias',0,0,0,1,0),(5,'Lector','Rol con acceso a comentarios de las publicaciones en el portal.',0,0,0,0,0);

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `identificacionUsu` varchar(12) DEFAULT NULL,
  `nombrePersona` varchar(255) DEFAULT NULL,
  `nomUsu` varchar(255) DEFAULT NULL,
  `passwords` varchar(255) DEFAULT NULL,
  `rol` int(2) DEFAULT NULL,
  `estado` int(2) DEFAULT NULL,
  `fechaCrea` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `usuarios` */

insert  into `usuarios`(`id`,`identificacionUsu`,`nombrePersona`,`nomUsu`,`passwords`,`rol`,`estado`,`fechaCrea`) values (10,'1036613885','Mario Benitez Orozco','maalben','123...',1,1,'2012-03-17 17:13:44'),(11,'147852369','Pedro Fernandez','pedrito','11111',5,1,'2012-03-17 17:20:16');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
