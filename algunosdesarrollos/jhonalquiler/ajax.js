// Declaro un array en el cual los indices son los ID's de los DIVS que funcionan como pesta�a y los valores son los identificadores de las secciones a cargar
var tabsId=new Array();
tabsId['tab1']='seccion1';
tabsId['tab2']='seccion2';
tabsId['tab3']='seccion3';
tabsId['tab4']='seccion4';
tabsId['tab5']='seccion5';
// Declaro el ID del DIV que actuar� como contenedor de los datos recibidos
var contenedor='tabContenido';


function objetoAjax(){
	var xmlhttp=false;
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(E){
			xmlhttp = false;
  		}
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}


function cargaContenido()
{
    /* Recorro las pesta�as para dejar en estado "apagado" a todas menos la que se ha clickeado. Teniendo en cuenta que solo puede haber una pesta�a "encendida"
    a la vez resultar�a mas �ptimo hacer un while hasta encontrar a esa pesta�a, cambiarle el estilo y luego salir, pero, creanme, se complicar�a un poco el
    ejemplo y no es mi intenci�n complicarlos */
    for(key in tabsId)
    {
        // Obtengo el elemento
        elemento=document.getElementById(key);
        // Si es la pesta�a activa
        if(elemento.className=='tabOn')
        {
            // Cambio el estado de la pesta�a a inactivo 
            elemento.className='tabOff';
        }
    }
    // Cambio el estado de la pesta�a que se ha clickeado a activo
    this.className='tabOn';
    
    /* De aqui hacia abajo se tratatan la petici�n y recepci�n de datos */
    
    // Obtengo el identificador vinculado con el ID del elemento HTML que referencia a la secci�n a cargar
    seccion=tabsId[this.id];
    
    // Coloco un mensaje mientras se reciben los datos
    tabContenedor.innerHTML='<img src="loading2.gif"> Cargando, por favor espere...';
    
    // Creo el objeto AJAX y envio la petici�n por POST (para evitar cacheos de datos)
    var ajax=objetoAjax();
    ajax.open("POST", "tabs_o_pestanas_con_javascript_no_intrusivo_proceso.php", true);
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax.send('seccion='+seccion);
    
    ajax.onreadystatechange=function()
    {
        if(ajax.readyState==4)
        {
            // Al recibir la respuesta coloco directamente el HTML en la capa contenedora
            tabContenedor.innerHTML=ajax.responseText;
        }
    }
}

function mouseSobre()
{
    // Si el evento no se produjo en la pesta�a seleccionada...
    if(this.className!='tabOn')
    {
        // Cambio el color de fondo de la pesta�a
        this.className='tabHover';
    }
}

function mouseFuera()
{
    // Si el evento no se produjo en la pesta�a seleccionada...
    if(this.className!='tabOn')
    {
        // Cambio el color de fondo de la pesta�a
        this.className='tabOff';
    }
}

onload=function()
{
    for(key in tabsId)
    {
        // Voy obteniendo los ID's de los elementos declarados en el array que representan a las pesta�as
        elemento=document.getElementById(key);
        // Asigno que al hacer click en una pesta�a se llame a la funcion cargaContenido
        elemento.onclick=cargaContenido;
        /* El cambio de estilo es en 2 funciones diferentes debido a la incompatibilidad del string de backgroundColor devuelto por Mozilla e IE.
        Se podr�a pasar de rgb(xxx, xxx, xxx) a formato #xxxxxx pero complicar�a innecesariamente el ejemplo */
        elemento.onmouseover=mouseSobre;
        elemento.onmouseout=mouseFuera;
    }
    // Obtengo la capa contenedora de datos
    tabContenedor=document.getElementById(contenedor);
}



///////////////////////EMPIEZA CODIGO PARA LAS PELICULAS


function enviarDatosPelicula(){
	//donde se mostrar� lo resultados
	divResultado = document.getElementById('resultado');
	divFormulario = document.getElementById('formulario');
	divResultado.innerHTML= 'Actualizando...';
	
	//valores de los cajas de texto
	id=document.frmpelicula.idpelicula.value;
	nom=document.frmpelicula.nombres.value;
	tipo=document.frmpelicula.tipo.value;
	descr=document.frmpelicula.descripcion.value;
	
	//instanciamos el objetoAjax
	ajax=objetoAjax();
	//usando del medoto POST
	//archivo que realizar� la operacion ->actualizacion.php
	ajax.open("POST", "actualizacion.php",true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			//mostrar los nuevos registros en esta capa
			divResultado.innerHTML = ajax.responseText
			//una vez actualizacion ocultamos formulario
			divFormulario.style.display="none";

		}
	}
	//muy importante este encabezado ya que hacemos uso de un formulario
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	//enviando los valores
	ajax.send("idpelicula="+id+"&nombres="+nom+"&tipo="+tipo+"&descripcion="+descr)
}

function pedirDatos(idpelicula){
	//donde se mostrar� el formulario con los datos
	divFormulario = document.getElementById('formulario');
	
	//instanciamos el objetoAjax
	ajax=objetoAjax();
	//uso del medotod POST
	ajax.open("POST", "consulta_por_id.php");
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			//mostrar resultados en esta capa
			divFormulario.innerHTML = ajax.responseText
			divFormulario.style.display="block";
		}
	}
	//como hacemos uso del metodo POST
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	//enviando el codigo del empleado
	ajax.send("idemp="+idpelicula)
}



function eliminarDato(idpelicula){
	//donde se mostrar� el resultado de la eliminacion
	divResultado = document.getElementById('resultado');
	
	
	//usaremos un cuadro de confirmacion	
	var eliminar = confirm("De verdad desea eliminar este dato?")
	if ( eliminar ) {
		//instanciamos el objetoAjax
		ajax=objetoAjax();
		//uso del medotod GET
		//indicamos el archivo que realizar� el proceso de eliminaci�n
		//junto con un valor que representa el id del empleado
		ajax.open("GET", "eliminacion.php?idpelicula="+idpelicula);
		divResultado.innerHTML= '<img src="anim.gif">';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}
		//como hacemos uso del metodo GET
		//colocamos null
		ajax.send(null)
	}
}



function enviarDatosPelicula2(){
  //donde se mostrar� lo resultados
  divResultado = document.getElementById('resultado');
  divResultado.innerHTML= '<img src="anim.gif">';
  //valores de las cajas de texto
  noms=document.nueva_pelicula.nombress.value;
  tip=document.nueva_pelicula.tipo.value;
  descr=document.nueva_pelicula.descripcion.value;
  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medoto POST
  //archivo que realizar� la operacion
  //registro.php
  ajax.open("POST", "registro.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divResultado.innerHTML = ajax.responseText
  //llamar a funcion para limpiar los inputs
  LimpiarCamposs();
  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores
  ajax.send("nombres="+noms+"&tipo="+tip+"&descripcion="+descr)
}

function LimpiarCamposs(){
  document.nueva_pelicula.nombress.value="";
  document.nueva_pelicula.tipo.value="";
  document.nueva_pelicula.descripcion.value="";
  document.nueva_pelicula.nombress.focus();
  }
  
//TERMINA CODIGO PARA LAS PELICULAS




//EMPIEZA CODIGO PARA LOS CLIENTES
//EMPIEZA CODIGO PARA LOS CLIENTES
//EMPIEZA CODIGO PARA LOS CLIENTES
//EMPIEZA CODIGO PARA LOS CLIENTES
//EMPIEZA CODIGO PARA LOS CLIENTES


function enviarDatosClienteC(){
    //donde se mostrar� lo resultados
    divResultado = document.getElementById('resultadoc');
    divFormulario = document.getElementById('formularioc');
    divResultado.innerHTML= 'Actualizando...';
    
    //valores de los cajas de texto
    id=document.frmcliente.idcliente.value;
    nomc=document.frmcliente.nombres.value;
    direc=document.frmcliente.direccion.value;
    telef=document.frmcliente.telefono.value;
    
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //usando del medoto POST
    //archivo que realizar� la operacion ->actualizacion.php
    ajax.open("POST", "actualizacionc.php",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar los nuevos registros en esta capa
            divResultado.innerHTML = ajax.responseText
            //una vez actualizacion ocultamos formulario
            divFormulario.style.display="none";

        }
    }
    //muy importante este encabezado ya que hacemos uso de un formulario
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando los valores
    ajax.send("idcliente="+id+"&nombres="+nomc+"&direccion="+direc+"&telefono="+telef)
}

function pedirDatosC(idcliente){
    //donde se mostrar� el formulario con los datos
    divFormulario = document.getElementById('formularioc');
    
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "consulta_por_idc.php");
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divFormulario.innerHTML = ajax.responseText
            divFormulario.style.display="block";
        }
    }
    //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("idemp="+idcliente)
}



function eliminarDatoC(idcliente){
    //donde se mostrar� el resultado de la eliminacion
    divResultado = document.getElementById('resultadoc');
    
    
    //usaremos un cuadro de confirmacion    
    var eliminar = confirm("De verdad desea eliminar este dato?")
    if ( eliminar ) {
        //instanciamos el objetoAjax
        ajax=objetoAjax();
        //uso del medotod GET
        //indicamos el archivo que realizar� el proceso de eliminaci�n
        //junto con un valor que representa el id del empleado
        ajax.open("GET", "eliminacionc.php?idcliente="+idcliente);
        divResultado.innerHTML= '<img src="anim.gif">';
        ajax.onreadystatechange=function() {
            if (ajax.readyState==4) {
                //mostrar resultados en esta capa
                divResultado.innerHTML = ajax.responseText
            }
        }
        //como hacemos uso del metodo GET
        //colocamos null
        ajax.send(null)
    }
}



function enviarDatosCliente2(){
  //donde se mostrar� lo resultados
  divResultado = document.getElementById('resultadoc');
  divResultado.innerHTML= '<img src="anim.gif">';
  //valores de las cajas de texto
  noms=document.nuevo_cliente.nombress.value;
  dire=document.nuevo_cliente.direccion.value;
  tele=document.nuevo_cliente.telefono.value;
  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medoto POST
  //archivo que realizar� la operacion
  //registro.php
  ajax.open("POST", "registroc.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divResultado.innerHTML = ajax.responseText
  //llamar a funcion para limpiar los inputs
  LimpiarCampossC();
  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores
  ajax.send("nombres="+noms+"&dire="+dire+"&telef="+tele)
}

function LimpiarCampossC(){
  document.nuevo_cliente.nombress.value="";
  document.nuevo_cliente.direccion.value="";
  document.nuevo_cliente.telefono.value="";
  document.nuevo_cliente.nombress.focus();
  }

//TERMINA CODIGO PARA LOS CLIENTES  
  
function pedirDatosAl(idpelicula,idcliente,cant,nombre_cli,precio,Submit){
    //donde se mostrar� el formulario con los datos
    divResultadoP = document.getElementById('resultadoP');
    divFormularioP = document.getElementById('formularioP');
    divResultadoP.innerHTML= '<img src="anim.gif">';
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "cambiarestado.php",true);
    ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divResultadoP.innerHTML = ajax.responseText
            divFormularioP.style.display="block";
        }
    }
    //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //enviando el codigo del empleado
    ajax.send("idpelicula="+idpelicula+"&idcliente="+idcliente+"&cant="+cant+"&nombre_cli="+nombre_cli+"&precio="+precio+"&Submit="+Submit)
} 

function buscaclientes(){
    //donde se mostrar� el formulario con los datos
    divResponClientes = document.getElementById('resp_clientes');
    divResponClientes.innerHTML= '<img src="anim.gif">';
    criteriox=document.findclient.criterio.value;
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "consul_sel_clientes.php",true);
        ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divResponClientes.innerHTML = ajax.responseText
           }
    }
   //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send("criterio="+criteriox)      
   
} 


function buscapeliculas(){
    //donde se mostrar� el formulario con los datos
    divResponPeliculas = document.getElementById('resp_peliculas');
    divResponPeliculas.innerHTML= '<img src="anim.gif">';
    criteriox=document.findmovies.criterio.value;
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "consul_sel_peliculas.php",true);
        ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divResponPeliculas.innerHTML = ajax.responseText
           }
    }
   //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send("criterio="+criteriox)      
   
}
                     
function siguientepaso(){
    //donde se mostrar� el formulario con los datos
    divSegundopaso = document.getElementById('segundopaso');
    divSegundopaso.innerHTML= '<img src="anim.gif">';
        
    nombrecliente=document.steptwo.nombre_cli.value;
    cantidad=document.steptwo.cant.value;
    precios=document.steptwo.precio.value;
    idcliente=document.steptwo.idcliente.value;
        if(nombrecliente == ""){
                  alert("Seleccione el cliente del listado")
                  document.steptwo.nombre_cli.focus()
                  return false;
        }
    
    //instanciamos el objetoAjax
    ajax=objetoAjax();
    //uso del medotod POST
    ajax.open("POST", "registroAlquiler_paso2.php",true);
        ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
            //mostrar resultados en esta capa
            divSegundopaso.innerHTML = ajax.responseText
           }
    }
   //como hacemos uso del metodo POST
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send("nombre_cli="+nombrecliente+"&cant="+cantidad+"&precio="+precios+"&idcliente="+idcliente)      
   
}



function ultimopaso(){
        var confirmar = confirm("Seguro de registrar alquiler?")
    if ( confirmar ) {


  //donde se mostrar� lo resultados
  divSegundopaso = document.getElementById('resultadoP');
  //divSegundopaso.innerHTML= '<img src="anim.gif">';  
  //valores de las cajas de texto
  cantidad = document.stepthree.cant.value;
  idcliente = document.stepthree.idcliente.value;
  nombre_cli = document.stepthree.nombre_cli.value;
  precio = document.stepthree.precio.value;
  precios=document.steptwo.precio.value;
        if(precios == ""){
                  alert("No haz ingresado el valor de la venta")
                  document.steptwo.precio.focus()
                  return false;
        }
  nombre1 = document.stepthree.nombre1.value;
  nombre2 = document.stepthree.nombre2.value;
  nombre3 = document.stepthree.nombre3.value;
  nombre4 = document.stepthree.nombre4.value;
  nombre5 = document.stepthree.nombre5.value;
  nombre6 = document.stepthree.nombre6.value;
  nombre7 = document.stepthree.nombre7.value;
  nombre8 = document.stepthree.nombre8.value;
  nombre9 = document.stepthree.nombre9.value;
  nombre10 = document.stepthree.nombre10.value;
/*  nombre11 = document.stepthree.nombre11.value;
  nombre12 = document.stepthree.nombre12.value;
  nombre13 = document.stepthree.nombre13.value;
  nombre14 = document.stepthree.nombre14.value;
  nombre15 = document.stepthree.nombre15.value;
  nombre16 = document.stepthree.nombre16.value;
  nombre17 = document.stepthree.nombre17.value;
  nombre18 = document.stepthree.nombre18.value;
  nombre19 = document.stepthree.nombre19.value;
  nombre20 = document.stepthree.nombre20.value; */
  cliente1 = document.stepthree.cliente1.value; 
  cliente2 = document.stepthree.cliente2.value; 
  cliente3 = document.stepthree.cliente3.value; 
  cliente4 = document.stepthree.cliente4.value; 
  cliente5 = document.stepthree.cliente5.value; 
  cliente6 = document.stepthree.cliente6.value; 
  cliente7 = document.stepthree.cliente7.value; 
  cliente8 = document.stepthree.cliente8.value; 
  cliente9 = document.stepthree.cliente9.value; 
  cliente10 = document.stepthree.cliente10.value; 
/*  cliente11 = document.stepthree.cliente11.value; 
  cliente12 = document.stepthree.cliente12.value; 
  cliente13 = document.stepthree.cliente13.value; 
  cliente14 = document.stepthree.cliente14.value; 
  cliente15 = document.stepthree.cliente15.value; 
  cliente16 = document.stepthree.cliente16.value; 
  cliente17 = document.stepthree.cliente17.value; 
  cliente18 = document.stepthree.cliente18.value; 
  cliente19 = document.stepthree.cliente19.value; 
  cliente20 = document.stepthree.cliente20.value;  */
  
  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medoto POST
  //archivo que realizar� la operacion
  //registro.php
  //ajax.open("POST", "registroAlquiler_paso3.php",true);
//ajax.open("GET", "registroAlquiler_paso3.php?cant="+cantidad+"&idcliente="+idcliente+"&nombre_cli="+nombre_cli+"&precio="+precio+"&nombre1="+nombre1+"&nombre2="+nombre2+"&nombre3="+nombre3+"&nombre4="+nombre4+"&nombre5="+nombre5+"&nombre6="+nombre6+"&nombre7="+nombre7+"&nombre8="+nombre8+"&nombre9="+nombre9+"&nombre10="+nombre10+"&cliente1="+cliente1+"&cliente2="+cliente2+"&cliente3="+cliente3+"&cliente4="+cliente4+"&cliente5="+cliente5);
ajax.open("GET", "registroAlquiler_paso3.php?cant="+cantidad+"&idcliente="+idcliente+"&nombre_cli="+nombre_cli+"&nombre1="+nombre1+"&nombre2="+nombre2+"&nombre3="+nombre3+"&nombre4="+nombre4+"&nombre5="+nombre5+"&nombre6="+nombre6+"&nombre7="+nombre7+"&nombre8="+nombre8+"&nombre9="+nombre9+"&nombre10="+nombre10+"&cliente1="+cliente1+"&cliente2="+cliente2+"&cliente3="+cliente3+"&cliente4="+cliente4+"&cliente5="+cliente5+"&cliente6="+cliente6+"&cliente7="+cliente7+"&cliente8="+cliente8+"&cliente9="+cliente9+"&cliente10="+cliente10+"&precio="+precios);
divSegundopaso.innerHTML= '<img src="anim.gif">';
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  //mostrar resultados en esta capa
  divSegundopaso.innerHTML = ajax.responseText
  divSegundopaso.style.display="block";
  //llamar a funcion para limpiar los inputs
 // LimpiarCampossC();
  }
  }
//  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores
//  ajax.send("cant="+cantidad+"&idcliente="+idcliente+"&nombre_cli="+nombre_cli+"&precio="+precio+"&nombre1="+nombre1+"&nombre2="+nombre2+"&nombre3="+nombre3+"&nombre4="+nombre4+"&nombre5="+nombre5+"&nombre6="+nombre6+"&nombre7="+nombre7+"&nombre8="+nombre8+"&nombre9="+nombre9+"&nombre10="+nombre10+"&nombre11="+nombre11+"&nombre12="+nombre12+"&nombre13="+nombre13+"&nombre14="+nombre14+"&nombre15="+nombre15+"&nombre16="+nombre16+"&nombre17="+nombre17+"&nombre18="+nombre18+"&nombre19="+nombre19+"&nombre20="+nombre20+"&cliente1="+cliente1+"&cliente2="+cliente2+"&cliente3="+cliente3+"&cliente4="+cliente4+"&cliente5="+cliente5+"&cliente6="+cliente6+"&cliente7="+cliente7+"&cliente8="+cliente8+"&cliente9="+cliente9+"&cliente10="+cliente10+"&cliente11="+cliente11+"&cliente12="+cliente12+"&cliente13="+cliente13+"&cliente14="+cliente14+"&cliente15="+cliente15+"&cliente16="+cliente16+"&cliente17="+cliente17+"&cliente18="+cliente18+"&cliente19="+cliente19+"&cliente20="+cliente20)
//ajax.send("cant="+cantidad+"&idcliente="+idcliente+"&nombre_cli="+nombre_cli+"&precio="+precio+"&nombre1="+nombre1+"&nombre2="+nombre2+"&nombre3="+nombre3+"&nombre4="+nombre4+"&nombre5="+nombre5+"&cliente1="+cliente1+"&cliente2="+cliente2+"&cliente3="+cliente3+"&cliente4="+cliente4+"&cliente5="+cliente5)
ajax.send(null)
    }
}