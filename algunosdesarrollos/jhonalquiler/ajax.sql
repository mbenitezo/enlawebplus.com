/*
SQLyog Community Edition- MySQL GUI v6.06
Host - 5.0.45-community-nt : Database - ajax3
*********************************************************************
Server version : 5.0.45-community-nt
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

create database if not exists `ajax3`;

USE `ajax3`;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `tabs_tabla_1` */

DROP TABLE IF EXISTS `tabs_tabla_1`;

CREATE TABLE `tabs_tabla_1` (
  `id` int(2) NOT NULL auto_increment,
  `datos` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tabs_tabla_1` */

insert  into `tabs_tabla_1`(`id`,`datos`) values (1,'tab1'),(2,'tab2'),(3,'tab3'),(5,'tab5'),(4,'tab4');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
