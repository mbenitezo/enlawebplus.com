<?php 
//referenciamos la clase DBManager
include_once("conexion.php");

//implementamos la clase pelicula
class cPelicula{
 //constructor	
 function cPelicula(){
 }	
 
 // consulta las peliculas de la BD
 function consultar(){
   //creamos el objeto $con a partir de la clase DBManager
   $con = new DBManager;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectar()==true){
     $query = "select * from peliculas order by nombre";
	 $result = @mysql_query($query);
	 if (!$result)
	   return false;
	 else
	   return $result;
   }
 }
 //inserta una nueva pelicula en la base de datos
 function crear($noms,$descri,$tipo){
     $espa = utf8_decode($noms);
     $espa2 = utf8_decode($descri);
     $espa3 = utf8_decode($tipo);
     $con = new DBManager;
   if($con->conectar()==true){
     $query = "INSERT INTO peliculas (nombre, descripcion, tipo) 
	 VALUES ('$espa','$espa2','$espa3')";
     $result = mysql_query($query);
      $query2 = "INSERT INTO auxiliar (estado) value ('0')";
      $result2 = mysql_query($query2);
     if (!$result)
	   return false;
     else
       return true;
   }
 }
 // actualizar una nueva pelicula en la base de datos
 function actualizar($cod,$nomm,$descrr,$tipoo){
     $ccod = utf8_decode($cod);
     $nnomm = utf8_decode($nomm);
     $ddescrr = utf8_decode($descrr);
     $ttipoo = utf8_decode($tipoo);
     $con = new DBManager;
   if($con->conectar()==true) {
     $query = "UPDATE peliculas SET nombre='$nnomm', descripcion='$ddescrr', tipo='$ttipoo' 
	 WHERE id=$cod";
     $result = mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 // consulta pelicula por su codigo
 function consultarid($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "SELECT * FROM peliculas WHERE id=$cod";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return $result;
    }
  
 }
 
  //elimina una pelicula en la base de datos
 function eliminar($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "DELETE FROM peliculas WHERE id=$cod";
     $query = "DELETE FROM auxiliar WHERE id_pelicula=$cod";
     $result = @mysql_query($query);
     if (!$result)
	   return false;
     else
       return true;
   }
 }



//implementamos la clase Clientes

 // consulta los clientes de la BD
 function consultarC(){
   //creamos el objeto $con a partir de la clase DBManager
   $con = new DBManager;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectar()==true){
     $query = "select * from clientes order by nombrec";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return $result;
   }
 }
 //inserta un nuevo cliente en la base de datos
 function crearC($nomc,$dire,$telef){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "INSERT INTO clientes (nombrec, direccionc, telefonoc) 
     VALUES ('$nomc','$dire','$telef')";
     $result = mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 // actualizar un nuev cliente en la base de datos
 function actualizarC($cod,$nomc,$direcc,$telefc){
     //$ccod = utf8_decode($cod);
     $nomcc = utf8_decode($nomc);
     $ddirecc = utf8_decode($direcc);
     $ttelefc = utf8_decode($telefc);  
   $con = new DBManager;
   if($con->conectar()==true) {
     $query = "UPDATE clientes SET nombrec='$nomcc', direccionc='$ddirecc', telefonoc='$ttelefc' 
     WHERE id=$cod";
     $result = mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
 // consulta clientes por su codigo
 function consultaridC($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "SELECT * FROM clientes WHERE id=$cod";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return $result;
    }
  
 }
 
  //elimina un cliente en la base de datos
 function eliminarC($cod){
   $con = new DBManager;
   if($con->conectar()==true){
     $query = "DELETE FROM clientes WHERE id=$cod";
     $result = @mysql_query($query);
     if (!$result)
       return false;
     else
       return true;
   }
 }
}
?>
