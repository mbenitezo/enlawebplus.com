-- phpMyAdmin SQL Dump
-- version 2.10.0.2
-- http://www.phpmyadmin.net
-- 
-- Servidor: localhost
-- Tiempo de generación: 23-10-2007 a las 13:51:47
-- Versión del servidor: 4.1.22
-- Versión de PHP: 4.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Base de datos: `interpas_SIPPASS`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `categorias`
-- 

CREATE TABLE `categorias` (
  `idcategoria` int(10) unsigned NOT NULL default '0',
  `catnombre` varchar(70) NOT NULL default '',
  PRIMARY KEY  USING BTREE (`idcategoria`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `categorias`
-- 

INSERT INTO `categorias` VALUES (1, 'SUPERMERCADOS');
INSERT INTO `categorias` VALUES (2, 'ALMACENES');
INSERT INTO `categorias` VALUES (3, 'ESTACIONES DE SERVICIO');
INSERT INTO `categorias` VALUES (4, 'VESTUARIO Y CALZADO');
INSERT INTO `categorias` VALUES (5, 'RESTAURANTES');
INSERT INTO `categorias` VALUES (6, 'TURISMO SOCIAL');
INSERT INTO `categorias` VALUES (7, 'LINEA HOGAR');
INSERT INTO `categorias` VALUES (8, 'DROGUERIAS');
INSERT INTO `categorias` VALUES (9, 'INFORMATICA');
INSERT INTO `categorias` VALUES (10, 'PAPELERIAS');
INSERT INTO `categorias` VALUES (11, 'HOTELES');
INSERT INTO `categorias` VALUES (12, 'BANQUETES');
INSERT INTO `categorias` VALUES (13, 'CLINICAS');
INSERT INTO `categorias` VALUES (14, 'ESCUELAS DE CONDUCCION');
INSERT INTO `categorias` VALUES (15, 'GIMNASIOS');
INSERT INTO `categorias` VALUES (16, 'PELUQUERIAS');
INSERT INTO `categorias` VALUES (17, 'REPOSTERIAS');
INSERT INTO `categorias` VALUES (18, 'CENTROS DE ESTETICA');
INSERT INTO `categorias` VALUES (19, 'TALLERES');
INSERT INTO `categorias` VALUES (20, 'TRANSPORTE EJECUTIVO');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `denominacion`
-- 

CREATE TABLE `denominacion` (
  `idden` int(10) unsigned NOT NULL default '0',
  `denominacion` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idden`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `denominacion`
-- 

INSERT INTO `denominacion` VALUES (1, '5000');
INSERT INTO `denominacion` VALUES (2, '10000');
INSERT INTO `denominacion` VALUES (3, '20000');
INSERT INTO `denominacion` VALUES (4, '50000');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `detallepedido`
-- 

CREATE TABLE `detallepedido` (
  `idpedido` int(10) unsigned NOT NULL default '0',
  `idlineas` int(10) unsigned NOT NULL default '0',
  `idden` int(10) unsigned NOT NULL default '0',
  `cantidad` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  USING BTREE (`idpedido`,`idlineas`,`idden`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `detallepedido`
-- 

INSERT INTO `detallepedido` VALUES (5, 7, 1, 4);
INSERT INTO `detallepedido` VALUES (5, 3, 2, 5);
INSERT INTO `detallepedido` VALUES (5, 1, 1, 2);
INSERT INTO `detallepedido` VALUES (4, 6, 1, 2);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `detallepedido2`
-- 

CREATE TABLE `detallepedido2` (
  `idped2` int(11) NOT NULL default '0',
  `idlinea2` int(11) NOT NULL default '0',
  `subtotal` int(11) NOT NULL default '0',
  PRIMARY KEY  (`idped2`,`idlinea2`,`subtotal`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `detallepedido2`
-- 

INSERT INTO `detallepedido2` VALUES (4, 6, 10000);
INSERT INTO `detallepedido2` VALUES (5, 1, 10000);
INSERT INTO `detallepedido2` VALUES (5, 3, 50000);
INSERT INTO `detallepedido2` VALUES (5, 7, 20000);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `establecimientos`
-- 

CREATE TABLE `establecimientos` (
  `idestablecimiento` int(10) unsigned NOT NULL auto_increment,
  `estcodigo` varchar(100) NOT NULL default '',
  `estnomcom` varchar(70) NOT NULL default '',
  `estelefono` varchar(20) NOT NULL default '',
  `estdireccion` varchar(70) NOT NULL default '',
  PRIMARY KEY  (`idestablecimiento`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=712 ;

-- 
-- Volcar la base de datos para la tabla `establecimientos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `lineas`
-- 

CREATE TABLE `lineas` (
  `idlineas` int(10) unsigned NOT NULL default '0',
  `nombrelinea` varchar(45) NOT NULL default '',
  PRIMARY KEY  USING BTREE (`idlineas`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `lineas`
-- 

INSERT INTO `lineas` VALUES (1, 'Canasta');
INSERT INTO `lineas` VALUES (3, 'Combustible');
INSERT INTO `lineas` VALUES (7, 'Tecnologia');
INSERT INTO `lineas` VALUES (8, 'Salud');
INSERT INTO `lineas` VALUES (4, 'Restaurante');
INSERT INTO `lineas` VALUES (9, 'Educacion');
INSERT INTO `lineas` VALUES (2, 'Regalo');
INSERT INTO `lineas` VALUES (5, 'Dotacion');
INSERT INTO `lineas` VALUES (6, 'Turismo');
INSERT INTO `lineas` VALUES (10, 'Otros');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `pedido`
-- 

CREATE TABLE `pedido` (
  `idpedido` int(10) unsigned NOT NULL default '0',
  `fecha` date NOT NULL default '0000-00-00',
  `valortotal` varchar(45) NOT NULL default '',
  `empresa` varchar(45) NOT NULL default '',
  `direccion` varchar(45) NOT NULL default '',
  `telefono` varchar(45) NOT NULL default '',
  `Fax` varchar(45) default NULL,
  `representantelegal` varchar(45) NOT NULL default '',
  `email` varchar(45) NOT NULL default '',
  `retencion` varchar(45) NOT NULL default '',
  `observaciones` longtext NOT NULL,
  `contacto` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idpedido`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `pedido`
-- 

INSERT INTO `pedido` VALUES (1, '2007-10-22', '10000', 'INTERPASS', 'carrera 43a#34155 oficina 503', '3810007', '2320007', 'Fernando Laverde', 'interpass@unipagopass.co', 'Autoretenedor', 'sahdlkashd', 'carlos andres laverde');
INSERT INTO `pedido` VALUES (2, '2007-10-22', '10000', 'INTERPASS', 'carrera 43a#34155 oficina 503', '3810007', '2320007', 'Fernando Laverde', 'interpass@unipagopass.co', 'Autoretenedor', 'sahdlkashd', 'carlos andres laverde');
INSERT INTO `pedido` VALUES (3, '2007-10-22', '10000', 'INTERPASS', 'carrera 43a#34155 oficina 503', '3810007', '2320007', 'Fernando Laverde', 'interpass@unipagopass.co', 'Autoretenedor', 'sahdlkashd', 'carlos andres laverde');
INSERT INTO `pedido` VALUES (4, '2007-10-22', '10000', 'INTERPASS', 'carrera 43a#34155 oficina 503', '3810007', '2320007', 'Fernando Laverde', 'interpass@unipagopass.co', 'Autoretenedor', 'sahdlkashd', 'carlos andres laverde');
INSERT INTO `pedido` VALUES (5, '2007-10-22', '80000', 'INTERPASS', 'carrera 43a#34155 oficina 503', '3810007', '2320007', 'Fernando Laverde', 'interpass@unipagopass.co', 'Autoretenedor', 'chequeras', 'carlos andres laverde');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `solicitudes`
-- 

CREATE TABLE `solicitudes` (
  `soltipodenegocio` varchar(20) NOT NULL default '',
  `solnomcom` varchar(45) NOT NULL default '',
  `solrepleg` varchar(50) NOT NULL default '',
  `solnit` varchar(20) NOT NULL default '',
  `solcedula` int(10) unsigned NOT NULL default '0',
  `solciudad` varchar(45) NOT NULL default '',
  `solsector` varchar(45) NOT NULL default '',
  `soltelefono` int(10) unsigned NOT NULL default '0',
  `solfax` int(10) unsigned default NULL,
  `solemail` varchar(45) NOT NULL default '',
  `soldireccion` varchar(45) NOT NULL default '',
  `solregimen` varchar(15) NOT NULL default '',
  `soltiporetencion` varchar(20) NOT NULL default '',
  `solaprobacion` varchar(20) NOT NULL default 'Pendiente',
  `solrazsoc` varchar(45) NOT NULL default '',
  `solfecha` date NOT NULL default '0000-00-00',
  `solcontacto` varchar(45) NOT NULL default '',
  `solcargo` varchar(45) NOT NULL default '',
  PRIMARY KEY  USING BTREE (`solnit`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `solicitudes`
-- 

INSERT INTO `solicitudes` VALUES ('Empresa', '34343434', '3434343434', '4343344', 43243434, '342342343', 'Comercial', 4343434, 3434334, '343434343@', '423423434', 'Comun', 'Autoretenedor', 'Aprobado', '34343434', '2007-10-22', '342342343', '423424234');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `usuarios`
-- 

CREATE TABLE `usuarios` (
  `usunit` varchar(20) NOT NULL default '',
  `usurepleg` varchar(50) NOT NULL default '',
  `usucedula` int(10) unsigned NOT NULL default '0',
  `usudireccion` varchar(45) NOT NULL default '',
  `usutelefono` int(10) unsigned NOT NULL default '0',
  `usufax` int(10) unsigned default NULL,
  `ususector` varchar(45) NOT NULL default '',
  `usuciudad` varchar(45) NOT NULL default '',
  `usunomcom` varchar(45) NOT NULL default '',
  `usurazsoc` varchar(45) NOT NULL default '',
  `usuregimen` varchar(15) NOT NULL default '',
  `usutiporetencion` varchar(20) NOT NULL default '',
  `usuemail` varchar(45) NOT NULL default '',
  `usulogin` varchar(45) NOT NULL default '',
  `usuclave` varchar(45) NOT NULL default '',
  `usutipodenegocio` varchar(20) NOT NULL default '',
  `usuperfil` varchar(20) NOT NULL default '',
  `usuestado` varchar(20) NOT NULL default '',
  `usufechasol` date NOT NULL default '0000-00-00',
  `usufechapro` date NOT NULL default '0000-00-00',
  `usucontacto` varchar(45) NOT NULL default '',
  `usucargo` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`usunit`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `usuarios`
-- 

INSERT INTO `usuarios` VALUES ('1121821879-1', 'LUIS ARMANDO BARBOSA PEREZ', 1121821879, 'CARRERA 84#33AA-1', 3117031247, NULL, 'Comercial', 'MEDELLIN', 'ADMINISTRADOR', 'ADMIN', 'Simplificado', 'Ninguna', 'LUISBAPER@HOTMAIL.COM', 'admin', 'admin', 'Establecimiento', 'Administrador', 'Aprobado', '2007-04-12', '2007-09-27', 'harold marin andres', 'Tintero');
INSERT INTO `usuarios` VALUES ('811046580-0', 'Fernando Laverde', 3395700, 'carrera 43a#34155 oficina 503', 3810007, 2320007, 'Privado', 'Medellin', 'INTERPASS', 'UNIPAGOPASS', 'Simplificado', 'Autoretenedor', 'interpass@unipagopass.co', 'interpass', 'interpass', 'Empresa', 'Cliente', 'Aprobado', '2007-09-24', '2007-09-24', 'carlos andres laverde', 'administrador');
