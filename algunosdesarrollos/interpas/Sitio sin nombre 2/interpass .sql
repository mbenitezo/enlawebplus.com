-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.24a-community-nt-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema interpaa
--

CREATE DATABASE IF NOT EXISTS interpaa;
USE interpaa;

--
-- Definition of table `denominaciones`
--

DROP TABLE IF EXISTS `denominaciones`;
CREATE TABLE `denominaciones` (
  `idden` int(10) unsigned NOT NULL,
  `denominacion` varchar(45) NOT NULL,
  PRIMARY KEY  (`idden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `denominaciones`
--

/*!40000 ALTER TABLE `denominaciones` DISABLE KEYS */;
INSERT INTO `denominaciones` (`idden`,`denominacion`) VALUES 
 (1,'5000'),
 (2,'10000'),
 (3,'20000'),
 (4,'50000');
/*!40000 ALTER TABLE `denominaciones` ENABLE KEYS */;


--
-- Definition of table `detallepedido`
--

DROP TABLE IF EXISTS `detallepedido`;
CREATE TABLE `detallepedido` (
  `idpedido` int(10) unsigned NOT NULL,
  `idlinea` int(10) unsigned NOT NULL,
  `idden` int(10) unsigned NOT NULL,
  `cantidad` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`idpedido`,`idlinea`,`idden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `detallepedido`
--

/*!40000 ALTER TABLE `detallepedido` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallepedido` ENABLE KEYS */;


--
-- Definition of table `lineas`
--

DROP TABLE IF EXISTS `lineas`;
CREATE TABLE `lineas` (
  `idlineas` int(10) unsigned NOT NULL,
  `nombrelinea` varchar(100) NOT NULL,
  PRIMARY KEY  (`idlineas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lineas`
--

/*!40000 ALTER TABLE `lineas` DISABLE KEYS */;
INSERT INTO `lineas` (`idlineas`,`nombrelinea`) VALUES 
 (1,'Canasta'),
 (2,'Combustible'),
 (3,'Hogar'),
 (4,'Informatica'),
 (5,'Salud'),
 (6,'Restaurante'),
 (7,'Educacion'),
 (8,'Regalo'),
 (9,'Dotacion'),
 (10,'Turismo'),
 (11,'Otros');
/*!40000 ALTER TABLE `lineas` ENABLE KEYS */;


--
-- Definition of table `pedido`
--

DROP TABLE IF EXISTS `pedido`;
CREATE TABLE `pedido` (
  `idpedido` int(10) unsigned NOT NULL,
  `fecha` datetime NOT NULL,
  `valortotal` varchar(45) NOT NULL,
  `empresa` varchar(200) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `telefono` varchar(200) NOT NULL,
  `responsable` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `cargo` varchar(200) NOT NULL,
  `observaciones` longtext NOT NULL,
  PRIMARY KEY  (`idpedido`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pedido`
--

/*!40000 ALTER TABLE `pedido` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
