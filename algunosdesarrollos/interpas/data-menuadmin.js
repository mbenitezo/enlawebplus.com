/*
   Deluxe Menu Data File
   Created by Deluxe Tuner v2.4
   http://deluxe-menu.com
*/


// -- Deluxe Tuner Style Names
var tstylesNames=["Font",];
var tXPStylesNames=[];
// -- End of Deluxe Tuner Style Names

//--- Common
var tlevelDX=15;
var texpanded=0;
var texpandItemClick=0;
var tcloseExpanded=0;
var tcloseExpandedXP=0;
var ttoggleMode=0;
var tnoWrap=1;
var titemTarget="_self";
var titemCursor="default";
var statusString="link";
var tblankImage="data-menuadmin.files/blank.gif";
var tpathPrefix_img="";
var tpathPrefix_link="";

//--- Dimensions
var tmenuWidth="170px";
var tmenuHeight="auto";

//--- Positioning
var tabsolute=0;
var tleft="0px";
var ttop="0px";

//--- Font
var tfontStyle="normal 8pt Tahoma";
var tfontColor=["#000000","#000000"];
var tfontDecoration=["none","none"];
var tfontColorDisabled="#AAAAAA";
var tpressedFontColor="#AA0000";

//--- Appearance
var tmenuBackColor="#008040";
var tmenuBackImage="data-menuadmin.files/blank.gif";
var tmenuBorderColor="#000000";
var tmenuBorderWidth=0;
var tmenuBorderStyle="none";

//--- Item Appearance
var titemAlign="left";
var titemHeight=20;
var titemBackColor=["#FFFFFF","#FFFFFF"];
var titemBackImage=["data-menuadmin.files/blank.gif","data-menuadmin.files/blank.gif"];

//--- Icons & Buttons
var ticonWidth=16;
var ticonHeight=16;
var ticonAlign="left";
var texpandBtn=["data-menuadmin.files/expand.gif","data-menuadmin.files/expand.gif","data-menuadmin.files/collapse.gif"];
var texpandBtnW=8;
var texpandBtnH=8;
var texpandBtnAlign="left";

//--- Lines
var tpoints=1;
var tpointsImage="";
var tpointsVImage="";
var tpointsCImage="";

//--- Floatable Menu
var tfloatable=0;
var tfloatIterations=6;
var tfloatableX=1;
var tfloatableY=1;

//--- Movable Menu
var tmoveable=0;
var tmoveHeight=12;
var tmoveColor="#AA0000";
var tmoveImage="";

//--- XP-Style
var tXPStyle=1;
var tXPIterations=5;
var tXPBorderWidth=1;
var tXPBorderColor="#31A17E";
var tXPTitleBackColor="";
var tXPTitleBackImg="data-menuadmin.files/back1.gif";
var tXPTitleLeft="data-menuadmin.files/left1.gif";
var tXPTitleLeftWidth=25;
var tXPIconWidth=25;
var tXPIconHeight=25;
var tXPExpandBtn=["data-menuadmin.files/xp_expand1.gif","data-menuadmin.files/xp_expand1.gif","data-menuadmin.files/xp_collapse1.gif","data-menuadmin.files/xp_collapse1.gif"];
var tXPBtnWidth=25;
var tXPBtnHeight=25;
var tXPFilter=1;

//--- Dynamic Menu
var tdynamic=0;

//--- State Saving
var tsaveState=0;
var tsavePrefix="menu1";

var tstyles = [
    ["tfontStyle=bold 8pt Tahoma","tfontColor=#1C512C,#1C512C"],
];

var tmenuItems = [

    ["SIPPASS","", "", "", "", "", "", "", "", ],
        ["|MENU","", "", "", "", "", "", "", "", ],
            ["||PEDIDOS","", "", "", "", "", "", "", "", ],
                ["|||REALIZAR","administrador/realizarpedido.php", "", "", "", "", "mainFrame", "", "", ],
                ["|||ELIMINAR","pedidos/listado de pedidos.php", "", "", "", "", "mainFrame", "", "", ],
            ["||SOLICITUDES","", "", "", "", "", "", "", "", ],
                ["|||APROBAR ","clientes/listado de clientes.php", "", "", "", "", "mainFrame", "", "", ],
                ["|||INGRESAR","clientes/ingresarcliente.php", "", "", "", "", "mainFrame", "", "", ],
                ["|||ELIMINAR","clientes/eliminarsolicitud.php", "", "", "", "", "mainFrame", "", "", ],
            ["||USUARIOS","", "", "", "", "", "", "", "", ],
                ["|||APROBADOS","usuario/listado de usuarios.php", "", "", "", "", "mainFrame", "", "", ],
                ["|||MODIFICAR","usuario/modificar.php", "", "", "", "", "mainFrame", "", "", ],
                ["|||ELIMINAR","usuario/Eliminarusuario.php", "", "", "", "", "mainFrame", "", "", ],
            ["||REPORTES","", "", "", "", "", "", "", "", ],
                ["|||USUARIOS ACTIVOS","reportes/activos.php", "", "", "", "", "_top", "", "", ],
                ["|||USUARIOS PENDIENTES","reportes/pendientes.php", "", "", "", "", "_top", "", "", ],
                ["|||PEDIDOS REALIZADOS","reportes/pedidosrealiz.php", "", "", "", "", "_top", "", "", ],
                ["|||DETALLE DE PEDIDOS","reportes/detallepedido.php", "", "", "", "", "_top", "", "", ],
            ["||ESTABLECIMIENTOS","", "", "", "", "", "", "", "", ],
                ["|||BUSQUEDA","establecimientos/establecimientos.php", "", "", "", "", "mainFrame", "", "", ],
                ["|||INGRESAR","establecimientos/nuevoestablecimiento.php", "", "", "", "", "mainFrame", "", "", ],
                ["|||ELIMINAR","establecimientos/eliminar.php", "", "", "", "", "mainFrame", "", "", ],
                ["|||MODIFICAR","establecimientos/modificarestab.php", "", "", "", "", "mainFrame", "", "", ],
        ["|SALIR","salir.php", "", "", "", "", "_top", "", "", ],
];

dtree_init();