<?php
$DosPaginas = $_GET["paginas"];
	$a = 0;
	$b = 0;
	$c = 0;
	$d = 0;
if($DosPaginas != ""){
	$a = 280;
	$b = 350;
	$c = -240;
	$d = -130;
	}else{
	$a = 0;
	$b = 0;
	$c = 0;
	$d = 0;	
		}

define('FPDF_FONTPATH','font/');
require('WriteHTML.php');
require('fpdf.php');
$pdf=new PDF('P','pt','letter');
$pdf->AddFont('ariblk','','ariblk.php');
$pdf->AddFont('Arial','','arial.php');
$pdf->AddFont('LCALLIG','','LCALLIG.php');
$pdf->AddPage();
$pdf->SetMargins(10,20,25); //Margenes del texto
$pdf->SetLineWidth(0.9); //Ancho para las lineas
$pdf->SetDrawColor(34,139,34); //colores las lineas

$pdf->SetTextColor(0,0,0);  //PARA EL COLOR DE LAS LETRAS EN EL PDF
//TEXTO DE LA INFORMACION DEL PACIENTE
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(105, 130);
$pdf->Cell(0,0,$_GET['nombre_paciente'],0,5);
$pdf->SetXY(450, 130);
$pdf->Cell(0,0,$_GET['edad'],0,5);
$pdf->SetXY(45, 140);
$pdf->Cell(0,0,$_GET['remite'],0,5);
$pdf->SetXY(100, 150);
$pdf->Cell(0,0,$_GET['motivos'],0,5);
$pdf->SetXY(407, 140);
$pdf->Cell(0,0,utf8_decode($_GET['fecha']),0,5);
//FIN TEXTO DE LA INFORMACION DEL PACIENTE
//INFORMACION HIGADO  DEL PACIENTE
$pdf->SetFont('Arial','',8);
$pdf->SetXY(50, 201);
$pdf->Cell(0,0,utf8_decode($_GET['tamanohigado']),0,5);
$pdf->SetXY(300, 201);
$pdf->Cell(0,0,utf8_decode($_GET['ecogenicidadhigado']),0,5);
$pdf->SetXY(60, 211);
$pdf->Cell(0,0,utf8_decode($_GET['contornohigado']),0,5);
$pdf->SetXY(185, 221);
$pdf->Cell(0,0,utf8_decode($_GET['ramasrupra']),0,5);
$pdf->SetXY(129, 231);
$pdf->Cell(0,0,utf8_decode($_GET['lesionesquisticas']),0,5);
//FIN INFORMCACION HIGADO    DEL PACIENTE

//INFORMACION  VESICULA   DEL PACIENTE
$pdf->SetXY(50, 261);
$pdf->Cell(0,0,utf8_decode($_GET['tamanovesicula']),0,5);
$pdf->SetXY(278, 261);
$pdf->Cell(0,0,utf8_decode($_GET['paredesvesicula']),0,5);
//FIN VESIVULA  DEL PACIENTE

//INFORMACION   VIAS BILIARES  DEL PACIENTE
$pdf->SetXY(74, 321+$a);
$pdf->Cell(0,0,utf8_decode($_GET['viasintra']),0,5);
$pdf->SetXY(56, 331+$a);
$pdf->Cell(0,0,utf8_decode($_GET['coledoco']),0,5);
//FIN INFORMACION  VIAS BILIARES DEL PACIENTE

//INFORMACION   PANCREAS DEL PACIENTE
$pdf->SetXY(50, 361+$a);
$pdf->Cell(0,0,utf8_decode($_GET['tamanopancreas']),0,5);
$pdf->SetXY(300, 361+$a);
$pdf->Cell(0,0,utf8_decode($_GET['econocigidadpancreas']),0,5);
$pdf->SetXY(60, 371+$a);
$pdf->Cell(0,0,utf8_decode($_GET['contornopancreas']),0,5);
$pdf->SetXY(80, 381+$a);
$pdf->Cell(0,0,utf8_decode($_GET['calcificacionpancreas']),0,5);
$pdf->SetXY(123, 391+$a);
$pdf->Cell(0,0,utf8_decode($_GET['quistespancreas']),0,5);
//FIN INFORMACION  PANCREAS DEL PACIENTE




$pdf->SetFont('Arial','',$_GET['tamano1']); //con esto coloco el tamano de letra que viene del textarea
$pdf->SetXY(55, 264);
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area1'])));

$pdf->SetTextColor(34,139,34);  //PARA EL COLOR DE LAS LETRAS EN EL PDF

$pdf->Image('abdominal.png', 5, 10, 600, 96, 'png','');
$pdf->SetFont('ariblk','',16);
$pdf->SetXY(160, 115);
$pdf->Cell(0,0,"ESTUDIO: ECOGRAF�A ABDOMINAL",0,20);
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 130);
$pdf->Cell(0,0,"Nombre del paciente: ",0,5);
$pdf->Line(108,135,420,135);// Linea horizontal
$pdf->SetXY(420, 130);
$pdf->Cell(0,0,"Edad: ",0,5);
$pdf->Line(450,135,500,135);// Linea horizontal
$pdf->SetXY(10, 140);
$pdf->Cell(0,0,"Remite: ",0,5);
$pdf->Line(47,144,370,144);// Linea horizontal
$pdf->SetXY(10, 150);
$pdf->Cell(0,0,"Motivos del examen: ",0,5);
$pdf->Line(102,154,420,154);// Linea horizontal
$pdf->SetXY(377, 140);
$pdf->Cell(0,0,"Fecha:",0,5);
$pdf->Line(410,144,540,144);// Linea horizontal

$pdf->SetFont('ariblk','',12);
$pdf->SetXY(10, 170);
$pdf->Cell(0,0,"A LA EXPLORACI�N ECOGR�FICA ENCONTRAMOS:",0,20);

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 190);
$pdf->Cell(0,0,"H�GADO:",0,5);

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 200);
$pdf->Cell(0,0,"Tama�o:",0,5);
$pdf->Line(50,205,235,205);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(237, 200);
$pdf->Cell(0,0,"Ecogenicidad:",0,5);
$pdf->Line(300,205,540,205);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 210);
$pdf->Cell(0,0,"Contornos:",0,5);
$pdf->Line(62,215,540,215);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 220);
$pdf->Cell(0,0,"Ramas portales y venas suprahep�ticas:",0,5);
$pdf->Line(190,225,540,225);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 230);
$pdf->Cell(0,0,"Masas o lesiones qu�sticas:",0,5);
$pdf->Line(130,235,540,235);// Linea horizontal

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 250);
$pdf->Cell(0,0,"VES�CULA:",0,5);
$pdf->SetXY(10, 260);
$pdf->Cell(0,0,"Tama�o:",0,5);
$pdf->Line(50,265,235,265);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(237, 260);
$pdf->Cell(0,0,"Paredes:",0,5);
$pdf->Line(280,265,560,265);// Linea horizontal

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 270);
$pdf->Cell(0,0,"Interior:",0,5);
$pdf->SetFont('Arial','',8);

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 310+$a);
$pdf->Cell(0,0,"V�AS BILIARES:",0,5);
$pdf->SetXY(10, 320+$a);
$pdf->Cell(0,0,"Intrahep�ticas:",0,5);
$pdf->Line(75,325+$a,560,325+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 330+$a);
$pdf->Cell(0,0,"Coledoco:",0,5);
$pdf->Line(60,335+$a,560,335+$a);// Linea horizontal

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 350+$a);
$pdf->Cell(0,0,"P�NCREAS:",0,5);
$pdf->SetXY(10, 360+$a);
$pdf->Cell(0,0,"Tama�o:",0,5);
$pdf->Line(50,365+$a,235,365+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(237, 360+$a);
$pdf->Cell(0,0,"Ecogenicidad:",0,5);
$pdf->Line(300,365+$a,560,365+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 370+$a);
$pdf->Cell(0,0,"Contornos:",0,5);
$pdf->Line(62,375+$a,560,375+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 380+$a);
$pdf->Cell(0,0,"Calcificaciones:",0,5);
$pdf->Line(81,385+$a,560,385+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 390+$a);
$pdf->Cell(0,0,"Quistes o Pseudoquistes:",0,5);
$pdf->Line(124,395+$a,560,395+$a);// Linea horizontal

if($DosPaginas != ""){
$pdf->AddPage();	
}
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 410-$b);
$pdf->Cell(0,0,"RI��N DERECHO:",0,5);

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 420-$b);
$pdf->Cell(0,0,"Di�metro:",0,5);
$pdf->Line(62,425-$b,300,425-$b);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(300, 420-$b);
$pdf->Cell(0,0,"Parenquima:",0,5);
$pdf->Line(360,425-$b,560,425-$b);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 430-$b);
$pdf->Cell(0,0,"Contornos:",0,5);
$pdf->Line(62,435-$b,560,435-$b);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 440-$b);
$pdf->Cell(0,0,"Espacios Perirrenales:",0,5);
$pdf->Line(112,445-$b,560,445-$b);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 450-$b);
$pdf->Cell(0,0,"Calcificaciones:",0,5);
$pdf->Line(81,455-$b,297,455-$b);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(300, 450-$b);
$pdf->Cell(0,0,"Masas:",0,5);
$pdf->Line(335,455-$b,560,455-$b);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 460-$b);
$pdf->Cell(0,0,"Uni�n Corticomedular:",0,5);
$pdf->Line(112,465-$b,560,465-$b);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 470-$b);
$pdf->Cell(0,0,"Cavidades Pielocaliciales:",0,5);
$pdf->Line(125,475-$b,560,475-$b);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 480-$b);
$pdf->Cell(0,0,"C�lculos:",0,5);



$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 520+$c);
$pdf->Cell(0,0,"RI��N IZQUIERDO:",0,5);
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 530+$c);
$pdf->Cell(0,0,"Di�mtetro:",0,5);
$pdf->Line(62,535+$c,300,535+$c);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(300, 530+$c);
$pdf->Cell(0,0,"Parenquima:",0,5);
$pdf->Line(360,535+$c,560,535+$c);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 540+$c);
$pdf->Cell(0,0,"Contornos:",0,5);
$pdf->Line(62,545+$c,560,545+$c);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 550+$c);
$pdf->Cell(0,0,"Espacios Perirrenales:",0,5);
$pdf->Line(112,555+$c,560,555+$c);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 560+$c);
$pdf->Cell(0,0,"Calcificaciones:",0,5);
$pdf->Line(81,565+$c,297,565+$c);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(300, 560+$c);
$pdf->Cell(0,0,"Masas:",0,5);
$pdf->Line(335,565+$c,560,565+$c);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 570+$c);
$pdf->Cell(0,0,"Uni�n Corticomedular:",0,5);
$pdf->Line(112,575+$c,560,575+$c);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 580+$c);
$pdf->Cell(0,0,"Cavidades Pielocaliciales:",0,5);
$pdf->Line(125,585+$c,560,585+$c);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 590+$c);
$pdf->Cell(0,0,"C�lculos:",0,5);


$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 630+$d);
$pdf->Cell(0,0,"BAZO:",0,5);
$pdf->SetXY(10, 640+$d);
$pdf->Cell(0,0,"Tama�o:",0,5);
$pdf->Line(50,645+$d,250,645+$d);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(250, 640+$d);
$pdf->Cell(0,0,"Alteraciones:",0,5);
$pdf->Line(310,645+$d,560,645+$d);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 650+$d);
$pdf->Cell(0,0,"Ecogenicidad:",0,5);
$pdf->Line(75,655+$d,560,655+$d);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 660+$d);
$pdf->Cell(0,0,"OTROS:",0,5);
$pdf->Line(50,665+$d,560,665+$d);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 670+$d);
$pdf->Cell(0,0,"DIAGN�STICO ECOGR�FICO:",0,5);

$pdf->SetXY(10, 720);
$pdf->SetFont('Arial',B,9);
$pdf->Cell(0,0,"FIRMA: ________________________________",0,0);
$pdf->SetFont('LCALLIG','',12);
$pdf->SetXY(175, 735);
$pdf->Cell(0,0,"UN COMPROMISO CON SU SALUD",0,0);
//$pdf->Output('EcografiaRenal.pdf','D');

$pdf->SetTextColor(0,0,0);  //PARA EL COLOR DE LAS LETRAS EN EL PDF
$pdf->SetFont('Arial','',8);
//INFORMACION   RINON DERECHO  PACIENTE
$pdf->SetXY(62, 421-$b);
$pdf->Cell(0,0,utf8_decode($_GET['rinondiaderecho']),0,5);
$pdf->SetXY(357, 421-$b);
$pdf->Cell(0,0,utf8_decode($_GET['rinonparenderecho']),0,5);
$pdf->SetXY(60, 431-$b);
$pdf->Cell(0,0,utf8_decode($_GET['contornoderecho']),0,5);
$pdf->SetXY(110, 441-$b);
$pdf->Cell(0,0,utf8_decode($_GET['rinonperiderecho']),0,5);
$pdf->SetXY(80, 451-$b);
$pdf->Cell(0,0,utf8_decode($_GET['rinoncalciderecho']),0,5);
$pdf->SetXY(332, 451-$b);
$pdf->Cell(0,0,utf8_decode($_GET['rinonmasaderecho']),0,5);
$pdf->SetXY(110, 461-$b);
$pdf->Cell(0,0,utf8_decode($_GET['rinoncortiderecho']),0,5);
$pdf->SetXY(123, 471-$b);
$pdf->Cell(0,0,utf8_decode($_GET['rinoncaviderecho']),0,5);
//FIN INFORMACION RINON DERECHO PACIENTE

//INFORMACION RINON IZQUIERDO PACIENTE
$pdf->SetXY(62, 531+$c);
$pdf->Cell(0,0,utf8_decode($_GET['rinondiaizquierdo']),0,5);
$pdf->SetXY(357, 531+$c);
$pdf->Cell(0,0,utf8_decode($_GET['rinonparenizquierdo']),0,5);
$pdf->SetXY(60, 541+$c);
$pdf->Cell(0,0,utf8_decode($_GET['contornoizquierdo']),0,5);
$pdf->SetXY(110, 551+$c);
$pdf->Cell(0,0,utf8_decode($_GET['rinonperiizquierdo']),0,5);
$pdf->SetXY(80, 561+$c);
$pdf->Cell(0,0,utf8_decode($_GET['rinoncalciizquierdo']),0,5);
$pdf->SetXY(332, 561+$c);
$pdf->Cell(0,0,utf8_decode($_GET['rinonmasaizquierdo']),0,5);
$pdf->SetXY(110, 571+$c);
$pdf->Cell(0,0,utf8_decode($_GET['rinoncortiizquierdo']),0,5);
$pdf->SetXY(123, 581+$c);
$pdf->Cell(0,0,utf8_decode($_GET['rinoncaviizquierdo']),0,5);
//FIN   INFORMACION RINON IZQUIERDO PACIENTE


//INFORMACION  BAZO  PACIENTE
$pdf->SetXY(50, 641+$d);
$pdf->Cell(0,0,utf8_decode($_GET['tamanobazo']),0,5);
$pdf->SetXY(310, 641+$d);
$pdf->Cell(0,0,utf8_decode($_GET['alteracionesbazo']),0,5);
$pdf->SetXY(73, 651+$d);
$pdf->Cell(0,0,utf8_decode($_GET['ecogenicidadbazo']),0,5);
$pdf->SetXY(47, 661+$d);
$pdf->Cell(0,0,utf8_decode($_GET['otros']),0,5);
//FIN  INFORMACION BAZO PACIENTE
$pdf->SetFont('Arial','',$_GET['tamano2']); //con esto coloco el tamano de letra que viene del textarea
$pdf->SetXY(55, 474-$b);
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area2'])));

$pdf->SetFont('Arial','',$_GET['tamano3']); //con esto coloco el tamano de letra que viene del textarea
$pdf->SetXY(55, 584+$c);
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area3'])));

$pdf->SetFont('Arial','',$_GET['tamano4']); //con esto coloco el tamano de letra que viene del textarea
$pdf->SetXY(145, 664+$d);
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area4'])));

$pdf->Output();
//shell_exec('lpr "puerto en el cual se desea imprimir"'); 
//$salida = shell_exec(�lpr PRN�);
?>