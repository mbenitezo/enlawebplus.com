<?php
$DosPaginas = $_GET["paginas"];
	$a = 0;
	$b = 0;
	$c = 0;
	$d = 0;
	$e = 0;
if($DosPaginas != ""){
	$a = 200;
	$b = 100;
	$c = 480;
	$d = 360;
	$e = 240;
	$f = 120;
	}else{
	$a = 0;
	$b = 0;
	$c = 0;
	$d = 0;
	$e = 0;	
		}
define('FPDF_FONTPATH','font/');
require('WriteHTML.php');
require('fpdf.php');
$pdf=new PDF('P','pt','letter');
$pdf->AddFont('ariblk','','ariblk.php');
$pdf->AddFont('Arial','','arial.php');
$pdf->AddFont('LCALLIG','','LCALLIG.php');
$pdf->AddPage();
$pdf->SetMargins(10,20,25); //Margenes del texto
$pdf->SetLineWidth(0.9); //Ancho para las lineas
$pdf->SetDrawColor(34,139,34); //colores las lineas
$pdf->SetTextColor(34,139,34);  //PARA EL COLOR VERDE DE LAS LETRAS EN EL PDF
//$pdf->SetTextColor(129,24,74);  //PARA EL COLOR DE LAS LETRAS EN EL PDF


$pdf->SetTextColor(0,0,0);  //TEXTO EN COLOR NEGRO - TEXTO EN COLOR NEGRO
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 160);
$pdf->Cell(0,0,"Nombre del paciente: ".$_GET['nombre_paciente'],0,5);
$pdf->SetXY(420, 160);
$pdf->Cell(0,0,"Edad: ".$_GET['edad'],0,5);
$pdf->Line(450,164,490,164);// Linea horizontal
$pdf->SetXY(10, 180);
$pdf->Cell(0,0,"Remite: ".$_GET['textfield2'],0,5);
$pdf->SetXY(10, 200);
$pdf->Cell(0,0,"Motivos del examen: ".$_GET['motivos'],0,5);
$pdf->SetXY(377, 180);
$pdf->Cell(0,0,"Fecha:",0,5);
$pdf->SetXY(407, 180);
$pdf->Cell(0,0,utf8_decode($_GET['fecha']),0,5);


//////////////////////INFORMACION DE RINON DERECHO
$pdf->SetFont('Arial','',8);
$pdf->SetXY(62, 281);
$pdf->Cell(0,0,utf8_decode($_GET['diametros2']),0,5);
$pdf->SetXY(357, 281);
$pdf->Cell(0,0,utf8_decode($_GET['parenquima2']),0,5);
$pdf->SetXY(60, 291);
$pdf->Cell(0,0,utf8_decode($_GET['contornos2']),0,5);
$pdf->SetXY(110, 301);
$pdf->Cell(0,0,utf8_decode($_GET['perirrenales2']),0,5);
$pdf->SetXY(80, 311);
$pdf->Cell(0,0,utf8_decode($_GET['calcificaciones2']),0,5);
$pdf->SetXY(42, 321);
$pdf->Cell(0,0,utf8_decode($_GET['masas2']),0,5);
$pdf->SetXY(110, 331);
$pdf->Cell(0,0,utf8_decode($_GET['corticomedular2']),0,5);
$pdf->SetXY(123, 341);
$pdf->Cell(0,0,utf8_decode($_GET['pielocaliciales2']),0,5);
$pdf->SetXY(53, 345);
$pdf->SetFont('Arial','',$_GET['tamano1']); //con esto coloco el tamano de letra que viene del textarea
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area1'])));
$pdf->SetXY(82, 385+$b);
$pdf->SetFont('Arial','',$_GET['tamano2']); //con esto coloco el tamano de letra que viene del textarea
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area2'])));
/////////////////FIN INFORMACION DE RINON DERECHO


/////////////////INFORMACION DE RINON IZQUIERDO
$pdf->SetXY(62, 461+$a);
$pdf->Cell(0,0,utf8_decode($_GET['diametros']),0,5);
$pdf->SetXY(357, 461+$a);
$pdf->Cell(0,0,utf8_decode($_GET['parenquima']),0,5);
$pdf->SetXY(60, 471+$a);
$pdf->Cell(0,0,utf8_decode($_GET['contornos']),0,5);
$pdf->SetXY(110, 481+$a);
$pdf->Cell(0,0,utf8_decode($_GET['perirrenales']),0,5);
$pdf->SetXY(80, 491+$a);
$pdf->Cell(0,0,utf8_decode($_GET['calcificaciones']),0,5);
$pdf->SetXY(42, 501+$a);
$pdf->Cell(0,0,utf8_decode($_GET['masas']),0,5);
$pdf->SetXY(110, 511+$a);
$pdf->Cell(0,0,utf8_decode($_GET['corticomedular']),0,5);
$pdf->SetXY(123, 521+$a);
$pdf->Cell(0,0,utf8_decode($_GET['pielocaliciales']),0,5);
//////////////////////////////////////////////////////////////////////////////////////

$pdf->SetTextColor(34,139,34); //TEXTO EN COLOR VERDE - TEXTO EN COLOR VERDE

//$pdf->SetTextColor(129,24,74);  //PARA EL COLOR DE LAS LETRAS EN EL PDF
$pdf->Image('abdominal.png', 5, 10, 600, 96, 'png','');
$pdf->SetFont('ariblk','',16);
$pdf->SetXY(180, 130);
$pdf->Cell(0,0,"ESTUDIO: ECOGRAF�A RENAL",0,20);
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 160);
$pdf->Cell(0,0,"Nombre del paciente: ",0,5);
$pdf->Line(104,165,340,165);// Linea horizontal
$pdf->SetXY(420, 160);
$pdf->Cell(0,0,"Edad: ",0,5);
$pdf->Line(450,164,490,164);// Linea horizontal
$pdf->SetXY(10, 180);
$pdf->Cell(0,0,"Remite: ",0,5);
$pdf->Line(50,185,340,185);// Linea horizontal
$pdf->SetXY(10, 200);
$pdf->Cell(0,0,"Motivos del examen: ",0,5);
$pdf->Line(100,205,580,205);// Linea horizontal
$pdf->SetXY(377, 180);
$pdf->Cell(0,0,"Fecha:",0,5);
$pdf->Line(410,185,560,185);// Linea horizontal
$pdf->SetXY(407, 180);



$pdf->SetFont('ariblk','',12);
$pdf->SetXY(10, 230);
$pdf->Cell(0,0,"A LA EXPLORACI�N ECOGR�FICA ENCONTRAMOS:",0,20);

//////////////////////INFORMACION DE RINON DERECHO
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 260);
$pdf->Cell(0,0,"RI��N DERECHO:",0,5);

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 280);
$pdf->Cell(0,0,"Di�mtetros:",0,5);
$pdf->Line(65,285,300,285);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(300, 280);
$pdf->Cell(0,0,"Parenquima:",0,5);
$pdf->Line(360,285,560,285);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 290);
$pdf->Cell(0,0,"Contornos:",0,5);
$pdf->Line(62,295,560,295);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 300);
$pdf->Cell(0,0,"Espacios Perirrenales:",0,5);
$pdf->Line(110,305,560,305);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 310);
$pdf->Cell(0,0,"Calcificaciones:",0,5);
$pdf->Line(83,315,560,315);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 320);
$pdf->Cell(0,0,"Masas:",0,5);
$pdf->Line(45,325,560,325);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 330);
$pdf->Cell(0,0,"Uni�n Corticomedular:",0,5);
$pdf->Line(110,335,560,335);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 340);
$pdf->Cell(0,0,"Cavidades Pielocaliciales:",0,5);
$pdf->Line(124,345,560,345);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 350);
$pdf->Cell(0,0,"C�lculos:",0,5);
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 390+$b);
$pdf->Cell(0,0,"Otros hallazgos:",0,5);
/////////////////FIN INFORMACION DE RINON DERECHO

/////////////////INFORMACION DE RINON IZQUIERDO
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 440+$a);
$pdf->Cell(0,0,"RI��N IZQUIERDO:",0,5);
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 460+$a);
$pdf->Cell(0,0,"Di�mtetros:",0,5);
$pdf->Line(65,465+$a,300,465+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(300, 460+$a);
$pdf->Cell(0,0,"Parenquima:",0,5);
$pdf->Line(360,465+$a,560,465+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 470+$a);
$pdf->Cell(0,0,"Contornos:",0,5);
$pdf->Line(62,475+$a,560,475+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 480+$a);
$pdf->Cell(0,0,"Espacios Perirrenales:",0,5);
$pdf->Line(110,485+$a,560,485+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 490+$a);
$pdf->Cell(0,0,"Calcificaciones:",0,5);
$pdf->Line(83,495+$a,560,495+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 500+$a);
$pdf->Cell(0,0,"Masas:",0,5);
$pdf->Line(45,505+$a,560,505+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 510+$a);
$pdf->Cell(0,0,"Uni�n Corticomedular:",0,5);
$pdf->Line(110,515+$a,560,515+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 520+$a);
$pdf->Cell(0,0,"Cavidades Pielocaliciales:",0,5);
$pdf->Line(124,525+$a,560,525+$a);// Linea horizontal
$pdf->SetFont('Arial',B,9);


//SEGUNDA PAGINA SI SE CUMPLE LA CONDICION QUE VOY A COLOCAR
if($DosPaginas != ""){
$pdf->AddPage();
	}
$pdf->SetTextColor(0,0,0);  //TEXTO EN COLOR NEGRO - TEXTO EN COLOR NEGRO	
$pdf->SetXY(53, 524-$c);
$pdf->SetFont('Arial','',$_GET['tamano3']); //con esto coloco el tamano de letra que viene del textarea
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area3'])));

$pdf->SetXY(82, 564-$d);
$pdf->SetFont('Arial','',$_GET['tamano4']); //con esto coloco el tamano de letra que viene del textarea
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area4'])));
/////////////////////////FIN INFORMACION DE RINON IZQUIERDO

////////////////////////INFORMACION DE OTROS HALLAZGOS
$pdf->SetXY(150, 614-$e);
$pdf->SetFont('Arial','',$_GET['tamano5']); //con esto coloco el tamano de letra que viene del textarea
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area5'])));

$pdf->SetXY(143, 664-$f);
$pdf->SetFont('Arial','',$_GET['tamano6']); //con esto coloco el tamano de letra que viene del textarea
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area6'])));

/////////////////////////////////FIN INFORMACION DE OTROS HALLAZGOS

$pdf->SetTextColor(34,139,34); //TEXTO EN COLOR VERDE - TEXTO EN COLOR VERDE




$pdf->SetXY(10, 530-$c);
$pdf->Cell(0,0,"C�lculos:",0,5);
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 570-$d);
$pdf->Cell(0,0,"Otros hallazgos:",0,5);
/////////////////////////FIN INFORMACION DE RINON IZQUIERDO

////////////////////////INFORMACION DE OTROS HALLAZGOS
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 620-$e);
$pdf->Cell(0,0,"Otras Valoraciones Ecogr�ficas:",0,5);
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 670-$f);
$pdf->Cell(0,0,"DIAGN�STICO ECOGR�FICO:",0,5);
/////////////////////////////////FIN INFORMACION DE OTROS HALLAZGOS

$pdf->SetXY(10, 720);
$pdf->SetFont('Arial',B,9);
$pdf->Cell(0,0,"FIRMA: ________________________________",0,0);
$pdf->SetFont('LCALLIG','',12);
$pdf->SetXY(175, 735);
$pdf->Cell(0,0,"UN COMPROMISO CON SU SALUD",0,0);
//$pdf->Output('EcografiaRenal.pdf','D');
$pdf->Output();
//shell_exec('lpr "puerto en el cual se desea imprimir"'); 
//$salida = shell_exec(�lpr PRN�);
?>