<? include ("fecha_espanol.php"); 
$fecha = date("Y-m-d"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<style type="text/css">
#flotante1 {
   position:fixed;
   top: 120px;
   bottom: 0;
   right: 0;
   left: 0;
   width: 100px;
   height: 100px;
} 

.estiloServimedicos {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-style: normal;
	line-height: normal;
	color: #000;
}
.estiloServimedicosTitulo {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 16px;
	font-style: italic;
}
.estiloServimedicosTitulo {
	font-family: "Lucida Calligraphy";
	font-size: 24px;
	font-weight: bold;
	text-transform: uppercase;
	font-style: normal;
	letter-spacing: 5px;
}
.estiloServimedicos2 {
	font-family: "French Script MT";
	font-size: 20px;
	font-weight: bolder;
}
.estiloServimedicosNor1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
}
.estiloServimedicoNor2 {
	font-family: "Arial Black", Gadget, sans-serif;
	font-size: 18px;
	font-weight: bolder;
}
.estiloServimedicoNor3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
}
.estiloServimedicoTextoContent {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-style: normal;
}
</style>
 
<script src="jquery.js" type="text/javascript"></script>
<script src="iColorPicker.js" type="text/javascript"></script>
<script type="text/javascript" src="nicEdit.js"></script>
        <script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
      </script>
</head>

<body>
<form name="form" action="archivo_eco_prostatica.php" method="get" onSubmit="return validar_enviar();"><center><table width="718" border="0">
  <tr>
    <td colspan="19" valign="top"><div align="center"><img src="abdominal.png" width="660" height="108" /></div></td>
    </tr>
  <tr>
    <td height="21" colspan="2" valign="top">&nbsp;</td>
    <td height="21" colspan="15" align="center" class="estiloServimedicoNor2">ESTUDIO: ECOGRAFÍA PROSTÁTICA.</td>
    <td width="102" valign="top" class="estiloServimedicos">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td colspan="19" align="center" class="estiloServimedicoTextoContent"  onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoNor3">TRANSRECTAL <strong>
      <input type="checkbox" name="checkTransvaginal" value="1" id="CheckboxGroup1_33" />
    </strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TRANSVESICAL</span> <strong>
    <input type="checkbox" name="checkTransvesical" value="1" id="CheckboxGroup1_34" />
    </strong></td>
    </tr>
  <tr valign="top">
<td colspan="19" class="estiloServimedicoTextoContent"  onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>PACIENTE: 
      <input name="nombre_paciente" type="text" id="textfield" size="48" />
      EDAD:
      <input name="edad" type="text" id="textfield13" size="3" maxlength="3" />
      CEDULA:
      <input name="cedula" type="text" id="cedula" size="14" maxlength="14" /></td>
  </tr>
  <tr valign="top">
    <td colspan="19" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoTextoContent">REMITE: 
        <input name="remite" type="text" id="remite" size="50" /> 
    &nbsp;&nbsp;FECHA:
      <input name="fecha" type="text" id="textfield3" size="40" value="<? convertirFecha($fecha); ?>" />
    </span></td>
  </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="19" valign="middle" class="estiloServimedicoNor3" >&nbsp;</td>
  </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="16" valign="middle" class="estiloServimedicoNor3" >VEJIGA</td>
    </tr>
  <tr valign="top">
    <td colspan="19" valign="middle" ><span class="estiloServimedicoTextoContent">
      <textarea name="area1" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano1" type="text" id="tamano1" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="19" valign="middle" >&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="16" valign="middle" class="estiloServimedicoNor3" >PROSTÁTICA</td>
    </tr>
  <tr valign="top">
    <td colspan="19" valign="middle" ><span class="estiloServimedicoTextoContent">
      <textarea name="area2" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano2" type="text" id="tamano2" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="19" valign="middle" >&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="16" valign="middle" class="estiloServimedicoNor3" >ZONA DE TRANSICIÓN</td>
  </tr>
  <tr valign="top">
    <td colspan="19" valign="middle" ><span class="estiloServimedicoTextoContent">
      <textarea name="area3" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano3" type="text" id="tamano3" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="19" valign="middle" >&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="17" valign="middle" class="estiloServimedicoNor3" >ZONA PERIFÉRICA</td>
    </tr>
  <tr valign="top">
    <td colspan="19" valign="middle" ><span class="estiloServimedicoTextoContent">
<textarea name="area4" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano4" type="text" id="tamano4" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="19" valign="middle" >&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="17" valign="middle" class="estiloServimedicoNor3" >CAPSULA QUIRURGICA</td>
    </tr>
  <tr valign="top">
    <td colspan="19" valign="middle" ><span class="estiloServimedicoTextoContent">
      <textarea name="area5" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano5" type="text" id="tamano5" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="19" valign="middle" >&nbsp;</td>
    </tr>
  <tr valign="top">
    <td colspan="19" valign="middle" ><span class="estiloServimedicoNor3"><strong>DIÁMETROS PROSTÁTICOS</strong>
        <strong>&nbsp;</strong>
        
    </span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="19" valign="middle" ><strong><span class="estiloServimedicosNor1">
      <input name="tamdere1" type="text" id="tamdere1" size="7" maxlength="7" />
    X
    <input name="tamdere2" type="text" id="tamdere2" size="7" maxlength="7" />
    X
    <input name="tamdere3" type="text" id="tamdere3" size="7" maxlength="7" />
    CM
    </span></strong></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="19" valign="middle" class="estiloServimedicosNor1" >Peso: <strong>
      <input name="tamdere4" type="text" id="peso" size="7" maxlength="7" />
    </strong></td>
  </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="19" valign="middle" class="estiloServimedicosNor1" >Volumen: <strong>
      <input name="peso" type="text" id="volumen" size="7" maxlength="7" />
    </strong></td>
  </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="19" valign="middle" class="estiloServimedicosNor3" ><p class="estiloServimedicoNor3">&nbsp;</p>
      <p class="estiloServimedicoTextoContent"><strong>VESÍCULOS SEMINALES</strong>
        <textarea name="area6" cols="90" rows="3" ></textarea>
        <strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano6" type="text" id="tamano6" size="4" maxlength="2" value="9" />
      </p></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="24" colspan="19" valign="bottom"><p class="estiloServimedicoTextoContent"><strong>OTRAS VALORACIONES</strong>
        <textarea name="area7" cols="90" rows="3"></textarea>
        <strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano7" type="text" id="tamano7" size="4" maxlength="2" value="9" />
      </p></td>
    </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="22" colspan="19" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    </tr>

  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="19" align="center" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicosTitulo">UN COMPROMISO CON SU SALUD.</span></td>
    </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td width="112" height="16" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td width="20" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td width="27" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td width="39" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td width="21" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td width="85" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td colspan="4" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td width="62" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td align="center" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td align="center" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
  </tr>
</table>
</center>
<div id="flotante1"><a href="index.html" border="0"><img id="flotante1" src="atras.png" /></a><br><br><br><br><br><br><input type="image" src="impresion.jpg" width="100" height="100" /><br><br><br>&nbsp;&nbsp;<strong>2 P&aacute;ginas</strong>
<input type="checkbox" name="paginas" value="1" id="paginas" /></div>
</form>
</body>
</html>
