<?php
define('FPDF_FONTPATH','font/');
require('WriteHTML.php');
require('fpdf.php');
$campo = stripcslashes($_GET['area1']);

$pdf=new PDF('P','pt','letter');
$pdf->AddFont('ariblk','','ariblk.php');
$pdf->AddFont('Arial','','arial.php');
$pdf->AddFont('LCALLIG','','LCALLIG.php');
$pdf->AddPage();
$pdf->SetTextColor(34,139,34);  //PARA EL COLOR DE LAS LETRAS EN EL PDF
$pdf->SetLineWidth(1.6); //Ancho para las lineas
$pdf->SetMargins(30,20,25); //Margenes del texto
$pdf->SetDrawColor(34,139,34); //colores las lineas
$pdf->Line(20,360,590,360);// Linea horizontal
$pdf->Line(20,320,590,320);// Linea horizontal
$pdf->Line(20,20,20,360); // Linea Vertical
$pdf->Line(20,20,590,20);// Linea horizontal
$pdf->Line(590,20,590,360); // Linea Vertical
$pdf->Line(20,107,590,107);// Linea horizontal
$pdf->Line(20,127,590,127);// Linea horizontal
$pdf->Image('recetario.png', 25, 25, 560, 80, 'png','');
$pdf->SetFont('LCALLIG',"",9);
$pdf->SetXY(24, 120);
$pdf->Cell(0,0,"Nombre del paciente: ",0,5);
$pdf->SetXY(380, 120);
$pdf->Cell(0,0,"Fecha: ",0,5);
$pdf->SetFont('LCALLIG',"",37);
$pdf->SetXY(30, 150);
$pdf->Cell(0,0,"R/.",0,0);

$pdf->SetTextColor(0,0,0);  //PARA EL COLOR DE LAS LETRAS EN EL PDF
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(130, 120);
$pdf->Cell(0,0,$_GET['nombre_paciente'],0,0);
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(415, 120);
$pdf->Cell(0,0,$_GET['fecha'],0,0);



$pdf->SetFont('Arial','',$_GET['tamano']); //con esto coloco el tamano de letra que viene del textarea
$pdf->SetXY(30, 175);
$pdf->WriteHTML(utf8_decode($campo));
$pdf->SetTextColor(34,139,34);  //PARA EL COLOR DE LAS LETRAS EN EL PDF
$pdf->SetFont('LCALLIG',"",9);
$pdf->SetXY(45, 330);
$pdf->Cell(0,0,"Consulta M�dica - Sala de Observaciones - Hidrataci�n - Suturas - Partos - Nebulizaciones - Cauterizaciones",0,0);
$pdf->SetXY(70, 345);
$pdf->Cell(0,0,"Control Prenatal - Ecograf�a Obst�trica - P�lvica Transvaginal - Abdominal - Renal - Prost�tica",0,0);

$pdf->SetFont('Arial','',8);
$pdf->SetXY(0, 370);
$pdf->Cell(0,0,"- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -",0,5);
$pdf->Output();
//shell_exec('lpr "puerto en el cual se desea imprimir"'); 
//$salida = shell_exec(�lpr PRN�);
?>