<?php
$DosPaginas = $_GET["paginas"];
	$a = 0;
	$b = 0;
	$c = 0;
	$d = 0;
	$e = 0;
if($DosPaginas != ""){
	$a = 280;
	$b = 500;
	$c = -340;
	$d = -580;
	$e = -300;
	}else{
	$a = 0;
	$b = 0;
	$c = 0;
	$d = 0;
	$e = 0;	
		}
// ESTA ES LA FUNCION PARA EL INTERCAMBIO DE COLORES HEXADECIMAL A RGB
function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);
 
   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   return implode(",", $rgb); // returns the rgb values separated by commas
  // return $rgb; // returns an array with the rgb values
}
/*
$color1 = $_GET['color1'];
$rgb1 = hex2rgb("$color1");
list($a,$b,$c) = explode(",",$rgb1);
echo $a;
echo $b;
echo $c;
exit; */
define('FPDF_FONTPATH','font/');
require('WriteHTML.php');
require('fpdf.php');
$pdf=new PDF('P','pt','letter');
$pdf->AddFont('ariblk','','ariblk.php');
$pdf->AddFont('Arial','','arial.php');
$pdf->AddFont('LCALLIG','','LCALLIG.php');
$pdf->AddPage();
$pdf->SetMargins(10,20,25); //Margenes del texto
$pdf->SetLineWidth(0.9); //Ancho para las lineas
$pdf->SetDrawColor(34,139,34); //colores las lineas

//OPCIONES QUE APLICARAN LOS COLORES ESCOGIDOS POR EL MEDICO
$pdf->SetFont('Arial',B,9);
if($_REQUEST["checkUnico"] == "1"){ 
$color = $_GET['color1'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(179, 176);
$pdf->Cell(0,0,"X",0,5);
$pdf->SetTextColor(0,0,0);
 }
if($_REQUEST["checkCefalica"] == "1"){ 
$color = $_GET['color2'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(179, 191);
$pdf->Cell(0,0,"X",0,5); 
$pdf->SetTextColor(0,0,0);
}
if($_REQUEST["checkLongitudinal"] == "1"){ 
$color = $_GET['color3'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(179, 206);
$pdf->Cell(0,0,"X",0,5); 
$pdf->SetTextColor(0,0,0);
}
if($_REQUEST["checkTransversal"] == "1"){ 
$color = $_GET['color4'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(179, 221);
$pdf->Cell(0,0,"X",0,5); 
$pdf->SetTextColor(0,0,0);
}

if($_REQUEST["checkMultiple"] == "1"){ 
$color = $_GET['color5'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(314, 176);
$pdf->Cell(0,0,"X",0,5); 
$pdf->SetTextColor(0,0,0);
}
if($_REQUEST["checkPodalica"] == "1"){ 
$color = $_GET['color6'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(314, 191);
$pdf->Cell(0,0,"X",0,5); 
$pdf->SetTextColor(0,0,0);
}
if($_REQUEST["checkDorsoDerecho"] == "1"){ 
$color = $_GET['color7'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(314, 206);
$pdf->Cell(0,0,"X",0,5); 
$pdf->SetTextColor(0,0,0);
}
if($_REQUEST["checkCefalicoDer"] == "1"){ 
$color = $_GET['color8'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(314, 221);
$pdf->Cell(0,0,"X",0,5); 
$pdf->SetTextColor(0,0,0);
}

if($_REQUEST["checkCambiante"] == "1"){ 
$color = $_GET['color9'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(414, 176);
$pdf->Cell(0,0,"X",0,5); 
$pdf->SetTextColor(0,0,0);
}
$pdf->SetXY(426, 176);
$pdf->Cell(0,0,$_REQUEST["txtmovilcam"],0,5);
if($_REQUEST["checkIzquierdo1"] == "1"){ 
$color = $_GET['color10'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(414, 206);
$pdf->Cell(0,0,"X",0,5); }
if($_REQUEST["checkIzquierdo2"] == "1"){ 
$color = $_GET['color11'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(414, 220);
$pdf->Cell(0,0,"X",0,5); 
$pdf->SetTextColor(0,0,0);
}
if($_REQUEST["checkDorsoSup"] == "1"){ 
$color = $_GET['color12'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(489, 221);
$pdf->Cell(0,0,"X",0,5); 
$pdf->SetTextColor(0,0,0);
}
if($_REQUEST["checkDorsoInf"] == "1"){ 
$color = $_GET['color12'];
$rgb = hex2rgb("$color");
list($a,$b,$c) = explode(",",$rgb);
$pdf->SetTextColor($a,$b,$c);
$pdf->SetXY(528, 221);
$pdf->Cell(0,0,"X",0,5); 
$pdf->SetTextColor(0,0,0);
}
if($_REQUEST["checkPoligoh"] == "1"){ $pdf->SetXY(409, 430);
$pdf->Cell(0,0,"X",0,5); 
$pdf->SetTextColor(0,0,0);
}
//FIN OPCIONES QUE APLICARAN LOS COLORES ESCOGIDOS POR EL MEDICO
//INFORMACION DEL PACIENTE
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(105, 130);
$pdf->Cell(0,0,$_GET['nombre_paciente'],0,5);
$pdf->SetXY(420, 130);
$pdf->Cell(0,0,"Edad: ".$_GET['edad'],0,5);
$pdf->SetXY(10, 140);
$pdf->Cell(0,0,"Doctor Y/O empresa: ".$_GET['remite'],0,5);
$pdf->SetXY(10, 150);
$pdf->Cell(0,0,"F.U.R.: ".$_GET['motivos'],0,5);
$pdf->SetXY(377, 140);
$pdf->Cell(0,0,"Fecha:",0,5);
$pdf->SetXY(407, 140);
$pdf->Cell(0,0,utf8_decode($_GET['fecha']),0,5);
$pdf->SetXY(240, 150);
$pdf->Cell(0,0,"FICHA GINECO - OBST�TRICA: ".$_GET['obstetrica'],0,5);
$pdf->SetXY(10, 160);
$pdf->Cell(0,0,"ANTECEDENTES: ".$_GET['antecedentes'],0,5);
//FIN INFORMACION DEL PACIENTE

//TEXTO DE LOS 3 TITULOS DE LA MITAD
$pdf->SetFont('Arial',"",8);
$pdf->SetXY(32, 265);
$pdf->Cell(0,0,$_REQUEST["txtdbp"],0,5);
$pdf->SetXY(25, 280);
$pdf->Cell(0,0,$_REQUEST["txthc"],0,5);
$pdf->SetXY(25, 295);
$pdf->Cell(0,0,$_REQUEST["txtac"],0,5);
$pdf->SetXY(50, 310);
$pdf->Cell(0,0,$_REQUEST["txthumero"],0,5);
$pdf->SetXY(25, 325);
$pdf->Cell(0,0,$_REQUEST["txtac"],0,5);
$pdf->SetXY(58, 340);
$pdf->Cell(0,0,$_REQUEST["txtcerebelo"],0,5);
$pdf->SetXY(52, 355);
$pdf->Cell(0,0,$_REQUEST["txtedadus"],0,5);
$pdf->SetXY(80, 370);
$pdf->Cell(0,0,$_REQUEST["txtpesoesti"],0,5);
$pdf->SetXY(205, 390);
$pdf->Cell(0,0,$_REQUEST["txtliquidooligo"],0,5);
$pdf->SetXY(120, 405);
$pdf->Cell(0,0,$_REQUEST["txtpolihi"],0,5);



$pdf->SetXY(227, 265);
$pdf->Cell(0,0,$_REQUEST["txtmm1"],0,5);
$pdf->SetXY(227, 280);
$pdf->Cell(0,0,$_REQUEST["txtmm2"],0,5);
$pdf->SetXY(227, 295);
$pdf->Cell(0,0,$_REQUEST["txtmm3"],0,5);
$pdf->SetXY(227, 310);
$pdf->Cell(0,0,$_REQUEST["txtmm4"],0,5);
$pdf->SetXY(227, 325);
$pdf->Cell(0,0,$_REQUEST["txtmm5"],0,5);
$pdf->SetXY(227, 340);
$pdf->Cell(0,0,$_REQUEST["txtmm6"],0,5);


$pdf->SetXY(446, 265);
$pdf->Cell(0,0,$_REQUEST["txtplaceAnterior"],0,5);
$pdf->SetXY(451, 280);
$pdf->Cell(0,0,$_REQUEST["txtplacenPosterior"],0,5);
$pdf->SetXY(446, 295);
$pdf->Cell(0,0,$_REQUEST["txtplacenDerecho"],0,5);
$pdf->SetXY(448, 310);
$pdf->Cell(0,0,$_REQUEST["txtplacenIzquierdo"],0,5);
$pdf->SetXY(440, 325);
$pdf->Cell(0,0,$_REQUEST["txtplacenfundica"],0,5);
$pdf->SetXY(427, 340);
$pdf->Cell(0,0,$_REQUEST["txtplacenbaja"],0,5);
$pdf->SetXY(446, 355);
$pdf->Cell(0,0,$_REQUEST["txtplacenDerecho"],0,5);
$pdf->SetXY(448, 370);
$pdf->Cell(0,0,$_REQUEST["txtplacenmarginal"],0,5);
$pdf->SetXY(454, 430);
$pdf->Cell(0,0,$_REQUEST["txtindice"],0,5);
$pdf->SetXY(395, 450);
$pdf->Cell(0,0,$_REQUEST["txtfcf"],0,5);
$pdf->SetXY(370, 480);
$pdf->Cell(0,0,$_REQUEST["txtduranteexamen"],0,5);
//FIN TEXTO DE LOS TITULOS DE LA MITAD

//OPCIONES DE SELECCION EN GENERAL
if($_REQUEST["checknormal1"] == "1"){ $pdf->SetXY(219, 430);
$pdf->Cell(0,0,"X",0,5); }
if($_REQUEST["checknormal2"] == "1"){ $pdf->SetXY(219, 450);
$pdf->Cell(0,0,"X",0,5); }
if($_REQUEST["checknormal3"] == "1"){ $pdf->SetXY(219, 470);
$pdf->Cell(0,0,"X",0,5); }
if($_REQUEST["checkpresentes"] == "1"){ $pdf->SetXY(219, 490);
$pdf->Cell(0,0,"X",0,5); }
if($_REQUEST["checkoligohid"] == "1"){ $pdf->SetXY(314, 430);
$pdf->Cell(0,0,"X",0,5); }
if($_REQUEST["checkdisminuido"] == "1"){ $pdf->SetXY(314, 450);
$pdf->Cell(0,0,"X",0,5); }
if($_REQUEST["checkpatologico"] == "1"){ $pdf->SetXY(314, 470);
$pdf->Cell(0,0,"X",0,5); }
if($_REQUEST["checkausentes"] == "1"){ $pdf->SetXY(314, 490);
$pdf->Cell(0,0,"X",0,5); }
if(($_REQUEST["checkcolumna1"] == "1") && ($_REQUEST["checkcolumna2"] == "")){ $pdf->SetXY(114, 550);
$pdf->Cell(0,0,"X",0,5); }elseif(($_REQUEST["checkcolumna1"] == "") && ($_REQUEST["checkcolumna2"] == "1")){
$pdf->SetXY(149, 550);
$pdf->Cell(0,0,"X",0,5); }
if(($_REQUEST["cordonvasos1"] == "1") && ($_REQUEST["cordonvasos2"] == "")){ $pdf->SetXY(314, 550);
$pdf->Cell(0,0,"X",0,5); }elseif(($_REQUEST["cordonvasos1"] == "") && ($_REQUEST["cordonvasos2"] == "1")){
$pdf->SetXY(349, 550);
$pdf->Cell(0,0,"X",0,5); }
if(($_REQUEST["checkCerebelo1"] == "1") && ($_REQUEST["checkCerebelo2"] == "")){ $pdf->SetXY(494, 550);
$pdf->Cell(0,0,"X",0,5); }elseif(($_REQUEST["checkCerebelo1"] == "") && ($_REQUEST["checkCerebelo2"] == "1")){
$pdf->SetXY(529, 550);
$pdf->Cell(0,0,"X",0,5); }

if(($_REQUEST["checkVejiga1"] == "1") && ($_REQUEST["checkVejiga2"] == "")){ $pdf->SetXY(114, 570);
$pdf->Cell(0,0,"X",0,5); }elseif(($_REQUEST["checkVejiga1"] == "") && ($_REQUEST["checkVejiga2"] == "1")){
$pdf->SetXY(149, 570);
$pdf->Cell(0,0,"X",0,5); }
if(($_REQUEST["checkInserCordon1"] == "1") && ($_REQUEST["checkInserCordon2"] == "")){ $pdf->SetXY(314, 570);
$pdf->Cell(0,0,"X",0,5); }elseif(($_REQUEST["checkInserCordon1"] == "") && ($_REQUEST["checkInserCordon2"] == "1")){
$pdf->SetXY(349, 570);
$pdf->Cell(0,0,"X",0,5); }
if(($_REQUEST["checkEstomago1"] == "1") && ($_REQUEST["checkEstomago2"] == "")){ $pdf->SetXY(494, 570);
$pdf->Cell(0,0,"X",0,5); }elseif(($_REQUEST["checkEstomago1"] == "") && ($_REQUEST["checkEstomago2"] == "1")){
$pdf->SetXY(529, 570);
$pdf->Cell(0,0,"X",0,5); }

if(($_REQUEST["checkrinones1"] == "1") && ($_REQUEST["checkrinones2"] == "")){ $pdf->SetXY(114, 590);
$pdf->Cell(0,0,"X",0,5); }elseif(($_REQUEST["checkrinones1"] == "") && ($_REQUEST["checkrinones2"] == "1")){
$pdf->SetXY(149, 590);
$pdf->Cell(0,0,"X",0,5); }
if(($_REQUEST["checkMovCard1"] == "1") && ($_REQUEST["checkMovCard2"] == "")){ $pdf->SetXY(314, 590);
$pdf->Cell(0,0,"X",0,5); }elseif(($_REQUEST["checkMovCard1"] == "") && ($_REQUEST["checkMovCard2"] == "1")){
$pdf->SetXY(349, 590);
$pdf->Cell(0,0,"X",0,5); }
if(($_REQUEST["checkCarmCard1"] == "1") && ($_REQUEST["checkCarmCard2"] == "")){ $pdf->SetXY(494, 590);
$pdf->Cell(0,0,"X",0,5); }elseif(($_REQUEST["checkCarmCard1"] == "") && ($_REQUEST["checkCarmCard2"] == "1")){
$pdf->SetXY(529, 590);
$pdf->Cell(0,0,"X",0,5); }

if(($_REQUEST["malformaciones1"] == "1") && ($_REQUEST["malformaciones2"] == "")){ $pdf->SetXY(114, 610);
$pdf->Cell(0,0,"X",0,5); }elseif(($_REQUEST["malformaciones1"] == "") && ($_REQUEST["malformaciones2"] == "1")){
$pdf->SetXY(149, 610);
$pdf->Cell(0,0,"X",0,5); }
//FIN OPCIONES DE SELECCION EN GENERAL

//SECCION DE LOS GRADOS
$pdf->SetXY(396, 405);
$pdf->Cell(0,0,$_REQUEST["grado1"],0,5);
$pdf->SetXY(438, 405);
$pdf->Cell(0,0,$_REQUEST["grado2"],0,5);
$pdf->SetXY(480, 405);
$pdf->Cell(0,0,$_REQUEST["grado3"],0,5);
$pdf->SetXY(521, 405);
$pdf->Cell(0,0,$_REQUEST["grado4"],0,5);
//FIN SECCION DE LOS GRADOS


$pdf->SetTextColor(34,139,34);  //PARA EL COLOR DE LAS LETRAS EN EL PDF
$pdf->Image('obstetrica.png', 10, 10, 600, 96, 'png','');
$pdf->SetFont('ariblk','',16);
$pdf->SetXY(140, 115);
//$pdf->Cell(0,0,"ESTUDIO: ECOGRAF�A OBSTETRICA",0,20);
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 130);
$pdf->Cell(0,0,"Nombre del paciente: ",0,5);
$pdf->Line(108,135,420,135);// Linea horizontal
$pdf->SetXY(420, 130);
$pdf->Cell(0,0,"Edad: ",0,5);
$pdf->Line(450,135,500,135);// Linea horizontal
$pdf->SetXY(10, 140);
$pdf->Cell(0,0,"Doctor Y/O empresa: ",0,5);
$pdf->Line(105,144,370,144);// Linea horizontal
$pdf->SetXY(10, 150);
$pdf->Cell(0,0,"F.U.R.: ",0,5);
$pdf->Line(45,154,235,154);// Linea horizontal
$pdf->SetXY(377, 140);
$pdf->Cell(0,0,"Fecha:",0,5);
$pdf->Line(410,144,560,144);// Linea horizontal
$pdf->SetXY(240, 150);
$pdf->Cell(0,0,"FICHA GINECO - OBST�TRICA: ",0,5);
$pdf->Line(380,154,560,154);// Linea horizontal
$pdf->SetXY(10, 160);
$pdf->Cell(0,0,"ANTECEDENTES: ",0,5);
$pdf->Line(92,164,560,164);// Linea horizontal

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 175);
$pdf->Cell(0,0,"FETO:",0,5);
$pdf->SetFont('Arial','',8);
$pdf->SetXY(150, 175);
$pdf->Cell(0,0,"�nico",0,5);
$pdf->SetXY(175, 175);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(275, 175);
$pdf->Cell(0,0,"M�ltiple",0,5);
$pdf->SetXY(310, 175);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(340, 175);
$pdf->Cell(0,0,"M�vil y Cambiante",0,5);
$pdf->SetXY(410, 175);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->Line(429,180,592,180);// Linea horizontal

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 190);
$pdf->Cell(0,0,"PRESENTACI�N",0,5);
$pdf->SetFont('Arial','',8);
$pdf->SetXY(142, 190);
$pdf->Cell(0,0,"Cef�lica",0,5);
$pdf->SetXY(175, 190);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(273, 190);
$pdf->Cell(0,0,"Pod�lica",0,5);
$pdf->SetXY(310, 190);
$pdf->Cell(0,0,"(    )",0,5);


$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 205);
$pdf->Cell(0,0,"SITUACI�N",0,5);
$pdf->SetFont('Arial','',8);
$pdf->SetXY(128, 205);
$pdf->Cell(0,0,"Longitudinal",0,5);
$pdf->SetXY(175, 205);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(250, 205);
$pdf->Cell(0,0,"Dorso Derecho",0,5);
$pdf->SetXY(310, 205);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(373, 205);
$pdf->Cell(0,0,"Izquierdo",0,5);
$pdf->SetXY(410, 205);
$pdf->Cell(0,0,"(    )",0,5);


$pdf->SetFont('Arial','',8);
$pdf->SetXY(129, 220);
$pdf->Cell(0,0,"Transversal",0,5);
$pdf->SetXY(175, 220);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(223, 220);
$pdf->Cell(0,0,"Polo Cef�lico: Derecho",0,5);
$pdf->SetXY(310, 220);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(373, 220);
$pdf->Cell(0,0,"Izquierdo",0,5);
$pdf->SetXY(410, 220);
$pdf->Cell(0,0,"(    )",0,5);

$pdf->SetXY(440, 220);
$pdf->Cell(0,0,"Dorso: Sup",0,5);
$pdf->SetXY(485, 220);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(510, 220);
$pdf->Cell(0,0,"Inf",0,5);
$pdf->SetXY(523, 220);
$pdf->Cell(0,0,"(    )",0,5);

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 250);
$pdf->Cell(0,0,"BIOMETR�AS",0,5);
$pdf->SetXY(220, 250);
$pdf->Cell(0,0,"SEMANAS",0,5);
$pdf->SetXY(420, 250);
$pdf->Cell(0,0,"PLACENTA",0,5);


$pdf->SetFont('Arial',B,8);
$pdf->SetXY(10, 265);
$pdf->Cell(0,0,"DBP:",0,5);
$pdf->Line(35,270,210,270);// Linea horizontal
$pdf->SetXY(210, 265);
$pdf->Cell(0,0,"MM:",0,5);
$pdf->Line(230,270,400,270);// Linea horizontal

$pdf->SetXY(400, 265);
$pdf->Cell(0,0,"ANTERIOR:",0,5);
$pdf->Line(450,270,590,270);// Linea horizontal

$pdf->SetXY(10, 280);
$pdf->Cell(0,0,"HC:",0,5);
$pdf->Line(27,285,210,285);// Linea horizontal
$pdf->SetXY(210, 280);
$pdf->Cell(0,0,"MM:",0,5);
$pdf->Line(230,285,400,285);// Linea horizontal
$pdf->SetXY(400, 280);
$pdf->Cell(0,0,"POSTERIOR:",0,5);
$pdf->Line(454,285,590,285);// Linea horizontal

$pdf->SetXY(10, 295);
$pdf->Cell(0,0,"AC:",0,5);
$pdf->Line(27,300,210,300);// Linea horizontal
$pdf->SetXY(210, 295);
$pdf->Cell(0,0,"MM:",0,5);
$pdf->Line(230,300,400,300);// Linea horizontal
$pdf->SetXY(400, 295);
$pdf->Cell(0,0,"DERECHO:",0,5);
$pdf->Line(450,300,590,300);// Linea horizontal
$pdf->SetXY(10, 310);
$pdf->Cell(0,0,"HUMERO:",0,5);
$pdf->Line(53,315,210,315);// Linea horizontal
$pdf->SetXY(210, 310);
$pdf->Cell(0,0,"MM:",0,5);
$pdf->Line(230,315,400,315);// Linea horizontal
$pdf->SetXY(400, 310);
$pdf->Cell(0,0,"IZQUIERDO:",0,5);
$pdf->Line(450,315,590,315);// Linea horizontal

$pdf->SetXY(10, 325);
$pdf->Cell(0,0,"L.F:",0,5);
$pdf->Line(29,330,210,330);// Linea horizontal
$pdf->SetXY(210, 325);
$pdf->Cell(0,0,"MM:",0,5);
$pdf->Line(230,330,400,330);// Linea horizontal
$pdf->SetXY(400, 325);
$pdf->Cell(0,0,"FUNDICA:",0,5);
$pdf->Line(443,330,590,330);// Linea horizontal

$pdf->SetXY(10, 340);
$pdf->Cell(0,0,"CEREBELO:",0,5);
$pdf->Line(60,345,210,345);// Linea horizontal
$pdf->SetXY(210, 340);
$pdf->Cell(0,0,"MM:",0,5);
$pdf->Line(230,345,400,345);// Linea horizontal
$pdf->SetXY(400, 340);
$pdf->Cell(0,0,"BAJA:",0,5);
$pdf->Line(430,345,590,345);// Linea horizontal

$pdf->SetXY(10, 355);
$pdf->Cell(0,0,"EDAD U.S:",0,5);
$pdf->Line(55,360,402,360);// Linea horizontal
$pdf->SetXY(400, 355);
$pdf->Cell(0,0,"OCLUSIVA:",0,5);
$pdf->Line(450,360,590,360);// Linea horizontal

$pdf->SetXY(10, 370);
$pdf->Cell(0,0,"PESO ESTIMADO:",0,5);
$pdf->Line(83,375,400,375);// Linea horizontal
$pdf->SetXY(400, 370);
$pdf->Cell(0,0,"MARGINAL:",0,5);
$pdf->Line(450,375,590,375);// Linea horizontal

$pdf->SetXY(10, 390);
$pdf->Cell(0,0,"LIQUIDO AMNI�TICO <5 Cms OLIGOHIDRAMNIOS ",0,5);
$pdf->Line(208,395,232,395);// Linea horizontal
$pdf->SetXY(400, 390);
$pdf->Cell(0,0,"GRADO:",0,5);

$pdf->SetXY(10, 405);
$pdf->Cell(0,0,">20 CMS POLIHIDRAMNIOS ",0,5);
$pdf->Line(123,410,146,410);// Linea horizontal
$pdf->SetXY(390, 405);
$pdf->Cell(0,0,"I:",0,5);
$pdf->Line(399,410,422,410);// Linea horizontal
$pdf->SetXY(430, 405);
$pdf->Cell(0,0,"II:",0,5);
$pdf->Line(441,410,463,410);// Linea horizontal
$pdf->SetXY(470, 405);
$pdf->Cell(0,0,"III:",0,5);
$pdf->Line(483,410,505,410);// Linea horizontal
$pdf->SetXY(510, 405);
$pdf->Cell(0,0,"IV:",0,5);
$pdf->Line(524,410,547,410);// Linea horizontal
$pdf->SetXY(180, 430);
$pdf->Cell(0,0,"Normal",0,5);
$pdf->SetXY(215, 430);
$pdf->Cell(0,0,"(    )",0,5);



$pdf->SetXY(180, 450);
$pdf->Cell(0,0,"Normal",0,5);
$pdf->SetXY(215, 450);
$pdf->Cell(0,0,"(    )",0,5);


$pdf->SetXY(180, 470);
$pdf->Cell(0,0,"Normal",0,5);
$pdf->SetXY(215, 470);
$pdf->Cell(0,0,"(    )",0,5);



$pdf->SetXY(170, 490);
$pdf->Cell(0,0,"Presentes",0,5);
$pdf->SetXY(215, 490);
$pdf->Cell(0,0,"(    )",0,5);



$pdf->SetXY(275, 430);
$pdf->Cell(0,0,"Oligohid",0,5);
$pdf->SetXY(310, 430);
$pdf->Cell(0,0,"(    )",0,5);



$pdf->SetXY(265, 450);
$pdf->Cell(0,0,"Disminuido",0,5);
$pdf->SetXY(310, 450);
$pdf->Cell(0,0,"(    )",0,5);



$pdf->SetXY(268, 470);
$pdf->Cell(0,0,"Patol�gico",0,5);
$pdf->SetXY(310, 470);
$pdf->Cell(0,0,"(    )",0,5);



$pdf->SetXY(273, 490);
$pdf->Cell(0,0,"Ausentes",0,5);
$pdf->SetXY(310, 490);
$pdf->Cell(0,0,"(    )",0,5);



$pdf->SetXY(370, 430);
$pdf->Cell(0,0,"Polihog",0,5);
$pdf->SetXY(405, 430);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(425, 430);
$pdf->Cell(0,0,"�ndice:",0,5);
$pdf->Line(455,435,554,435);// Linea horizontal
$pdf->SetXY(450, 430);
$pdf->Cell(0,0,"          ",0,5);
$pdf->SetXY(552, 430);
$pdf->Cell(0,0,"cms",0,5);

$pdf->SetXY(370, 450);
$pdf->Cell(0,0,"F.C.F: ",0,5);
$pdf->Line(397,455,554,455);// Linea horizontal
$pdf->SetXY(552, 450);
$pdf->Cell(0,0,"L.P.M",0,5);
$pdf->SetXY(370, 470);
$pdf->Cell(0,0,"Durante el examen",0,5);
$pdf->Line(372,484,590,484);// Linea horizontal
$pdf->SetXY(10, 450);
$pdf->Cell(0,0,"MOVIMIENTOS FETALES",0,5);
$pdf->SetXY(10, 470);
$pdf->Cell(0,0,"ACTIVIDAD Y TONO FETAL",0,5);
$pdf->SetXY(10, 490);
$pdf->Cell(0,0,"MOVIMIENTOS RESPIRATORIOS",0,5);
$pdf->SetXY(10, 530);
$pdf->Cell(0,0,"DESCRIPCI�N FETAL. Se observan las siguientes estructuras:",0,5);

$pdf->SetXY(10, 550);
$pdf->Cell(0,0,"- Columna",0,5);
$pdf->SetXY(480, 550);
$pdf->Cell(0,0,"SI",0,5);
$pdf->SetXY(490, 550);
$pdf->Cell(0,0,"(    )",0,5);


$pdf->SetXY(510, 550);
$pdf->Cell(0,0,"NO",0,5);
$pdf->SetXY(525, 550);
$pdf->Cell(0,0,"(    )",0,5);

$pdf->SetXY(200, 550);
$pdf->Cell(0,0,"- Cord�n 3 vasos",0,5);
$pdf->SetXY(200, 570);
$pdf->Cell(0,0,"- Inserci�n cord�n",0,5);
$pdf->SetXY(200, 590);
$pdf->Cell(0,0,"- Mov. Card.",0,5);
$pdf->SetXY(165, 610);
$pdf->Cell(0,0,"Se observaron Malformaciones Macrosc�pica",0,5);

$pdf->SetXY(400, 550);
$pdf->Cell(0,0,"- Cerebelo",0,5);
$pdf->SetXY(400, 570);
$pdf->Cell(0,0,"- Estomago",0,5);
$pdf->SetXY(400, 590);
$pdf->Cell(0,0,"- Cam Card",0,5);

$pdf->SetXY(480, 570);
$pdf->Cell(0,0,"SI",0,5);
$pdf->SetXY(490, 570);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(510, 570);
$pdf->Cell(0,0,"NO",0,5);
$pdf->SetXY(525, 570);
$pdf->Cell(0,0,"(    )",0,5);

$pdf->SetXY(480, 590);
$pdf->Cell(0,0,"SI",0,5);
$pdf->SetXY(490, 590);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(510, 590);
$pdf->Cell(0,0,"NO",0,5);
$pdf->SetXY(525, 590);
$pdf->Cell(0,0,"(    )",0,5);

$pdf->SetXY(300, 550);
$pdf->Cell(0,0,"SI",0,5);
$pdf->SetXY(310, 550);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(330, 550);
$pdf->Cell(0,0,"NO",0,5);
$pdf->SetXY(345, 550);
$pdf->Cell(0,0,"(    )",0,5);

$pdf->SetXY(300, 570);
$pdf->Cell(0,0,"SI",0,5);
$pdf->SetXY(310, 570);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(330, 570);
$pdf->Cell(0,0,"NO",0,5);
$pdf->SetXY(345, 570);
$pdf->Cell(0,0,"(    )",0,5);

$pdf->SetXY(300, 590);
$pdf->Cell(0,0,"SI",0,5);
$pdf->SetXY(310, 590);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(330, 590);
$pdf->Cell(0,0,"NO",0,5);
$pdf->SetXY(345, 590);
$pdf->Cell(0,0,"(    )",0,5);

$pdf->SetXY(100, 550);
$pdf->Cell(0,0,"SI",0,5);
$pdf->SetXY(110, 550);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(130, 550);
$pdf->Cell(0,0,"NO",0,5);
$pdf->SetXY(145, 550);
$pdf->Cell(0,0,"(    )",0,5);

$pdf->SetXY(100, 570);
$pdf->Cell(0,0,"SI",0,5);
$pdf->SetXY(110, 570);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(130, 570);
$pdf->Cell(0,0,"NO",0,5);
$pdf->SetXY(145, 570);
$pdf->Cell(0,0,"(    )",0,5);


$pdf->SetXY(10, 570);
$pdf->Cell(0,0,"- Vejiga",0,5);


$pdf->SetXY(100, 590);
$pdf->Cell(0,0,"SI",0,5);
$pdf->SetXY(110, 590);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(130, 590);
$pdf->Cell(0,0,"NO",0,5);
$pdf->SetXY(145, 590);
$pdf->Cell(0,0,"(    )",0,5);

$pdf->SetXY(100, 610);
$pdf->Cell(0,0,"SI",0,5);
$pdf->SetXY(110, 610);
$pdf->Cell(0,0,"(    )",0,5);
$pdf->SetXY(130, 610);
$pdf->Cell(0,0,"NO",0,5);
$pdf->SetXY(145, 610);
$pdf->Cell(0,0,"(    )",0,5);

$pdf->SetXY(10, 590);
$pdf->Cell(0,0,"- Ri�ones",0,5);




if($DosPaginas != ""){
$pdf->AddPage();	
}

//SECCION DE LOS EDITORES DE3 TEXSTO
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('Arial','',$_GET['tamano1']); //con esto coloco el tamano de letra que viene del textarea
$pdf->SetXY(74, 634+$d);
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area1'])));

$pdf->SetFont('Arial','',$_GET['tamano2']); //con esto coloco el tamano de letra que viene del textarea
$pdf->SetXY(130, 673+$e);
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area2'])));
$pdf->SetTextColor(34,139,34);  //PARA EL COLOR DE LAS LETRAS EN EL PDF
//FIN SECCION DE LOS EDITORES DE TEXTO

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 640+$d);
$pdf->Cell(0,0,"CONCLUSI�N:",0,5);

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 680+$e);
$pdf->Cell(0,0,"IMPRESI�N ECOGR�FICA:",0,5);

$pdf->SetXY(10, 720);
$pdf->SetFont('Arial',B,9);
$pdf->Cell(0,0,"FIRMA: ________________________________",0,0);
$pdf->SetFont('LCALLIG','',12);
$pdf->SetXY(175, 735);
$pdf->Cell(0,0,"UN COMPROMISO CON SU SALUD",0,0);
//$pdf->Output('EcografiaRenal.pdf','D');

$pdf->Output();
//shell_exec('lpr "puerto en el cual se desea imprimir"'); 
//$salida = shell_exec(�lpr PRN�);
?>