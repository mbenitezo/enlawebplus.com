<? include ("fecha_espanol.php"); 
$fecha = date("Y-m-d"); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<style type="text/css">
#flotante1 {
   position:fixed;
   top: 120px;
   bottom: 0;
   right: 0;
   left: 0;
   width: 100px;
   height: 100px;
}

.estiloServimedicos {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-style: normal;
	line-height: normal;
	color: #000;
}
.estiloServimedicosTitulo {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 16px;
	font-style: italic;
}
.estiloServimedicosTitulo {
	font-family: "Lucida Calligraphy";
	font-size: 24px;
	font-weight: bold;
	text-transform: uppercase;
	font-style: normal;
	letter-spacing: 5px;
}
.estiloServimedicos2 {
	font-family: "French Script MT";
	font-size: 20px;
	font-weight: bolder;
}
.estiloServimedicosNor1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.estiloServimedicoNor2 {
	font-family: "Arial Black", Gadget, sans-serif;
	font-size: 18px;
	font-weight: bolder;
}
.estiloServimedicoNor3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
}
.estiloServimedicoTextoContent {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-style: normal;
}
</style>

<script src="jquery.js" type="text/javascript"></script>
<script src="iColorPicker.js" type="text/javascript"></script>
<script type="text/javascript" src="nicEdit.js"></script>
        <script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
      </script>

</head>

<body>
<form action="archivo_eco_hepato_biliar.php" method="get"><center><table width="661" border="0">
  <tr>
    <td colspan="3" valign="top"><div align="center"><img src="abdominal.png" width="660" height="108" /></div></td>
    </tr>
  <tr>
    <td width="102" height="21" valign="top">&nbsp;</td>
    <td width="443" height="21" align="center" class="estiloServimedicoNor2">ESTUDIO: ECOGRAFÍA HEPATO BILIAR.</td>
    <td width="102" valign="top" class="estiloServimedicos">&nbsp;</td>
  </tr>
  <tr valign="top">
<td colspan="3" class="estiloServimedicoTextoContent"  onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>Nombre del paciente: 
      <input name="nombre_paciente" type="text" id="textfield" size="70" />EDAD:
      <input name="edad" type="text" id="edad" size="3" maxlength="3" /></td>
  </tr>
  <tr valign="top">
    <td colspan="3" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoTextoContent">Remite: 
      <input name="remite" type="text" id="remite" size="45" /> 
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha:
      <input name="fecha" type="text" id="textfield3" size="40" value="<? convertirFecha($fecha); ?>" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoTextoContent">Motivos del examen: 
      <input name="motivos" type="text" id="textfield4" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" class="estiloServimedicoNor2">A LA EXPLORACIÓN ECOGRÁFICA ENCONTRAMOS:</td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#F8F9F2&quot;' class="estiloServimedicoNor3">HIGADO:</td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#F8F9F2&quot;'><span class="estiloServimedicoTextoContent">Tamaño:
      <input name="tamanohigado" type="text" id="tamanohigado" size="10" maxlength="34" style="height:20px;width:200px; font-size:9px;" />
      </span><span class="estiloServimedicoTextoContent">Ecogenicidad:
        <input name="ecogenicidadhigado" type="text" id="ecogenicidadhigado" size="10" maxlength="50" style="height:20px;width:280px; font-size:9px;" />
      </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#F8F9F2&quot;' class="estiloServimedicoTextoContent">Contornos:
      <input name="contornohigado" type="text" id="contornohigado" size="10" maxlength="90" style="height:20px;width:505px; font-size:9px;" /></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#F8F9F2&quot;' class="estiloServimedicoTextoContent">Ramas portales y Venas Suprahepáticas:
      <input name="ramasrupra" type="text" id="ramasrupra" size="10" maxlength="70" style="height:20px;width:380px; font-size:9px;" /></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#F8F9F2&quot;' class="estiloServimedicoTextoContent">Masas o lesiones quísticas:
      <input name="lesionesquisticas" type="text" id="lesionesquisticas" size="10" maxlength="88" style="height:20px;width:470px; font-size:9px;" /></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#D6FFD6" onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#D6FFD6&quot;' class="estiloServimedicoNor3">VESÍCULA:</td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#D6FFD6" onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#D6FFD6&quot;'><span class="estiloServimedicoTextoContent">Tamaño:
      <input name="tamanovesicula" type="text" id="tamanovesicula" size="10" maxlength="34" style="height:20px;width:200px; font-size:9px;" />
      </span><span class="estiloServimedicoTextoContent">Paredes:
        <input name="paredesvesicula" type="text" id="paredesvesicula" size="10" maxlength="58" style="height:20px;width:318px; font-size:9px;" />
      </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#D6FFD6"><p><span class="estiloServimedicoTextoContent">Interior:</span><span class="estiloServimedicoTextoContent">
              <textarea name="area1" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano1" type="text" id="tamano1" size="4" maxlength="2" value="9" />
      </span></p></td>
  </tr>
    <tr valign="top">
      <td colspan="3" bgcolor="#FAFCE3"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#FAFCE3&quot;'  class="estiloServimedicoNor3">VIAS BILIARES:</td>
    </tr>
    <tr valign="top">
      <td colspan="3" bgcolor="#FAFCE3"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#FAFCE3&quot;'  ><span class="estiloServimedicoTextoContent">Intrahepáticas:
        <input name="viasintra" type="text" id="viasintra" size="10" maxlength="100" style="height:20px;width:545px; font-size:9px;" />
      </span></td>
    </tr>
    <tr valign="top">
      <td colspan="3" bgcolor="#FAFCE3"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#FAFCE3&quot;'  class="estiloServimedicoTextoContent">Coledoco:
        <input name="coledoco" type="text" id="coledoco" size="10" maxlength="105" style="height:20px;width:575px; font-size:9px;" /></td>
    </tr>
    <tr valign="top">
      <td colspan="3" bgcolor="#F2FFF8"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#F2FFF8&quot;' class="estiloServimedicoNor3">PÁNCREAS:</td>
    </tr>
    <tr valign="top">
      <td colspan="3" bgcolor="#F2FFF8"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#F2FFF8&quot;' ><span class="estiloServimedicoTextoContent">Tamaño:
        <input name="tamanopancreas" type="text" id="tamanopancreas"  size="10" maxlength="34" style="height:20px;width:210px; font-size:9px;" />
        </span><span class="estiloServimedicoTextoContent">Ecogenicidad:
          <input name="econocigidadpancreas" type="text" id="econocigidadpancreas" size="10" maxlength="50" style="height:20px;width:274px; font-size:9px;" />
        </span></td>
    </tr>
    <tr valign="top">
      <td colspan="3" bgcolor="#F2FFF8"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#F2FFF8&quot;' class="estiloServimedicoTextoContent">Contornos:
        <input name="contornopancreas" type="text" id="contornopancreas" size="10" maxlength="90" style="height:20px;width:505px; font-size:9px;" /></td>
    </tr>
    <tr valign="top">
      <td colspan="3" bgcolor="#F2FFF8"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#F2FFF8&quot;'><span class="estiloServimedicoTextoContent">Calcificaciones:
        <input name="calcificacionpancreas" type="text" id="calcificacionpancreas" size="70" maxlength="90" style="height:20px;width:505px; font-size:9px;" />
      </span></td>
    </tr>
    <tr valign="top">
      <td colspan="3" bgcolor="#F2FFF8"  onmouseover='this.style.background=&quot;#F4B8B8&quot;' onmouseout='this.style.background=&quot;#F2FFF8&quot;' ><span class="estiloServimedicoTextoContent">Quistes o Pseudoquistes:
        <input name="quistespancreas" type="text" id="quistespancreas" size="70" maxlength="90" style="height:20px;width:475px; font-size:9px;" />
      </span></td>
    </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#FCF5FB"><p><span class="estiloServimedicoTextoContent">DIAGNÓSTICO ECOGRÁFICO:</span></p>
      <span class="estiloServimedicoTextoContent">
                <textarea name="area2" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano2" type="text" id="tamano2" size="4" maxlength="2" value="9" />
        </span>
      </td>
  </tr>
  <tr valign="top">
    <td colspan="3" align="center" bgcolor="#FCF5FB"><span class="estiloServimedicosTitulo">UN COMPROMISO CON SU SALUD.</span></td>
  </tr>
</table>
</center>
<div id="flotante1"><a href="index.html" border="0"><img id="flotante1" src="atras.png" /></a><br><br><br><br><br><br><input type="image" src="impresion.jpg" width="100" height="100" /><br><br><br>&nbsp;&nbsp;<strong>2 P&aacute;ginas</strong>
<input type="checkbox" name="paginas" value="1" id="paginas" /></div>
</form>
</body>
</html>
