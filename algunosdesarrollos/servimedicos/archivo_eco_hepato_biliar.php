<?php
$DosPaginas = $_GET["paginas"];
	$a = 0;
	$b = 0;
	$c = 0;
	$d = 0;
	$e = 0;
if($DosPaginas != ""){
	$a = 200;
	$b = 70;
	$c = 480;
	$d = 360;
	$e = 240;
	$f = 120;
	}else{
	$a = 0;
	$b = 0;
	$c = 0;
	$d = 0;
	$e = 0;	
		}
define('FPDF_FONTPATH','font/');
require('WriteHTML.php');
require('fpdf.php');
$pdf=new PDF('P','pt','letter');
$pdf->AddFont('ariblk','','ariblk.php');
$pdf->AddFont('Arial','','arial.php');
$pdf->AddFont('LCALLIG','','LCALLIG.php');
$pdf->AddPage();
$pdf->SetMargins(10,20,25); //Margenes del texto
$pdf->SetLineWidth(0.9); //Ancho para las lineas
$pdf->SetDrawColor(34,139,34); //colores las lineas
$pdf->SetTextColor(34,139,34);  //PARA EL COLOR VERDE DE LAS LETRAS EN EL PDF

$pdf->SetTextColor(0,0,0);  //TEXTO EN COLOR NEGRO - TEXTO EN COLOR NEGRO

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 140);
$pdf->Cell(0,0,"Nombre del paciente: ".$_GET['nombre_paciente'],0,5);

$pdf->SetXY(420, 160);
$pdf->Cell(0,0,"Edad: ".$_GET['edad'],0,5);
$pdf->Line(450,164,490,164);// Linea horizontal
$pdf->SetXY(10, 150);
$pdf->Cell(0,0,"Remite: ".$_GET['remite'],0,5);
$pdf->SetXY(10, 160);
$pdf->Cell(0,0,"Motivos del examen: ".$_GET['motivos'],0,5);
$pdf->SetXY(377, 150);
$pdf->Cell(0,0,"Fecha:",0,5);
$pdf->SetXY(407, 150);
$pdf->Cell(0,0,utf8_decode($_GET['fecha']),0,5);

////////////////////////// INFORMACION DE HIGADO
$pdf->SetFont('Arial','',8);
$pdf->SetXY(50, 241);
$pdf->Cell(0,0,utf8_decode($_GET['tamanohigado']),0,5);
$pdf->SetXY(300, 241);
$pdf->Cell(0,0,utf8_decode($_GET['ecogenicidadhigado']),0,5);
$pdf->SetXY(60, 251);
$pdf->Cell(0,0,utf8_decode($_GET['contornohigado']),0,5);
$pdf->SetXY(185, 261);
$pdf->Cell(0,0,utf8_decode($_GET['ramasrupra']),0,5);
$pdf->SetXY(129, 271);
$pdf->Cell(0,0,utf8_decode($_GET['lesionesquisticas']),0,5);
//////////////////////////FIN INFORMACION DE HIGADO

/////////////////////////INFORMACION VESICULA
$pdf->SetXY(50, 321);
$pdf->Cell(0,0,utf8_decode($_GET['tamanovesicula']),0,5);
$pdf->SetXY(278, 321);
$pdf->Cell(0,0,utf8_decode($_GET['paredesvesicula']),0,5);
$pdf->SetXY(45, 324);
$pdf->SetFont('Arial','',$_GET['tamano1']); //con esto coloco el tamano de letra que viene del textarea
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area1'])));

$pdf->SetXY(142, 504+$b);
$pdf->SetFont('Arial','',$_GET['tamano2']); //con esto coloco el tamano de letra que viene del textarea
$pdf->WriteHTML(utf8_decode(stripcslashes($_GET['area2'])));
/////////////////////////INFORMACION VESICULA

/////////////////////////INFORMACION VIAS BILIARES
$pdf->SetXY(74, 401+$b);
$pdf->Cell(0,0,utf8_decode($_GET['viasintra']),0,5);
$pdf->SetXY(56, 411+$b);
$pdf->Cell(0,0,utf8_decode($_GET['coledoco']),0,5);
/////////////////////////INFORMACION VIAS BILIARES


///////////////////////////INFORMACION PANCREAS
$pdf->SetXY(50, 461+$b);
$pdf->Cell(0,0,utf8_decode($_GET['tamanopancreas']),0,5);
$pdf->SetXY(300, 461+$b);
$pdf->Cell(0,0,utf8_decode($_GET['econocigidadpancreas']),0,5);
$pdf->SetXY(60, 471+$b);
$pdf->Cell(0,0,utf8_decode($_GET['contornopancreas']),0,5);
$pdf->SetXY(80, 481+$b);
$pdf->Cell(0,0,utf8_decode($_GET['calcificacionpancreas']),0,5);
$pdf->SetXY(123, 491+$b);
$pdf->Cell(0,0,utf8_decode($_GET['quistespancreas']),0,5);
///////////////////////////  FIN  INFORMACION PANCREAS

$pdf->SetTextColor(34,139,34); //TEXTO EN COLOR VERDE - TEXTO EN COLOR VERDE

//$pdf->SetTextColor(129,24,74);  //PARA EL COLOR DE LAS LETRAS EN EL PDF
$pdf->Image('abdominal.png', 5, 10, 600, 96, 'png','');
$pdf->SetFont('ariblk','',16);
$pdf->SetXY(130, 125);
$pdf->Cell(0,0,"ESTUDIO: ECOGRAF�A HEPATO BILIAR",0,20);
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 140);
$pdf->Cell(0,0,"Nombre del paciente: ",0,5);
$pdf->Line(104,145,360,145);// Linea horizontal
$pdf->SetXY(10, 150);
$pdf->Cell(0,0,"Remite: ",0,5);
$pdf->Line(47,155,370,155);// Linea horizontal
$pdf->SetXY(420, 160);
$pdf->Cell(0,0,"Edad: ",0,5);
$pdf->SetXY(10, 160);
$pdf->Cell(0,0,"Motivos del examen: ",0,5);
$pdf->Line(104,165,400,165);// Linea horizontal
$pdf->SetXY(377, 150);
$pdf->Cell(0,0,"Fecha:",0,5);
$pdf->Line(410,155,560,155);// Linea horizontal


$pdf->SetFont('ariblk','',12);
$pdf->SetXY(10, 190);
$pdf->Cell(0,0,"A LA EXPLORACI�N ECOGR�FICA ENCONTRAMOS:",0,20);

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 220);
$pdf->Cell(0,0,"H�GADO:",0,5);

////////////////////////// INFORMACION DE HIGADO
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 240);
$pdf->Cell(0,0,"Tama�o:",0,5);
$pdf->Line(55,245,235,245);// Linea horizontal
$pdf->SetXY(237, 240);
$pdf->Cell(0,0,"Ecogenicidad:",0,5);
$pdf->Line(300,245,560,245);// Linea horizontal
$pdf->SetXY(10, 250);
$pdf->Cell(0,0,"Contornos:",0,5);
$pdf->Line(62,255,560,255);// Linea horizontal
$pdf->SetXY(10, 260);
$pdf->Cell(0,0,"Ramas portales y venas suprahep�ticas:",0,5);
$pdf->Line(185,265,560,265);// Linea horizontal
$pdf->SetXY(10, 270);
$pdf->Cell(0,0,"Masas o lesiones qu�sticas:",0,5);
$pdf->Line(132,275,560,275);// Linea horizontal
//////////////////////////FIN INFORMACION DE HIGADO

/////////////////////////INFORMACION VESICULA
$pdf->SetXY(10, 300);
$pdf->Cell(0,0,"VES�CULA:",0,5);
$pdf->SetXY(10, 320);
$pdf->Cell(0,0,"Tama�o:",0,5);
$pdf->Line(55,325,235,325);// Linea horizontal
$pdf->SetXY(237, 320);
$pdf->Cell(0,0,"Paredes:",0,5);
$pdf->Line(280,325,560,325);// Linea horizontal
$pdf->SetXY(10, 330);
$pdf->Cell(0,0,"Interior:",0,5);
/////////////////////////INFORMACION VESICULA

/////////////////////////INFORMACION VIAS BILIARES
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 380+$b);
$pdf->Cell(0,0,"V�AS BILIARES:",0,5);
$pdf->SetXY(10, 400+$b);
$pdf->Cell(0,0,"Intrahep�ticas:",0,5);
$pdf->Line(77,405+$b,560,405+$b);// Linea horizontal
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 410+$b);
$pdf->Cell(0,0,"Coledoco:",0,5);
$pdf->Line(60,415+$b,560,415+$b);// Linea horizontal
/////////////////////////INFORMACION VIAS BILIARES

///////////////////////////INFORMACION PANCREAS
$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 440+$b);
$pdf->Cell(0,0,"P�NCREAS:",0,5);
$pdf->SetXY(10, 460+$b);
$pdf->Cell(0,0,"Tama�o:",0,5);
$pdf->Line(55,465+$b,235,465+$b);// Linea horizontal
$pdf->SetXY(237, 460+$b);
$pdf->Cell(0,0,"Ecogenicidad:",0,5);
$pdf->Line(300,465+$b,560,465+$b);// Linea horizontal
$pdf->SetXY(10, 470+$b);
$pdf->Cell(0,0,"Contornos:",0,5);
$pdf->Line(62,475+$b,560,475+$b);// Linea horizontal
$pdf->SetXY(10, 480+$b);
$pdf->Cell(0,0,"Calcificaciones:",0,5);
$pdf->Line(82,485+$b,560,485+$b);// Linea horizontal
$pdf->SetXY(10, 490+$b);
$pdf->Cell(0,0,"Quistes o Pseudoquistes:",0,5);
$pdf->Line(126,495+$b,560,495+$b);// Linea horizontal
///////////////////////////  FIN  INFORMACION PANCREAS

$pdf->SetFont('Arial',B,9);
$pdf->SetXY(10, 510+$b);
$pdf->Cell(0,0,"DIAGN�STICO ECOGR�FICO:",0,5);
$pdf->SetFont('Arial','',8);



$pdf->SetXY(10, 720);
$pdf->SetFont('Arial',B,9);
$pdf->Cell(0,0,"FIRMA: ________________________________",0,0);
$pdf->SetFont('LCALLIG','',12);
$pdf->SetXY(175, 735);
$pdf->Cell(0,0,"UN COMPROMISO CON SU SALUD",0,0);
//$pdf->Output('EcografiaRenal.pdf','D');

$pdf->Output();
//shell_exec('lpr "puerto en el cual se desea imprimir"'); 
//$salida = shell_exec(�lpr PRN�);
?>