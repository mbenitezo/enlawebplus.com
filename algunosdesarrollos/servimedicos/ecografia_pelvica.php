<? include ("fecha_espanol.php"); 
$fecha = date("Y-m-d"); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<style type="text/css">
#flotante1 {
   position:fixed;
   top: 120px;
   bottom: 0;
   right: 0;
   left: 0;
   width: 100px;
   height: 100px;
} 

.estiloServimedicos {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-style: normal;
	line-height: normal;
	color: #000;
}
.estiloServimedicosTitulo {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 16px;
	font-style: italic;
}
.estiloServimedicosTitulo {
	font-family: "Lucida Calligraphy";
	font-size: 24px;
	font-weight: bold;
	text-transform: uppercase;
	font-style: normal;
	letter-spacing: 5px;
}
.estiloServimedicos2 {
	font-family: "French Script MT";
	font-size: 20px;
	font-weight: bolder;
}
.estiloServimedicosNor1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.estiloServimedicoNor2 {
	font-family: "Arial Black", Gadget, sans-serif;
	font-size: 18px;
	font-weight: bolder;
}
.estiloServimedicoNor3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
}
.estiloServimedicoTextoContent {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-style: normal;
}
</style>

<script>
function validar_enviar(){
if( (document.form.checkcolumna1.checked) && (document.form.checkcolumna2.checked)){
		alert("No puede seleccionar 2 opciones en la DESCRIPCIÓN FETAL de la estructura COLUMNA");
		return false;			
					}
if( (document.form.checkVejiga1.checked) && (document.form.checkVejiga2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura VEJIGA");
		return false;			
					}
if( (document.form.checkrinones1.checked) && (document.form.checkrinones2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura RIÑONES");
		return false;			
					}
if( (document.form.malformaciones1.checked) && (document.form.malformaciones2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura MALFORMACIONES MACROSCÓPICAS");
		return false;			
					}
if( (document.form.cordonvasos1.checked) && (document.form.cordonvasos2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura CORDÓN 3 VASOS");
		return false;			
					}
if( (document.form.checkInserCordon1.checked) && (document.form.checkInserCordon2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura INSERCIÓN CORDÓN");
		return false;			
					}
if( (document.form.checkMovCard1.checked) && (document.form.checkMovCard2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura MOV CARD");
		return false;			
					}
if( (document.form.checkCerebelo1.checked) && (document.form.checkCerebelo2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura CEREBELO");
		return false;			
					}
if( (document.form.checkEstomago1.checked) && (document.form.checkEstomago2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura ESTOMAGO");
		return false;			
					}
if( (document.form.checkCarmCard1.checked) && (document.form.checkCarmCard2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura CAM CARD");
		return false;			
					}

}//CIERRE EL COMPROBADOR DE LOS CHECKBOX


</script> 
<script src="jquery.js" type="text/javascript"></script>
<script src="iColorPicker.js" type="text/javascript"></script>
<script type="text/javascript" src="nicEdit.js"></script>
        <script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
      </script>
</head>

<body>
<form name="form" action="archivo_eco_pelvica.php" method="get" onSubmit="return validar_enviar();"><center><table width="718" border="0">
  <tr>
    <td colspan="20" valign="top"><div align="center"><img src="abdominal.png" width="660" height="108" /></div></td>
    </tr>
  <tr>
    <td height="21" colspan="3" valign="top">&nbsp;</td>
    <td height="21" colspan="16" align="center" class="estiloServimedicoNor2">ESTUDIO: ECOGRAFÍA PÉLVICA.</td>
    <td width="102" valign="top" class="estiloServimedicos">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td colspan="20" align="center" class="estiloServimedicoTextoContent"  onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoNor3">TRANSVAGINAL <strong>
      <input type="checkbox" name="checkTransvaginal" value="1" id="CheckboxGroup1_33" />
    </strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TRANSVESICAL</span> <strong>
    <input type="checkbox" name="checkTransvesical" value="1" id="CheckboxGroup1_34" />
    </strong></td>
    </tr>
  <tr valign="top">
<td colspan="20" class="estiloServimedicoTextoContent"  onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>PACIENTE: 
      <input name="nombre_paciente" type="text" id="textfield" size="48" />
      EDAD:
      <input name="edad" type="text" id="textfield13" size="3" maxlength="3" />
      CEDULA:
      <input name="cedula" type="text" id="cedula" size="14" maxlength="14" /></td>
  </tr>
  <tr valign="top">
    <td colspan="20" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoTextoContent">DOCTOR Y/O EMPRESA: 
        <input name="remite" type="text" id="remite" size="35" /> 
    &nbsp;&nbsp;&nbsp;FECHA:
      <input name="fecha" type="text" id="textfield3" size="37" value="<? convertirFecha($fecha); ?>" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="20" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoTextoContent">F.U.R.:
        <input name="motivos" type="text" id="textfield4" size="45" maxlength="45" />
DÍA DEL CICLO:
<input name="obstetrica" type="text" id="obstetrica" size="28" maxlength="30" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="20" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoTextoContent">ANTECEDENTES:
        <input name="antecedentes" type="text" id="antecedentes" size="84" />
    </span></td>
  </tr>
  <tr valign="top">
    <td height="33" colspan="20" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>UTERO</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td height="22" colspan="3" valign="middle" ><strong><span class="estiloServimedicosNor1">POSICIÓN:</span></strong></td>
    <td colspan="3" align="left" valign="middle"><strong><span class="estiloServimedicosNor1">A.V.F</span></strong></td>
    <td width="21" align="center" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checkLongitudinal" value="1" id="CheckboxGroup1_2" />
      </span></strong></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td colspan="2" align="left" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicosNor1"><strong>R.V.F</strong></span></td>
    <td colspan="5" align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicoNor3"><strong>
      <input type="checkbox" name="CheckboxGroup1_4" value="1" id="CheckboxGroup1_35" />
    </strong></span></td>
    <td width="59" align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Indefinido</strong></td>
    <td width="21" align="center" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checkIzquierdo1" value="1" id="CheckboxGroup1_2" />
      </span></strong></td>
    <td colspan="2" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>
    <td height="22" colspan="3" valign="middle" ><strong><span class="estiloServimedicosNor1">TAMAÑO:</span></strong></td>
    <td colspan="3" align="left" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong><span class="estiloServimedicosNor1">Normal</span></strong></td>
    <td align="center" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checkCefalica" value="1" id="CheckboxGroup1_" />
    </span></strong></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicosNor1" >&nbsp;</td>
    <td colspan="2" align="left" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicosNor1"><strong>Aumentado</strong></span></td>
    <td colspan="4" align="left" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoNor3"><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checkPodalica" value="1" id="CheckboxGroup1_36" />
    </span></strong></span></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong class="estiloServimedicosNor1">Disminuido</strong></td>
    <td align="center" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checkdisminuido" value="1" id="CheckboxGroup1_7" />
    </span></strong></td>
    <td colspan="2" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>
    <td height="22" colspan="3" valign="middle" ><strong><span class="estiloServimedicosNor1">ECOTEXTURA:</span></strong></td>
    <td colspan="3" align="left" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong><span class="estiloServimedicosNor1">Homogénea</span></strong></td>
    <td align="center" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checkHomogenea" value="1" id="CheckboxGroup1_2" />
    </span></strong></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td colspan="2" align="left" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicosNor1"><strong>Heterogénea</strong></span></td>
    <td colspan="5" align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoNor3"><strong>
      <input type="checkbox" name="checkDorsoDerecho" value="1" id="CheckboxGroup1_37" />
    </strong></span></td>
    <td align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td align="center" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td colspan="2" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="2" valign="middle" ><strong><span class="estiloServimedicosNor1">CONTORNOS</span></strong>:</td>
    <td colspan="4" align="right" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">Bien definidos</span></strong></td>
    <td align="center" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="CheckboxGroup1_5" value="1" id="CheckboxGroup1_39" />
    </span></strong></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td colspan="2" align="left" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicosNor1"><strong>Mal definidos </strong></span></td>
    <td colspan="5" align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicoNor3"><strong>
      <input type="checkbox" name="checkMaldefinido" value="1" id="CheckboxGroup1_40" />
    </strong></span></td>
    <td align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Lobulados</strong></td>
    <td align="center" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checklobulados" value="1" id="CheckboxGroup1_39" />
    </span></strong></td>
    <td colspan="2" align="center" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Globoso
      <input type="checkbox" name="checkgloboso" value="1" id="CheckboxGroup1_41" />
    </strong></td>
  </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" class="estiloServimedicoNor3" >&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="2" valign="middle" class="estiloServimedicoNor3" >MIOMETRIO</td>
    <td colspan="4" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Normal</strong></td>
    <td align="center" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      <input type="checkbox" name="checkmionormal" value="1" id="CheckboxGroup1_6" />
    </strong></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Patológico</strong></td>
    <td colspan="11" align="left" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      <input type="checkbox" name="checkmiopatologico" value="1" id="CheckboxGroup1_8" />
    </strong></td>
    </tr>
  <tr valign="top">
    <td colspan="20" valign="middle" ><span class="estiloServimedicoTextoContent">
      <textarea name="area1" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano1" type="text" id="tamano1" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" >&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="2" valign="middle" class="estiloServimedicoNor3" >ENDOMETRIO</td>
    <td colspan="4" align="right" valign="middle" class="estiloServimedicosNor1" ><strong>Normal</strong></td>
    <td align="center" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      <input type="checkbox" name="checkendonormal" value="1" id="CheckboxGroup1_6" />
    </strong></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Patológico</strong></td>
    <td colspan="11" align="left" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      <input type="checkbox" name="checkendopatologico" value="1" id="CheckboxGroup1_8" />
    </strong></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" ><span class="estiloServimedicoTextoContent">
      <textarea name="area2" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano2" type="text" id="tamano2" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" >&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" align="center" valign="middle" class="estiloServimedicoNor2" >HISTEROMETRIAS</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="2" valign="middle" ><strong><span class="estiloServimedicosNor1">Longitudinal:</span></strong></td>
    <td colspan="4" align="left" valign="middle" ><span class="estiloServimedicoTextoContent">
      <input name="cmlongitudinal" type="text" id="cmlongitudinal" size="7" maxlength="7" />
    </span><strong><span class="estiloServimedicosNor1">cm</span></strong></td>
    <td align="center" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td colspan="9" align="center" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Anteroposterior:</strong><span class="estiloServimedicoTextoContent">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="cmanteroposterior" type="text" id="cmanteroposterior" size="5" maxlength=" 5" />
    </span><strong>cm</strong></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Trans:</strong></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicoTextoContent">
      <input name="cmtrans" type="text" id="cmtrans" size="5" maxlength=" 5" />
    </span><strong>cm</strong></td>
  </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="2" valign="middle" ><strong><span class="estiloServimedicosNor1">VOLUMEN</span></strong></td>
    <td colspan="4" align="left" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicoTextoContent">
      <input name="volhisteriometria" type="text" id="volhisteriometria" size="12" maxlength="12" />
    </span></td>
    <td align="center" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">CC</span></strong></td>
    <td colspan="13" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" >&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" class="estiloServimedicoNor3" >OVARIOS</td>
  </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="2" valign="middle" class="estiloServimedicosNor1" ><strong>Derecho: Mide:</strong></td>
    <td colspan="4" align="right" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">
      <input name="tamdere1" type="text" id="tamdere1" size="7" maxlength="7" />
    </span></strong></td>
    <td align="center" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>x</strong></td>
    <td width="42" align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">
      <input name="tamdere2" type="text" id="tamdere2" size="7" maxlength="7" />
    </span></strong></td>
    <td width="43" align="center" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>x</strong></td>
    <td width="52" align="left" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">
      <input name="tamdere3" type="text" id="tamdere3" size="7" maxlength="7" />
    </span></strong></td>
    <td width="17" align="left" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">cm</span></strong></td>
    <td colspan="5" align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>VOLUMEN</strong></td>
    <td colspan="3" align="left" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicoTextoContent">
      <input name="tamdere4" type="text" id="tamdere4" size="12" maxlength="12" />
    </span><strong><span class="estiloServimedicosNor1">CC</span></strong></td>
  </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="2" valign="middle" class="estiloServimedicosNor1" ><strong>Izquierdo: Mide:</strong></td>
    <td colspan="4" align="right" valign="middle" >      <strong><span class="estiloServimedicosNor1">
      <input name="tamizq1" type="text" id="tamizq1" size="7" maxlength="7" />    
    </span></strong></td>
    <td align="center" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>x</strong></td>
    <td align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>      <strong><span class="estiloServimedicosNor1">
      <input name="tamizq2" type="text" id="tamizq2" size="7" maxlength="7" />    
      </span></strong></td>
    <td align="center" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>x</strong></td>
    <td align="left" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
      <strong><span class="estiloServimedicosNor1">
        <input name="tamizq3" type="text" id="tamizq3" size="7" maxlength="7" />
        </span></strong></td>
    <td align="left" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">cm</span></strong></td>
    <td colspan="5" align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>VOLUMEN</strong></td>
    <td colspan="3" align="left" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicoTextoContent">
      <input name="tamizq4" type="text" id="tamizq4" size="12" maxlength="12" />
      </span><strong><span class="estiloServimedicosNor1">CC</span></strong></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td width="112" valign="bottom" ><strong>DERECHO:</strong></td>
    <td colspan="5" valign="bottom" ><strong>Normal<span class="estiloServimedicoNor3">
      <input type="checkbox" name="dernormalovarios" value="1" id="CheckboxGroup1_38" />
    </span></strong></td>
    <td colspan="3" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Patológico
      <input type="checkbox" name="derpatoloovarios" value="1" id="CheckboxGroup1_42" />
    </strong></td>
    <td colspan="2" align="left" valign="bottom" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td colspan="5" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>IZQUIERDO:</strong></td>
    <td colspan="2" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Normal
      <input type="checkbox" name="izqnormalovarios" value="1" id="CheckboxGroup1_43" />
    </strong></td>
    <td colspan="2" align="center" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Patológico
      <input type="checkbox" name="izqpatoloovarios" value="1" id="CheckboxGroup1_44" />
    </strong></td>
  </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="7" valign="middle" class="estiloServimedicosNor1" ><strong>ACTIVIDAD FOLICULAR: &nbsp;&nbsp;&nbsp;&nbsp;Si<span class="estiloServimedicoNor3">
      <input type="checkbox" name="siactividadfoli" value="1" id="CheckboxGroup1_45" />
      </span>&nbsp;&nbsp;&nbsp;&nbsp;No<span class="estiloServimedicoNor3">
  <input type="checkbox" name="noactividadfoli" value="1" id="CheckboxGroup1_46" />
      </span></strong></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td colspan="2" align="left" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td colspan="5" align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td align="center" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td colspan="2" align="center" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="7" valign="middle" class="estiloServimedicosNor1" ><strong>FOLÍCULO DOMINANTE: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Si
        <input type="checkbox" name="sifoliculodominante" value="1" id="CheckboxGroup1_47" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No
    <input type="checkbox" name="nofoliculodominante" value="1" id="CheckboxGroup1_48" />
    </strong></td>
    <td colspan="2" align="center" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Derecho 
        <input type="checkbox" name="CheckboxGroup1_15" value="1" id="CheckboxGroup1_49" />
    </strong></td>
    <td colspan="2" align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Izquierdo 
        <input type="checkbox" name="CheckboxGroup1_16" value="1" id="CheckboxGroup1_50" />
    </strong></td>
    <td colspan="6" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>TAMAÑO DE FOLÍCULO</strong></td>
    <td colspan="3" align="left" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
      <strong>
    <input name="tamanofoliculomm" type="text" id="tamanofoliculomm" size="7" maxlength="7" />
    mm</strong></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" class="estiloServimedicosNor1" ><strong>PATOLÓGICOS: 
      <input type="checkbox" name="checkpatologicotxt" value="1" id="CheckboxGroup1_51" />
    </strong></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" ><span class="estiloServimedicoTextoContent">
      <textarea name="area3" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano3" type="text" id="tamano3" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" >&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="2" valign="middle" class="estiloServimedicoNor3" >ANEXOS:</td>
    <td colspan="5" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Normal</strong><strong>
      <input type="checkbox" name="checkanexonormal" value="1" id="CheckboxGroup1_52" />
    </strong></td>
    <td colspan="13" align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Patológico</strong><strong>
      <input type="checkbox" name="checkanexopatologico" value="1" id="CheckboxGroup1_53" />
    </strong></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" ><span class="estiloServimedicoTextoContent">
<textarea name="area4" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano4" type="text" id="tamano4" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" >&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="4" valign="middle" class="estiloServimedicoNor3" >FONDO DE SACOS:</td>
    <td colspan="4" align="left" valign="middle" class="estiloServimedicosNor1" ><strong>Libres<strong>
      <input type="checkbox" name="checkfondolibres" value="1" id="CheckboxGroup1_54" />
    </strong></strong></td>
    <td colspan="12" align="left" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicosNor1"><strong>Patológicos</strong></span><span class="estiloServimedicoNor3"><strong>
      <input type="checkbox" name="checkfondopatologico" value="1" id="CheckboxGroup1_55" />
    </strong></span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" ><span class="estiloServimedicoTextoContent">
      <textarea name="area5" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano5" type="text" id="tamano5" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" >&nbsp;</td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" ><span class="estiloServimedicoTextoContent"><strong>IMPRESIÓN ECOGRÁFICA:</strong>
        <strong>&nbsp;Normal<span class="estiloServimedicoNor3">
        <input type="checkbox" name="checkimpresionnormal" value="1" id="CheckboxGroup1_3" />
        </span>&nbsp;&nbsp;&nbsp;&nbsp;Patológico<span class="estiloServimedicoNor3">
        <input type="checkbox" name="checkimpresionpatologico" value="1" id="CheckboxGroup1_4" />
        </span></strong>
        <textarea name="area6" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano6" type="text" id="tamano6" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>
    <td colspan="20" valign="middle" >&nbsp;</td>
    </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="24" colspan="20" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicoTextoContent"><strong>COMENTARIOS</strong>
        <textarea name="area7" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano7" type="text" id="tamano7" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="22" colspan="20" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    </tr>

  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="20" align="center" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicosTitulo">UN COMPROMISO CON SU SALUD.</span></td>
    </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="16" colspan="2" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td width="20" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td width="27" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td width="39" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td colspan="2" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td colspan="2" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td colspan="5" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td width="62" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td colspan="2" align="center" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td colspan="2" align="center" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
  </tr>
</table>
</center>
<div id="flotante1"><a href="index.html" border="0"><img id="flotante1" src="atras.png" /></a><br><br><br><br><br><br><input type="image" src="impresion.jpg" width="100" height="100" /><br><br><br>&nbsp;&nbsp;<strong>2 P&aacute;ginas</strong>
<input type="checkbox" name="paginas" value="1" id="paginas" /></div>
</form>
</body>
</html>
