<? include ("fecha_espanol.php"); 
$fecha = date("Y-m-d"); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<style type="text/css">
#flotante1 {
   position:fixed;
   top: 120px;
   bottom: 0;
   right: 0;
   left: 0;
   width: 100px;
   height: 100px;
} 
.estiloServimedicos {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-style: normal;
	line-height: normal;
	color: #000;
}
.estiloServimedicosTitulo {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 16px;
	font-style: italic;
}
.estiloServimedicosTitulo {
	font-family: "Lucida Calligraphy";
	font-size: 24px;
	font-weight: bold;
	text-transform: uppercase;
	font-style: normal;
	letter-spacing: 5px;
}
.estiloServimedicos2 {
	font-family: "French Script MT";
	font-size: 20px;
	font-weight: bolder;
}
.estiloServimedicosNor1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.estiloServimedicoNor2 {
	font-family: "Arial Black", Gadget, sans-serif;
	font-size: 18px;
	font-weight: bolder;
}
.estiloServimedicoNor3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
}
.estiloServimedicoTextoContent {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-style: normal;
}
</style>

<script src="jquery.js" type="text/javascript"></script>
<script src="iColorPicker.js" type="text/javascript"></script>
<script type="text/javascript" src="nicEdit.js"></script>
        <script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
      </script>
</head>

<body>
<form action="archivo_eco_renal.php" method="get"><center><table width="661" border="0">
  <tr>
    <td colspan="3" valign="top"><div align="center"><img src="abdominal.png" width="660" height="108" /></div></td>
    </tr>
  <tr>
    <td width="102" height="21" valign="top">&nbsp;</td>
    <td width="443" height="21" align="center" class="estiloServimedicoNor2">ESTUDIO: ECOGRAFÍA RENAL.</td>
    <td width="102" valign="top" class="estiloServimedicos">&nbsp;</td>
  </tr>
  <tr valign="top">
<td colspan="3" class="estiloServimedicoTextoContent"  onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>Nombre del paciente: 
      <input name="nombre_paciente" type="text" id="textfield" size="70" />
      EDAD:
      <input name="edad" type="text" id="edad" size="3" maxlength="3" />
      </td>
  </tr>
  <tr valign="top">
    <td colspan="3" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoTextoContent">Remite: 
      <input name="textfield2" type="text" id="textfield2" size="45" /> 
    &nbsp;&nbsp;&nbsp;Fecha:
      <input name="fecha" type="text" id="textfield3" size="40" value="<? convertirFecha($fecha); ?>" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoTextoContent">Motivos del examen: 
      <input name="motivos" type="text" id="textfield4" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" class="estiloServimedicoNor2">A LA EXPLORACIÓN ECOGRÁFICA ENCONTRAMOS:</td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2" class="estiloServimedicoNor3">RIÑÓN DERECHO:</td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2" onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F8F9F2"'><span class="estiloServimedicoTextoContent">Diámetros: 
      <input name="diametros2" type="text" id="textfield5" size="30" /> 
       Parenquima: 
       <input name="parenquima2" type="text" id="textfield6" size="30" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3"  bgcolor="#F8F9F2" onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F8F9F2"'><span class="estiloServimedicoTextoContent">Contornos: 
      <input name="contornos2" type="text" id="textfield7" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2"  onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F8F9F2"'><span class="estiloServimedicoTextoContent">Espacios Perirrenales: 
      <input name="perirrenales2" type="text" id="textfield8" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2"  onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F8F9F2"'><span class="estiloServimedicoTextoContent">Calcificaciones: 
      <input name="calcificaciones2" type="text" id="textfield9" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2"  onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F8F9F2"'><span class="estiloServimedicoTextoContent">Masas: 
      <input name="masas2" type="text" id="textfield10" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2"  onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F8F9F2"'><span class="estiloServimedicoTextoContent">Unión Corticomeadular: 
      <input name="corticomedular2" type="text" id="textfield11" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2"  onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F8F9F2"'><span class="estiloServimedicoTextoContent">Cavidades Pielocaliciales: 
      <input name="pielocaliciales2" type="text" id="textfield12" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2"><p><span class="estiloServimedicoTextoContent">Cálculos:</span></p>
      <p><span class="estiloServimedicoTextoContent">
        <textarea name="area1" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano1" type="text" id="tamano1" size="4" maxlength="2" value="9" />
      </span></p></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F8F9F2"><p><span class="estiloServimedicoTextoContent">Otros hallazgos:</span></p>
      <p><span class="estiloServimedicoTextoContent">
        <textarea name="area2" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano2" type="text" id="tamano2" size="4" maxlength="2" value="9" />
      </span></p></td>
  </tr>
    <tr valign="top">
    <td colspan="3" bgcolor="#F2FAFD" class="estiloServimedicoNor3">RIÑÓN IZQUIERDO:</td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F2FAFD"  onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F2FAFD"'><span class="estiloServimedicoTextoContent">Diámetros: 
      <input name="diametros" type="text" id="textfield5" size="30" /> 
       Parenquima: 
       <input name="parenquima" type="text" id="textfield6" size="30" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F2FAFD"  onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F2FAFD"'><span class="estiloServimedicoTextoContent">Contornos: 
      <input name="contornos" type="text" id="textfield7" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F2FAFD"  onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F2FAFD"'><span class="estiloServimedicoTextoContent">Espacios Perirrenales: 
      <input name="perirrenales" type="text" id="textfield8" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F2FAFD"  onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F2FAFD"'><span class="estiloServimedicoTextoContent">Calcificaciones: 
      <input name="calcificaciones" type="text" id="textfield9" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F2FAFD"  onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F2FAFD"'><span class="estiloServimedicoTextoContent">Masas: 
      <input name="masas" type="text" id="textfield10" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F2FAFD"  onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F2FAFD"'><span class="estiloServimedicoTextoContent">Unión Corticomeadular: 
      <input name="corticomedular" type="text" id="textfield11" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F2FAFD"  onmouseover='this.style.background="#F4B8B8"' onmouseout='this.style.background="#F2FAFD"'><span class="estiloServimedicoTextoContent">Cavidades Pielocaliciales: 
      <input name="pielocaliciales" type="text" id="textfield12" size="70" />
    </span></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F2FAFD"><p><span class="estiloServimedicoTextoContent">Cálculos:</span></p>
      <p><span class="estiloServimedicoTextoContent">
        <textarea name="area3" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano3" type="text" id="tamano3" size="4" maxlength="2" value="9" />
      </span></p></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#F2FAFD"><p><span class="estiloServimedicoTextoContent">Otros hallazgos:</span></p>
      <p><span class="estiloServimedicoTextoContent">
       <textarea name="area4" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano4" type="text" id="tamano4" size="4" maxlength="2" value="9" />
      </span></p></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#FCF5FB"><p><span class="estiloServimedicoTextoContent">Otras Valoraciones Ecográficas:</span></p>
      <p><span class="estiloServimedicoTextoContent">
        <textarea name="area5" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano5" type="text" id="tamano5" size="4" maxlength="2" value="9" />
      </span> </p></td>
  </tr>
  <tr valign="top">
    <td colspan="3" bgcolor="#FCF5FB"><p><span class="estiloServimedicoTextoContent">DIAGNÓSTICO ECOGRÁFICO:</span></p>
     <span class="estiloServimedicoTextoContent">
        <textarea name="area6" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano6" type="text" id="tamano6" size="4" maxlength="2" value="9" />
      </span>
      </td>
  </tr>
  <tr valign="top">
    <td colspan="3" align="center" bgcolor="#FCF5FB"><span class="estiloServimedicosTitulo">UN COMPROMISO CON SU SALUD.</span></td>
  </tr>
</table>
</center>
<div id="flotante1"><a href="index.html" border="0"><img id="flotante1" src="atras.png" /></a><br><br><br><br><br><br><input type="image" src="impresion.jpg" width="100" height="100" /><br><br><br>&nbsp;&nbsp;<strong>2 P&aacute;ginas</strong>
<input type="checkbox" name="paginas" value="1" id="paginas" /></div>
</form>
</body>
</html>
