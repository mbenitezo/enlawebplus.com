<? include ("fecha_espanol.php"); 
$fecha = date("Y-m-d"); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<style type="text/css">
#flotante1 {
   position:fixed;
   top: 120px;
   bottom: 0;
   right: 0;
   left: 0;
   width: 100px;
   height: 100px;
}

.estiloServimedicos {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-style: normal;
	line-height: normal;
	color: #000;
}
.estiloServimedicosTitulo {
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size: 16px;
	font-style: italic;
}
.estiloServimedicosTitulo {
	font-family: "Lucida Calligraphy";
	font-size: 24px;
	font-weight: bold;
	text-transform: uppercase;
	font-style: normal;
	letter-spacing: 5px;
}
.estiloServimedicos2 {
	font-family: "French Script MT";
	font-size: 20px;
	font-weight: bolder;
}
.estiloServimedicosNor1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.estiloServimedicoNor2 {
	font-family: "Arial Black", Gadget, sans-serif;
	font-size: 18px;
	font-weight: bolder;
}
.estiloServimedicoNor3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
}
.estiloServimedicoTextoContent {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-style: normal;
}
</style>

<script>
function cuenta1(){
if(document.forms[0].vesiculainterior1.value.length == 100){
	document.forms[0].vesiculainterior2.focus();
	}
}
function cuenta2(){
if(document.forms[0].vesiculainterior2.value.length == 100){
	document.forms[0].vesiculainterior3.focus();
	}
}

function borrar1(){
var str = document.forms[0].vesiculainterior1.value;
var newStr = str.substring(0, str.length-1);
document.forms[0].vesiculainterior1.value = newStr;
	}
function borrar2(){
var str = document.forms[0].vesiculainterior2.value;
var newStr = str.substring(0, str.length-1);
document.forms[0].vesiculainterior2.value = newStr;
	}

function cuenta4(){
if(document.forms[0].calderecho1.value.length == 100){
	document.forms[0].calderecho2.focus();
	}
}
function cuenta5(){
if(document.forms[0].calderecho2.value.length == 100){
	document.forms[0].calderecho3.focus();
	}
}
function borrar4(){
var str = document.forms[0].calderecho1.value;
var newStr = str.substring(0, str.length-1);
document.forms[0].calderecho1.value = newStr;
	}
function borrar5(){
var str = document.forms[0].calderecho2.value;
var newStr = str.substring(0, str.length-1);
document.forms[0].calderecho2.value = newStr;
	}


function cuenta7(){
if(document.forms[0].calizquierdo1.value.length == 100){
	document.forms[0].calizquierdo2.focus();
	}
}
function cuenta8(){
if(document.forms[0].calizquierdo2.value.length == 110){
	document.forms[0].calizquierdo3.focus();
	}
}
function borrar7(){
var str = document.forms[0].calizquierdo1.value;
var newStr = str.substring(0, str.length-1);
document.forms[0].calizquierdo1.value = newStr;
	}
function borrar8(){
var str = document.forms[0].calizquierdo2.value;
var newStr = str.substring(0, str.length-1);
document.forms[0].calizquierdo2.value = newStr;
	}


function cuenta10(){
if(document.forms[0].diagnosticoeco1.value.length == 100){
	document.forms[0].diagnosticoeco2.focus();
	}
}
function cuenta11(){
if(document.forms[0].diagnosticoeco2.value.length == 110){
	document.forms[0].diagnosticoeco3.focus();
	}
}
function borrar10(){
var str = document.forms[0].diagnosticoeco1.value;
var newStr = str.substring(0, str.length-1);
document.forms[0].diagnosticoeco1.value = newStr;
	}
function borrar11(){
var str = document.forms[0].diagnosticoeco2.value;
var newStr = str.substring(0, str.length-1);
document.forms[0].diagnosticoeco2.value = newStr;
	}

function validar_enviar(){
if( (document.form.checkcolumna1.checked) && (document.form.checkcolumna2.checked)){
		alert("No puede seleccionar 2 opciones en la DESCRIPCIÓN FETAL de la estructura COLUMNA");
		return false;			
					}
if( (document.form.checkVejiga1.checked) && (document.form.checkVejiga2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura VEJIGA");
		return false;			
					}
if( (document.form.checkrinones1.checked) && (document.form.checkrinones2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura RIÑONES");
		return false;			
					}
if( (document.form.malformaciones1.checked) && (document.form.malformaciones2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura MALFORMACIONES MACROSCÓPICAS");
		return false;			
					}
if( (document.form.cordonvasos1.checked) && (document.form.cordonvasos2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura CORDÓN 3 VASOS");
		return false;			
					}
if( (document.form.checkInserCordon1.checked) && (document.form.checkInserCordon2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura INSERCIÓN CORDÓN");
		return false;			
					}
if( (document.form.checkMovCard1.checked) && (document.form.checkMovCard2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura MOV CARD");
		return false;			
					}
if( (document.form.checkCerebelo1.checked) && (document.form.checkCerebelo2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura CEREBELO");
		return false;			
					}
if( (document.form.checkEstomago1.checked) && (document.form.checkEstomago2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura ESTOMAGO");
		return false;			
					}
if( (document.form.checkCarmCard1.checked) && (document.form.checkCarmCard2.checked)){
		alert("No puede seleccionar 2 opciones en la  DESCRIPCIÓN FETAL de la estructura CAM CARD");
		return false;			
					}

}//CIERRE EL COMPROBADOR DE LOS CHECKBOX


</script> 
<script src="jquery.js" type="text/javascript"></script>
<script src="iColorPicker.js" type="text/javascript"></script>
<script type="text/javascript" src="nicEdit.js"></script>
        <script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
      </script>
</head>

<body>

<form name="form" action="archivo_eco_obstetrica.php" method="get" onSubmit="return validar_enviar();"><center><table width="780" border="0">
  <tr>
    <td colspan="18" valign="top"><div align="center"><img src="obstetrica.png" width="802" height="160" /></div></td>
    </tr>
  <tr>
    <td height="21" colspan="2" valign="top">&nbsp;</td>
    <td height="21" colspan="15" align="center" class="estiloServimedicoNor2">&nbsp;</td>
    <td width="177" valign="top" class="estiloServimedicos">&nbsp;</td>
  </tr>
  <tr valign="top">
<td colspan="17" class="estiloServimedicoTextoContent"  onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>PACIENTE: 
      <input name="nombre_paciente" type="text" id="textfield" size="62" />&nbsp;</td>
<td class="estiloServimedicoTextoContent"  onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="right">EDAD:
  <input name="edad" type="text" id="textfield13" size="3" maxlength="3" />
</div></td>
  </tr>
  <tr valign="top">
    <td colspan="14" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoTextoContent">DOCTOR Y/O EMPRESA: 
        <input name="remite" type="text" id="remite" size="40" /> 
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
    <td colspan="4" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="right"><span class="estiloServimedicoTextoContent">FECHA:
      <input name="fecha" type="text" id="textfield3" size="40" value="<? convertirFecha($fecha); ?>" />
    </span></div></td>
    </tr>
  <tr valign="top">
    <td colspan="8" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoTextoContent">F.U.R.:
        <input name="motivos" type="text" id="textfield4" size="28" maxlength="30" />
    </span></td>
    <td colspan="9" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="right"><span class="estiloServimedicoTextoContent">FICHA GINECO - OBSTÉTRICA.: </span></div></td>
    <td onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="right"><span class="estiloServimedicoTextoContent">
      <input name="obstetrica" type="text" id="obstetrica" size="28" maxlength="30" />
    </span></div></td>
  </tr>
  <tr valign="top">
    <td colspan="18" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoTextoContent">ANTECEDENTES:
        <input name="antecedentes" type="text" id="antecedentes" size="84" />
    </span></td>
  </tr>
  <tr valign="top">
    <td height="33" colspan="2" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong><span class="estiloServimedicosNor1">FETO:</span></strong></td>
    <td colspan="3" align="right" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><p><strong><span class="estiloServimedicosNor1">Único</span></strong></p></td>
    <td width="22" align="center" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><p>
      <strong><span class="estiloServimedicosNor1">
        <label>
          <input type="checkbox" name="checkUnico" value="1" id="CheckboxGroup1_0" />
          </label>
        <br />
      </span></strong></p></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="left"><input id="color1" name="color1" class="iColorPicker" type="text" value="#000000" style="width:10px; height:10px" /></div></td>
    <td width="48" align="center" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><p> <strong><span class="estiloServimedicosNor1"><strong>Múltiple</strong><br />
    </span></strong></p></td>
    <td colspan="3" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="left"><strong>
      <input type="checkbox" name="checkMultiple" value="1" id="CheckboxGroup1_33" />
    </strong></div></td>
    <td colspan="3" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><input id="color5" name="color5" class="iColorPicker" type="text" value="#000000" style="width:10px; height:10px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <strong>Móvil y Cambiante</strong></td>
    <td width="22" align="center" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><p>
      <strong><span class="estiloServimedicosNor1">
        <label>
          <input type="checkbox" name="checkCambiante" value="1" id="CheckboxGroup1_0" />
          </label>
        <br />
      </span></strong></p></td>
    <td colspan="2" align="left" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>      <strong><span class="estiloServimedicosNor1">
     <input id="color9" name="color9" class="iColorPicker" type="text" value="#000000" style="width:10px; height:10px" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input name="txtmovilcam" type="text" id="txtmovilcam" size="20" maxlength="25" />    
      </span></strong></td>
    </tr>
  <tr valign="top">
    <td height="38" colspan="2" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong><span class="estiloServimedicosNor1">PRESENTACIÓN</span></strong></td>
    <td colspan="3" align="right" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong><span class="estiloServimedicosNor1">Cefálica</span></strong></td>
    <td align="center" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checkCefalica" value="1" id="CheckboxGroup1_" />
    </span></strong></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="left"><input id="color2" name="color2" class="iColorPicker" type="text" value="#000000" style="width:10px; height:10px" /></div></td>
    <td align="center" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicosNor1"><strong>Podálica</strong></span></td>
    <td colspan="4" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="left"><span class="estiloServimedicoNor3"><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checkPodalica" value="1" id="CheckboxGroup1_34" />
    </span></strong></span><input id="color6" name="color6" class="iColorPicker" type="text" value="#000000" style="width:10px; height:10px" /></div></td>
    <td colspan="2" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="left"></div></td>
    <td align="center" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td width="14" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
  </tr>
  <tr valign="top">
    <td height="38" colspan="2" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong><span class="estiloServimedicosNor1">SITUACIÓN</span></strong></td>
    <td colspan="3" align="right" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong><span class="estiloServimedicosNor1">Longitudinal</span></strong></td>
    <td align="center" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checkLongitudinal" value="1" id="CheckboxGroup1_2" />
    </span></strong></td>
    <td colspan="3" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicosNor1"><input id="color3" name="color3" class="iColorPicker" type="text" value="#000000" style="width:10px; height:10px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Dorso Derecho</strong></span></td>
    <td colspan="5" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="left"><span class="estiloServimedicoNor3"><strong>
      <input type="checkbox" name="checkDorsoDerecho" value="1" id="CheckboxGroup1_35" />
    </strong></span><input id="color7" name="color7" class="iColorPicker" type="text" value="#000000" style="width:10px; height:10px" /></div></td>
    <td align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>Izquierdo</strong></td>
    <td align="center" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checkIzquierdo1" value="1" id="CheckboxGroup1_2" />
      </span></strong></td>
    <td colspan="2" valign="middle" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><input id="color10" name="color10" class="iColorPicker" type="text" value="#000000" style="width:10px; height:10px" /></td>
    </tr>
  <tr valign="top">
    <td height="35" colspan="2" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td colspan="3" align="right" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">Transversal</span></strong></td>
    <td align="center" valign="middle" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checkTransversal" value="1" id="CheckboxGroup1_6" />
    </span></strong></td>
    <td colspan="3" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><span class="estiloServimedicosNor1"><input id="color4" name="color4" class="iColorPicker" type="text" value="#000000" style="width:10px; height:10px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Polo Cefálico: Derecho</strong></span></td>
    <td colspan="5" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><div align="left"><span class="estiloServimedicoNor3"><strong>
      <input type="checkbox" name="checkCefalicoDer" value="1" id="CheckboxGroup1_36" />
    </strong></span><input id="color8" name="color8" class="iColorPicker" type="text" value="#000000" style="width:10px; height:10px" /></div></td>
    <td align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Izquierdo</strong></td>
    <td align="center" valign="middle" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong><span class="estiloServimedicosNor1">
      <input type="checkbox" name="checkIzquierdo2" value="1" id="CheckboxGroup1_6" />
    </span></strong></td>
    <td colspan="2" align="right" valign="middle" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><input id="color11" name="color11" class="iColorPicker" type="text" value="#000000" style="width:10px; height:10px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Dorso: sup
      <input type="checkbox" name="checkDorsoSup" value="1" id="CheckboxGroup1_7" />
      INF
      <input type="checkbox" name="checkDorsoInf" value="1" id="CheckboxGroup1_8" />
    </strong><input id="color12" name="color12" class="iColorPicker" type="text" value="#000000" style="width:10px; height:10px" /></td>
  </tr>
  <tr valign="top" class="estiloServimedicoNor3">
    <td height="35" colspan="2" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>BIOMÉTRICAS</td>
    <td colspan="3" align="right" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td align="center" valign="bottom" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td colspan="3" align="center" valign="bottom" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>SEMANAS</td>
    <td colspan="9" align="center" valign="bottom" class="estiloServimedicoNor3" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>PLACENTA</td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="6" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>DBP
      <input name="txtdbp" type="text" id="txtdbp" size="22" maxlength="30" />
    </strong></td>
    <td colspan="7" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      MM
        <input name="txtmm1" type="text" id="txtmm1" size="24" maxlength="28" />
    </strong></td>
    <td colspan="5" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><div align="right"><strong>
      ANTERIOR
      <input name="txtplaceAnterior" type="text" id="txtplaceAnterior" size="28" maxlength="22" />
    </strong></div></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="6" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>HC
      <input name="txthc" type="text" id="txthc" size="22" maxlength="30" />
      &nbsp;&nbsp;</strong></td>
    <td colspan="7" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      MM
        <input name="txtmm2" type="text" id="txtmm2" size="24" maxlength="28" />
    </strong></td>
    <td colspan="5" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><div align="right"><strong>
      POSTERIOR
      <input name="txtplacenPosterior" type="text" id="txtplacenPosterior" size="28" maxlength="20" />
    </strong></div></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="6" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>AC
        <input name="txtac" type="text" id="txtac" size="22" maxlength="30" />
      &nbsp;&nbsp;</strong></td>
    <td colspan="7" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      MM
        <input name="txtmm3" type="text" id="txtmm3" size="24"  maxlength="28" />
    </strong></td>
    <td colspan="5" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><div align="right"><strong>
      DERECHO
      <input name="txtplacenDerecho" type="text" id="txtplacenDerecho" size="28" maxlength="22" />
    </strong></div></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="6" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>HUMERO
        <input name="txthumero" type="text" id="txthumero" size="17" maxlength="25" />
      &nbsp;&nbsp;</strong></td>
    <td colspan="7" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      MM
        <input name="txtmm4" type="text" id="txtmm4" size="24" maxlength="28" />
    </strong></td>
    <td colspan="5" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><div align="right"><strong>
      IZQUIERDO
      <input name="txtplacenIzquierdo" type="text" id="txtplacenIzquierdo" size="28" maxlength="21" />
    </strong></div></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="6" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>L.F
        <input name="txtlf" type="text" id="txtlf" size="22" maxlength="30" />
      &nbsp;&nbsp;</strong></td>
    <td colspan="7" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      MM
        <input name="txtmm5" type="text" id="txtmm5" size="24" maxlength="28" />
    </strong></td>
    <td colspan="5" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><div align="right"><strong>
      FUNDICA
      <input name="txtplacenfundica" type="text" id="txtplacenfundica" size="28" maxlength="23" />
    </strong></div></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="6" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>CEREBELO
        <input name="txtcerebelo" type="text" id="txtcerebelo" size="15" maxlength="25" />
      &nbsp;&nbsp;</strong></td>
    <td colspan="7" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      MM
        <input name="txtmm6" type="text" id="txtmm6" size="24" maxlength="28" />
    </strong></td>
    <td colspan="5" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><div align="right"><strong>
      BAJA
      <input name="txtplacenbaja" type="text" id="txtplacenbaja" size="28" maxlength="25" />
    </strong></div></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="12" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>EDAD U.S.
      <input name="txtedadus" type="text" id="txtedadus" size="50" maxlength="57" />
    </strong></td>
    <td width="42" align="right" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td colspan="5" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="right"><strong>
      OCLUSIVA
      <input name="txtplacenbaja" type="text" id="txtplacenbaja" size="28" maxlength="22" />
    </strong></div></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="12" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>PESO ESTIMADO
        <input name="txtpesoesti" type="text" id="txtpesoesti" size="44" maxlength="52" />
    </strong></td>
    <td align="right" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td colspan="5" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="right"><strong>
      MARGINAL
      <input name="txtplacenmarginal" type="text" id="txtplacenmarginal" size="28" maxlength="22" />
    </strong></div></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="24" colspan="12" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>LIQUIDO AMNIÓTICO &lt; 5 Cms OLIGOHIDRAMNIOS<span class="estiloServimedicoTextoContent">
      <input name="txtliquidooligo" type="text" id="textfield9" size="3" maxlength="3" />
    </span></strong></td>
    <td align="right" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td colspan="5" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>GRADO:</strong></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="24" colspan="12" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>&gt;20 CMS POLIHIDRAMNIOS <span class="estiloServimedicoTextoContent">
      <input name="txtpolihi" type="text" id="txtpolihi" size="3" maxlength="3" />
      </span></strong></td>
    <td colspan="6" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><div align="center"><strong>  I<span class="estiloServimedicoTextoContent">
      <input name="grado1" type="text" id="grado1" size="3" maxlength="3" />
      &nbsp;&nbsp;</span> II<span class="estiloServimedicoTextoContent">
        <input name="grado2" type="text" id="grado2" size="3" maxlength="3" />
        &nbsp; </span>III<span class="estiloServimedicoTextoContent">
          <input name="grado3" type="text" id="grado3" size="3" maxlength="3" />
          &nbsp; </span>IV<span class="estiloServimedicoTextoContent">
            <input name="grado4" type="text" id="grado4" size="3" maxlength="3" />
        </span></strong></div></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="2" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td height="35" colspan="3" align="right" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><div align="center"><strong>Normal</strong></div></td>
    <td height="35" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      <input type="checkbox" name="checknormal1" value="1" id="CheckboxGroup1_9" />
    </strong></td>
    <td align="right" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Oligohid</strong></td>
    <td align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      <input type="checkbox" name="checkoligohid" value="1" id="CheckboxGroup1_10" />
    </strong></td>
    <td align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'>&nbsp;</td>
    <td colspan="3" align="right" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Poligoh
      <input type="checkbox" name="checkPoligoh" value="1" id="CheckboxGroup1_11" />
    </strong></td>
    <td width="67" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>In<strong>d<strong><strong>ice</strong></strong></strong></strong></td>
    <td colspan="3" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      <input name="txtindice" type="text" id="txtindice" size="19" maxlength="16" />
      <strong>cms</strong></strong></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="24" colspan="5" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>MOVIMIENTOS FETALES</strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Normal</strong></td>
    <td height="24" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      <input type="checkbox" name="checknormal2" value="1" id="CheckboxGroup1_3" />
    </strong></td>
    <td align="right" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>Disminuido</strong></td>
    <td align="left" valign="bottom" onmouseover='this.style.background=&quot;#BFE7EE&quot;' onmouseout='this.style.background=&quot;white&quot;'><strong>
      <input type="checkbox" name="checkdisminuido" value="1" id="CheckboxGroup1_5" />
    </strong></td>
    <td align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td colspan="7" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>F.C.F: <strong><strong><strong></strong></strong></strong></strong><strong>
    <input name="txtfcf" type="text" id="txtfcf" size="31" maxlength="25" />
L.P.M</strong></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="22" colspan="5" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>ACTIVIDAD Y TONO FETAL</strong><strong>&nbsp; Normal</strong></td>
    <td height="22" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>
      <input type="checkbox" name="checknormal3" value="1" id="CheckboxGroup1_3" />
    </strong></td>
    <td width="67" align="right" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>Patológico</strong></td>
    <td width="22" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>
      <input type="checkbox" name="checkpatologico" value="1" id="CheckboxGroup1_5" />
      </strong></td>
    <td align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td width="7" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td width="7" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td colspan="7" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>Durante el examen</strong><strong>
    </strong></td>
    </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="22" colspan="5" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>MOV RESPIRATORIOS &nbsp;&nbsp;Presentes</strong><strong>&nbsp; </strong></td>
    <td height="22" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>
      <input type="checkbox" name="checkpresentes" value="1" id="CheckboxGroup1_4" />
    </strong></td>
    <td align="right" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>Ausentes</strong></td>
    <td align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>
      <input type="checkbox" name="checkausentes" value="1" id="CheckboxGroup1_12" />
    </strong></td>
    <td align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'>&nbsp;</td>
    <td colspan="7" align="left" valign="bottom" class="estiloServimedicosNor1" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>
      <input name="txtduranteexamen" type="text" id="txtduranteexamen" size="37" maxlength="35" />
    </strong></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="18" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><strong>DESCRIPCIÓN FETAL. Se observan las siguientes estructuras:</strong></td>
    </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td width="83" height="34" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>-Columna</strong></td>
    <td width="34" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>SI
        <input type="checkbox" name="checkcolumna1" value="1" id="CheckboxGroup1_17" />
    </strong></td>
    <td width="57" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>NO
        <input type="checkbox" name="checkcolumna2" value="1" id="CheckboxGroup1_18" />
    </strong></td>
    <td width="9" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td colspan="2" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td colspan="2" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>-Cordón 3 vasos</strong></td>
    <td colspan="4" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>SI
      <input type="checkbox" name="cordonvasos1" value="1" id="CheckboxGroup1_15" />
    </strong></td>
    <td valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>NO
      <input type="checkbox" name="cordonvasos2" value="1" id="CheckboxGroup1_16" />
    </strong></td>
    <td colspan="2" align="center" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>-Cerebelo</strong></td>
    <td colspan="2" align="center" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>SI
      <input type="checkbox" name="checkCerebelo1" value="1" id="CheckboxGroup1_14" />
    </strong></td>
    <td valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>NO
      <input type="checkbox" name="checkCerebelo2" value="1" id="CheckboxGroup1_13" />
    </strong></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="34" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>-Vejiga</strong></td>
    <td valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>SI
      <input type="checkbox" name="checkVejiga1" value="1" id="CheckboxGroup1_19" />
    </strong></td>
    <td align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>NO
      <input type="checkbox" name="checkVejiga2" value="1" id="CheckboxGroup1_20" />
    </strong></td>
    <td align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td colspan="2" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td colspan="2" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>-Inserción cordón</strong></td>
    <td colspan="4" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>SI
      <input type="checkbox" name="checkInserCordon1" value="1" id="CheckboxGroup1_21" />
    </strong></td>
    <td valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>NO
      <input type="checkbox" name="checkInserCordon2" value="1" id="CheckboxGroup1_22" />
    </strong></td>
    <td colspan="2" align="center" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>-Estomago</strong></td>
    <td colspan="2" align="center" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>SI
      <input type="checkbox" name="checkEstomago1" value="1" id="CheckboxGroup1_23" />
    </strong></td>
    <td valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>NO
      <input type="checkbox" name="checkEstomago2" value="1" id="CheckboxGroup1_24" />
    </strong></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="34" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>-Riñones</strong></td>
    <td valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>SI
      <input type="checkbox" name="checkrinones1" value="1" id="CheckboxGroup1_25" />
    </strong></td>
    <td align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>NO
      <input type="checkbox" name="checkrinones2" value="1" id="CheckboxGroup1_26" />
    </strong></td>
    <td align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td colspan="2" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td colspan="2" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>-Mov. Card.</strong></td>
    <td colspan="4" align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>SI
      <input type="checkbox" name="checkMovCard1" value="1" id="CheckboxGroup1_27" />
    </strong></td>
    <td valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>NO
      <input type="checkbox" name="checkMovCard2" value="1" id="CheckboxGroup1_28" />
    </strong></td>
    <td colspan="2" align="center" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>-Cam Card</strong></td>
    <td colspan="2" align="center" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>SI
      <input type="checkbox" name="checkCarmCard1" value="1" id="CheckboxGroup1_29" />
    </strong></td>
    <td valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>NO
      <input type="checkbox" name="checkCarmCard2" value="1" id="CheckboxGroup1_30" />
    </strong></td>
  </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="34" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'">&nbsp;</td>
    <td valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>SI
      <input type="checkbox" name="malformaciones1" value="1" id="CheckboxGroup1_31" />
    </strong></td>
    <td align="right" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>NO
      <input type="checkbox" name="malformaciones2" value="1" id="CheckboxGroup1_32" />
    </strong></td>
    <td colspan="15" align="left" valign="middle" onmouseover="this.style.background='#BFE7EE'" onmouseout="this.style.background='white'"><strong>Se Observaron Malformaciones Macroscópica</strong></td>
    </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="18" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><span class="estiloServimedicoTextoContent"><strong>Conclusión:</strong>
    <textarea name="area1" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano1" type="text" id="tamano1" size="4" maxlength="2" value="9" />
    </span></td>
    </tr>
  <tr valign="top" class="estiloServimedicosNor1">
    <td height="35" colspan="18" valign="bottom" onmouseover='this.style.background="#BFE7EE"' onmouseout='this.style.background="white"'><p><span class="estiloServimedicoTextoContent"><strong>IMPRESIÓN ECOGRÁFICO:</strong>
    <textarea name="area2" cols="90" rows="3"></textarea>
<strong>Tama&ntilde;o del texto:</strong>
        <input name="tamano2" type="text" id="tamano2" size="4" maxlength="2" value="9" />
      </span></p></td>
    </tr>
  <tr valign="top">
    <td colspan="18" align="center" bgcolor="#FCF5FB"><span class="estiloServimedicosTitulo">UN COMPROMISO CON SU SALUD.</span></td>
  </tr>
</table>
</center>
<div id="flotante1"><a href="index.html" border="0"><img id="flotante1" src="atras.png" /></a><br><br><br><br><br><br><input type="image" src="impresion.jpg" width="100" height="100" /><br><br><br>&nbsp;&nbsp;<strong>2 P&aacute;ginas</strong>
<input type="checkbox" name="paginas" value="1" id="paginas" /></div>
</form>
</body>
</html>
