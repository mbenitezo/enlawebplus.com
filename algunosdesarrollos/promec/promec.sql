-- MySQL dump 10.10
--
-- Host: localhost    Database: promec_bd
-- ------------------------------------------------------
-- Server version	5.0.7-beta-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES latin1 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barrio`
--

DROP TABLE IF EXISTS `barrio`;
CREATE TABLE `barrio` (
  `idbarrio` int(10) NOT NULL default '0',
  `barnombre` varchar(255) NOT NULL,
  PRIMARY KEY  (`idbarrio`),
  UNIQUE KEY `BARRIO_UK` (`barnombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barrio`
--


/*!40000 ALTER TABLE `barrio` DISABLE KEYS */;
LOCK TABLES `barrio` WRITE;
INSERT INTO `barrio` VALUES (2,'BELEN'),(3,'ENVIGADO'),(1,'LAURELES');
UNLOCK TABLES;
/*!40000 ALTER TABLE `barrio` ENABLE KEYS */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `idcliente` int(255) unsigned NOT NULL,
  `clinombre` varchar(255) NOT NULL,
  `clidireccion` varchar(255) NOT NULL,
  `idbarrio` int(255) unsigned default NULL,
  `clitelefono` int(255) unsigned NOT NULL,
  `clirepresentante` varchar(255) default NULL,
  `clisupervisor` varchar(255) default NULL,
  `idtiposervicio` int(255) unsigned default NULL,
  `idtipocliente` int(255) unsigned default NULL,
  `idtipocontrato` int(255) unsigned default NULL,
  `clifeccontrato` date default NULL,
  `idruta` int(255) unsigned default NULL,
  PRIMARY KEY  (`idcliente`),
  KEY `BarrioClientes` (`idbarrio`),
  KEY `ClientesTpcCodigo` (`idtiposervicio`),
  KEY `RutasClientes` (`idruta`),
  KEY `TipoClienteClientes` (`idtipocliente`),
  KEY `TipoContratoClientes` (`idtipocontrato`),
  KEY `TipoServicioClientes` (`idtiposervicio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientes`
--


/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
LOCK TABLES `clientes` WRITE;
INSERT INTO `clientes` VALUES (12345678,'Constancio de los angeles','Las Flores',2,1234567,'nimiento','Mante',2,1,2,'2007-09-05',2),(1036613885,'Juan Perez','Calle 32 N� 87A-110',1,2563738,'Tommy','Anacleto',1,1,1,'2007-09-08',1),(1128424393,'maquinas unac','cra84#33',1,3427646,'saray galeano','juan david',3,1,5,'2007-09-09',7);
UNLOCK TABLES;
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

--
-- Table structure for table `conyuge tecnico`
--

DROP TABLE IF EXISTS `conyuge tecnico`;
CREATE TABLE `conyuge tecnico` (
  `idpersonaconyuge` int(10) NOT NULL,
  `Ctcodigo` int(10) default NULL,
  PRIMARY KEY  (`idpersonaconyuge`),
  UNIQUE KEY `PersonaConyuge Tecnico` (`idpersonaconyuge`),
  KEY `TecnicosConyuge Tecnico` (`Ctcodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conyuge tecnico`
--


/*!40000 ALTER TABLE `conyuge tecnico` DISABLE KEYS */;
LOCK TABLES `conyuge tecnico` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `conyuge tecnico` ENABLE KEYS */;

--
-- Table structure for table `dllepersonaruta`
--

DROP TABLE IF EXISTS `dllepersonaruta`;
CREATE TABLE `dllepersonaruta` (
  `idpersona` int(10) default NULL,
  `idruta` int(10) default NULL,
  KEY `idruta` (`idruta`),
  KEY `PersonaDllePersonaRuta` (`idpersona`),
  KEY `RutasDllePersonaRuta` (`idruta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dllepersonaruta`
--


/*!40000 ALTER TABLE `dllepersonaruta` DISABLE KEYS */;
LOCK TABLES `dllepersonaruta` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `dllepersonaruta` ENABLE KEYS */;

--
-- Table structure for table `empleados potenciales`
--

DROP TABLE IF EXISTS `empleados potenciales`;
CREATE TABLE `empleados potenciales` (
  `PotCodigo` int(10) unsigned NOT NULL,
  `PotNombre` varchar(255) collate latin1_bin default NULL,
  `PotApellido` varchar(255) collate latin1_bin default NULL,
  `PotDireccion` varchar(255) collate latin1_bin default NULL,
  `PotBarrio` varchar(255) collate latin1_bin default NULL,
  `PotTelefono` int(10) unsigned default NULL,
  `TpeCodigo` int(10) unsigned default NULL,
  PRIMARY KEY  (`PotCodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Dumping data for table `empleados potenciales`
--


/*!40000 ALTER TABLE `empleados potenciales` DISABLE KEYS */;
LOCK TABLES `empleados potenciales` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `empleados potenciales` ENABLE KEYS */;

--
-- Table structure for table `estadocivil`
--

DROP TABLE IF EXISTS `estadocivil`;
CREATE TABLE `estadocivil` (
  `idestcivil` int(10) NOT NULL,
  `tipoestado` varchar(255) default NULL,
  PRIMARY KEY  (`idestcivil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estadocivil`
--


/*!40000 ALTER TABLE `estadocivil` DISABLE KEYS */;
LOCK TABLES `estadocivil` WRITE;
INSERT INTO `estadocivil` VALUES (1,'Casado (a)'),(2,'Soltero (a)'),(3,'Viudo (a)'),(4,'Uni�n Libre');
UNLOCK TABLES;
/*!40000 ALTER TABLE `estadocivil` ENABLE KEYS */;

--
-- Table structure for table `estadomantenimiento`
--

DROP TABLE IF EXISTS `estadomantenimiento`;
CREATE TABLE `estadomantenimiento` (
  `idestadomantenimiento` int(10) NOT NULL auto_increment,
  `descripcion` varchar(255) default NULL,
  PRIMARY KEY  USING BTREE (`idestadomantenimiento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estadomantenimiento`
--


/*!40000 ALTER TABLE `estadomantenimiento` DISABLE KEYS */;
LOCK TABLES `estadomantenimiento` WRITE;
INSERT INTO `estadomantenimiento` VALUES (1,'Terminado'),(2,'Pendiente');
UNLOCK TABLES;
/*!40000 ALTER TABLE `estadomantenimiento` ENABLE KEYS */;

--
-- Table structure for table `hijo`
--

DROP TABLE IF EXISTS `hijo`;
CREATE TABLE `hijo` (
  `idpersonahijo` int(10) NOT NULL,
  `Htcodigo` int(10) default NULL,
  `idpersonatecnico` int(10) default NULL,
  PRIMARY KEY  (`idpersonahijo`),
  UNIQUE KEY `PersonaHijo` (`idpersonahijo`),
  KEY `idpersonatecnico` (`idpersonatecnico`),
  KEY `TecnicosHijo` (`idpersonatecnico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hijo`
--


/*!40000 ALTER TABLE `hijo` DISABLE KEYS */;
LOCK TABLES `hijo` WRITE;
INSERT INTO `hijo` VALUES (1,1,2),(2,8,4);
UNLOCK TABLES;
/*!40000 ALTER TABLE `hijo` ENABLE KEYS */;

--
-- Table structure for table `mantenimiento`
--

DROP TABLE IF EXISTS `mantenimiento`;
CREATE TABLE `mantenimiento` (
  `idcodigo` int(11) NOT NULL auto_increment,
  `idcliente` int(10) default NULL,
  `idtipomantenimiento` int(10) default NULL,
  `idpersona` int(10) default NULL,
  `manfecha` datetime default NULL,
  `manhoraentradatec` datetime default NULL,
  `manhorasalidatec` datetime default NULL,
  `manhoranotificacionatec` datetime default NULL,
  `manvalor` decimal(19,4) default NULL,
  `idusuario` int(10) default NULL,
  `manllamadacliente` datetime default NULL,
  `manestado` int(11) default NULL,
  PRIMARY KEY  (`idcodigo`),
  KEY `ClientesMantenimiento` (`idcliente`),
  KEY `PersonaMantenimiento` (`idpersona`),
  KEY `TipoMantenimientoMantenimiento` (`idtipomantenimiento`),
  KEY `UsuarioMantenimiento` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mantenimiento`
--


/*!40000 ALTER TABLE `mantenimiento` DISABLE KEYS */;
LOCK TABLES `mantenimiento` WRITE;
INSERT INTO `mantenimiento` VALUES (1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `mantenimiento` ENABLE KEYS */;

--
-- Table structure for table `mantenimientofinal`
--

DROP TABLE IF EXISTS `mantenimientofinal`;
CREATE TABLE `mantenimientofinal` (
  `codiman` int(255) unsigned NOT NULL,
  `codicliente` int(255) unsigned NOT NULL,
  `coditipoman` int(255) unsigned NOT NULL,
  `codiper` int(255) unsigned NOT NULL,
  `fechaman` date NOT NULL,
  `horaentraman` varchar(255) collate latin1_bin NOT NULL,
  `minentraman` varchar(255) collate latin1_bin NOT NULL,
  `horasaliman` varchar(255) collate latin1_bin NOT NULL,
  `minsaliman` varchar(255) collate latin1_bin NOT NULL,
  `horanotiman` varchar(255) collate latin1_bin NOT NULL,
  `minnotiman` varchar(255) collate latin1_bin NOT NULL,
  `valorman` varchar(255) collate latin1_bin NOT NULL,
  `codiusu` int(255) unsigned NOT NULL,
  `horallamacli` varchar(255) collate latin1_bin NOT NULL,
  `minllamacli` varchar(255) collate latin1_bin NOT NULL,
  `estaman` int(255) unsigned NOT NULL,
  `eem` varchar(255) collate latin1_bin NOT NULL,
  `esm` varchar(255) collate latin1_bin NOT NULL,
  `enm` varchar(255) collate latin1_bin NOT NULL,
  `ellc` varchar(255) collate latin1_bin NOT NULL,
  `descripcion1` longtext collate latin1_bin NOT NULL,
  `descripcion2` longtext collate latin1_bin NOT NULL,
  `llamante` varchar(255) collate latin1_bin NOT NULL,
  PRIMARY KEY  (`codiman`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Dumping data for table `mantenimientofinal`
--


/*!40000 ALTER TABLE `mantenimientofinal` DISABLE KEYS */;
LOCK TABLES `mantenimientofinal` WRITE;
INSERT INTO `mantenimientofinal` VALUES (1,1036613885,2,1036613885,'2007-09-09','12','25','1','0','12','0','89000',1,'1','0',1,'am','am','am','am','Arreglo de impresora...','Se pudo arreglar sin problema alguno.','Arnaldo'),(2,12345678,3,123456789,'2007-09-09','5','0','6','0','5','10','189000',1,'3','0',1,'pm','pm','am','pm','sin comentarios','','Tommy'),(3,12345678,1,123456789,'2007-09-09','3','0','3','20','2','55','11200',1,'1','0',1,'pm','pm','pm','pm','PA','','Benito'),(4,12345678,1,87218920,'2007-09-09','1','20','6','15','1','10','156000',1,'1','0',1,'pm','pm','pm','pm','momoo','','Anacleto'),(5,12345678,3,1036613885,'2007-09-09','2','0','3','0','3','0','9000',1,'5','0',1,'am','am','am','am','nininininknn','','Bastidas'),(6,12345678,3,1036613885,'2007-09-09','5','0','2','0','6','0','123200',1,'3','0',1,'pm','pm','pm','am','ajajaj','','Cualquiera'),(7,1036613885,2,123456789,'2007-09-09','5','0','3','0','1','0','1239900',1,'2','0',1,'am','am','am','am','ananaa','','Un sapo'),(8,12345678,1,1036613885,'2007-09-09','1','20','7','0','2','0','78800',1,'3','0',1,'am','am','am','am','sasa','','Desconocido'),(9,1036613885,3,87218920,'2007-09-09','4','0','4','0','5','0','129000',1,'4','0',1,'am','am','am','am','asasd','','N.N'),(10,12345678,1,123456789,'2007-09-09','4','0','4','0','3','0','2129000',1,'4','0',1,'am','am','am','am','asdasd','','Sin nombres'),(11,12345678,3,87218920,'2007-09-09','4','0','4','0','3','5','123000',1,'2','0',1,'am','am','am','am','asdasd','','No se sabe'),(12,1036613885,2,1036613885,'2007-09-09','3','0','3','0','3','0','12000',1,'2','0',1,'am','am','am','am','assfdf','','El primero que tom� el tel�fono'),(13,1036613885,1,1036613885,'2007-09-09','2','0','2','0','2','0','2234000',1,'1','0',1,'am','am','am','am','skfsldf','','El mas cercano'),(14,12345678,3,87218920,'2007-09-09','2','10','3','0','2','0','234900',1,'1','5',1,'am','am','am','am','asidfosdf','sdmf','El que esta en el embale'),(15,12345678,1,123456789,'2007-09-09','5','0','4','0','9','0','23900',1,'5','0',1,'am','am','am','am','asdasd\r\n\r\n','','Mama'),(16,12345678,1,87218920,'2007-09-09','10','45','3','0','8','45','1000',1,'12','0',1,'am','am','am','am','sdfsdf\r\n\r\n','','Papa'),(17,12345678,3,1036613885,'2007-09-09','9','0','12','0','8','30','50000',1,'8','20',1,'am','am','am','am','se da�o   la maquina de coser','','Hermano'),(18,1036613885,1,123456789,'2007-09-09','2','0','12','0','9','0','43243443',1,'10','0',1,'am','am','am','am','da�os en las cabezas de ...','','El gerente del chuzo'),(19,12345678,3,87218920,'2007-09-15','0','0','0','0','0','0','',1,'1','0',0,'am','am','am','am','CPU','','El chavo'),(20,12345678,3,87218920,'2007-09-14','0','0','0','0','0','0','',1,'12','0',0,'pm','pm','am','am','','','Piroberta'),(21,12345678,1,87218920,'2007-09-15','0','0','0','0','0','0','',1,'4','0',0,'am','am','am','am','','','San Martin');
UNLOCK TABLES;
/*!40000 ALTER TABLE `mantenimientofinal` ENABLE KEYS */;

--
-- Table structure for table `parentescos`
--

DROP TABLE IF EXISTS `parentescos`;
CREATE TABLE `parentescos` (
  `idparentesco` int(10) NOT NULL,
  `descripcion` varchar(255) default NULL,
  PRIMARY KEY  (`idparentesco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parentescos`
--


/*!40000 ALTER TABLE `parentescos` DISABLE KEYS */;
LOCK TABLES `parentescos` WRITE;
INSERT INTO `parentescos` VALUES (1,'Hijo (a)'),(2,'C�nyuge'),(3,'Primo (a)'),(4,'T�o (a)'),(5,'Sobrino (a)'),(6,'Abuelo (a)');
UNLOCK TABLES;
/*!40000 ALTER TABLE `parentescos` ENABLE KEYS */;

--
-- Table structure for table `parentescoxpersona`
--

DROP TABLE IF EXISTS `parentescoxpersona`;
CREATE TABLE `parentescoxpersona` (
  `contador` int(255) unsigned NOT NULL,
  `idparentesco` int(255) unsigned default NULL,
  `idpersona` int(255) unsigned default NULL,
  `idtecnico` int(255) unsigned NOT NULL,
  PRIMARY KEY  (`contador`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Dumping data for table `parentescoxpersona`
--


/*!40000 ALTER TABLE `parentescoxpersona` DISABLE KEYS */;
LOCK TABLES `parentescoxpersona` WRITE;
INSERT INTO `parentescoxpersona` VALUES (13,1,87218920,0),(14,0,1036613885,0),(15,2,123456789,1036613885);
UNLOCK TABLES;
/*!40000 ALTER TABLE `parentescoxpersona` ENABLE KEYS */;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona` (
  `idpersona` int(10) NOT NULL default '0',
  `pernombre` varchar(255) character set latin1 default NULL,
  `perapellido` varchar(255) character set latin1 default NULL,
  `perfecnacim` date default NULL,
  `pertelefono` varchar(255) character set latin1 default NULL,
  `idbarrio` int(10) default NULL,
  `perdireccion` varchar(255) character set latin1 default NULL,
  `idvinculacion` int(10) default NULL,
  `idestcivil` int(10) default NULL,
  `idtecnico` int(255) unsigned default NULL,
  PRIMARY KEY  (`idpersona`),
  KEY `BarrioPersona` (`idbarrio`),
  KEY `estadoCivilPersona` (`idestcivil`),
  KEY `idvinculacion` (`idvinculacion`),
  KEY `VinculacionPersona` (`idvinculacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Dumping data for table `persona`
--


/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
LOCK TABLES `persona` WRITE;
INSERT INTO `persona` VALUES (87218920,'C�sar Mauricio','Vargas Herrera','1985-02-23','353 44 39 - 315 460 1445',4,'Cra 83A No 32-88',1,2,0),(123456789,'Anacleto','Perez','1992-09-17','987654',1,'Las Flores',1,3,1036613885),(1036613885,'Mario Alejandro','Benitez Orozco','2007-08-22','2563738',2,'Calle 32 N� 87A-110',1,2,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;

--
-- Table structure for table `rutas`
--

DROP TABLE IF EXISTS `rutas`;
CREATE TABLE `rutas` (
  `idruta` int(10) NOT NULL auto_increment,
  `rutnombre` varchar(255) default NULL,
  PRIMARY KEY  (`idruta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rutas`
--


/*!40000 ALTER TABLE `rutas` DISABLE KEYS */;
LOCK TABLES `rutas` WRITE;
INSERT INTO `rutas` VALUES (1,'NORTE'),(2,'SUR'),(3,'ORIENTE'),(4,'OCCIDENTE'),(5,'CENTRO');
UNLOCK TABLES;
/*!40000 ALTER TABLE `rutas` ENABLE KEYS */;

--
-- Table structure for table `tecnicos`
--

DROP TABLE IF EXISTS `tecnicos`;
CREATE TABLE `tecnicos` (
  `idpersonatecnico` int(10) unsigned NOT NULL,
  `TecCodigo` int(10) unsigned default NULL,
  `TecEstCivil` int(10) unsigned default NULL,
  `TecCedula` int(10) unsigned default NULL,
  PRIMARY KEY  (`idpersonatecnico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Dumping data for table `tecnicos`
--


/*!40000 ALTER TABLE `tecnicos` DISABLE KEYS */;
LOCK TABLES `tecnicos` WRITE;
INSERT INTO `tecnicos` VALUES (2,1,3,2147483647),(4,2,4,2147483647);
UNLOCK TABLES;
/*!40000 ALTER TABLE `tecnicos` ENABLE KEYS */;

--
-- Table structure for table `tipo empleado`
--

DROP TABLE IF EXISTS `tipo empleado`;
CREATE TABLE `tipo empleado` (
  `TpeCodigo` int(10) NOT NULL auto_increment,
  `TpeNombre` varchar(255) default NULL,
  `TpeDescripci�n` varchar(255) default NULL,
  PRIMARY KEY  (`TpeCodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo empleado`
--


/*!40000 ALTER TABLE `tipo empleado` DISABLE KEYS */;
LOCK TABLES `tipo empleado` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `tipo empleado` ENABLE KEYS */;

--
-- Table structure for table `tipocliente`
--

DROP TABLE IF EXISTS `tipocliente`;
CREATE TABLE `tipocliente` (
  `idtipocliente` int(10) NOT NULL auto_increment,
  `nombre` varchar(255) default 'Sin Comentarios',
  `descripcion` varchar(255) default 'Sin Comentarios',
  PRIMARY KEY  (`idtipocliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipocliente`
--


/*!40000 ALTER TABLE `tipocliente` DISABLE KEYS */;
LOCK TABLES `tipocliente` WRITE;
INSERT INTO `tipocliente` VALUES (1,'FIJO','2 VECES POR SEMANA'),(2,'ESPORADICO','CADA 15 DIAS');
UNLOCK TABLES;
/*!40000 ALTER TABLE `tipocliente` ENABLE KEYS */;

--
-- Table structure for table `tipocontrato`
--

DROP TABLE IF EXISTS `tipocontrato`;
CREATE TABLE `tipocontrato` (
  `idtipocontrato` int(10) NOT NULL auto_increment,
  `nombre` varchar(255) default NULL,
  `descripcion` varchar(255) default NULL,
  PRIMARY KEY  (`idtipocontrato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipocontrato`
--


/*!40000 ALTER TABLE `tipocontrato` DISABLE KEYS */;
LOCK TABLES `tipocontrato` WRITE;
INSERT INTO `tipocontrato` VALUES (1,'DE PRUEBA','1 A 2 MESES'),(2,'INDEFINIDO','SUJETO A TERMINOS DE ENTRE PARTES');
UNLOCK TABLES;
/*!40000 ALTER TABLE `tipocontrato` ENABLE KEYS */;

--
-- Table structure for table `tipomantenimiento`
--

DROP TABLE IF EXISTS `tipomantenimiento`;
CREATE TABLE `tipomantenimiento` (
  `idtipomantenimiento` int(10) unsigned NOT NULL auto_increment,
  `descripcion` varchar(45) NOT NULL,
  PRIMARY KEY  (`idtipomantenimiento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipomantenimiento`
--


/*!40000 ALTER TABLE `tipomantenimiento` DISABLE KEYS */;
LOCK TABLES `tipomantenimiento` WRITE;
INSERT INTO `tipomantenimiento` VALUES (1,'Llamada'),(2,'Visita'),(3,'General');
UNLOCK TABLES;
/*!40000 ALTER TABLE `tipomantenimiento` ENABLE KEYS */;

--
-- Table structure for table `tiposervicio`
--

DROP TABLE IF EXISTS `tiposervicio`;
CREATE TABLE `tiposervicio` (
  `idtiposervicio` int(10) NOT NULL auto_increment,
  `nombre` varchar(255) default NULL,
  `descripcion` varchar(255) default NULL,
  PRIMARY KEY  (`idtiposervicio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiposervicio`
--


/*!40000 ALTER TABLE `tiposervicio` DISABLE KEYS */;
LOCK TABLES `tiposervicio` WRITE;
INSERT INTO `tiposervicio` VALUES (1,'1 SERVICIO TIPO','PRIMER REGISTRO'),(2,'MI SEGUNDO REGISTRO','MNMNMNMNM');
UNLOCK TABLES;
/*!40000 ALTER TABLE `tiposervicio` ENABLE KEYS */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `idusuario` int(10) NOT NULL auto_increment,
  `usulogin` varchar(255) default NULL,
  `usuclave` varchar(255) default NULL,
  `nomcompleto` varchar(255) NOT NULL,
  PRIMARY KEY  (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuario`
--


/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
LOCK TABLES `usuario` WRITE;
INSERT INTO `usuario` VALUES (1,'.admin','.admin','ADMINISTRADOR GENERAL'),(2,'limitado','limitado','INVITADO'),(3,'jhondavis','juliana','JUAN DAVID MENESES JAIMES');
UNLOCK TABLES;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

--
-- Table structure for table `vinculacion`
--

DROP TABLE IF EXISTS `vinculacion`;
CREATE TABLE `vinculacion` (
  `idvinculacion` int(10) NOT NULL auto_increment,
  `descripcion` varchar(255) default NULL,
  PRIMARY KEY  (`idvinculacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vinculacion`
--


/*!40000 ALTER TABLE `vinculacion` DISABLE KEYS */;
LOCK TABLES `vinculacion` WRITE;
INSERT INTO `vinculacion` VALUES (1,'T�cnico'),(2,'Empleado Potencial');
UNLOCK TABLES;
/*!40000 ALTER TABLE `vinculacion` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

